A bug anyway (or at least a GUI quirk which can be more enervating than necessary): you can Edit Task for a Job which can't be edited, lose some time in clicking the dialog, and being told that "None of the selected jobs are suitable this operation, so the operation will not be performed" only when you close the Edit Task dialog. You should be told upfront. I am into this because developing plugins I'm constantly Restarting tasks and reediting.


oh, another bug (beware, I have a talent for finding them; hope you can deal with the reports). Please look at the following two snapshots of the Parameters GUI. The bug is that when I collapse the parameters for ppImageTransform, the controls for Manual become visible. Should be like in the third image instead.


parallel processing (once matlabpool->parpool) seems to work, but is verbose about progress on the shell window. Apparently because proj.callback is empty within the parfor (can't really be set), but how should it be set instead? Would concurrent updates to the progress bar make sense?

The stop job button has no icon or symbol. What is the purpose of 'case ['pushbutton ' char(1)]' at line 628 of bwtt.m?

Start running a job, stop it before completion, Little Pony says: I'm sorry. Ctrl-E shows that the user cancelled the job. However: restart this job, and run it again successfully to the end. Little Pony says again: I'm sorry (it shouldn't), while Ctrl-E shows no error.

>> bwtt
Warning: Java stuff did not work - some GUI misbehaviour is possible (please report this if it happens every time) 
> In stMainGUI/prepareJava (line 488)
  In stMainGUI/finalize (line 421)
  In bwtt (line 226) 

