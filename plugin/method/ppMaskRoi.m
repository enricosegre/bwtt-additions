
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Enrico Segre.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function output = ppMaskRoi(operation, job, input)


switch operation
	
	case 'info'
		
		output.author = 'Enrico Segre';
		
        
	case 'parameters'
		
		% empty
		pars = {};
		
		% parameter
		par = [];
		par.name = 'maskValue';
		par.label = 'Mask value (%)';
		par.range = [0 100];
		par.step = [1 5];
		par.value = 50;
		par.type = 'scalar';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'maskInside';
		par.label = 'Mask inner region';
		par.type = 'flag';
		par.value = false;
		pars{end+1} = par;

		% parameter
        par = [];
		par.name = 'roimask';
		par.label = 'ROI';
		par.value = {[],[]};
		par.type = 'custom';
		pars{end+1} = par;

        % collect all pars
        output.pars = pars;
		

    case 'editParameter'
        
        % switch
        switch input.par.name
            case 'roimask'
                
                % get frame
                fi = job.getFrameRange('initial');
                rawFrame = job.getVideoFrames(fi, 'a');
                
                % create GUI
                gui = stGUI([800 600], gcbf, 'Define Objects', ...
                    'style', 'modal', 'resizable', 'on');
                panelRoot = gui.rootPanel;
                h_dialog = gui.getMainWindow();
                
                % image axis
                h_axis = axes('parent', h_dialog, 'units', 'pixels');
                h_image = image(repmat(rawFrame, [1 1 3]), 'parent', h_axis);
                set(h_axis, 'xtick', [], 'ytick', []);
                hold(h_axis, 'on');
                axis(h_axis, 'image');
                set(h_axis, 'Color', 0.5*[1 1 1]);
                title('left click to add vertex, right click to close and end')
                xlabel({'type A on the edge to add a vertex',...
                    'double click inside the ROI  to accept'})
                
                % plot the older contour for reference (pity it cannot
                %  be used as input for roipoly()
                plot(input.value{1},input.value{2},'y--')
                
                [~,r,c] = roipoly();
                
                % closing the gui instead of pointing and clicking equates
                %  to accepting the existing ROI
                if isvalid(gui)
                    input.value={r,c};
                    close(gui)
                end
        end
        
        % propagate
        output = input;

		
	case 'initialize'
		
		% retrieve parameters
		output.pars = job.getRuntimeParameters();
		
		
	
	case 'process'
		
		% get frame
		frame = job.getRuntimeFrame(input.frameIndex);
        		
		% process
        mv=uint8(input.pars.maskValue*255/100);
        if isempty(input.pars.roimask{1}) || isempty(input.pars.roimask{2})
            bwmask=true(size(frame{1}));
        else
            bwmask=poly2mask(input.pars.roimask{1},input.pars.roimask{2},...
                size(frame{1},1),size(frame{1},2));
        end
        if input.pars.maskInside
            frame{1}(bwmask) = mv;
        else
            frame{1}(~bwmask) = mv;
        end
        
		% set frame
		job.setRuntimeFrame(input.frameIndex, frame);
		
		% propagate
		output = input;
		
		
		
	otherwise
		output = [];
		
		
end

