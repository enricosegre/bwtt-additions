
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% author ben mitch
% citation not required



function output = ppImageTransform(operation, job, input)



switch operation
	
	case 'info'
		
		output.author = 'Ben Mitch';
		
		
		
	case 'parameters'
		
		% empty
		pars = {};
		
		% parameter
		par = [];
		par.name = 'debug';
		par.label = 'Debug';
		par.value = false;
		par.type = 'flag';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'invert';
		par.label = 'Invert Image';
		par.value = false;
		par.type = 'flag';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'mode';
		par.label = 'Transform Mode';
		par.help = 'The transform mode is either No transform (disabled), Manual, or Automatic. In Automatic mode, the image is normalised (linearly, unity gamma) so that the 2nd and 98th percentiles of the image histogram lie at 2% and 98% luminance levels.';
		par.value = 'Automatic';
		par.type = 'option';
		par.options = {'No transform' 'Automatic' 'Manual'};
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'blackLevel';
		par.label = 'Black Level';
		par.type = 'scalar';
		par.range = [0 1];
		par.value = 0;
		par.step = [0.01 0.1];
		par.precision = 2;
		par.constraints = {
			{'<' 'whiteLevel'}
			};
		par.show = 'strcmp(pars.mode, ''Manual'')';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'whiteLevel';
		par.label = 'White Level';
		par.type = 'scalar';
		par.range = [0 1];
		par.value = 1;
		par.step = [0.01 0.1];
		par.precision = 2;
		par.constraints = {
			{'>' 'blackLevel'}
			};
		par.show = 'strcmp(pars.mode, ''Manual'')';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'gamma';
		par.label = 'Gamma';
		par.type = 'scalar';
		par.range = [0.1 4];
		par.value = 1;
		par.step = [0.01 0.1];
		par.precision = 2;
		par.show = 'strcmp(pars.mode, ''Manual'')';
		pars{end+1} = par;
		
		% ok
		output.pars = pars;
		
		
		
	case 'initialize'
		
		% retrieve parameters
		output.pars = job.getRuntimeParameters();
		
		% if automatic, do this
		if strcmp(output.pars.mode, 'Automatic')
			
			% get auto data from cache
			auto = job.cacheGet('auto');
			
			% if absent, recreate it
			if isempty(auto)
				
				hh = stGetImageHistogram(job);
				chh = cumsum(hh);
				lim = 2;
				auto.lev = [lim 100-lim] / 100;
				l1 = find(chh > auto.lev(1), 1, 'first');
				l2 = find(chh < auto.lev(2), 1, 'last');
				auto.l1 = l1 / 255;
				auto.l2 = l2 / 255;
				
				% cache it
				job.cacheSet('auto', auto, 'vp');
				
			end
			
			% store it
			output.auto = auto;
			
		end
		
		
		
	case 'process'
		
		% get frame
		frame = job.getRuntimeFrame(input.frameIndex);
		
		% debug
		if input.pars.debug
			stSelectFigure ppImageTransform_debug
			clf
			p = panel();
			p.pack(3);
			p(1).select();
			imshow(frame{1});
			axis image
		end
		
		% invert
		if input.pars.invert
			frame{1} = 255 - frame{1};
		end

		% debug
		if input.pars.debug
			p(2).select();
			imshow(frame{1});
			axis image
		end
		
		% switch on mode
		switch input.pars.mode
			
			case 'No transform'
				% do nothing
				
			case 'Manual'
				frame{1} = imadjust(frame{1}, [input.pars.blackLevel; input.pars.whiteLevel], [0; 1], input.pars.gamma);
				
			case 'Automatic'
				if input.pars.invert
					frame{1} = imadjust(frame{1}, [1-input.auto.l2; 1-input.auto.l1], input.auto.lev', 1);
				else
					frame{1} = imadjust(frame{1}, [input.auto.l1; input.auto.l2], input.auto.lev', 1);
				end
				
		end
		
		% debug
		if input.pars.debug
			p(3).select();
			imshow(frame{1});
			axis image
		end
		
		% set frame
		job.setRuntimeFrame(input.frameIndex, frame);
		
		% propagate state
		output = input;
		
		
		
	otherwise
		output = [];
		
		
end



