%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% FAIR USE
%
% Citation requirement. This method has been published and
% you should cite the publication in your own work. See the
% URL below for more information:
%
% http://bwtt.sourceforge.net/fairuse


function output = otBlobEllipseFit(operation, job, input)

switch operation
	
	case 'info'
		output.author = 'Enrico Segre';
		output.citation = 'citation';
	
	case 'parameters'
		% empty
		pars = {};
       
		totalArea=100000; % TODO retrieve # pixels in the image
		% parameter
        par = [];
        par.name = 'minArea';
        par.type = 'scalar';
        par.label = 'minimal object area';
        par.range = [0 totalArea];
        par.value = ceil(totalArea/200);
        par.step = [10 100];
 		par.precision = 0;
        par.constraints = {{'<' 'maxArea'}};
        pars{end+1} = par;

        % parameter
        par = [];
        par.name = 'maxArea';
        par.type = 'scalar';
        par.label = 'maximal object area';
        par.range = [0 totalArea];
        par.value = ceil(totalArea/100);
        par.step = [10 100];
 		par.precision = 0;
        par.constraints = {{'>' 'minArea'}};
        pars{end+1} = par;
		
		% ok
		output.pars = pars;
        
		
    case 'initialize'
        output = [];
        output.pars = job.getRuntimeParameters();
		
	case 'process'
        bwp=job.getRuntimeState(input.frameIndex, 'odBlobPacked');
        if ~isempty(bwp)
        bw=bwunpack(bwp);
        s=regionprops(bw,'Area','Centroid','Orientation',...
                         'MajorAxisLength','MinorAxisLength');
        s=s(horzcat(s.Area)>input.pars.minArea & ...
            horzcat(s.Area)<input.pars.maxArea);
        else
            s={};
        end
        % store the result for this frame
        job.setResults(input.frameIndex, 'blob', s);
        output=input;

	case 'plot'
		output.h = [];
        result = job.getResults(input.frameIndex);
		result = result{1};
        if isfield(result, 'blob')
            s=result.blob;
            for k = 1:length(s)
                alpha=-s(k).Orientation*pi/180;
                R=sqrt(s(k).Area);
                cx=s(k).Centroid(1);
                cy=s(k).Centroid(2);
                output.h(end+1)=plot(input.h_axis,cx,cy,'og');
                output.h(end+1)=plot(input.h_axis,cx+R*cos(alpha)*[-1,1],...
                    cy+R*sin(alpha)*[-1,1],'--g');
                phi=(0:50)*pi/25;
                ex=s(k).MajorAxisLength*cos(phi)/2;
                ey=s(k).MinorAxisLength*sin(phi)/2;
                output.h(end+1)=plot(input.h_axis,...
                    cx+ex*cos(alpha)-ey*sin(alpha),...
                    cy+ex*sin(alpha)+ey*cos(alpha),':g','LineWidth',2);
            end
        end
        
	otherwise
		output = [];
		
end



