
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [assignement, objects] = manageTracking(objects, costMatrix, method, param)

%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************

% <\inDOC>
% Inputs:
%       - costMatrix is the set of similarity matrices
%       - objects is a cell array of cubic splines in polar coordinates
%       - method defines the method used for calculatinf
%       - param is the structure of paramters
%       - flagPlot is a flag used for selecting plot of one or more
%      whiskers

assignement = zeros(1,numel(objects));

switch (method)
    
    case 'k&w'
        %% get measurments
        bweight = param.beta;
        costMatrix{1}(isinf(costMatrix{3})) = inf;
        costMatrix{3}(isinf(costMatrix{1})) = inf;
              
        cMatrix = bweight *abs(costMatrix{1}) + (1-bweight)*abs(costMatrix{3});
        
        % for occlusions - the tip difference does not matter
        [r,c] = find((abs(costMatrix{1})< 1) & (isinf(costMatrix{3})));
        for i=1:numel(r),
            cMatrix(r(i), c(i)) = abs(costMatrix{1});
        end
            
        

        % 0. get cost matrix
%         param.NAW = 1;  % naive whisker
%         param.NAO = 0.2; % naive measurement
        c = cMatrix;
        
        numberOfObservations = size(c,1)+1;
        numberOfWhiskers = size(c,2)+1;        
        
        c = [c,param.NAW*ones(numberOfObservations-1,1)]; 
        c = [c;param.NAO*ones(1,numberOfWhiskers)];
        c(end,end) = 0;
        
       
    
        f = c;
        f(isinf(c)) = 100; % penality

        % 1. each measurement gest assigned to at least one object 
        Ao = repmat(eye(numberOfObservations),1, numberOfWhiskers);        
        bo = ones(size(Ao,1),1);
        %Ao(1:numberOfObservations, (((numberOfWhiskers-1)*numberOfObservations)+1):end) = 0;
        
        %[val,x0] = min(c);
        
        % 2. every whisker gets exactly one measurements (excpet the null whisker)
        Am = zeros(numberOfWhiskers-1,numberOfObservations*numberOfWhiskers);
        for i=1:numberOfWhiskers-1,
            for j=1:numberOfObservations,
                Am(i,(i-1)*numberOfObservations + j) = 1;
            end
        end
        bm = ones(numberOfWhiskers-1,1);

        
         options = optimset('Display','iter','BranchStrategy','mininfeas','NodeSearchStrategy','bn');
        [x,fval,exitflag,output] = bintprog(f, -Ao,-bo ,Am, bm,[] ,options);

         % good solutions
         x = x(1:end-(numberOfObservations));
                 measOut = repmat(1:numberOfObservations,1,numberOfWhiskers);

        assignement = measOut(logical(x));
        % replace null measurement
        assignement(assignement == numberOfObservations) = 0;
        assignement
        outCost = assignement;
        cM = abs(costMatrix{1});
        for i=1:numel(outCost),
            if assignement(i)
            outCost(i) = cM(assignement(i),i);
            end
        end
        [fval, outCost]
        
    
    case 'basic'
        % assigns the best match if under distT
        % for every object (i.e. for every column)
        % a meaasurment can be assigned only to one object
        % if an other object is claiming the same is discard
        %% okt2010 updated - filter bad costs
        cMatrix = abs(costMatrix{1});
        for iObject=1:size(cMatrix,2),
            cvalue = param.minCostT + (objects{iObject}.theCost);
            idx = cMatrix(:,iObject)>cvalue;
            cMatrix(idx,iObject) = nan;
        end
        [allCost, allpos] = sort(cMatrix);
        allpos(isnan(allCost)) = nan;
        bestCost = allCost(1,:);
        pos = allpos(1,:);
        
        for i=1:numel(pos)
            
            % check if there are other candidates with the same measure
            idx = (pos == pos(i));
            if sum(idx) == 1,
                assignement(i) = pos(i);
            elseif bestCost(i) == min(bestCost(idx))
                assignement(i) = pos(i);
            end
            
        end
        
        
    case '3d'
        % combines lenght difference with theta spacing
        % and sorts cost according best option
        weightSpace = 0.3:0.1:0.7;%0.1:0.05:1;
        globalCost = nan(1,numel(weightSpace));
        setOk = zeros(1,numel(weightSpace));
        pos = nan(numel(weightSpace),size(costMatrix{1},2));
        %
        iS = 0;
        for bweight = weightSpace,
            % assigns the best match if under distT
            % for every object (i.e. for every column)
            % a meaasurment can be assigned only to one object
            % if an other object is claiming the same is discard
            % okt2010 updated - filter bad costs
            cMatrix = bweight *abs(costMatrix{1}) + (1-bweight)*abs(costMatrix{3});
            idx = abs(costMatrix{1}) > param.threedT;
            cMatrix(idx) = nan;
            [allCost, allpos] = sort(cMatrix);
            
            sortedBestCost =  sort(allCost(1,:));
            iS = iS + 1;
            globalCost(iS) = sum(sortedBestCost);
            pos(iS,:) = allpos(1,:);
            if min(diff(sort(allpos(1,:))))
                % all classes are separated
                setOk(iS) = 1;
            end
        end
        
        % get the minimums of two classes (different, equal)
        [minD,posD] = min(setOk.*globalCost + (1-setOk)*1000000);
        [minE,posE] = min((1-setOk).*globalCost + setOk*1000000);
        
        
        %%
%         costMatrix{1}(isinf(costMatrix{3})) = inf;
%         costMatrix{3}(isinf(costMatrix{1})) = inf;
%         [a1,ap1]= sort(costMatrix{1});
%         [b1,bp1]= sort(costMatrix{3});
%         bp1(isinf(b1)) = nan;
%         ap1(isinf(a1)) = nan;
%         % set the matches on both metrics
%         idx = (ap1(1,:) == bp1(1,:));
%         assignement = zeros(1,numel(idx));
%         assignement(idx) = ap1(1,idx);
%         % check lack of duplicates
%         for i=1:numel(assignement),
%             idx = assignement == i;
%             if sum(idx)>1
%                 assignement(idx) = 0;
%             end
%         end    
%         
%         % set cost of tip to max and not to inf
%         b1(isinf(b1)) = 1;
%         idx = assignement == 0;
%         % for every unassinged combination
%         N = sum(assignement == 0);
%         cna = cell(1,N);
%         naidx = nan(1,N);
%         t = 0;
%         % get all candidates...
%         for i=find(assignement == 0),
%             % add elements to array
%             t = t + 1;
%             idx = ~isnan(ap1(:,i));
%             cna{t} = ap1(idx,i)';
%             naidx(t) = i;
%         end
%         % check if there are unassigned measurments
%         [a,notAssignedM] = setxor(1:size(b1,1),assignement);
%         %% check individual independence ans set the results
%         for i=1:t,
%             noti = [1:(i-1),(i+1):t];
%             if isempty(intersect(cna{i},cell2mat(cna(noti))))
%                 opts = cna{i};
%                 % assign it to the first not assigned meas
%                 for j=1:numel(opts),
%                     if sum(notAssignedM == opts(j)),
%                         assignement(naidx(i)) = opts(j)                     
%                         break;
%                     end
%                 end
%                 % if no unassingned measurements, assign the best fit
%                 if assignement(naidx(i)) == 0,
%                     assignement(naidx(i)) = ap1(1,naidx(i));
%                 end
%                 naidx(i) = nan;
%             end
%         end
%         %% get remaining data and make all combninations
%         [a,notAssignedM] = setxor(1:size(b1,1),assignement);
%         idx = ~isnan(naidx);
%         naidx = naidx(idx);
%         cna = cna(idx);
%         cMatrix = bweight *abs(costMatrix{1}) + (1-bweight)*abs(costMatrix{3});
%         rcMatrix = cMatrix(:,naidx);
%         
        
        
        % bintprog works for all!1
       
        
       
            
            
                 
             
             
        
                
            
            
       
        
        
        
        %%
        
        %         %% if not occlusion differentiate solution
        %         if max(setOk == 0),
        %             % start with shape
        %             [allCost, allpos] = sort(costMatrix{1});
        %             [val, sortedPos] = sort(allCost(1,:));
        %             assignement = zeros(1,numel(val));
        %             for t=sortedPos,
        %                 for m=1:size(cMatrix,1),
        %                     if min(assignement ~= allpos(m,t))
        %
        %
        %
        %
        %
        %
        %             % run occlusion detector on posE
        %             if size(costMatrix{1},1) >= size(costMatrix{1},2)
        %                 minD = 0;
        %                 bweight = weightSpace(posE);
        %                 cMatrix = bweight *abs(costMatrix{1}) + (1-bweight)*abs(costMatrix{3});
        %                 %idx = abs(costMatrix{1}) > param.threedT;
        %                 %cMatrix(idx) = nan;
        %                 [allCost, allpos] = sort(cMatrix);
        %
        %                     % note sortedPos is lawyas different
        %                     [val, sortedPos] = sort(allCost(1,:));
        %                     assignement = zeros(1,numel(val));
        %                     for t=sortedPos,
        %                         for m=1:size(cMatrix,1),
        %                             if min(assignement ~= allpos(m,t))
        %                                 assignement(t) = allpos(m,t)
        %                                 minD = minD + allCost(m,t);
        %                                 break;
        %                             end
        %                         end
        %                     end
        %                     if minE == 1,
        %                         posD = 2;
        %                     else
        %                         posD = 1;
        %                     end
        %                     pos(posD,:) = assignement;
        %                 end
        %             end
        %             % output selection
        %             if minD <= (minE+1)
        %                 assignement = pos(posD,:);
        %             else
        %                 assignement = pos(posE,:);
        %             end
        %         end
        
        
        
        % get the minimums of two classes (different, equal)
        [minD,posD] = min(setOk.*globalCost + (1-setOk)*1000000);
        [minE,posE] = min((1-setOk).*globalCost + setOk*1000000);
        %% if not occlusion differentiate solution
        if max(setOk == 0),
            % run occlusion detector on posE
            if size(costMatrix{1},1) >= size(costMatrix{1},2)
                minD = 0;
                bweight = weightSpace(posE);
                cMatrix = bweight *abs(costMatrix{1}) + (1-bweight)*abs(costMatrix{3});
                %idx = abs(costMatrix{1}) > param.threedT;
                %cMatrix(idx) = nan;
                [allCost, allpos] = sort(cMatrix);
                
                % note sortedPos is lawyas different
                [val, sortedPos] = sort(allCost(1,:));
                assignement = zeros(1,numel(val));
                for t=sortedPos,
                    for m=1:size(cMatrix,1),
                        if min(assignement ~= allpos(m,t))
                            assignement(t) = allpos(m,t)
                            minD = minD + allCost(m,t);
                            break;
                        end
                    end
                end
                if minE == 1,
                    posD = 2;
                else
                    posD = 1;
                end
                pos(posD,:) = assignement;
            end
        end
        % output selection
        if minD <= (minE+1)
            assignement = pos(posD,:);
        else
            assignement = pos(posE,:);
        end
        
        
        
        
        
    
    case 'basicWithTail'
        % assigns the best match if under distT
        % for every object (i.e. for every column)
        % a meaasurment can be assigned only to one object
        % if an other object is claiming the same is discard
        %% okt2010 updated - filter bad costs
        cMatrix = abs(costMatrix{1});
        for iObject=1:size(cMatrix,2),
            cvalue = param.minCostT + (objects{iObject}.theCost);
            idx = cMatrix(:,iObject)>cvalue;
            cMatrix(idx,iObject) = nan;
        end
        [allCost, allpos] = sort(cMatrix);
        allpos(isnan(allCost)) = nan;
        bestCost = allCost(1,:);
        pos = allpos(1,:);
        
        for i=1:numel(pos)
            if bestCost(i) < param.minCostT,
                % check if there are other candidates with the same measure
                idx = (pos == pos(i));
                if sum(idx) == 1,
                    assignement(i) = pos(i);
                elseif bestCost(i) == min(bestCost(idx))
                    assignement(i) = pos(i);
                end
            end
        end
        
        
    case '3layers'
        % assigns the best match if under distT
        % for every object (i.e. for every column)
        % a meaasurment can be assigned only to one object
        % if an other object is claiming the same we check the length
        % difference. If is not significative, we go down an other layer
        % and we check curvature difference
        %% filter bad costs
        cMatrix = abs(costMatrix{1});
        for iObject=1:size(cMatrix,2),
            cvalue = param.minCostT + (objects{iObject}.theCost);
            idx = cMatrix(:,iObject)>cvalue;
            cMatrix(idx,iObject) = nan;
        end
        [allCost, allpos] = sort(cMatrix);
        allpos(isnan(allCost)) = nan;
        bestCost = allCost(1,:);
        pos = allpos(1,:);
        
        for iObject=1:numel(pos)
            minCost = (param.minCostT + (objects{iObject}.theCost));
            if isnan(bestCost(iObject))
                assignement(iObject) = 0;
            else
                % check if there are other candidates with the same measure
                idx = (pos == pos(iObject));
                M = sum(idx);
                if M == 1,
                    assignement(iObject) = pos(iObject);
                else
                    % for every candidate check if the second option is acceptable
                    if allCost(M,iObject) < minCost
                        %take the min lenght and min Curvature difference
                        lengthCosts = nan(1,M);
                        curvatureCosts = nan(1,M);
                        for j=1:M,
                            lengthCosts(j) = costMatrix{3}(allpos(j,iObject),iObject);
                            curvatureCosts(j) = costMatrix{2}(allpos(j,iObject),iObject);
                        end
                        %..and decide wich criteria
                        if max(abs(diff(lengthCosts))) > param.minLengthDifference,
                            % length is different, use length
                            [val, lenpos] = min(lengthCosts);
                        else
                            % try with curvature difference
                            [val, lenpos] = min(curvatureCosts);
                        end
                        
                        if val < param.minLengthDifference
                            assignement(iObject) = allpos(lenpos,iObject);
                        else
                            objects{iObject}.flagIsOccluded = true;
                        end
                        %                     elseif max(allCost(1,idx)) < minCost
                        %                         %
                        %                         if bestCost(iObject) == min(bestCost(idx))
                        %                             assignement(iObject) = pos(iObject);
                        %                         else
                        %                             objects{iObject}.flagIsOccluded = true;
                        %                         end
                    else %allCost(M,iObject) >= minCost, ...
                        % wins the shorter cost
                        if bestCost(iObject) == min(bestCost(idx))
                            assignement(iObject) = pos(iObject);
                            if sum(assignement == pos(iObject)) > 1,
                                % only one is allowed
                                assignement(iObject) = 0;
                            end
                        end
                    end
                end
            end
        end
        
        
    case 'costReformulation'
        % assigns the best match if under distT
        % for every object (i.e. for every column)
        % a meaasurment can be assigned only to one object
        % if an other object is claiming the same we check the length
        % difference. If is not significative, we go down an other layer
        % and we check curvature difference
        %% filter bad costs
        cMatrix = abs(costMatrix{1});
        for i=1:size(cMatrix,2),
            cvalue = param.minCostT + (objects{iObject}.theCost);
            idx = cMatrix(:,i)>cvalue;
            cMatrix(idx,i) = nan;
        end
        [allCost, allpos] = sort(cMatrix);
        allpos(isnan(allCost)) = nan;
        bestCost = allCost(1,:);
        pos = allpos(1,:);
        %%
        
        
        
        for j=1:3,
            flagCostIsAssigned = false(size(pos));
            for iObject=1:numel(pos)
                if flagCostIsAssigned(iObject),
                    % do nothing
                elseif bestCost(i) < (param.minCostT + (objects{iObject}.theCost)),
                    % check if there are other candidates with the same measure
                    idx = (pos == pos(i));
                    if sum(idx) == 1,
                        assignement(i) = pos(i);
                        flagCostIsNotAssigned(i) = true;
                    else
                        %                     % find the other candidates
                        %                     ids = find(idx);
                        %                     % check the cost difference
                        %                     if bestCost(1,ids)
                        %                     p = perms(ids);
                        %                     size(p,1) = M;
                        %                     bestFits = inf(1,M);
                        %                     for k=1:M,
                        %                         sum(costMatrix(
                        
                    end
                end
            end
            
        end
        
        
end