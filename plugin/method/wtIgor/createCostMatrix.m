
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [costMatrix, additionalOutput] = createCostMatrix(measures, objects, method, param, flagPlot)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:
%       - measures is a cell array of cubic splines in polar coordinates
%       - objects is a cell array of cubic splines in polar coordinates
%       - method defines the method used for calculatinf
%       - param is the structure of paramters
%       - flagPlot is a flag used for selecting plot of one or more
%       whiskers

additionalOutput = [];
M = numel(measures);
N = numel(objects);
costMatrix = nan(M, N);
drho = costMatrix;

curvTheta = costMatrix;
signedDistTheta = costMatrix;
numP = 50;


% add to structure the maxTheta and minTheta for gating
switch (method)
    
        case 'autoT'
        %% auto Threshold
        wNumber = 1453;
        for iObject = 1:N,
            if isfield(objects{iObject},'state')
                spo = objects{iObject}.state;
                %********************************************
                %   prediction
                %********************************************
                temp = polyval(objects{iObject}.tscan,objects{iObject}.rscan) + polyval(objects{iObject}.velocity, objects{iObject}.rscan);
                thetaPlus = temp + 2*objects{iObject}.sigma * ones(size(temp));
                thetaMinus = temp - 2*objects{iObject}.sigma * ones(size(temp));
                rscan = objects{iObject}.rscan;
            end
            if wNumber == iObject,
                figure(8)
                hold off
                fnplt(spo);
                hold on
                plot(rscan, thetaPlus,'k');
                plot(rscan, thetaMinus,'k');
            end
            sporho = spo.knots;
            %%
            for iMeasure = 1:M,
                %% take the extension
                spm = measures{iMeasure};
                spmrho = spm.knots;
                
                % project measurement and check limits
                pMeas = fnval(fnxtr(spm),rscan);
                if max(pMeas - thetaPlus) > 6,
                    pMeas = pMeas - 2*pi;
                elseif  max(pMeas - thetaPlus) < -6,
                    pMeas = pMeas + 2*pi;
                elseif max(pMeas - thetaMinus) > 6,
                    pMeas = pMeas - 2*pi;
                elseif  max(pMeas - thetaMinus) < -6,
                    pMeas = pMeas + 2*pi;
                end
                    
                    
                
                if median((pMeas < thetaPlus).*(pMeas > thetaMinus)) == 0,
                    % out of range
                    signedDistTheta(iMeasure, iObject) = inf;
                    curvTheta(iMeasure, iObject) = inf;
                    drho(iMeasure, iObject) = inf;
                    if wNumber == iObject,
                        fnplt(spm,'r');
                    end
                else
                    if wNumber == iObject,
                        fnplt(spm,'g');
                    end
                    % calculate the lenght difference
                    [dx1,dy1] = pol2cart(fnval(spm,spmrho(end)), spmrho(end));
                    [dx2,dy2] = pol2cart(fnval(spo,sporho(end)),sporho(end));
                    
                    drho(iMeasure, iObject) = sqrt((dx1-dx2).^2 + (dy1-dy2).^2);
                    % calculate the local orientation difference up to the
                    % shortest tip
                    rhoTest = linspace(40,150, numP);
                    % calculate metric
                    curvTheta(iMeasure, iObject) = mean(abs(fnval(fnder(fnxtr(spm)),rhoTest) - fnval(fnder(fnxtr(spo)),rhoTest)));
                    thetaSeries = abs((fnval(fnxtr(spm),rhoTest) - fnval(fnxtr(spo),rhoTest)));
                    if max(thetaSeries) > 6,
                        thetaSeries = thetaSeries - 2*pi;
                    end
                    signedDistTheta(iMeasure, iObject) = mean(thetaSeries)-param.snoutOrientationDifference;
                end
            end
        end
        costMatrix = cell(1,4);
        idx = abs(signedDistTheta) > param.cost1T;
        signedDistTheta(idx) = inf;
        signedDistTheta = signedDistTheta / param.cost1T;
        costMatrix{1} = abs(signedDistTheta);
        costMatrix{2} = curvTheta;
        idx = drho > param.cost3T;
        drho(idx) = inf;
        drho = drho / param.cost3T;
        costMatrix{3} = drho;
        costMatrix{4} = signedDistTheta;
        
    
    case 'basic'
        
        for iObject = 1:N,
            if isfield(objects{iObject},'state')
                spo = objects{iObject}.state;
            else
                spo = objects{iObject};
            end
            sporho = spo.knots;
            for iMeasure = 1:M,
                %% take the extension
                spm = measures{iMeasure};
                spmrho = spm.knots;
                % calculate the lenght difference
                drho(iMeasure, iObject) = abs(spmrho(end) - sporho(end));
                % calculate the local orientation difference up to the
                % shortest tip
                rhoTest = linspace(min(spmrho(1), sporho(1)), max(spmrho(end), sporho(end)), numP);
                % calculate metric
                curvTheta(iMeasure, iObject) = mean(abs(fnval(fnder(fnxtr(spm)),rhoTest) - fnval(fnder(fnxtr(spo)),rhoTest)));
                signedDistTheta(iMeasure, iObject) = mean(fnval(fnxtr(spm),rhoTest) - fnval(fnxtr(spo),rhoTest))-param.snoutOrientationDifference;
                
            end
        end
        costMatrix = cell(1,4);
        costMatrix{1} = abs(signedDistTheta);
        costMatrix{2} = curvTheta;
        costMatrix{3} = drho;
        costMatrix{4} = signedDistTheta;
        
    case '3d'
        % TAIL DIFFERENCE + normALIZED THETA
        
        for iObject = 1:N,
            if isfield(objects{iObject},'state')
                spo = objects{iObject}.state;
            else
                spo = objects{iObject};
            end
            sporho = spo.knots;
            for iMeasure = 1:M,
                %% take the extension
                spm = measures{iMeasure};
                spmrho = spm.knots;
                % calculate the lenght difference
                [dx1,dy1] = pol2cart(fnval(spm,spmrho(end)), spmrho(end));
                [dx2,dy2] = pol2cart(fnval(spo,sporho(end)),sporho(end));
                
                drho(iMeasure, iObject) = sqrt((dx1-dx2).^2 + (dy1-dy2).^2);
                % calculate the local orientation difference up to the
                % shortest tip
                rhoTest = linspace(40,150, numP);
                % calculate metric
                curvTheta(iMeasure, iObject) = mean(abs(fnval(fnder(fnxtr(spm)),rhoTest) - fnval(fnder(fnxtr(spo)),rhoTest)));
                thetaSeries = abs((fnval(fnxtr(spm),rhoTest) - fnval(fnxtr(spo),rhoTest)));
                if max(thetaSeries) > 6,
                    thetaSeries = thetaSeries - 2*pi;
                end
                
                signedDistTheta(iMeasure, iObject) = mean(thetaSeries)-param.snoutOrientationDifference;
                
            end
        end
        costMatrix = cell(1,4);
        idx = abs(signedDistTheta) > param.cost1T;
        signedDistTheta(idx) = inf;
        signedDistTheta = signedDistTheta / param.cost1T;
        costMatrix{1} = abs(signedDistTheta);
        costMatrix{2} = curvTheta;
        idx = drho > param.cost3T;
        drho(idx) = inf;
        drho = drho / param.cost3T;
        costMatrix{3} = drho;
        costMatrix{4} = signedDistTheta;
        
        

    case 'MH'
        % MODIFIED HAUSDORFF
        for iObject = 1:N,
            if isfield(objects{iObject},'state')
                spo = objects{iObject}.state;
            else
                spo = objects{iObject};
            end
            sporho = spo.knots;
            %fnplt(spo,'r'), hold on,
            for iMeasure = 1:M,
                %% take the extension
                spm = measures{iMeasure};
                %fnplt(spm)
                spmrho = spm.knots;
                % calculate the lenght difference
                drho(iMeasure, iObject) = abs(spmrho(end) - sporho(end));
                % calculate the local orientation difference up to the
                % shortest tip
                rhoTest = linspace(min(spmrho(1), sporho(1)), max(spmrho(end), sporho(end)), numP);
                % calculate metric
                curvTheta(iMeasure, iObject) = mean(abs(fnval(fnder(fnxtr(spm)),rhoTest) - fnval(fnder(fnxtr(spo)),rhoTest)))-param.snoutOrientationDifference;
                %modified hausdorf distance
                rhom =rhoTest(rhoTest >= spmrho(1) & rhoTest <= spmrho(end));
                rhoo =rhoTest(rhoTest >= sporho(1) & rhoTest <= sporho(end));
                thetam = unwrap(fnval(spm,rhom));
                thetao =  unwrap(fnval(spo,rhoo));
                [xm,ym] = pol2cart(thetam-param.snoutOrientationDifference,rhom);
                [xo,yo] = pol2cart(thetao, rhoo);
                
                dx = xm'*ones(numel(xo),1)'-(xo'*ones(1,numel(xm)))';
                dy = ym'*ones(numel(yo),1)'-(yo'*ones(1,numel(ym)))';
                d = dx.^2+dy.^2;
                
                
                if size(d,2)>size(d,1)
                    sortedDistances = sqrt(sort(min(abs(d))));
                else
                    sortedDistances = sqrt(sort(min(abs(d'))));
                end
                
                signedDistTheta(iMeasure, iObject) = sortedDistances(round(numel(sortedDistances)*0.7));
                
            end
        end
        costMatrix = cell(1,4);
        costMatrix{1} = abs(signedDistTheta);
        costMatrix{2} = curvTheta;
        costMatrix{3} = drho;
        costMatrix{4} = signedDistTheta;
end




