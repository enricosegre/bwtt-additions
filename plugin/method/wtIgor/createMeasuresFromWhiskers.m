
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function measures = createMeasuresFromWhiskers(whiskers, snoutTrackingData, param, method)

% Author: Perkon Igor    -    perkon@sissa.it
%
% measures = createMeasurmentsFromWhiskers(whiskers, snoutTrackingData)
% 
% The function creates from whiskers list the associated 1d cubic splines
% using different methods and uses param to set a max approximation
% threshold
%%
if nargin < 4,
    method = 'default';
end
if nargin < 3,
    param = inf;
end
    

M = numel(whiskers);
measures = cell(1, M);
approxError = nan(1,M);
switch method

    otherwise
        for i=1:M,
            [thetaa, rhoU] = cart2pol(whiskers{i}(:,1)-snoutTrackingData.center(2), whiskers{i}(:,2)-snoutTrackingData.center(1));
            [rhoa, idx] = sort(rhoU);
            thetaa = thetaa(idx);
            %%
%             if numel(thetaa) < max(size(whiskers{i}))
%                 [thetaa, rhoa] = cart2pol(whiskers{i}(2,:)-snoutTrackingData.center(2), whiskers{i}(1,:)-snoutTrackingData.center(1));
%             end
                
            thetaa = unwrap(thetaa);
            if numel(rhoa) > 10
                measures{i} = fn2fm(spap2(2,  3, rhoa, thetaa),'B-');                
            elseif numel(rhoa) > 6
                measures{i} = fn2fm(spap2(1,  3, rhoa, thetaa),'B-');
            else
                measures{i} = fn2fm(spap2(1,  2, rhoa, thetaa),'B-');
            end
            
            approxError(i) = mean(abs(thetaa-fnval(measures{i}, rhoa)));
            
            
        end
end

measures  = measures(approxError < param);