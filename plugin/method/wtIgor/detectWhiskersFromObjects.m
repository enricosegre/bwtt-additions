
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function whiskers = detectWhiskersFromObjects(trackingData, snoutTrackingData, allwhiskers, oldobjects, flagFigure1On)
% create whiskers from detection data using tracked whiskers as
% shape priors
%
%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************

%
numOfOverlappingPts = 6;
iFrame = 1;
GRAYshape = trackingData{iFrame,1};
whiskerTreeLUTwithOrientations = trackingData{iFrame,4};
idx = whiskerTreeLUTwithOrientations(:,3) > 0;
whiskerTreeLUTwithOrientations = whiskerTreeLUTwithOrientations(idx,:);
%   [t,r] = cart2pol(whiskerTreeLUTwithOrientations(:,3)-snoutTrackingData.center(1), ...
%    whiskerTreeLUTwithOrientations(:,4)-snoutTrackingData.center(2));
% change format to that compatible with wdIgor
%whiskerTreeLUTwithOrientations =
%whiskerTreeLUTwithOrientations(:,[1 2 4 3 5 : 10]);
whiskerTreeLUTwithOrientations(:,6) = pi/2 - whiskerTreeLUTwithOrientations(:,6);

contourLUT = trackingData{iFrame,10};
contourLUT(:,6) = pi/2 - contourLUT(:,6);
furConnectivityMatrix = trackingData{iFrame,8};
connectivityMatrix = trackingData{iFrame,5};
nodesListWithHistAndMarker = trackingData{iFrame,2};
eqGRAY = GRAYshape;
strokesData = trackingData{iFrame,7};
minimalLength = 4; % minimal number of points to trace a stroke
minimalDistance = 0;
debugFigure = false;

detectedW = createWhiskersFromObjects(whiskerTreeLUTwithOrientations, contourLUT, furConnectivityMatrix, ...
    connectivityMatrix, nodesListWithHistAndMarker, eqGRAY, strokesData, debugFigure, minimalLength, minimalDistance, oldobjects, snoutTrackingData);

 for i=1:numel(detectedW)
        detectedW{i} =  fliplr((detectedW{i})');
 end

if flagFigure1On
    figure(3)
    hold off
    GRAYshape = trackingData{iFrame,1};
    imshow(GRAYshape)
    colormap(gray);
    hold on;
    for i=1:numel(allwhiskers)
        plot(allwhiskers{i}(:,1), allwhiskers{i}(:,2),'color',[1 0 rand],'linewidth',2);
    end
    for i=1:numel(detectedW)
        plot(detectedW{i}(:,1), detectedW{i}(:,2),'color',[1 1 1],'linestyle','--');
    end
end
%%  prune whiskers
M = numel(allwhiskers);
N = numel(detectedW);
didx = ones(1,N);
aidx = ones(1,M);
for i=1:M,
    for j=1:N,
        [c,a,b] = setxor(round(detectedW{j}), round(allwhiskers{i}),'rows');
        if numel(b) < numOfOverlappingPts || isempty(b)
            % set priority to motion history
            %plot(allwhiskers{i}(:,1), allwhiskers{i}(:,2),'color',[0 1 0],'linewidth',2);
            aidx(i) = 0;
        elseif numel(a) < numOfOverlappingPts || isempty(a),
            %plot(detectedW{j}(:,1), detectedW{j}(:,2),'color',[0 0 0],'linestyle',':','linewidth',2);
            didx(j) = 0;
        end
    end
end

whiskers = [allwhiskers(logical(aidx)); detectedW(logical(didx))];