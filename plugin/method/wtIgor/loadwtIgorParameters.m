
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function pars = loadwtIgorParameters();


%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************


% empty
pars = {};

% parameter framesNotAssigned
par = [];
par.name = 'framesNotAssigned';
par.help = 'num of frames after which a tracked whisker not corresponding to any detected whisker is stopped';
par.range = [1 20];
par.value = 4;
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'startCostT';
par.help = 'matches two non tracked whiskers in consecutive frames when their distance is < startCostT';
par.range = [0.01 1];
par.precision = 2;
par.value = 0.70;
par.step = [0.01 0.1];
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'NAW';
par.help = 'cost of a measurement assigned to dummy object';
par.range = [0.01 1];
par.precision = 2;
par.value = 1;
par.step = [0.01 0.1];
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'NAO';
par.help = 'cost of not assigning a tracked whisker to any measurement';
par.range = [0.01 1];
par.precision = 2;
par.value = 0.8;
par.step = [0.01 0.1];
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'exclusiveInitializeCost';
par.help = 'do not start a new whisker if it is less than this distance from exisitng whiskers';
par.range = [0.01 1];
par.precision = 2;
par.value = 0.1;
par.step = [0.01 0.1];
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.type = 'constant';
par.name = 'minSigma';
par.value = 0.15;
pars{end+1} = par;

par = [];
par.type = 'constant';
par.name = 'maxL';
par.value = 0;
pars{end+1} = par;

par = [];
par.type = 'constant';
par.name = 'minR';
par.value = 0;
pars{end+1} = par;

par = [];
par.name = 'minL';
par.help = 'max left snout side angle of detetced whiskers (degrees)';
par.range = [-160 -70];
par.value = -115;
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'maxR';
par.help = 'max right snout side angle of detetced whiskers (degrees)';
par.range = [70 160];
par.value = 115;
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'maxOcclusionTime';
par.help = 'max occlusion time after which an occluded object is deleted from objects list (frames)';
par.range = [1 50];
par.value = 28;
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'approxError';
par.lable = 'maxApproxError';
par.help = 'max angular distance (in radians) between any detected point of the whisker and its spline approximation';
par.range = [0.005 0.15];
par.precision = 3;
par.value = 0.02;
par.step = [0.005 0.05];
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'beta';
par.help = 'blending factor between whisker tips distance and whisker shape distance';
par.range = [0.01 1];
par.precision = 2;
par.value = 0.3;
par.step = [0.01 0.1];
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'cost1T';
par.label = 'maxShapeDistance'; 
par.help = 'normalization threshold for shape distance (radians)';
par.range = [0.01 0.5];
par.precision = 2;
par.value = 0.18;
par.step = [0.01 0.1];
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'cost3T';
par.help = 'normalization threshold for tip distance (pixels)';
par.range = [1 200];
par.value = 130;
par.type = 'scalar';
pars{end+1} = par;

par = [];
par.name = 'minSigma';
par.help = 'minimum dynamic threshold for cost function';
par.range = [0.01 0.5];
par.precision = 2;
par.value = 0.15;
par.step = [0.01 0.1];
par.type = 'scalar';
pars{end+1} = par;