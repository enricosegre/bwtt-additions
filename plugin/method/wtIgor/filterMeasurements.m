
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function newWhiskers = filterMeasurements(whiskers, snoutTrackingData, minL, maxL, minR, maxR)

%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************

% <\inDOC>
% Inputs:
%       - measures is a cell array of cubic splines in polar coordinates
%       - objects is a cell array of cubic splines in polar coordinates
%       - method defines the method used for calculatinf
%
% The function assigns measurements and updates position and speed of the
% tracked object - it also deletes old objects

if nargin < 3
minL = -140;
maxL = -0;
maxR = 140;
minR = 0;
end

shaftAngleW = nan(1,numel(whiskers));
%% calculate base position
for j=1:numel(whiskers)
     pts = whiskers{j};
            [t,r] = cart2pol(pts(1,2)-snoutTrackingData.center(1), ...
                pts(1,1)-snoutTrackingData.center(2));
            shaftAngleW(j) = t;
end

%% compensate rotation and convert to degrees
temp = (shaftAngleW - snoutTrackingData.orientation)*180/pi;
idx = temp < - 180;
temp(idx) = temp(idx) + 360;
idx = temp > 180;
temp(idx) = temp(idx) - 360;
shaftAngleW = temp;

%% find right and left angles
idxL = shaftAngleW > minL &  shaftAngleW < maxL;
idxR = shaftAngleW > minR &  shaftAngleW < maxR;

newWhiskers = whiskers(idxL | idxR);
   