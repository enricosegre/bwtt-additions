
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function updatedObjects = assignMeasurementAndUpdateObjects(objects, measurements,...
    assignement, method, param)


% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:
%       - measures is a cell array of cubic splines in polar coordinates
%       - objects is a cell array of cubic splines in polar coordinates
%       - method defines the method used for calculatinf
%
% The function assigns measurements and updates position and speed of the
% tracked object - it also deletes old objects

switch method
    
    
    case 'shapedifference'
        numP = 50;
        % every assigned object gets updated
        for i=find(assignement),
            spo = objects{i}.state;
            sporho = spo.knots;
            spm = objects{i}.oldState;
            spmrho = spm.knots;
            rhoTest = linspace(min(spmrho(1), sporho(1)), min(spmrho(end), sporho(end)), numP);
            signedDistTheta = fncmb(fncmb(fnxtr(spm),'-', fnxtr(spo)),'-',param.snoutOrientationDifference);
            
            objects{i}.oldState = objects{i}.state;
            objects{i}.state = measurements{assignement(i)};
            
            objects{i}.theCost = param.costMatrixObjects{1}(assignement(i),i);
            
            newspeed = signedDistTheta;
            objects{i}.acceleration = 1/2*(objects{i}.speed - newspeed);
            objects{i}.speed = newspeed;
            objects{i}.flagNotAssigned = false;
            objects{i}.framesNotAssigned = param.framesNotAssigned;
            s.minTheta = min(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            s.maxTheta = max(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            
        end
        
    otherwise
        numP = 50;
        % every assigned object gets updated
        for i=find(assignement),
            spo = objects{i}.state;
            sporho = spo.knots;
            spm = objects{i}.oldState;
            spmrho = spm.knots;
            rhoTest = linspace(min(spmrho(1), sporho(1)), min(spmrho(end), sporho(end)), numP);
            signedDistTheta = mean(fnval(fnxtr(spm),rhoTest) - fnval(fnxtr(spo),rhoTest))-param.snoutOrientationDifference;
            
            objects{i}.oldState = objects{i}.state;
            objects{i}.state = measurements{assignement(i)};
            
            objects{i}.theCost = param.costMatrixObjects{1}(assignement(i),i);
            
            newspeed = signedDistTheta;
            objects{i}.acceleration = 1/2*(objects{i}.speed - newspeed);
            objects{i}.speed = newspeed;
            objects{i}.flagNotAssigned = false;
            objects{i}.framesNotAssigned = param.framesNotAssigned;
            s.minTheta = min(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            s.maxTheta = max(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            
        end
end

% every unassigned object is flagged with status
% not assigned, its state is updated only with prediciton
% and its flag starts counting down in update
for i=find((assignement==0)),
    objects{i}.flagNotAssigned = true;
    objects{i}.framesNotAssigned =  objects{i}.framesNotAssigned - 1;
    objects{i}.state = objects{i}.predictedState;
end



% every new object, if assigned enters the list of object to
% be displayed, otherwise it exits the list
for i=1:numel(objects)
    if objects{i}.flagIsNew == true,
        if assignement(i)
            % chnage status
            objects{i}.flagIsNew = false;
        else
            % to be deleted
            objects{i}.framesNotAssigned = 0;
        end
    end
end



% update object postion
updatedObjectsIdx = true(1,numel(objects));
for i=1:numel(objects)
    if objects{i}.framesNotAssigned == 0,
        updatedObjectsIdx(i) = 0;
    end
end
updatedObjects = objects(updatedObjectsIdx);