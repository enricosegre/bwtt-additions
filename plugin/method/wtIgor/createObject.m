
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function objects = createObject(currentState, previousState, method, param, flagPlot)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:
%       - curentState is a cell array of cubic splines in polar coordinates
%       - previousState is a cell array of cubic splines in polar coordinates
%       - snoutDifference is the polar difference of snout motion
%       - param is the structure of paramters
%       - flagPlot is a flag used for selecting plot of one or more
%       whiskers
% Output
%       - objects is the objects struct




numP = 50;

objects = cell(1, numel(currentState));
% add to structure the maxTheta and minTheta for gating
switch (method)

    case 'basic'
        for i=1:numel(currentState)
            spo = currentState{i};
            sporho = spo.knots;
            spm = previousState{i};
            spmrho = spm.knots;
            rhoTest = linspace(min(spmrho(1), sporho(1)), min(spmrho(end), sporho(end)), numP);
            signedDistTheta = mean(fnval(fnxtr(spm),rhoTest) - fnval(fnxtr(spo),rhoTest))-param.snoutOrientationDifference;
            % create object s
            pts = fnval(spo, linspace(spo.knots(1), spo.knots(end), numP));
            s.minTheta = min(pts);
            s.maxTheta = max(pts);
            s.state = spo;
            s.oldState = spm;
            s.predictedState = spo;
            s.speed = signedDistTheta;
            s.acceleration = 0;
            s.framesNotAssigned = param.framesNotAssigned;
            s.flagIsOccluded = false;
            s.flagIsOccluding = false;
            s.flagNotAssigned = false;
            s.OcclusionTime = param.maxOcclusionTime;% frames
            s.flagIsNew = true;
            s.color = [rand rand rand];
            s.theCost = 5;
            s.startState =spo;
            param.startId = param.startId + 1;
            s.Id = param.startId;
            s.learnedRho = spo.knots(end);
           
            % AR motion model
            DR = 5;
            pOrder = 2;
            s.rscan = 40:DR:max(s.learnedRho-15,70);
            s.tscan = polyfit(s.rscan, fnval(fnxtr(s.state), s.rscan), pOrder);
            s.velocity = zeros(1,pOrder);
            s.sigma = 0.2;
            objects{i} = s;
        end
end