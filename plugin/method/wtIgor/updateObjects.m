
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function updatedObjects = updateObjects(objects, flagPlot, param)

%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************

for i=1:numel(objects)
    if objects{i}.speed > 5,
        objects{i}.speed = objects{i}.speed - 2*pi;
    elseif objects{i}.speed < -5,
         objects{i}.speed = objects{i}.speed + 2*pi;
    end
end
        
        


%disp('Running altered speed factor');
for i=1:numel(objects)
    objects{i}.speed = objects{i}.speed*0.1;
    % update state
    if isstruct(objects{i}.speed)
        objects{i}.predictedState = fn2fm(fncmb(fncmb(fnxtr(objects{i}.state), '+', fnxtr(objects{i}.speed)),...
            '+',param.snoutOrientationDifference),'B-');
    else
        objects{i}.predictedState = fn2fm(fncmb(fncmb((objects{i}.state), '+', (objects{i}.speed)),...
            '+',param.snoutOrientationDifference),'B-');
    end
end
if flagPlot
    for i=1:numel(objects)
        plotData(objects{i}.predictedState, snoutTrackingData, [1 0 0]);
    end
end
updatedObjects = objects;