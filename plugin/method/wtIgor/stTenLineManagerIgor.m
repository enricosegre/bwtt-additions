
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


% ten line manager illustration - during development we do
% clear just to free locked classes
clear

% create job
job = stJob('D:\zuo1\stickAndSlip\WithTouch\Sun-Sep05-2010-56-test.avi');

% select frame range
job.setFrameRange(301:320);

% lay in required SSK pars - if you don't do this, you'll be
% asked for them when they are required, as before.
job.setParameters('snout', 'stShapeSpaceKalman', struct( ...
    'noseTip', [ 200 190], ...
    'snoutCenter', [200 130] ...
    ));

% create a "task" - a task is a single execution (forward and
% backward pass) of a set of pipelines. the job object
% supports multiple tasks, with results from later ones
% overlaid on results from earlier ones. here, though, we're
% only going to run one.
%
% note that we have to make this call even if we're going to
% manage execution of the task manually. this is because the
% job manages the exchange of runtime state and results
% between methods, and it has to be given a chance to set up
% the context for these exchanges.
job.taskCreate({
    {'snout', {'sdGeneric' 'stShapeSpaceKalman'}}
    {'whisker', {'ppBgExtractionAndFilter' 'wdIgor' 'wtIgor'}}
    });

% two ways to proceed
if 0
    
    % DEBUG: OVERWRITE TASK
    job.taskDelete();
    job.taskCreate({
        {'snout', {'sdGeneric' 'stShapeSpaceKalman'}}
        });
    
    % ask job to supervise running of the task
    job.taskRun();
    
    % retrieve results
    results = job.getResults('selected');
    
else
    
    % alternatively, we can supervise the run ourselves. this
    % allows us to view the states as they evolve. note that
    % we can review the states after the fact, even if we used
    % taskRun(), so that's an alternative. this isn't
    % described, here, though.
    
    % start the task - primarily, this gives the job a chance
    % to set up the "runtime state", which is the medium of
    % exchange of information between methods at runtime. it
    % also marks the task as having been started.
    job.taskStart();
    
    % prepare init data - this isn't strictly necessary, since
    % the methods we're using don't use it, but we do it just
    % to show the procedure. we're only going to do the
    % forward pass, the backward pass is equivalent. the job
    % offers the function it uses to prepare init data as a
    % utility, so we don't have to build it ourselves.
    initdata = job.getInitializeData('forward');
    
    % initialize each method
    sdState = job.methodInitialize('snout', 'sdGeneric', initdata);
    stState = job.methodInitialize('snout', 'stShapeSpaceKalman', initdata);
    wpState = job.methodInitialize('whisker', 'ppBgExtractionAndFilter', initdata);
    wdState = job.methodInitialize('whisker', 'wdIgor', initdata);
    wtState = job.methodInitialize('whisker', 'wtIgor', initdata);    
    
    % for each frame
    for frameIndex = initdata.frameIndices
        
        % process each method
        sdState = job.methodProcess('snout', 'sdGeneric', sdState, frameIndex);
        stState = job.methodProcess('snout', 'stShapeSpaceKalman', stState, frameIndex);
        wpState = job.methodProcess('whisker', 'ppBgExtractionAndFilter', wpState, frameIndex);
        wdState = job.methodProcess('whisker', 'wdIgor', wdState, frameIndex);
        wtState = job.methodProcess('whisker', 'wtIgor', wtState, frameIndex);
        
        
        % MONITOR EXECUTION
        %
        % select figure
        stSelectFigure tenLineManager
        clf
        plotdata.frameIndex = frameIndex;
        
        % plot snout frame
        plotdata.h_axis = subplot(2, 1, 1);
        f = job.getRuntimeFrame(frameIndex, 'snout');
        image(repmat(f{1}, [1 1 3]));
        axis image
        hold on
        job.methodPlot('snout', 'sdGeneric', plotdata);
        job.methodPlot('snout', 'stShapeSpaceKalman', plotdata);
        
        % plot whisker frame
        plotdata.h_axis = subplot(2, 1, 2);
        f = job.getRuntimeFrame(frameIndex, 'whisker');
        image(repmat(f{1}, [1 1 3]));
        axis image
        hold on
        %job.methodPlot('whisker', 'wdIgor', plotdata);
        job.methodPlot('whisker', 'wtIgor', plotdata);       
        % finalise figure
        title(frameIndex)
        drawnow
        %
        % MONITOR EXECUTION
        
        % interim clean up (optional, keeps memory usage low)
        job.cleanupRuntimeState(frameIndex);
        
    end
    
    % finalize calls go here, if you need them. they're much
    % like initialize calls, but you pass in the last state
    % object returned by the method as the input. note that
    % you only need to make these calls if the methods require
    % finalization - the job object won't mind either way.
    % 	job.methodFinalize('snout', 'sdGeneric', sdState);
    % 	job.methodFinalize('snout', 'stShapeSpaceKalman', stState);
    % 	job.methodFinalize('whisker', 'ppBgExtractionAndFilter', wpState);
    % 	job.methodFinalize('whisker', 'wdIgorMeanAngle', wdState);
    
    % this isn't strictly necessary if you're going to discard
    % the job, but this is where you'd do it. it cleans up the
    % runtime state, and marks the job as finished.
    % 	job.taskFinish();
    
end

% if you wanted to save the results of the run for later,
% you would do that about here...
% job.save('tenLineManager.stJob');







