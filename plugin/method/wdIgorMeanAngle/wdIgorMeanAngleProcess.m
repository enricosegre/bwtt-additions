
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% function [whiskerContour, nextWDChunkData, optionalOutputs]= wdIgorMeanAngle(imageBuffer, wdParameters, previousWDChunkData, snoutTrackingData, objectTrackingData)


function state = wdIgorMeanAngle(job, state)




% wdIgorMeanAngle.m
% syntax:
%   [whiskerContour, nextWDChunkData, optionalOutputs]=
%               wdIgor(imageBuffer, wdParameters, previousWDChunkData, snoutTrackingData, objectTrackingData)
%
% Inputs:
%           - imageBuffer contains the input image/s
%           - wdParam is a data structure containing whisker detection
%           algorithm parameters.
%           Variable:        
%               Variable{1} - if active shows window with detected points
%               Varibale{2} - encodes distance along the shaft  between
%               intensity peaks
%               Varibale{3} - encodes the distance from the snout where
%               segments are analyzed
%               Variable{4} - encodes the width of the internal band used
%               for calculating segments orientation
%               Variable{5} - threshold for intensity peak detection
%               Variable{6} - number of pixel considered around the
%               detected  peak
%               Variable{7} - minimum number of points that defines a
%               shared edge
%           Constant:
%               Constant{1} - ratio for selecting the number of sinks for
%               every source
%               Constant{2} - maximum perpendicular distance used for
%               assigning points to a linear cluster
%               Constant{3} - binary flag that reduces smoothing
%               operations and improves speed.
%               
%           - previousChunkData is empty
%           - snoutTrackingData is a data structure containg three fields:
%           snoutTrackingData.center,snoutTrackingData.orientation and
%           snoutTrackingData.contour. Snout center contains [x,y]
%           coordinates and snout.orientation is the theta value in
%           radians, while contour is the [x,y] list of the tracked contour
%           - objectTrackingData contains contours of tracked objects
%           if frameChunk > 1 objectTrackingData is a cell matrix of size(1xframeChunk). For
%           multiple views the size is (numberOfViews X framechunk). Each cell element contains
%           an other cell element with size 1xnumberOfObjects. With one view and frameChunk = 1,
%           objectTrackingData is only the the cell of size 1xnumberOfObjects, where
%           data matrix requires 1st row contains coordinates
%           with y data (row) and the second row contains x data (column
%           coordinates).
%
%
%
% Outputs:
%           - whisker output contains the xy coordinates of detetcted
%           point sources and sinks
%           - nextChunkData is the updated version of previousChunkData
%           - optionalOutputs contains a table, where the first value is
%           the theta position of the edge, the second the radius,  
%           while the third is the link orientation
%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it




%% RETRIEVE STATE

% assume one frame at a time, for now
frameIndex = state.frameIndex;

% may as well alert user if we fail on no preproc, since it
% takes a long time to compute this in the error condition
if ~job.isPreprocPresent(frameIndex)
	error('cannot compute wdIgorMeanAngle - no pre-processing is configured (recommend ppBgExtractionAndFilter)');
end

% get the image
GRAYshape = job.getRuntimeFrame(frameIndex);

% this isn't efficient, but it's easy...
GRAYshape = GRAYshape{1};

% get snoutTrackingData
results = job.getResults(frameIndex);
if ~isfield(results{1}, 'snout')
	error('cannot compute wdIgorMeanAngle - no snout tracking available');
end
stdata = results{1}.snout;

% decompress
stdata.perpdists = double(stdata.perpdists);
stdata.contour = double(stdata.contour);
snoutTrackingData = stdata;

% get pars
wdParameters = state.pars;








%% IGOR'S CODE


flagFigure1On = double(wdParameters.showDebug);
samplingStep =  round(wdParameters.samplingStep);
% bandSize =  round(wdParameters.Variable{3}.value);
% internalBand =  round(wdParameters.Variable{4}.value);
distanceFromSnout =  round(wdParameters.distanceFromSnout);
bandWidth =  round(wdParameters.bandWidth);
peakT =  round(wdParameters.peakT);
localTokens =  round(wdParameters.localTokens);
nptsSharedPaths = round(wdParameters.sharedPts);

secThresh = wdParameters.sourcesAndSinksDist; % used for defining source and sink points and is measured in 
% pixel distance


clusteringThreshold = wdParameters.clusteringThreshold;



distanceFactor = wdParameters.sourceSinkDistanceRatio;
fasterProcessing =  wdParameters.fasterProcessing;
sinkRange = (bandWidth)*distanceFactor;
internalBand = distanceFromSnout+bandWidth;


%% 1. preprocess image
scale = 2;

%[temp,  BWshape] = stExtractWhiskersGrayValuesWithMorphologyAndShave(GRAYshape, maskT, 0.01, razorSize, logical(BWmask));
snoutContour = snoutTrackingData.contour;
BWshape = zeros(size(GRAYshape));
BWshape(sub2ind(size(GRAYshape), snoutContour(1,:), snoutContour(2,:))) = 1;
bigRing = imdilate(uint8(BWshape), strel('disk',internalBand));
smallRing = imdilate(uint8(BWshape), strel('disk',distanceFromSnout));
ringMask = bigRing - smallRing;
    

mImage = immultiply(GRAYshape, ringMask);
% rescale 1
if fasterProcessing
    testImage = imresize(mImage,scale);
else
    temp = imresize(mImage,scale*2,'bicubic');
    testImage = impyramid(temp,'reduce');
end


X = scale*fliplr(snoutTrackingData.center);
trBinary = max(peakT-10,1)/255; trInnerRing = peakT/255; trOuterRing = 0; rhoThresh = 1000;
param = struct('whiskersSmoothingT',trBinary,'localTokensN',localTokens,'thresh1',trInnerRing, 'thresh2',trOuterRing, 'rhoThresh', rhoThresh);
potentialDensityArray = samplingStep;

whiskerTree = createWhiskerTree([], potentialDensityArray, testImage, X, 'ORIENTEDRADIALDISTANCE', 'maxOnly', param);

% whiskerTree = filterWhiskerTreeByTheta(whiskerTree,-asin(X(4)), thetaRange)


if flagFigure1On
    stSelectFigure wdIgorMeanAngle_debug1
    hold off
    imshow(testImage)
    colormap(gray)
    axis image
    hold on
    for i=1:numel(whiskerTree),
        if ~isempty(whiskerTree{i})
            plot(whiskerTree{i}(:,2), whiskerTree{i}(:,1),'rx','MarkerSize', 5, 'LineWidth',2);
        end
    end
end

% sort in beams according theta
%% 2. get sources and sinks

whiskerTreeLUT = sortrows(convertInNodesToTable(whiskerTree),[7 6]);
if isempty(whiskerTreeLUT)
%     disp('Error: no peak detected - change threshold parameter');
text_Process_Control = get(findobj('Tag','edit_Process_Control'),'String');
     text_Process_Control = strvcat(text_Process_Control,...
           'Error: no peak detected - change threshold parameter');
        set(findobj('Tag','edit_Process_Control'), 'Max',2,'Min',0,'String',...
            text_Process_Control,'FontSize',8,'ForegroundColor','red');
end
distanceMask = bwdist((1-bigRing));
% note scaling in coordinates
distanceLUT = distanceMask((sub2ind(size(distanceMask),round(1/scale*whiskerTreeLUT(:,3)), round(1/scale * whiskerTreeLUT(:,4)))));
idxSinks = distanceLUT < secThresh;
if flagFigure1On
    plot(whiskerTreeLUT(idxSinks,4), whiskerTreeLUT(idxSinks,3),'gs');
end

% ringMask = imdilate(uint8(BWshape), strel('disk',internalBand));
% ringMask = imresize(ringMask,scale,'nearest');
% distanceLUT = ringMask((sub2ind(size(ringMask),round(whiskerTreeLUT(:,3)), round(whiskerTreeLUT(:,4)))));
% idxSources = distanceLUT > 0;
idxSources = distanceLUT > (bandWidth-secThresh);
if flagFigure1On
    plot(whiskerTreeLUT(idxSources,4), whiskerTreeLUT(idxSources,3),'bs');
end
%Uimage = zeros(size(testImage));
%Uimage(sub2ind(size(Uimage), round(whiskerTreeLUT(:,3)), round(whiskerTreeLUT(:,4)))) = 1;




%% 3. fit detection
flagDebugOn = false;
iSources = find(idxSources);
iSinks = find(idxSinks);
combinationLUT = zeros(numel(iSources)* numel(iSinks),4);
whiskerTreeLUTfit = sparse(size(whiskerTreeLUT,1),(numel(iSources)* numel(iSinks)));
combinationLUTid = 0;
for i=1:numel(iSources),
    if flagDebugOn
        stSelectFigure wdIgorMeanAngle_debug2
        hold off
        imshow(testImage)
        colormap(gray)
        axis image
        hold on
        for k=1:numel(whiskerTree),
            if ~isempty(whiskerTree{k})
                plot(whiskerTree{k}(:,2), whiskerTree{k}(:,1),'rx','MarkerSize', 5, 'LineWidth',2);
            end
        end
    end
    
    
    [t,r] = cart2pol(whiskerTreeLUT(iSources(i), 3) - whiskerTreeLUT(iSinks, 3), ...
        whiskerTreeLUT(iSources(i), 4) - whiskerTreeLUT(iSinks, 4));
    selectedSinks = find((r < sinkRange) & (whiskerTreeLUT(iSources(i), 7) < whiskerTreeLUT(iSinks, 7)));
    [tPointsVsSource,rPointsVsSource] = cart2pol(whiskerTreeLUT(iSources(i), 3) - whiskerTreeLUT(:, 3), ...
        whiskerTreeLUT(iSources(i), 4) - whiskerTreeLUT(:, 4));
    idx = find(rPointsVsSource < sinkRange);
    
    
    for j=1:numel(selectedSinks),
        if flagDebugOn
            plot(whiskerTreeLUT(iSources(i),4), whiskerTreeLUT(iSources(i),3),'ys');
            plot(whiskerTreeLUT(iSinks(selectedSinks),4), whiskerTreeLUT(iSinks(selectedSinks),3),'ms');
            plot(whiskerTreeLUT(iSinks(selectedSinks(j)),4), whiskerTreeLUT(iSinks(selectedSinks(j)),3),'gs');
        end
        px = polyfit([whiskerTreeLUT(iSources(i), 7), whiskerTreeLUT(iSinks(selectedSinks(j)), 7)], ...
            [whiskerTreeLUT(iSources(i), 3) whiskerTreeLUT(iSinks(selectedSinks(j)), 3)], 1);
        py = polyfit([whiskerTreeLUT(iSources(i), 7), whiskerTreeLUT(iSinks(selectedSinks(j)), 7)], ...
            [whiskerTreeLUT(iSources(i), 4) whiskerTreeLUT(iSinks(selectedSinks(j)), 4)], 1);
        % intensity check
        
        % error in cartesian coordinates!!
        errX = polyval(px, whiskerTreeLUT(idx, 7));
        errY = polyval(py, whiskerTreeLUT(idx, 7));
        if flagDebugOn
            plot(polyval(py, whiskerTreeLUT(idx, 7)), polyval(px, whiskerTreeLUT(idx, 7)),'gx');
        end
        [tLocal,rLocal] = cart2pol(errX-whiskerTreeLUT(idx, 3), errY-whiskerTreeLUT(idx, 4));
        selectedPointsOnLink = (rLocal<clusteringThreshold) & ...
            (whiskerTreeLUT(idx,7) > whiskerTreeLUT(iSources(i), 7)) & ...
            (whiskerTreeLUT(idx,7) < whiskerTreeLUT(iSinks(selectedSinks(j)), 7)) ;
        combinationLUTid = combinationLUTid + 1;
        combinationLUT(combinationLUTid, :) = [iSources(i) iSinks(selectedSinks(j)) ...
            round(255*mean(whiskerTreeLUT(idx(selectedPointsOnLink),5))),...
            round(255*std((whiskerTreeLUT(idx(selectedPointsOnLink),5))))];
        %sum(selectedPointsOnLink),pause
        whiskerTreeLUTfit(idx(selectedPointsOnLink),combinationLUTid) = sum(selectedPointsOnLink);
    end
end
combinationLUT = combinationLUT(1:combinationLUTid, :);
whiskerTreeLUTfit = transpose(whiskerTreeLUTfit(:,1:combinationLUTid));
%% ****************************************
% 4. cluster unique points - best fit plot
flagDebugOn = false;
if flagDebugOn
    stSelectFigure wdIgorMeanAngle_debug3
    imshow(GRAYshape)
    hold on
    stSelectFigure wdIgorMeanAngle_debug4
end

%

idW = 0;
detectedShaft = zeros(50,3);
whiskerContour = cell(50,1);


%% *******************************************************************
% 5. sort new vals this time according connection and not point
realPoints = full(sum(transpose(whiskerTreeLUTfit>0)));
[sortedVal, sortedPos] = sort(realPoints,'descend');
for i=1:sum(realPoints>nptsSharedPaths),
    extractedLine = full(whiskerTreeLUTfit(sortedPos(i),:));
    idxp = (extractedLine > 0);
    numOfPoints = sum(idxp);
    %  if numOfPoints == sortedVal(i),
    if numOfPoints >nptsSharedPaths,
        % plot selected link
        iSourceVal = combinationLUT(sortedPos(i),1);
        iSinkVal = combinationLUT(sortedPos(i),2);
        whiskerTreeLUTfit(:,idxp) = 0;
        if flagFigure1On
            plot([whiskerTreeLUT(iSourceVal, 4), whiskerTreeLUT(iSinkVal, 4)],...
                [whiskerTreeLUT(iSourceVal, 3) whiskerTreeLUT(iSinkVal, 3)],'g--');
            plot([whiskerTreeLUT(idxp, 4), whiskerTreeLUT(idxp, 4)],...
            [whiskerTreeLUT(idxp, 3) whiskerTreeLUT(idxp, 3)],'gx')
        end
        
        idW = idW + 1;
        detectedShaft(idW,:) = [whiskerTreeLUT(iSourceVal,6), whiskerTreeLUT(iSourceVal,7),...
            180/pi*atan2((whiskerTreeLUT(iSourceVal, 3) - whiskerTreeLUT(iSinkVal, 3)),...
            (whiskerTreeLUT(iSourceVal, 4) -  whiskerTreeLUT(iSinkVal, 4)))];
        
        % output vals
        whiskerContour{idW} = 1/(scale) * [[whiskerTreeLUT(iSourceVal, 4), whiskerTreeLUT(iSinkVal, 4)];...
            [whiskerTreeLUT(iSourceVal, 3) whiskerTreeLUT(iSinkVal, 3)]];
        if flagDebugOn
            stSelectFigure wdIgorMeanAngle_debug5
            plot(1/scale*[whiskerTreeLUT(iSourceVal, 4), whiskerTreeLUT(iSinkVal, 4)],...
                1/scale*[whiskerTreeLUT(iSourceVal, 3) whiskerTreeLUT(iSinkVal, 3)],'y')
            stSelectFigure wdIgorMeanAngle_debug6
        end
    else
        if flagDebugOn
            plot([whiskerTreeLUT(iSourceVal, 4), whiskerTreeLUT(iSinkVal, 4)],...
                [whiskerTreeLUT(iSourceVal, 3) whiskerTreeLUT(iSinkVal, 3)],'b')
        end
    end
    %numOfPoints
end

% nextWDChunkData = previousWDChunkData;

whiskerContour = whiskerContour(1:idW);
wcTemp = whiskerContour;

whiskerContour = cell(1,idW);
for k=1:idW
    whiskerContour{k} = wcTemp{k}(2:-1:1,:);    
end


[optionalOutputs, ib] = sortrows(detectedShaft(1:idW,:));
whiskerContour = whiskerContour(ib);







%% STORE RESULTS

% compress results
for w = 1:length(whiskerContour)
	whiskerContour{w} = single(whiskerContour{w});
end

% store results
results = [];
results.contour = whiskerContour;
job.setResults(frameIndex, 'whisker', results);








