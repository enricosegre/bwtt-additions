
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


% function [pars, prevChunkData, chunkFrame, jumpFrame, flagGetDataFrom] = func(videoFilename, initialFrameToProcess)

function pars = wdIgorMeanAngleParameters()




% wdIgorMeanAngleInitialize.m
% syntax:
%   [wdParameters, wdPrevChunkData, wdChunkFrame, wdJumpFrame,
%               flagGetDataFromWT] = wdIgorMeanAngleInitialize(filename, frameNum)
%
% loads parameters by gui or xml file and allocates algorithm parameters in
% wdParam and iterative algorithm data in previousChunkData
%
% Inputs:
%           - filename is the complete pathname of the movie film ending in
%           *.avi. If the user needs to load a configuration file, he will
%           take care of parsing the filename and modifying its parts in
%           order to get automatically the configuration file
%           - frameNumber is the startFrame given to the initialize
%           function by the general manager. It is used for loading the
%           original movie, makig a custom initialization GUI, loading chunks
%           off data etc..
%
% Outputs:
%           - wdParam is a data structure containing whisker detection
%           algorithm parameters.
%                Variable:
%               Variable{1} - if active shows window with detected points
%               Varibale{2} - encodes distance along the shaft  between
%               intensity peaks
%               Varibale{3} - encodes the distance from the snout where
%               segments are analyzed
%               Variable{4} - encodes the width of the internal band used
%               for calculating segments orientation
%               Variable{5} - threshold for intensity peak detection
%               Variable{6} - number of pixel considered around the
%               detected  peak
%               Variable{7} - minimum number of points that defines a
%               shared edge
%           Constant:
%               Constant{1} - ratio for selecting the number of sinks for
%               every source
%               Constant{2} - maximum perpendicular distance used for
%               assigning points to a linear cluster
%               Constant{3} - binary flag that reduces smoothing
%               operations and improves speed.
%               Constant{4} - distances in pixels used for marking source
%               and sink points in the ring
%           - previousChunkData is empty
%           - frameChunk is an integer data describing the number of frames
%           the general manager needs to load and feed to snout detection
%           algorithm
%           - frameJump is an integer data describing how many frames are
%           between one chunk and the other
%           - flagGetDataFromWT is a boolean variable. If true the general
%           manager merges previousFrame data from whisker tracking with the
%           previous frame data of the current algorithm
%
%
% NOTE: uint8 casting might in future be removed for 12 or 16 bit sensors
%
%*************************************************************************
%  Author: Igor Perkon    -   perkon@sissa.it




pars = {};


%% show debug
par = [];
par.name = 'showDebug';
par.label = 'Show Debug Information';
par.type = 'flag';
par.value = false;
pars{end+1} = par;

%% sampling Step distance between points
par = [];
par.name = 'samplingStep';
par.type = 'scalar';
par.help = 'Distance between head-centered circles used for sampling intensity peaks';
par.range = [1 10];
par.value = 3;
par.step = [1 2];
pars{end+1} = par;

%% external distance from the snout of the band
par = [];
par.name = 'distanceFromSnout';
par.type = 'scalar';
par.label = 'minD';
par.help = 'Minimal distance from the snout';
par.range = [1 100];
par.value = 30;
par.step = [1 1];
pars{end+1} = par;

%% width of the band used for extracting segments
% NOTE must be snmaller than par
par = [];
par.name = 'bandWidth';
par.type = 'scalar';
par.help = 'bandWidth = maxD - minD';
par.range = [6 100];
par.step = [1 1];
par.value = 16;
pars{end+1} = par;

%% threshold for detection of intensity peaks
par = [];
par.name = 'peakT';
par.type = 'scalar';
par.help = 'Minimum intensity of peaks threshold';
par.range = [1 128];
par.value = 40;
par.step = [1 10];
pars{end+1} = par;

%% number of pixels considered in the peak neighbourhood
par = [];
par.name = 'localTokens';
par.type = 'scalar';
par.label = 'thetaT';
par.help = 'Minimum angular distance (in pixels) between consecutive peaks';
par.range = [1 15];
par.value = 6;
par.step = [1 1];
pars{end+1} = par;

%% minimum number of points necessary to detect and edge
par = [];
par.name = 'sharedPts';
par.label = 'cmassT';
par.help = 'Cluster mass threshold - sets the minimum number of inline intensity peaks';
par.type = 'scalar';
par.range = [1 20];
par.value = 4;
par.step = [1 4];
pars{end+1} = par;



%% CONSTANTS

%% distance factor between bandSize and sinkRange (1 to 4)
par = [];
par.name = 'sourceSinkDistanceRatio';
par.value = 2;
pars{end+1} = par;

%% distance factor for clustering points around line cluster
%% center (0 to 4)
par = [];
par.name = 'clusteringThreshold';
par.value = 1.2;
pars{end+1} = par;

%% activate faster processing with lower quality smoothing
par = [];
par.name = 'fasterProcessing';
par.value = true;
pars{end+1} = par;

%% distance from borders in pixels used for marking points
%% as sinks or sources (4 to 20)
par = [];
par.name = 'sourcesAndSinksDist';
par.value = 6;
pars{end+1} = par;






