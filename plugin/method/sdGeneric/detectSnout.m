
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [snoutContour, optionalOutputs] = detectSnout(imageBuffer, method, param)

% detectSnout.m
% syntax:
%   [snoutContour, optionalOutputs] = detectSnout(imageBuffer, method,...
%                   param)
%
%   Inputs:     
%               - imageBuffer is the buffer containing the input frames
%               selected with idx array. For multiple view tracking,
%               imageBuffer is a cell array with various buffers
%               - method is a string selector for different processing
%               functions. See methods section belows
%               - param is a struct containing the configuration parameters for
%               the selected method
%
%   Outputs:    
%               - snoutContour is the cell array containing the x-y coordinates of 
%               detected snout outlines. For multiple view tracking,
%               snoutContour is a cell matrix with number of rows equal to
%               the number of frames and number of columns equal to the
%               number of views. If there is only one image
%               in the buffer, the output is a two-dimensional x-y array.
%               - optionalOutputs is a struct with extra outputs
%               returned by methods
%
% Function detectSnouts extract the snout 2-d or 3-d outlines and all data
% relevant for the snoutTracking method. 
%
%   Methods:
%               -'moritzsMaze' is designed for animals entering from the
%               upper part of the image and exploring a texture in the
%               lower part of the image - for the parameters type help
%               moritzsMaze
% *************************************************************************
% Igor Perkon - Goren Gordon  # april 2009
%
% 

if ~iscell(filename)
    % work on 2d movies

    switch method
        case 'moritzsMaze' 
            [snoutContour, optionalOutputs] = moritzsMaze(imageBuffer, param);
    end
            
               
end