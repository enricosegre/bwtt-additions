
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [xNose yNose largestBoundary] = getNoseProfile(startContour, endContour, edgeImage);

% RELEASE 2.1 -NOSE TRACKING -
%
% Author: Perkon Igor    -    perkon@sissa.it


% The function traces the boundary of the nose and returns the xNose and
% yNose approximation by taking the elemnt of the boundary with the lowest
% coordinates

%%

% NO EDGE IMAGE for speed improvment
borderImage = imcomplement(edgeImage);
cIndex = startContour(2)+1;
rIndexes = find(borderImage(:,cIndex));
startPoint = [rIndexes(1), cIndex];


% the edgeImage was already modified with iterativeNoseExtraction
B = bwtraceboundary(borderImage, startPoint, 'S', 4)';

largestBoundary = B; %dont'forget to modify yNose

if isempty(B)
    yNose = [];
    xNose = [];
else
    % delete boundary elements sticking to the top screen
    idx = find(largestBoundary(1,:) == 1);
    largestBoundary(1,idx) = 0;
    % delete boundary elements on the left of endContour
    idx = find(largestBoundary(2,:) == (endContour(2)-1));
    largestBoundary(1,idx) = 0;
    % delete boundary elements on the right of endContour
    idx = find(largestBoundary(2,:) == (startContour(2)+1));
    largestBoundary(1,idx) = 0;
    % delete null elements
    idx = largestBoundary(1,:) ~= 0;
    largestBoundaryNew = largestBoundary(:,idx);
    largestBoundary = largestBoundaryNew;


    % find the nose as the minimum of the contour
    [yNose, pos] = max(largestBoundary(1,:));
    xNose = largestBoundary(2,pos);
end