
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function  [startContour endContour edgeImage] = iterativeNoseExtraction(BWshape);

% RELEASE 2.1 -NOSE TRACKING -
%
% Author: Perkon Igor    -    perkon@sissa.it

S = size(BWshape);



% <\inDOC>
% iterative nose extraction looks for the top/leftmost point and for the
% top right most point. The code works in the following way. It start
% scanning each column of the image from right and takes the point with
% the lowest row: this is the startContour. The same process is done by
% scanning by left and this is endContour. Than the contour is traced and
% the lowest point is assigned as nose approximation
% <\outDOC>

%edgeImage = edge(BWshape, 'canny'); TOO SLOW
edgeImage = BWshape;

% MONOTONICNA RAST

startRow = S(1);    % worstest coordinates

for cIndex = 1:S(2),
    edgeElements = find(edgeImage(:,cIndex));
    if ~isempty(edgeElements)
        if edgeElements(1) <= startRow
            startRow = edgeElements(1);
        else
            startContour = [startRow, cIndex-1];
            break;
        end
    end
end

endRow = S(1);

for cIndex = S(2):-1:1,
    edgeElements = find(edgeImage(:,cIndex));
    if ~isempty(edgeElements)
        if edgeElements(1) <= endRow
            endRow = edgeElements(1);
        else
            endContour = [endRow, cIndex+1];
            break;
        end
    end
end


% modify edge image for bwtrace boundary with a stop row
edgeImage(1:endContour(1), endContour(2)) = 1;
edgeImage(1:startContour(1), startContour(2)) = 1;

