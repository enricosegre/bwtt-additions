
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function state = sdGenericInitialize(job, state)

% sdGenericInitialize.m
%
% Inputs:
%           - the job
% Outputs:
%           - previousChunkData are the data required for the detector to be
%           initalized
%           - flagGetDataFromST flag is true if the detection method
%           requires data from snout tracker
%           - frameChunk is the number of frames required by the method to
%           get data from the general manager
%           - frameJump is the number of frames the general manager needs
%           to increment in order to derive new data
%
%*************************************************************************
%  Author: Perkon Igor    -    perkon@sissa.it



state = [];
state.pars = job.getRuntimeParameters();

% state.prevChunkData = [];
% state.chunkFrame = 1;
% state.jumpFrame = 1;
% state.flagGetDataFrom = 0;


