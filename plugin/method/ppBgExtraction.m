
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% author goren gordon and/or igor perkon
% citation not required



function output = ppBgExtraction(operation, job, input)

%*************************************************************************
%  Author: Goren Gordon    -    mail@gorengordon.com



%% TODO - add parameters

% select mask region using roipoly
% select frame range to cover



switch operation
	
	case 'info'
		
		output.author = 'Goren Gordon';
		
		
		
	case 'parameters'
		
		% empty
		pars = {};
		
		% parameter
		par = [];
		par.name = 'onlyBackground';
		par.label = 'Return only background image';
		par.type = 'flag';
		par.value = false;
		pars{end+1} = par;
		
		% ok
		output.pars = pars;
		
		
		
	case 'initialize'
		
		% get background image from cache
		bgImage = job.cacheGet('bgImage');
		
		% if absent, recreate it
		if isempty(bgImage)
			bgImage = stGetBackgroundImage(job);
			job.cacheSet('bgImage', bgImage, 'vp');
		end
		
		% and store it in state
		output.bgImage = bgImage;
		
		% retrieve parameters
		output.pars = job.getRuntimeParameters();
		
		
		
	case 'process'

		% extract background
		if input.pars.onlyBackground
			frame = input.bgImage;
		else
			frame = job.getRuntimeFrame(input.frameIndex);
			frame = (input.bgImage - frame{1});
		end

		% set frame
		job.setRuntimeFrame(input.frameIndex, {frame});
		
		% propagate state
		output = input;
		
		
		
	otherwise
		output = [];
		
		
end


