
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [Xnew, nn2,P]  = trackInSplineSpace(Xold, contourCoordinates, splineTemplate, W, Q0, N, T, plotOptions)
% Author: Perkon Igor    -    perkon@sissa.it

%<\inDOC>
%
% The function gets as input the old shape space vector Xold, which
% corresponds to the current template value. Other inputs include
% - Xold is the old shape space
% - contourCoordinates, wich is the 2XM matrix of curve points
% - splineTemplate, represents the  the template 
% - W is the shape matrix
% - Q0 is the spline coefficient offset
% - N number of control points where we calculate perpendiculars
% - T validation gate
% - plotOptions - if positive, indicates the number of figure to plot in
% TRACK CHANGES
% 1.1 -31.08.2007 - invertion of contourCoordinates
% 1.2 -31.08.2007 - validation gate introduced
%------------------------------------------------
% bb = splineTemplate;
% x = (W*X + Q0);
% 
% bb2.coefs =[x([1:numel(x)/2]), x([numel(x)/2+1:end])]';
% fnplt(bb2,'m')
% hold off
% N = 15
% T = 5
% splineTemplate = pp;
% plotOptions = 1;
% Xold = zeros(4,1)
%<\outDOC>

%% 1. convert to B-form if necessary
if strcmp(splineTemplate.form,'pp')
    bbAvg = fn2fm(splineTemplate,'B-');
else
    bbAvg = splineTemplate;
end

% 1000 discrete points of the candidate curv
%contourCoordinates = fnval(bbAvg, linspace(bbAvg.knots(1), bbAvg.knots(end), 1000))+1;


%% 2. find the perpendiculars to the average template

probeKnots = linspace(bbAvg.knots(5), bbAvg.knots(end-4),N);
probePoints = fnval(bbAvg,probeKnots);
tangentVectors = fnval(fnder(bbAvg), probeKnots);
tangentNorm = sqrt(tangentVectors(1,:).^2+tangentVectors(2,:).^2);
normalizedTangentVectors = tangentVectors./[tangentNorm; tangentNorm];
normalVectors = flipud(normalizedTangentVectors).*[ones(1,N); -ones(1,N)];

if plotOptions
    figure(plotOptions)
    plot(contourCoordinates(2,:), contourCoordinates(1,:));
    hold on;
    
    fnplt(bbAvg,'r');
    %plot(probePoints(1,:), probePoints(2,:),'g+');
    axis image;
    legend('unk contour', 'template');
end

%% 2b. gate measures
[thetaM, rhoM] = cart2pol(contourCoordinates(2,:) - Xold(1),...
    contourCoordinates(1,:) - Xold(2));
[thetaT, rhoT] = cart2pol(probePoints(1,:) - Xold(1),...
    probePoints(2,:) - Xold(2));



rhoTresh = 2*max(abs((diff(rhoT))));


%% 3. get perpendicular distances nn(i)
nn = inf(1,N);
for i = 1:N,
    idx = (abs(thetaM - thetaT(i)) < 0.2);
    minRho = min(rhoM(idx));
    if isempty(minRho)
        x = T;
    else
        idx = (abs(thetaM - thetaT(i)) < 0.2) & (abs(minRho - rhoM)) < rhoTresh;
        banana = @(t)(sqrt(min((contourCoordinates(2,idx) - (probePoints(1,i) + t*normalVectors(1,i))).^2+...
            (contourCoordinates(1,idx) - (probePoints(2,i) + t*normalVectors(2,i))).^2)));
        x= fminbnd(banana, -T,T);
    end
    nn(i) = x;
    X1 = probePoints(1,i) + x*normalVectors(1,i);
    Y1 = probePoints(2,i) + x*normalVectors(2,i);
    X2 = probePoints(1,i);
    Y2 = probePoints(2,i) ;
    if plotOptions
        if mod(i,7) == 0
        plot(X2, Y2,'k');
        plot([X2 X1], [Y2 Y1]);
        if abs(x) <= (T-0.02)
            plot(X1, Y1,'rs');
        else
            plot(X1, Y1,'go');
        end
        end
    end
end



%% fitting algorithm
P = 100000000*(eye(4) + 0.01 *ones(4));
%P = 1000*(eye(4));
%XVAR = [sigmaX, sigmaY, cos(sigmaTheta)-1, sin(sigmaTheta)]';
%P = XVAR * XVAR';


Xavg =Xold;
X = Xold;


Basis = spmak(bbAvg.knots, eye(bbAvg.number));
%%
sigma = inf(1,N);
nn2 = inf(1,N);

for i = 1 : N,
    if abs(nn(i))  <= (T-0.02)
        sigma(i) = sqrt(N);
        U = [fnval(Basis, probeKnots(i))', zeros(1,bbAvg.number);  zeros(1,bbAvg.number) ,fnval(Basis, probeKnots(i))'];
        h = transpose(normalVectors(:,i)' *  U * W);
        nn2(i) = nn(i) - h' * (X - Xavg);
        K = P * h * inv(h' * P * h + (sigma(i)^2));
        X = X + K * nn2(i);
        P = (eye(4) - K*h')*P;
    end
end
%%

Xnew = X;

if plotOptions
    bb2 = splineTemplate;
    x = (W*Xnew + Q0);
    bb2.coefs =[x([1:numel(x)/2]), x([numel(x)/2+1:end])]';
    fnplt(bb2,'m-.')
    hold off
end    