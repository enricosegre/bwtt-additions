
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

%function [stParameters, chunkData, frameChunk,
%frameJump, flagGetDataFromSD] =
%stShapeSpaceKalmanInitialize(fullFilename,
%initialFrameToProcess)

function state = stShapeSpaceKalmanInitialize(job, state)

% stShapeSpaceKalmanInitialize.m
% syntax:
%       [stparam, chunkData, frameChunk, frameJump,
%       flagGetDataFromSD] = stShapeSpaceInitialize(fullfilename)
%
% Inputs:
%           - fullFilename is the name of the movie and it is used for
%           loading parameters inside the movie
%
% Outputs:
%           -stParameters is a struct with the following parameters
%           *)stParameters.numberOfProbePoints is the number of probe points
%           used for measuring distance
%           *)stParameters.maxDistance is the maximum distance allowed
%           - chunkData are the data initialized in the previous
%           step
%           *) chunkData.X is the internal value of the shape space
%           *) chunkData.bTemplate is the pp form of the snout template
%           *) chunkData.W is the shape space matrix
%           *) chunkData.Q0 is the average displacement vector
%           *) chunkData.pp is the snout spline template
%           *) chunkData.A is the internal matrix dynamics
%           *) chunkData.H is the I/O matrix
%           *) chunkData.Q is the state noise matrix
%           *) chunkData.R is the measurements noise matrix
%           *) chunkData.Xs is the state matrix
%           *) chunkData.P is the error covariance matrix
%           - frameChunk is the number of frames required to the loader
%           - frameJump is the jump in the buffer
%           - flagGetDataFromSD is a flag setting if the snout tracker
%           needs data from SD
%
%
%*************************************************************************
%  Author: Perkon Igor    -    perkon@sissa.it



state.pars = job.getRuntimeParameters();
state.ppTemplate = load('ppTemplate.mat');



DT = 1;
A = [1 0 DT 0 0 0;
	0 1 0 DT 0 0;
	0 0 1 0 0 0;
	0 0 0 1 0 0;
	0 0 0 0 1 0;
	0 0 0 0 0 1];

H = [1 0 0 0 0 0;
	0 1 0 0 0 0;
	0 0 0 0 1 0;
	0 0 0 0 0 1];



% the state matrix is
% X(1) is the x position
% X(2) is the y position
% X(3) is the x speed
% X(4) is the y speed
% X(5) is the scaling rotational compoment of ss
% X(6) is the scaling rotational compoment of ss
Xs = state.pars.initialXState;

% state.frameChunk = 1;
% state.frameJump = 1;
% state.flagGetDataFromSD = false;



% initialization
Q = eye(6);
Q(1,3) = 2/DT;
Q(2,4) = 2/DT;
Q(3,1) = 2/DT;
Q(4,2) = 2/DT;
Q(3,3) = 4/DT^2;
Q(4,4) = 4/DT^2;
Q = 1*Q;
% reduce the variability for translation and scaling
Q(5,5) = 0.001;
Q(6,6) = 0.001;




% override with new method
R = 1/1000*eye(4);

% % test values, in discussion with Igor, 03/12/11
% Q = 1/1000*eye(6);
% R = 1/1000*eye(4); 


P = Q;

% construct chunkData
chunkData = struct( ...
	'X', [], ...
	'bTemplate', [], ...
	'W', [], ...
	'Q0', [], ...
	'pp', state.ppTemplate.pp, ...
	'A', A, 'H', H, 'Q', Q, 'R', R, 'Xs', Xs, 'P', P ...
	);




state.chunkData = chunkData;







