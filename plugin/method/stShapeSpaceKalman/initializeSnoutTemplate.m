
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [X, bTemplate, W, Q0, pp] = initializeSnoutTemplate(largestBoundary, numberOfProbePoints, maxDistance, pp)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:   - largestBoundary is the vector of all boundary points of the rat-s
%           rat's mask
%           - numberOfProbePoints is the number of points used for
%           calculating distance
%           - maxDistance is the validation gate distance
%           - pp is the snout template
%
% Outputs:  - X shape array 1x4
%           - bTemplate is the Bspline template
%           - W standard Blake notation
%           - Q0  standard Blake notation
%           - pp shape of the contour
% <\outDOC>


% find the matrix W with average coefficient Q0
[W, Q0, UR, bTemplate, X, H] = getSplineSpaceParameters(pp);

% initialize the appropriate coordinates
 X(1) =  mean(largestBoundary(2,:))';
 X(2) =  mean(largestBoundary(1,:))';
%[X(1),pos] = max(largestBoundary(2,:))';
%X(1) = 256;
%X(2) = 256;

[theta, rho] = cart2pol(largestBoundary(2,:) - X(1), largestBoundary(1,:) - X(2));
% consider all the points within rhoscaling distance of the center
% of gravity, and then recalculate the mean only of these points.
rhoscaling = 150;
idx = find(rho < rhoscaling);

% the mean of points within rhoscaling is the inital X value
X(1) =  mean(largestBoundary(2,idx));
X(2) =  mean(largestBoundary(1,idx));

% hold on
% plot(X(2),X(1),'rx')
% plot(largestBoundary(2,:), largestBoundary(1,:),'r')

% update
q = (W*X + Q0);
bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';


for i = 4:-1:1,

    X = trackInSplineSpace(X, largestBoundary, bTemplate, W, Q0, numberOfProbePoints, i*maxDistance, 0);

    % UPDATE TEMPLATES - transformation in the coefficient space Q = W*X+Q0
    q = (W*X + Q0);
    bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';

    pp = fn2fm(bTemplate,'pp');

end




