
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [X, bTemplate, W, Q0, pp] = initializeAutomaticallySnoutTemplate(largestBoundary, numberOfProbePoints, maxDistance,pp, fig, stparam)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:   - largestBoundary is the vector of all boundary points of the rat-s
%           rat's mask
%           - numberOfProbePoints is the number of points used for
%           calculating distance
%           - maxDistance is the validation gate distance
%           - pp is the snout template loaded from a mat file
%
% Outputs:  - X shape array 1x4
%           - bTemplate is the Bspline template
%           - W standard Blake notation
%           - Q0  standard Blake notation
%           - pp shape of the contour
%           - perpdist perpendicular distances
% <\outDOC>
%%
% find the matrix W with average coefficient Q0
[W, Q0, UR, bTemplate, X, H] = getSplineSpaceParameters(pp);

% initialize the appropriate coordinates
% X(1) =  mean(largestBoundary(2,:))';
% X(2) =  mean(largestBoundary(1,:))';
%[X(1),pos] = max(largestBoundary(2,:))';


hold on
%plot(largestBoundary(2,:), largestBoundary(1,:));
%largestBoundary

Xt = stparam.centroid;
X(1) = Xt(1); % add dummy displacement in incoming direction
X(2) = Xt(2);

%[newTemplateOrientation, newtemplateScale] = cart2pol(1+newX(3), newX(4));
X(3) = stparam.scale;
X(4) = stparam.orientation;

q = (W*X + Q0);
bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';
pp = fn2fm(bTemplate,'pp');
% 
% CONTOUR ADAPTATION
flagAllowScalingOfTemplateDuringTracking = stparam.flagAllowScalingOfTemplateDuringTracking;
for i = 1:5,
    scaleDist = 2;
    [newX,perpdist] = trackInSplineSpace(X, largestBoundary, bTemplate, W, Q0, numberOfProbePoints, scaleDist*maxDistance, 0);
    % FIX VALUES
    if flagAllowScalingOfTemplateDuringTracking == 0,
        oldX = X;
        [templateOrientation, templateScale] = cart2pol(1+oldX(3), oldX(4));
        [newTemplateOrientation, newtemplateScale] = cart2pol(1+newX(3), newX(4));
        [Xa, newX(4)] = pol2cart(newTemplateOrientation, templateScale);
        newX(3) = Xa-1;
    end
    X = newX;
    % UPDATE TEMPLATES - transformation in the coefficient space Q = W*X+Q0
    q = (W*X + Q0);
    bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';
    
    pp = fn2fm(bTemplate,'pp');
%     figure(fig)
% hold on
% fnplt(pp,'r');
%     hold off
end


if ~isempty(fig)
	figure(fig)
	hold on
	fnplt(pp,'g');
end


