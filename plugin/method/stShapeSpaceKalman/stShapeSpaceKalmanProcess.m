
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% function [snoutTrackingData, nextChunkData, optionalOutputs, previousChunkData] = stShapeSpaceKalman(snoutContour, stParameters, previousChunkData)

function state = stShapeSpaceKalmanProcess(job, state)

% function stShapeSpaceKalman
%
% syntax:
%       [snoutTrackingData, nextChunkData, optionalOutputs] =
%       stShapeSpaceKalman(snoutContour, stParameters, previousChunkData)
%
% Inputs:
%           - stParameters is a struct with the following parameters
%           *) stParameters.numberOfProbePoints is the number of probe points
%           used for measuring distance
%           *) stParameters.maxDistance is the maximum distance allowed
%           *) stParameters.snoutImage is the image used for initialization
%           - previousChunkData are the data initialized in the previous
%           step
%           *) previousChunkData.X is the internal value of the shape space
%           *) previousChunkData.bTemplate is the pp form of the snout template
%           *) previousChunkData.W is the shape space matrix
%           *) previousChunkData.Q0 is the average displacement vector
%           *) previousChunkData.pp is the snout spline template
%           *) previousChunkData.A is the internal matrix dynamics
%           *) previousChunkData.H is the I/O matrix
%           *) previousChunkData.Q is the state noise matrix
%           *) previousChunkData.R is the measurements noise matrix
%           *) previousChunkData.Xs is the state matrix
%           *) previousChunkData.P is the error covariance matrix
%
% Outputs:
%           - snoutTrackingData is a data structure containg the following fields:
%                   .center - Snout center contains [x,y]  coordinates
%                   .orientation  - is the theta value in radians
%                   .contour - is the [x,y] list of the tracked contour
%                   .noseTip - is the [x,y] coordinates of the nose tip
%           -  nextChunkData is a struct, with
%           *) nextChunkData.template
%
%*************************************************************************
%  Author: Perkon Igor    -    perkon@sissa.it

numberOfProbePoints = state.pars.numberOfProbePoints;
maxDistance = state.pars.maxDistance;
flagAllowScalingOfTemplateDuringTracking = state.pars.flagAllowScalingOfTemplateDuringTracking;



snoutContour = job.getRuntimeState(state.frameIndex, 'sdContour');
if isempty(snoutContour)
	error('cannot compute SSK - snout detection contour is not present (use sdGeneric?)');
end


previousChunkData = state.chunkData;







%% NEW USER/AUTOMATIC INITIALISATION CODE

% do we need to initialise the state information?
if isempty(previousChunkData.X)
	
	% different ways to initialize state vector
	if state.pars.automaticInitialize
		
		%% CASE 1: initialize automatically
		[previousChunkData.X, previousChunkData.bTemplate, ...
			previousChunkData.W, previousChunkData.Q0, previousChunkData.pp] = ...
			initializeAutomaticallySnoutTemplate( ...
			snoutContour, state.pars.numberOfProbePoints, ...
			state.pars.maxDistance, previousChunkData.pp, [], state.pars);
		
		
		
	elseif ~any(isnan(state.pars.noseTip)) && ~any(isnan(state.pars.snoutCenter))
		
		%% CASE 2: initialize from parameters already set
		[previousChunkData.X, previousChunkData.bTemplate, ...
			previousChunkData.W, previousChunkData.Q0, previousChunkData.pp] = ...
			initializeFromParametersSnoutTemplate( ...
			snoutContour, state.pars.numberOfProbePoints, ...
			state.pars.maxDistance, previousChunkData.pp, state.pars);

		
		
	else
		
		%% CASE 3: initialise with help from the user

		% if in unattended mode, let's not hang...
		if job.flagIsSet('unattended')
			error('cannot compute SSK - nose and snout position are not set');
		end
		
		% if in GUI mode, let's force the user to use the proper
		% GUI for this...
		if job.flagIsSet('gui')
			error('cannot compute SSK - nose and snout position are not set');
		end
		
		% get initial frame
		snoutImage = job.getRuntimeFrame('initial');
		
		% prepare local GUI
		f = stSelectFigure('sskInitialize');
		imagesc(snoutImage{1});
		colormap(gray)
		axis image
		hold on
		plot(snoutContour(2,:), snoutContour(1,:), 'w-', 'linewidth', 2)
		
		% manually initialize
		[previousChunkData.X, previousChunkData.bTemplate, previousChunkData.W, previousChunkData.Q0, previousChunkData.pp, clicks]...
			= initializeManuallySnoutTemplate(snoutContour, ...
			state.pars.numberOfProbePoints, state.pars.maxDistance, ...
			previousChunkData.pp, f, snoutImage{1});
		
		% store in pars for the second pass
		state.pars.noseTip = clicks.noseTip;
		state.pars.snoutCenter = clicks.snoutCenter;
		job.setRuntimeParameters(state.pars);
		
		% close local GUI
		close(f);
		drawnow
		
		
	end
	
	
	
	% initialize Kalman filter state
	previousChunkData.Xs(1) = previousChunkData.X(1);
	previousChunkData.Xs(2) = previousChunkData.X(2);
	previousChunkData.Xs(3) = 0;
	previousChunkData.Xs(4) = 0;
	previousChunkData.Xs(5) = previousChunkData.X(3);
	previousChunkData.Xs(6) = previousChunkData.X(4);
	
	
	
% MITCH TO IGOR: what should happen, here? perhaps these
% lines are no longer needed?
% 	
% 	% update flag of data initialized
% 	stParameters.Constant{10}.value = 1;
% 	
% 	% update state
% 	stParameters.Constant{9}.value = previousChunkData.Xs;
% 	stParameters.Constant{11}.value = previousChunkData.X;
% 	stParameters.Constant{12}.value = previousChunkData.bTemplate;
% 	stParameters.Constant{13}.value = previousChunkData.W;
% 	stParameters.Constant{14}.value = previousChunkData.Q0;
% 	stParameters.Constant{15}.value = previousChunkData.pp;
	

	
	
end









%% MAIN ITERATION CODE

nextChunkData = previousChunkData;
oldX = previousChunkData.X;

% predict with Kalman filter
nextChunkData.Xs = previousChunkData.A * previousChunkData.Xs;

% project the error covaiance
previousChunkData.P = previousChunkData.A * previousChunkData.P * transpose(previousChunkData.A) + nextChunkData.Q;

% predict the output state and feed to fitting procedure
previousChunkData.X = nextChunkData.H * nextChunkData.Xs;


			
% fitting
for i = 1:state.pars.nIter,
    [nextChunkData.X, nextChunkData.bTemplate,...
        nextChunkData.W, nextChunkData.Q0, nextChunkData.pp, perpdists, nextChunkData.R]...
        = trackAndUpdateTemplate(previousChunkData.X, snoutContour, ...
        previousChunkData.bTemplate, previousChunkData.W, previousChunkData.Q0, ...
        numberOfProbePoints, maxDistance);
    previousChunkData.X = nextChunkData.X;
    previousChunkData.bTemplate = nextChunkData.bTemplate;
    previousChunkData.W = nextChunkData.W;
    previousChunkData.pp = nextChunkData.pp;
end



% form the innovation vector
Inn = nextChunkData.X - nextChunkData.H * nextChunkData.Xs;
% compute the covairance of innovation
%S = nextChunkData.H * previousChunkData.P * transpose(nextChunkData.H) + previousChunkData.R;
S = nextChunkData.H * previousChunkData.P * transpose(nextChunkData.H) + nextChunkData.R;
% kalman gain
K = previousChunkData.P * transpose(nextChunkData.H) * inv(S);
% update state estimate
nextChunkData.Xs = nextChunkData.Xs + K * Inn;
% update coviariance matrix
nextChunkData.P = (eye(6) - K * nextChunkData.H)*previousChunkData.P;

% translate into shape space
nextChunkData.X = nextChunkData.H * nextChunkData.Xs;

X = nextChunkData.X;
if flagAllowScalingOfTemplateDuringTracking == 0,
    [templateOrientation, templateScale] = cart2pol(1+oldX(3), oldX(4));
    newX = nextChunkData.X;
    [newTemplateOrientation, newtemplateScale] = cart2pol(1+newX(3), newX(4));
    [Xa, newX(4)] = pol2cart(newTemplateOrientation, templateScale);
    newX(3) = Xa-1;
    nextChunkData.X = newX;
    nextChunkData.Xs([1 2 5 6]) = newX;
end



snoutTrackingData.center = [X(2), X(1)];
snoutTrackingData.orientation = atan2(1+X(3), X(4))-pi/2;

snoutTrackingData.noseTip = flipud(fnval(nextChunkData.bTemplate,(nextChunkData.bTemplate.knots(1)+nextChunkData.bTemplate.knots(end))/2)-1)';
snoutTrackingData.perpdists = perpdists;


% 12.10.2010
DELTA = 0;
contour = flipud(fnval(fnxtr(nextChunkData.bTemplate),linspace(nextChunkData.bTemplate.knots(1)-DELTA, nextChunkData.bTemplate.knots(end)+DELTA,300)));
newcontour = zeros(size(contour));
[t,r] = cart2pol(snoutContour(2,:) - snoutTrackingData.center(2),snoutContour(1,:) - snoutTrackingData.center(1));
for i=1:300,
    [val,pos] = min((contour(1,i)-snoutContour(1,:)).^2 + (contour(2,i)-snoutContour(2,:)).^2);
    % check if there is a closer fit
    tpoint = t(pos(1));
    rpoint = r(pos(1));
    idx = find(abs(t -tpoint) < (5/180*pi));
    [val,pos]=min(r(idx));
    newcontour(:,i) = snoutContour(:,idx(pos(1)));
end%%
snoutTrackingData.contour = newcontour;




%% MITCH: DEBUG FITTING COOLIZATION
%
% h = selectfigure('test')
% clf
% subplot(2, 1, 1)
% plot(snoutContour(2, :), snoutContour(1, :), 'k-');
% hold on
% plot(newcontour(2, :), newcontour(1, :), 'r.', 'linewidth', 2);
% subplot(2, 1, 2)
% plot(perpdists, 'r-o')




state.chunkData = nextChunkData;
state.perpdists = perpdists;


% MITCH: compress data before storing results
% NOTE: discuss with Igor whether this will affect
% processing. it saves a lot of storage space, but obviously
% we have to be careful not to fucks things up.
st = snoutTrackingData;
repeat = [false all(st.contour(:, 2:end) == st.contour(:, 1:end-1))];
st.contour = single(st.contour(:, ~repeat));
st.perpdists = single(st.perpdists);

% store results
job.setResults(state.frameIndex, 'snout', st);




