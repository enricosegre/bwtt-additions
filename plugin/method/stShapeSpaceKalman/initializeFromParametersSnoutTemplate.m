
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [X, bTemplate, W, Q0, pp] = initializeFromParametersSnoutTemplate( ...
	largestBoundary, numberOfProbePoints, maxDistance, pp, pars)

% Author: Perkon Igor    -    perkon@sissa.it
% Modified: Ben Mitch 

% <\inDOC>
% Inputs:   - largestBoundary is the vector of all boundary points of the rat-s
%           rat's mask
%           - numberOfProbePoints is the number of points used for
%           calculating distance
%           - maxDistance is the validation gate distance
%           - pp is the snout template loaded from a mat file
%
% Outputs:  - X shape array 1x4
%           - bTemplate is the Bspline template
%           - W standard Blake notation
%           - Q0  standard Blake notation
%           - pp shape of the contour
% <\outDOC>
%%
% find the matrix W with average coefficient Q0
[W, Q0, UR, bTemplate, X, H] = getSplineSpaceParameters(pp);

% initialize the appropriate coordinates
% X(1) =  mean(largestBoundary(2,:))';
% X(2) =  mean(largestBoundary(1,:))';
%[X(1),pos] = max(largestBoundary(2,:))';



%plot(largestBoundary(2,:), largestBoundary(1,:));
%largestBoundary
% Xinit = [0 0 0 0]';
% button = 1;
% while button == 1
%     if Xinit(1) == 0
% 				title('Click two times left button to create the shape, right button to accept changes')
%         [a , b, button] = ginput(1);
%         if button == 1
%             figure(fig);
%             hold off
%             imagesc(img);
%             colormap(gray)
%             axis image
%             hold on
%             plot(largestBoundary(2,:),largestBoundary(1,:),'r')
%             X(1) = a;
%             X(2) = b;
%             hold on
%             plot(X(1),X(2),'yo');
%             % second click
%             [a, b, button] = ginput(1);


X = pars.snoutCenter';
noseTip = pars.noseTip;

% if the user has not set them, overwrite NaNs
if any(isnan(X))
	X = ones(size(X));
end
if any(isnan(noseTip))
	noseTip = 100 * ones(size(noseTip));
end

a = noseTip(1);
b = noseTip(2);


            %theta = -asin((X(1)-a)/(X(2)-b));
            %scale = sqrt((X(1)-a).^2 + (X(2)-b).^2);
            [theta, r] = cart2pol(X(1)-a,X(2)-b);

            X(3) = r/80*(cos(theta+pi/2)) - 1;
            X(4) = r/80*(sin(theta+pi/2));



            % hold on
            % plot(X(2),X(1),'rx')
            % plot(largestBoundary(2,:), largestBoundary(1,:),'r')

            % update


            q = (W*X + Q0);
            bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';
            pp = fn2fm(bTemplate,'pp');

% EXCLUDE CONTOUR ADAPTATION
%             for i = 1:2,
% 
%                 X = trackInSplineSpace(X, largestBoundary, bTemplate, W, Q0, numberOfProbePoints, maxDistance, 0);
% 
%                 % UPDATE TEMPLATES - transformation in the coefficient space Q = W*X+Q0
%                 q = (W*X + Q0);
%                 bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';
% 
%                 pp = fn2fm(bTemplate,'pp');
% 
%             end
%         end
%     else
%         button = 2;
%         X = Xinit;
%         q = (W*X + Q0);
%         bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';
%         pp = fn2fm(bTemplate,'pp');
% %     end
% 
% 
%  figure(fig)
%     hold on
%     fnplt(pp,'y');
% 
% end
% 
% 
