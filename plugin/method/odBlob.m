%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% FAIR USE
%
% Citation requirement. This method has been published and
% you should cite the publication in your own work. See the
% URL below for more information:
%
% http://bwtt.sourceforge.net/fairuse


function output = odBlob(operation, job, input)

switch operation
	
	case 'info'
		output.author = 'Enrico Segre';
		output.citation = 'citation';
	
	case 'parameters'
		% empty
		pars = {};
		
		% parameter (sort of superfluous; ppImageTransform can also invert)
% 		par = [];
% 		par.name = 'invertImage';
% 		par.label = 'invert image (blobs are black)';
% 		par.type = 'flag';
% 		par.value = false;
% 		pars{end+1} = par;

        % parameter
        par = [];
        par.name = 'threshold';
        par.type = 'scalar';
        par.label = 'greyscale threshold';
        par.range = [0 1];
        par.value = 0.6;
        par.step = [0.01 0.1];
 		par.precision = 3;
        pars{end+1} = par;

        % parameter
        par = [];
        par.name = 'erosion';
        par.type = 'scalar';
        par.label = 'morphologic erosion';
        par.range = [0 25];
        par.value = 7;
        par.step = [1 4];
        pars{end+1} = par;
		
		% ok
		output.pars = pars;
        
		
    case 'initialize'
        output = [];
        output.pars = job.getRuntimeParameters();
		
	case 'process'
		GRAYshape = job.getRuntimeFrame(input.frameIndex);
        bw=im2bw(GRAYshape{1},input.pars.threshold);
%         if input.pars.invertImage
%             bw=~bw;
%         end
        SE=strel('disk',input.pars.erosion);
        bw=imopen(bw,SE);
        bwp=bwpack(bw);
        % store the result for this frame
        job.setRuntimeState(input.frameIndex, 'odBlobPacked', bwp);
        output=input;

	case 'plot'
		output.h = [];
		bwp = job.getRuntimeState(input.frameIndex, 'odBlobPacked');
        if ~isempty(bwp)
            odBlobContour = bwboundaries(bwunpack(bwp),'noholes');
            for k = 1:length(odBlobContour)
                boundary = odBlobContour{k};
                output.h(end+1)=plot(input.h_axis,...
                                     boundary(:,2), boundary(:,1), 'r');
            end
        end
        
	otherwise
		output = [];
		
end



