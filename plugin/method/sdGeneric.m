

%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% FAIR USE
%
% Citation requirement. This method has been published and
% you should cite the publication in your own work. See the
% URL below for more information:
%
% http://bwtt.sourceforge.net/fairuse



% this is a stub that wraps the directory underneath, where
% the implementation is. you can equally well place the
% implementation in this file.

function output = sdGeneric(operation, job, input)

switch operation
	
	case 'info'
		output.author = 'Igor Perkon';
		output.citation = 'citation';
	
	case 'parameters'
		output.pars = sdGenericParameters();
		
	case 'initialize'
		output = sdGenericInitialize(job, input);
		
	case 'process'
		output = sdGenericProcess(job, input);
		
	case 'plot'
		output.h = [];
		sdContour = job.getRuntimeState(input.frameIndex, 'sdContour')';
		if ~isempty(sdContour)
			d = diff(sdContour);
			s = sqrt(sum(d.^2, 2));
			f = [0; find(s > 8); length(s)];
			h = [];
			for n = 2:length(f)
				rng = f(n-1)+1:f(n);
				h(end+1) = plot(input.h_axis, sdContour(rng, 2), sdContour(rng, 1), ...
					'c-', 'linewidth', 2);
			end
			output.h = h;
		end
		
	otherwise
		output = [];
		
end



