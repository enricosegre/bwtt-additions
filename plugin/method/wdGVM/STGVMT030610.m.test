
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2012 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

clear all;
close all;

clc;
warning off;
Zoom = false;
numFramesStart = 171;
numFramesEnd = 190;     %ppParam.Constant{1};

[filenameName, filenamePath] = uigetfile('*.avi');
% filenameName = '100205-0005.avi';
% filenamePath = 'E:/hs_temp/A/';
filename = [filenamePath, filenameName];

set(findobj('Tag','editFilename'), 'String', sprintf('%s%s', filenamePath, filenameName));
% filename = 'D:\VideosGVMTracker\070625-0026.avi';

[imageBuffer, infoMovie] = stLoadMovieFrames(filename,numFramesStart);

ppParam.Constant{1} = infoMovie.NumFrames;
ppParam.Constant{2} = filename;
ppParam.Constant{3} = min([200, round(infoMovie.NumFrames/20)]);

% ppParam.Variable = [];

frames = 1:ppParam.Constant{3}:min([2000, ppParam.Constant{1}]);
curFrames = stLoadMovieFrames(ppParam.Constant{2},frames);
ppParam.Constant{4} = max(curFrames,[],3);

% spOptionalOutputs = [];
imageBuffer = imageBuffer - ppParam.Constant{4};
% mask = uint8(imageBuffer);
% imshow(imageBuffer)
clear ppParam;
flagCorrectlyFinalized = 1;

fig = figure;
img = stLoadMovieFrames(filename,numFramesStart);%frameNum
colormap(gray);
imagesc(img);

title('Zoom to snout');
zoomSnout = round(ginput(1));
axis([max([0 round(zoomSnout(1) - size(img,2)/6)]) min([size(img,2) round(zoomSnout(1) + size(img,2)/6)])...
    max([0 round(zoomSnout(2) - size(img,1)/6)]) min([size(img,1) round(zoomSnout(2) + size(img,1)/6)])]);

title('Mark the snout center');
markSnoutCenter = round(ginput(1));
text(markSnoutCenter(1), markSnoutCenter(2), 'cm', 'color', 'cyan');

title('Mark the nose tip');
markNoseTip = round(ginput(1));
text(markNoseTip(1), markNoseTip(2), 'nose', 'color', 'cyan');

close(fig);

% Constant paramters for the head detection
sdParameters.Constant{1}.name = 'Radii';
sdParameters.Constant{1}.value = [0, sqrt(sum( (markSnoutCenter - markNoseTip).^2))*3.5]; %minimum and maximum of head radius

sdParameters.Constant{2}.name = 'Delta angle';
sdParameters.Constant{2}.value = 0.02;

% Varialbe parameters for the head detection
% percent for threshold
sdParameters.Variable{1}.name = 'thresholdPercent';
sdParameters.Variable{1}.min = 0.0;
sdParameters.Variable{1}.max = 1.0;
sdParameters.Variable{1}.value = 0.5;
sdParameters.Variable{1}.minStep = 0.05;
sdParameters.Variable{1}.maxStep = 0.2;

sdParameters.Variable{2}.name = 'maxAngle';
sdParameters.Variable{2}.min = 0.0;
sdParameters.Variable{2}.max = 1.0;
sdParameters.Variable{2}.value = 0.7;
sdParameters.Variable{2}.minStep = 0.01;
sdParameters.Variable{2}.maxStep = 0.2;


% Prior calculations
orientation = cart2pol(markNoseTip(1)-markSnoutCenter(1), markNoseTip(2)-markSnoutCenter(2));
snoutContour = GetSnoutFromPolarToCartesian(img, orientation, markSnoutCenter, sdParameters);
rNoseTip = (snoutContour(2,:) - markNoseTip(1)).^2 + (snoutContour(1,:) - markNoseTip(2)).^2;
knoseTip = find(rNoseTip == min(rNoseTip), 1, 'first');
searchNoseTip = max([1 knoseTip-10]):min([size(snoutContour,2) knoseTip+10]);
rFromCenter = (snoutContour(2,searchNoseTip) - markSnoutCenter(1)).^2 + (snoutContour(1,searchNoseTip) - markSnoutCenter(2)).^2;
kTrueNoseTip = find(rFromCenter == max(rFromCenter));
noseTip = round([snoutContour(2,searchNoseTip(kTrueNoseTip)), snoutContour(1, searchNoseTip(kTrueNoseTip))]);

orientation = cart2pol(noseTip(1)-markSnoutCenter(1), noseTip(2)-markSnoutCenter(2));

% From tracker
flagGetDataFromST = 0;
sdPrevChunkData.snoutCenter = markSnoutCenter;
sdPrevChunkData.noseTip = markNoseTip;
sdPrevChunkData.orientation = orientation;
sdParameters.dnosetipSnoutcenter = sqrt(sum((markNoseTip - markSnoutCenter).^2));

% Process frame by frame
sdChunkFrame = 1;
sdJumpFrame = 1;

% internal variables
snoutCenter = sdPrevChunkData.snoutCenter;
orientation = sdPrevChunkData.orientation;
noseTip = sdPrevChunkData.noseTip;


% Find the snout contour, based on the snout center and orientation
[snoutContour, r, a, r_basic] = GetSnoutFromPolarToCartesian(imageBuffer, orientation, snoutCenter, sdParameters);

% Find next snout center (instead of snout tracker)

% Find ears / edges
edge1 = find( (r_basic(1:(end-1)) == 0) & (r_basic(2:end) > 0), 1, 'last');
if(isempty(edge1));edge1 = 1;end;
edge2 = find( (r_basic(1:(end-1)) > 0) & (r_basic(2:end) == 0), 1, 'first');
if(isempty(edge2));edge2 = length(r_basic)-1;end;
maxRadius = max(r_basic);
[leftEar(1) leftEar(2)] = pol2cart(a(edge1), maxRadius);
leftEar(:) = leftEar(:) + snoutCenter(:);
[rightEar(1) rightEar(2)] = pol2cart(a(edge2), maxRadius);
rightEar(:) = rightEar(:) + snoutCenter(:);
betweenEars = (leftEar + rightEar)/2;

% Nose tip is max between ears
knoseTip1 = round((edge1+edge2)/2);
nearNoseTip = knoseTip1 + ((-10):10);
nearNoseTip = nearNoseTip .* (nearNoseTip > 0) + (nearNoseTip <= 0 );
nearNoseTip = nearNoseTip .* (nearNoseTip <= length(r_basic)) + length(r_basic) .* (nearNoseTip > length(r_basic));
knoseTip2 = find( r_basic(nearNoseTip) == max(r_basic(nearNoseTip)), 1, 'first');
knoseTip = nearNoseTip(knoseTip2);

[noseTip(1) noseTip(2)] = pol2cart(a(knoseTip), r_basic(knoseTip));
noseTip(:) = noseTip(:) + snoutCenter(:);

% Change snout center
dNosetipBetweenEars = sqrt(sum( (noseTip - betweenEars).^2));
p = 1 - sdParameters.dnosetipSnoutcenter / dNosetipBetweenEars;
snoutCenter = noseTip * p + betweenEars * (1-p);

% Update orientation
orientation = cart2pol(noseTip(1)-snoutCenter(1), noseTip(2)-snoutCenter(2));

% %debugging
% plotPolarSnout = 0;
% if plotPolarSnout; figure;plot(a,r_basic,'g');hold on; end; % debugging
% if plotPolarSnout; plot(a([edge1 edge2]),r_basic([edge1 edge2]),'xb');hold on; end; % debugging
% if plotPolarSnout; plot([orientation orientation],[0 70],'-k');hold on; end; % debugging

% Update chunk data
previousSDChunkData.snoutCenter = snoutCenter;
previousSDChunkData.noseTip = noseTip;
previousSDChunkData.orientation = orientation;
previousSDChunkData.ears.left = leftEar;
previousSDChunkData.ears.right = rightEar;
previousSDChunkData.ears.betweenEars = betweenEars;
nextSDChunkData = previousSDChunkData;

optionalOutputs = [];

% clear sdParam previousChunkData;
flagCorrectlyFinalized = 1;

stParameters.Constant = [];
stParameters.Variable = [];

stPrevChunkData.snoutCenter = [];
stPrevChunkData.noseTip = [];
stPrevChunkData.orientation = [];
stPrevChunkData.ears.left = [0 0];
stPrevChunkData.ears.right = [0 0];
stPrevChunkData.ears.betweenEars = [0 0];

stChunkFrame = 1;
stJumpFrame = 1;

flagGetDataFromSD = 1;

% center of contour
snoutCenter(2:(-1):1) = previousSDChunkData.snoutCenter;
noseTip(2:(-1):1) = previousSDChunkData.noseTip;
orientation = previousSDChunkData.orientation;

% For detection
nextChunkData.snoutCenter = snoutCenter;
nextChunkData.noseTip = noseTip;
nextChunkData.orientation = orientation;

% export data
snoutTrackingData.center = snoutCenter;
snoutTrackingData.orientation = orientation - (pi/2);
snoutTrackingData.contour = snoutContour;
snoutTrackingData.noseTip = noseTip;

optionalOutputs = [];

% clear stParam previousChunkData;
flagCorrectlyFinalized = 1;

sdParameters.sdChunkFrame = 1;
sdParameters.sdJumpFrame = 1;
stParameters.stChunkFrame =1;
stParameters.stJumpFrame =1;
framesToProcess = numFramesStart:numFramesEnd;

sdChunkFrame = sdParameters.sdChunkFrame;
sdJumpFrame = sdParameters.sdJumpFrame;
stChunkFrame = stParameters.stChunkFrame;
stJumpFrame = stParameters.stJumpFrame;

totalFramesToProcess = length(framesToProcess);

% The show frame
% figure(10);close(10);figure(10);

%  Initialize
% snChunkFrame is the number of frames per cycle.
if (sdChunkFrame == 1) && (stChunkFrame == 1) && (stJumpFrame ==1) && (sdJumpFrame == 1)
    snChunkFrame = 1;
else
    snChunkFrame = min( [max([sdChunkFrame, stChunkFrame+stJumpFrame]), max([sdChunkFrame+sdJumpFrame, stChunkFrame])]);
end;
snJumpFrame = max([sdJumpFrame stJumpFrame]);

% Initialize cell arrays
snoutContour = cell(totalFramesToProcess,1);
previousSDChunkData = cell(snChunkFrame,1);
if sdChunkFrame == 1
    previousSDChunkData{1} = sdPrevChunkData;
else
    previousSDChunkData(1:sdChunkFrame) = sdPrevChunkData(1:sdChunkFrame);
end;
nextSDChunkData = cell(snChunkFrame,1);

snoutTrackingData = cell(totalFramesToProcess,1);
previousSTChunkData = cell(snChunkFrame,1);
if stChunkFrame == 1
    previousSTChunkData{1} = stPrevChunkData;
else
    previousSTChunkData(1:stChunkFrame) = stPrevChunkData(1:stChunkFrame);
end;
nextSTChunkData = cell(snChunkFrame,1);


% --- Snout cycle ---
tic;
% Go over all the frames that requires processing
for iSnoutCycle = 1 : snJumpFrame : (totalFramesToProcess-snChunkFrame+1)

    chunkFrames = iSnoutCycle + (0:(snChunkFrame-1));

    realChunkFrames = framesToProcess(chunkFrames);

    %     Load the chunk of frames
    imageBuffer = stLoadMovieFrames(filename, realChunkFrames);
    displayImage = imageBuffer(:,:,1);

    %     Preprocess on all the chunk
%     for iSP = 1:length(snoutPreprocessMethodList)
%         for iChunk = 1 : length(chunkFrames)
%             strSP = sprintf('[imageBuffer(:,:,iChunk), spOptionalOutputs{%d}] = %s(imageBuffer(:,:,iChunk), spParameters{%d});', iSP, snoutPreprocessMethodList{iSP}, iSP);
%             eval(strSP);
%         end;
%     end;
snoutDetectionMethod = 'sdGordon';
snoutTrackingMethod = 'stGordon';
    %     Snout detector: go over the chunks
    for iSDChunk = chunkFrames(1) : sdJumpFrame : (chunkFrames(end)-sdChunkFrame+1)
        kSDChunkFrames = iSDChunk + (0:(sdChunkFrame-1));
        tSDChunkFrames = kSDChunkFrames - chunkFrames(1) + 1; % counts internal chunk frames
        if isempty(snoutContour{kSDChunkFrames(end)}) % perform snout detection only if not done before
            % Run the method
            %         If only one frame in chunk, then the outputs are arrays.
            %         Otherwise, they are cell arrays, one for each frame in the chunk.
            if sdChunkFrame == 1
                % {kSDChunkFrames} is a struct
                strSD = '[snoutContour{kSDChunkFrames}, nextSDChunkData{tSDChunkFrames}, optionalOutputs{kSDChunkFrames}] =';
                strSD = sprintf( '%s%s(imageBuffer(:,:,tSDChunkFrames), sdParameters, previousSDChunkData{tSDChunkFrames});', strSD, snoutDetectionMethod);
            else
                % (kSDChunkFrames) is a cell array
                strSD = '[snoutContour(kSDChunkFrames), nextSDChunkData(tSDChunkFrames), optionalOutputs(kSDChunkFrames)] =';
                strSD = sprintf( '%s%s(imageBuffer(:,:,tSDChunkFrames), sdParameters, previousSDChunkData(tSDChunkFrames));', strSD, snoutDetectionMethod);
            end;
            eval(strSD);

            %         Update chunk data
            if(sdChunkFrame < snChunkFrame)
                previousSDChunkData(tSDChunkFrames + sdJumpFrame) = nextSDChunkData(tSDChunkFrames);
                if flagGetDataFromST
                    %  Add the fields from the previous snout tracker
                    previousSDChunkData = stUpdateFieldsofChunk(previousSDChunkData, previousSTChunkData, (tSDChunkFrames + sdJumpFrame));
                end;
            end;
        end;
    end;
    
    %         Update chunk data
    if flagGetDataFromSD
        %  Add the fields from the current snout detection
        previousSTChunkData = stUpdateFieldsofChunk(previousSTChunkData, previousSDChunkData, 1:snChunkFrame);
    end;

    %     Snout tracking: go over the chunks
    for iSTChunk = chunkFrames(1) : stJumpFrame : (chunkFrames(end)-stChunkFrame+1)
        kSTChunkFrames = iSTChunk + (0:(stChunkFrame-1));
        tSTChunkFrames = kSTChunkFrames - chunkFrames(1) + 1; % counts internal chunk frames
        if isempty(snoutTrackingData{kSDChunkFrames(end)}) % perform snout tracking only if not done before
            % Run the method
            %         If only one frame in chunk, then the outputs are arrays.
            %         Otherwise, they are cell arrays, one for each frame in the chunk.
            if stChunkFrame == 1
                strST = '[snoutTrackingData{kSTChunkFrames}, nextSTChunkData{tSTChunkFrames}, optionalOutputs{kSTChunkFrames}] =';
                strST = sprintf( '%s%s(snoutContour{kSTChunkFrames}, stParameters, previousSTChunkData{tSTChunkFrames});', strST, snoutTrackingMethod);
            else
                strST = '[snoutTrackingData(kSTChunkFrames), nextSTChunkData(tSTChunkFrames), optionalOutputs(kSTChunkFrames)] =';
                strST = sprintf( '%s%s(snoutContour(kSTChunkFrames), stParameters, previousSTChunkData(tSTChunkFrames));', strST, snoutTrackingMethod);
            end;
            eval(strST);

            %         Update chunk data
            if(stChunkFrame < snChunkFrame)
                previousSTChunkData(tSTChunkFrames + stJumpFrame) = nextSTChunkData(tSTChunkFrames);
                if flagGetDataFromSD
                    %  Add the fields from the previous snout detection
                    previousSTChunkData = stUpdateFieldsofChunk(previousSTChunkData, previousSDChunkData, (tSTChunkFrames + stJumpFrame));
                end;
            end;
        end;
    end;
    
    % Update data chunks
    updateSDChunk = (1+snJumpFrame-sdJumpFrame):snChunkFrame;
    previousSDChunkData(1:length(updateSDChunk)) = nextSDChunkData(updateSDChunk);
    if flagGetDataFromST
        %  Add the fields from the previous snout detection
        previousSDChunkData = stUpdateFieldsofChunk(previousSDChunkData, previousSTChunkData, 1:snChunkFrame);
    end;

    updateSTChunk = (1+snJumpFrame-stJumpFrame):snChunkFrame;    
    previousSTChunkData(1:length(updateSTChunk)) = nextSTChunkData(updateSTChunk);
    if flagGetDataFromSD
        %  Add the fields from the previous snout detection
        previousSTChunkData = stUpdateFieldsofChunk(previousSTChunkData, previousSDChunkData, 1:snChunkFrame);
    end;
    
    % Show output
    figure(10);%figure;%
    colormap(gray);
    imagesc(displayImage);hold on;
    plot(snoutTrackingData{chunkFrames(1)}.contour(2,:), snoutTrackingData{chunkFrames(1)}.contour(1,:), 'w', 'Linewidth', 2.0);hold on;
    plot(snoutTrackingData{chunkFrames(1)}.center(2), snoutTrackingData{chunkFrames(1)}.center(1), 'xw');hold on;
    plot(snoutTrackingData{chunkFrames(1)}.noseTip(2), snoutTrackingData{chunkFrames(1)}.noseTip(1), 'db');
    title(sprintf('%d [%d]', iSnoutCycle, (totalFramesToProcess-snChunkFrame+1)));
    hold off;
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x=0:250;
y(1:251)=0;
% numFramesStart = 525;
% numFramesEnd = 744;
for i = 1:numFramesEnd-numFramesStart+1
    dataNOSE(i,1) = snoutTrackingData{i}.noseTip(1);    
    dataNOSE(i,2) = snoutTrackingData{i}.noseTip(2);
    dataWhCe(i,1) = snoutTrackingData{i}.center(1);    
    dataWhCe(i,2) = snoutTrackingData{i}.center(2);
end

% [dataNOSE, dataWhCe] = dataTrack(dataNOSE, dataWhCe);
d_cen_nose = dataNOSE - dataWhCe;
d_cen_nose = complex(d_cen_nose(:,1), d_cen_nose(:,2));
head_bearing = angle(d_cen_nose);
numberOfFrames = numFramesEnd-numFramesStart+1;


video = mmreader(filename);
h_wb = waitbar(0);
for i = numFramesStart:numFramesEnd
    imlist(i).image = read(video,i);
end



for fi = 1:numberOfFrames
     waitbar(fi/numberOfFrames, h_wb);
     img0 = imlist(fi+numFramesStart-1).image;  
     img = rgb2gray(img0);
%      img = flipud(img);
%      mask = zeros(1024, 'uint8');
     imHist = imhist(img);
     [imHistMax imIdx]=max(imHist(20:255));
     amp = 0.82*200/imIdx;
%      if fi ==1
%          amp
%      end
     [Xdim Ydim] = size(img);
     mask = zeros([1024, 1024],'uint8');
     img2 = amp*img;
     
%      if Zoom
%          dim=100;
%          if (round(dataWhCe(fi,2))-dim)>0&&(round(dataWhCe(fi,2))+dim)<=1024&&...
%             (round(dataWhCe(fi,1))-dim)>0&&(round(dataWhCe(fi,1))+dim)<=1024
%             image10 = img2(round(dataWhCe(fi,1))-dim:round(dataWhCe(fi,1))+dim,...
%                            round(dataWhCe(fi,2))-dim:round(dataWhCe(fi,2))+dim);
%             img2 = uint8(imresize(image10,[1024 1024],'bicubic'));
% %             H = fspecial('unsharp');
% %             img2 = imfilter(img2,H,'replicate');
% %             dataNOSE(fi,1) = round(dataNOSE(fi,1)*1024/(2*dim+1))    
% %             dataNOSE(fi,2) = round(dataNOSE(fi,2)*1024/(2*dim+1))
%             dataWhCe(fi,1) = round(1024/2)+1;
%             dataWhCe(fi,2) = round(1024/2)+1;
%             imlist(fi+numFramesStart-1).image = img2;
% %             for xx =1:1024
% %                 for yy =1:1024
% %                     if img2(xx,yy) == 0
% %                        img2(xx,yy) = 25;
% %                     end
% %                 end
% %             end
%            % disp('Zoom is on')
%          else
%              disp('Zoom is off')
%          end
%      end
     
    pars = [];
    pars.whiskerCentre = dataWhCe(fi, :);
    pars.headBearing = head_bearing(fi);
    pars.erodeRadius = 2;           % default 6
    pars.radInv = 110;              % default 160
    pars.hhThreshold = 160;          % default 160
    pars.erodeBtoW = 10;            % default 10
    pars.edgeDetectLevelNum = 12;   % default 32
        
     [out, debug] = GVM030610(img2, mask, pars);
     
     outMat = out';
     s = size(outMat);
%      for m = 1:s(1)
%          outMat(m,3) = outMat(m,3)-180+head_bearing(fi)*180/pi;
%      end

        % get angle relative to head bearing
        thetas = outMat(:,3);
        
        % wrap to -180 -> 180
        thetas(thetas > 180) = thetas(thetas > 180) - 360;
        thetas(thetas < -180) = thetas(thetas < -180) + 360;
        
        % store back
        outMat(:,3) = thetas;

     whiskerData(fi).outMat = outMat(:,1:3); % relative to head bearing
     whiskerData(fi).outMat(:, 4) = thetas + head_bearing(fi)*180/pi - 90; % relative to image space
     whiskerData(fi).outMat(:, 5) = 0 * whiskerData(fi).outMat(:, 4); % space to record (later) whether whisker is left/right
end

close(h_wb);

for i= 1:(numFramesEnd-numFramesStart+1)
   noseX(i) = dataNOSE(i,2);
   noseY(i) = dataNOSE(i,1);
   whCeX(i) = dataWhCe(i,2);
   whCeY(i) = dataWhCe(i,1);
   whiskerData(i).nose = [noseX(i) noseY(i)];
   whiskerData(i).whCe = [whCeX(i) whCeY(i)];
   
end



% clc

for i = 1: length(whiskerData)
    leftWhiskers = [];
    rightWhiskers = [];
    leftBasePointsX = [];
    leftBasePointsY = [];
    rightBasePointsX = [];
    rightBasePointsY = [];
    for j = 1:length(whiskerData(i).outMat(:,3))        
        
        % get angle of base of whisker from head center
        bx = whiskerData(i).outMat(j,2);
        by = whiskerData(i).outMat(j,1);
        v = [bx; by] - [whCeX(i); whCeY(i)];
        vang = atan2(-v(2), v(1));
        
        % compare with head angle
        vang = vang - (head_bearing(i) - pi/2);
        vang(vang > pi) = vang(vang > pi) - 2*pi;
        vang(vang < -pi) = vang(vang < -pi) + 2*pi;

%         if whiskerData(i).outMat(j,3)-head_bearing(i)*180/pi>-180&&whiskerData(i).outMat(j,3)-head_bearing(i)*180/pi<=0
        if vang > 0
            whiskerData(i).outMat(j, 5) = 1;
            %+180/pi*atan((whCeX(i)-noseX(i))/(whCeY(i)-noseY(i)))
            leftWhiskers = [leftWhiskers, whiskerData(i).outMat(j,3)];
            leftBasePointsX = [leftBasePointsX whiskerData(i).outMat(j,1)];
            leftBasePointsY = [leftBasePointsY whiskerData(i).outMat(j,2)];
%         elseif whiskerData(i).outMat(j,3)-head_bearing(i)*180/pi<=-180&&whiskerData(i).outMat(j,3)-head_bearing(i)*180/pi>-360
        else
            whiskerData(i).outMat(j, 5) = 2;
            rightWhiskers = [rightWhiskers, whiskerData(i).outMat(j,3)];
            rightBasePointsX = [rightBasePointsX whiskerData(i).outMat(j,1)];
            rightBasePointsY = [rightBasePointsY whiskerData(i).outMat(j,2)];
%         else
%             disp = 'error1';
        end
        
        
    end
    
%     asd
    
    meanLeftWhiskers(i) = mean(leftWhiskers);
    meanRightWhiskers(i) = mean(rightWhiskers);
    meanLeftBasePointsX(i) = mean(leftBasePointsX);
    meanLeftBasePointsY(i) = mean(leftBasePointsY);
    meanRightBasePointsX(i) = mean(rightBasePointsX);
    meanRightBasePointsY(i) = mean(rightBasePointsY);
    
%     medianLeftWhiskers(i) = median(leftWhiskers);
%     medianRightWhiskers(i) = median(rightWhiskers);
%     medianLeftBasePointsX(i) = median(leftBasePointsX);
%     medianLeftBasePointsY(i) = median(leftBasePointsY);
%     medianRightBasePointsX(i) = median(rightBasePointsX);
%     medianRightBasePointsY(i) = median(rightBasePointsY);
end



szMat = [];
for i = 1:length(whiskerData)    
    sz = size(whiskerData(i).outMat);
    szMat = [szMat sz(1)];
end
% for i = 1:length(whiskerData)-7+1
%      aveMeanLeftWhiskers(i)= mean(meanLeftWhiskers(i:i+6));
%      aveMeanRightWhiskers(i)= mean(meanRightWhiskers(i:i+6));
% end
% for i = length(whiskerData)-7+1:length(whiskerData)
%     aveMeanLeftWhiskers(i) = meanLeftWhiskers(i);
%     aveMeanRightWhiskers(i) = meanRightWhiskers(i);
% end
[a idx] = max(szMat);
baseX = whiskerData(idx).outMat(:,1);
baseY = whiskerData(idx).outMat(:,2);
baseT = whiskerData(idx).outMat(:,3);
for i = 1:length(baseX)
    distBN(i)=sqrt((baseX(i)-noseX(idx))^2+(baseY(i)-noseY(idx))^2);
    distBW(i)=sqrt((baseX(i)-whCeX(idx))^2+(baseY(i)-whCeY(idx))^2);
end


for z = 1:(numFramesEnd-numFramesStart+1)

    l = 150;
    lw = 4;
    
    figure(2)
    clf
    imshow(imlist(z+numFramesStart-1).image)
    hold on
    plot(noseX(z),noseY(z), '.w')
    hold on
    plot(whCeX(z),whCeY(z), '.b')
    hold on
    for j=1:length(whiskerData(idx).outMat)
        try
            bx = whiskerData(z).outMat(j,2);
            by = whiskerData(z).outMat(j,1);
            wx = cosd(whiskerData(z).outMat(j,4)) * l;
            wy = -sind(whiskerData(z).outMat(j,4)) * l;
            is_left = whiskerData(z).outMat(j,5) == 1;
            
            if is_left
                plot([bx bx+wx], [by by+wy], 'color', 0.75*[0.75 1 0.75])
            else
                plot([bx bx+wx], [by by+wy], 'color', 0.75*[1 0.75 0.75])
            end
            
%             X=x*cosd(whiskerData(z).outMat(j,4))-y*sind(whiskerData(z).outMat(j,4));
%             Y=x*sind(whiskerData(z).outMat(j,4))+y*cosd(whiskerData(z).outMat(j,4));
%             plot(-Y+whiskerData(z).outMat(j,2),(-X+whiskerData(z).outMat(j,1)),'color', 0.75*[1 1 1])
% %             whiskerData(z).outMat(j,3)
% %             pause;
%             hold on
%             plot(whiskerData(z).outMat(j,2),whiskerData(z).outMat(j,1),'.b')
%             hold on

             hold on
             plot(bx, by, '.b')
        
        catch ME1
            idSegLast = regexp(ME1.identifier,'(?<=:)\w+$', 'match');
        end
    end
    
    ol = meanLeftWhiskers(z) + head_bearing(z)*180/pi-90;
    or = meanRightWhiskers(z) + head_bearing(z)*180/pi-90;
    
    blx = meanLeftBasePointsY(z);
    bly = meanLeftBasePointsX(z);
    
    brx = meanRightBasePointsY(z);
    bry = meanRightBasePointsX(z);
    
    plot([blx blx+cosd(ol)*l], [bly bly-sind(ol)*l], '-g', 'linewidth', lw);
    plot([brx brx+cosd(or)*l], [bry bry-sind(or)*l], '-r', 'linewidth', lw);
    
%     lw = 4;
%     X1=x*cosd(meanLeftWhiskers(z))-y*sind(meanLeftWhiskers(z));
%     Y1=x*sind(meanLeftWhiskers(z))+y*cosd(meanLeftWhiskers(z));
%     plot(-Y1+meanLeftBasePointsY(z),(-X1+meanLeftBasePointsX(z)),'-r', 'linewidth', lw)
%     X2=x*cosd(meanRightWhiskers(z))-y*sind(meanRightWhiskers(z));
%     Y2=x*sind(meanRightWhiskers(z))+y*cosd(meanRightWhiskers(z));
%     plot(-Y2+meanRightBasePointsY(z),(-X2+meanRightBasePointsX(z)),'-g', 'linewidth', lw)
    
%     lw = 3;
%      X2=x*cosd(aveMeanLeftWhiskers(z))-y*sind(aveMeanLeftWhiskers(z));
%     Y2=x*sind(aveMeanLeftWhiskers(z))+y*cosd(aveMeanLeftWhiskers(z));
%     plot(-Y2+meanLeftBasePointsY(z),(-X2+meanLeftBasePointsX(z)),'-y', 'linewidth', lw)
%     X3=x*cosd(aveMeanRightWhiskers(z))-y*sind(aveMeanRightWhiskers(z));
%     Y3=x*sind(aveMeanRightWhiskers(z))+y*cosd(aveMeanRightWhiskers(z));
%     plot(-Y3+meanRightBasePointsY(z),(-X3+meanRightBasePointsX(z)),'-b', 'linewidth', lw)
%     lw = 2;
%     X1=x*cosd(medianLeftWhiskers(z))-y*sind(medianLeftWhiskers(z));
%     Y1=x*sind(medianLeftWhiskers(z))+y*cosd(medianLeftWhiskers(z));
%     plot(-Y1+medianLeftBasePointsY(z),(-X1+medianLeftBasePointsX(z)),'-c', 'linewidth', lw)
%     X2=x*cosd(medianRightWhiskers(z))-y*sind(medianRightWhiskers(z));
%     Y2=x*sind(medianRightWhiskers(z))+y*cosd(medianRightWhiskers(z));
%     plot(-Y2+medianRightBasePointsY(z),(-X2+medianRightBasePointsX(z)),'-m', 'linewidth', lw)
    drawnow 
    
    
end
% toc;



