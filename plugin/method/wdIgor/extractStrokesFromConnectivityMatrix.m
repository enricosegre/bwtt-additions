
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [nodesListWithHistAndMarker , strokesData, connectivityMap] = extractStrokesFromConnectivityMatrix...
    (connectivityMatrix, whiskerTree, thetaRange, filteredWhiskerTreeLUT)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function getSimplerConnectivityMatrix is aimed into extracting the larger
% connections from the connectivtyMatrix.
%
% Inputs:   The format of the input data is
%           -connectivityMatrix = [inCurver, inMarker, outCurve, outMarker, distance]
%
%           - whiskerTree structure gives the necessary details:
%           whiskerTree{curveId} = [xofmarker, yofmarker, c, theta,
%           rho](for ORIENTED RADIAL DISTANCE in createWhiskerTree
%           - thetaRange represent the range of elements that get processed
%           and the format is [thetaMin thetaMax]
%           - filteredWhiskerTreeLUT represent markers that are close to
%           the rat snout
%
% Outputs:  - nodesListWithHistAndMarker is a list with
%           [inCurve, inmarker, histogramOFedges, whiskerId]
%           - strokesData is a matrix with the following elements
%           .. [inCurve inMarker outCurve outMarker]
%           - connectivityMap is the map of the connections, with the format
%           [inCurve inMarker whiskerId ConnectivtyLabel flagIsInCurve]
%           if [0 0] inside means max iter reached, if flagIsInCUrve is 2,
%           means fur element
%
% The role of this routine is to extract information connectivity matrix by
% identifying long edges and marking the nodes with the correspondingID. Of
% course the lenght of the edges is updated correspsndingly. The algorithm
% works in the following way.
% For each node we calculate the occurence frequency and we sort according
% to inNode. Than we start with the first node, we mark it as visited
% and we search which nodes are connected to this node. For every
% destination node we check if it has been visited. If visited, we don't add it
% to the que of nodes to visit, otherwise we add it. Then we visit the
% first node in the que and we add its not visited neigbours to the end of
% the que. If the node to visit has an occurence <= 2 we add it into a
% speical que, that is visited when the que of normal elements has been
% finished. In this way we can have two different functions for node
% exploraton. The advantage of this approach is that we don't iterate the
% same function a lot of times.
% NOTE: problem: at first we should visit all the whisker segments, and
% thn we can make the connectivity map! We modified even the start
% condition, so for nodes with hist less than 2 strokes, and for nodes with
% more than 2 elements
%
% NOTE2: the connectivityMap is a pour tool if we don't find all the
% possible connections, even to single points. Since at the moment we want
% to get rid from the base point, we consider as candidate elements in the
% connectivityMatrix also the nodes enough close to the snout, which are
% given by filteredWhiskerTreeLut
%<\outDOC>





%% set-up and calclate the histogram of nodes on a bidirectional graph
whiskerIdArray = zeros(size(connectivityMatrix,1),1);


% calculate the histogram of edges on OUTNODES
histogramOfEdges = whiskerIdArray;

nodesListWithHist = zeros(10000,3);
nodesIndex = 0;


connectivityMatrix = sortrows(connectivityMatrix, [3 4 1 2 3]);
lastMarker = connectivityMatrix(1,3:4);
occurence = 1;
startIndex = 1;
for i=2:size(connectivityMatrix,1),
    currentMarker = connectivityMatrix(i,3:4);
    if isequal(currentMarker,lastMarker)
        occurence = occurence+1;
    else
        nodesIndex = nodesIndex + 1;
        nodesListWithHist(nodesIndex,:) = [lastMarker, occurence];

        histogramOfEdges(startIndex:i-1) = occurence;
        occurence = 1;
        startIndex = i;

        lastMarker = currentMarker;
    end
end

%% sort rows according rho!! not iCurve,iNode!
nodesListWithHist = nodesListWithHist(1:nodesIndex,:);
rhoValues = zeros(nodesIndex,1);
for i=1:nodesIndex,
    inCurve = nodesListWithHist(i,1);
    inMarker = nodesListWithHist(i,2);
    %if inCurve> 0
    rhoValues(i) = whiskerTree{inCurve}(inMarker,5);
    %end
end

nodesListWithHistAndRho = sortrows([rhoValues,nodesListWithHist]);
nodesListWithHist = nodesListWithHistAndRho(:,2:4);


%% start by searching strokes
visitedNodes = zeros(nodesIndex,1);
nodesListWithHistAndMarker = [nodesListWithHist,zeros(nodesIndex,1)];


whiskerId = 0;
strokeData = -ones(1000,4); % in order to avoid false match with [0 0 0 0]
connectivityLabel = 0;
connectivityMapIndex = 0;
connectivityMap = zeros(10000,5);
% connectivityMap format
% [inCurve inMarker whiskerId ConnectivtyLabel flagIsInCurve]
%-----------------------
% FAST FIX - theta filtering
%----------------------
for i=1:nodesIndex,
    if ~visitedNodes(i) && (nodesListWithHist(i,3) <= 2),
        inCurve = nodesListWithHist(i,1);
        inMarker = nodesListWithHist(i,2);



        %rho1 = whiskerTree{inCurve}(inMarker,4);
        theta1 = whiskerTree{inCurve}(inMarker,4);

        if theta1 >= thetaRange(1) && theta1 <= thetaRange(2),
            whiskerId = whiskerId+1;

            %             x1 = whiskerTree{inCurve}(inMarker,1);
            %             y1 = whiskerTree{inCurve}(inMarker,2);
            %             plot(y1,x1,'cd');

            [strokeData(whiskerId,:), visitedNodes, nodesListWithHistAndMarker] = exploreConnectivityMatrixAndMarkStrokes...
                (inCurve, inMarker, whiskerId, connectivityMatrix, whiskerTree, nodesListWithHistAndMarker,visitedNodes);
            %             whiskerId
            %             idc = nodesListWithHistAndMarker(:,4) == whiskerId;
            %             nodesListWithHistAndMarker(idc,:)

            if ~strokeData(whiskerId,1)
                whiskerId = whiskerId-1;
            end

        end
    end
end

strokeData = strokeData(1:whiskerId,:);
%% second FIX
% Due to spurious edges, i.e. triangles, the previously marked nodes are
% visited can block the propagation of the connectivity map. For this
% reason we reset the visitedNodes by putting to zero all unmarked nodes
%OLD visitedNodes = nodesListWithHistAndMarker(:,4) > 0;
visitedNodes(nodesListWithHistAndMarker(:,4) == 0) = 0;

%% SEE NOTE2 marking of elements close to the snout
oldHist = nodesListWithHistAndMarker(:,3);
[ac,iFUR] = intersect(nodesListWithHistAndMarker(:,1:2),filteredWhiskerTreeLUT(:,1:2),'rows');


%% third FIX in the exploreConnectivityMatrix
% Since the stroke in/out nodes have already been marked, the
% exploreConnectivtyMatrixAndMarkNodes is not going to work on them. For
% this reason, we mark them as unvisited and allow to be redscobvered
for i=1:whiskerId,
    idx = ((nodesListWithHist(:,1) == strokeData(i,1)) & (nodesListWithHist(:,2) == strokeData(i,2)));
    if (nodesListWithHist(idx,3) > 1)
        visitedNodes(idx) = 0;
    end
    idx = ((nodesListWithHist(:,1) == strokeData(i,3)) & (nodesListWithHist(:,2) == strokeData(i,4)));
    if (nodesListWithHist(idx,3) > 1)
        visitedNodes(idx) = 0;
    end
end

%% FIFTH FIX elements can note stay inside iFUR and inside a stroke
idx = visitedNodes(iFUR) > 0;
iFUR = iFUR(~idx);

nodesListWithHistAndMarker(iFUR,3) = -ones(size(nodesListWithHistAndMarker(iFUR,3)));
%% FORTH FIX EXPLORE ONLY NODES STARTING FROM INNODE OR OUTNODES
startMarkers = [strokeData(:,1:2); strokeData(:,3:4)];
for i=1:size(startMarkers,1),
    % find the first unmarked node and start from that
    idxOfConnectedEdges = ((connectivityMatrix(:,3) == startMarkers(i,1)) & (connectivityMatrix(:,4) == startMarkers(i,2)));
    connectedNodes= connectivityMatrix(idxOfConnectedEdges,[1 2]);
    inCurve = 0;
    for k=1:size(connectedNodes,1),
        idxOFConnectedNode = ((nodesListWithHistAndMarker(:,1) == connectedNodes(k,1)) &...
            (nodesListWithHistAndMarker(:,2) == connectedNodes(k,2)));
        flagIsVisited = visitedNodes(idxOFConnectedNode);
        if ~flagIsVisited,
            inCurve = connectedNodes(k,1);
            inMarker =connectedNodes(k,2);
            % <\inDOC>
%                         hold on
%                         x1 = whiskerTree{inCurve}(inMarker,1);
%                         y1 = whiskerTree{inCurve}(inMarker,2);
%                         plot(y1,x1,'md');
            %<\outDOC>
            break;
        end
    end

    if inCurve,
        %rho1 = whiskerTree{inCurve}(inMarker,4);
        theta1 = whiskerTree{inCurve}(inMarker,4);

        if theta1 >= thetaRange(1) && theta1 <= thetaRange(2),
            %%
%             if inCurve == 7 && inMarker == 3
%                 pause(0.1)
%             end
            [startPoints, visitedNodes] = exploreConnectivityMatrixAndMarkNodes...
                (inCurve, inMarker, connectivityMatrix, whiskerTree, nodesListWithHistAndMarker,visitedNodes);
            % FIX FOR ALLOWING MULTIPLE COVERS OF POINTS
            visitedNodes(iFUR) = 0;
            %% 5 CASES to handle
                if ~isempty(startPoints)
                    connectivityLabel = connectivityLabel+1;
                    for j=1:size(startPoints,1),
                        % CASE 1: [0 0 0] indicates max iterations reached
                        if startPoints(j,3) == 0,
                            connectivityMapIndex = connectivityMapIndex+1;
                            flagIsStartPoint = 0;
                            connectivityMap(connectivityMapIndex,:) = [zeros(1,3), connectivityLabel, flagIsStartPoint];
                        else
                            whiskerId = find((strokeData(:,1) == startPoints(j,1)) & (strokeData(:,2) == startPoints(j,2)));
                            if ~isempty(whiskerId),
                                % CASE 2: startPoints originates a StrokeData
                                connectivityMapIndex = connectivityMapIndex+1;
                                flagIsStartPoint = 1;
                                connectivityMap(connectivityMapIndex,:) = [strokeData(whiskerId,1:2), whiskerId, connectivityLabel, flagIsStartPoint];
                            else
                                % CASE 3: startPoints ends in a StrokeData
                                whiskerId = find((strokeData(:,3) == startPoints(j,1)) & (strokeData(:,4) == startPoints(j,2)));
                                if ~isempty(whiskerId),
                                    connectivityMapIndex = connectivityMapIndex+1;
                                    flagIsStartPoint = 0;
                                    connectivityMap(connectivityMapIndex,:) = [strokeData(whiskerId,3:4), whiskerId, connectivityLabel, flagIsStartPoint];
                                else
                                    if startPoints(j,3) < 0,
                                        %CASE 5: we reached the fur
                                        connectivityMapIndex = connectivityMapIndex+1;
                                        flagIsStartPoint = 2;
                                        connectivityMap(connectivityMapIndex,:) = [startPoints(j,1:2),0, connectivityLabel, flagIsStartPoint];
                                    else
                                        %CASE 4: startpoints is a single node with a hist == 2
                                        % we skip from the map this
                                        % startPoints
                                    end
                                end
                            end
                        end
                        % <\inDOC>
                        %                     inCurve = startPoints(j,1);
                        %                     inMarker =startPoints(j,2);
                        %                     x1 = whiskerTree{inCurve}(inMarker,1);
                        %                     y1 = whiskerTree{inCurve}(inMarker,2);
                        %                     plot(y1,x1,'yd');
                        % <\outDOC>



                    end
                end
        end
        %        pause;
    end

end

% restore data
nodesListWithHistAndMarker(:,3) = oldHist;

strokesData = strokeData;
connectivityMap = connectivityMap(1:connectivityMapIndex,:);
