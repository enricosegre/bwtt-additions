
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% 	wdParameters.Variable{1}.name = 'input image';
% 	wdParameters.Variable{1}.min = 1.0;
% 	wdParameters.Variable{1}.max = 4;
% 	wdParameters.Variable{1}.value = 1;
% 	wdParameters.Variable{1}.minStep = 1;
% 	wdParameters.Variable{1}.maxStep = 1;
% 	wdParameters.Variable{1}.precision = 2;
	
% 	%% sampling Step in create whisker tree
% 	wdParameters.Variable{2}.name = 'samplingStep';
% 	wdParameters.Variable{2}.min = 1;
% 	wdParameters.Variable{2}.max = 30;
% 	wdParameters.Variable{2}.value = 5;
% 	wdParameters.Variable{2}.minStep = 1;
% 	wdParameters.Variable{2}.maxStep = 5;
% 	wdParameters.Variable{2}.precision = 0;
	
% 	%% stick voting flag
% 	wdParameters.Variable{3}.name = 'stick voting activation flag';
% 	wdParameters.Variable{3}.min = 0;
% 	wdParameters.Variable{3}.max = 1;
% 	wdParameters.Variable{3}.value = 0;
% 	wdParameters.Variable{3}.minStep = 1;
% 	wdParameters.Variable{3}.maxStep = 1;
% 	wdParameters.Variable{3}.precision = 0;
	
% 	%% stick voting threshold - valid only if wdParameters.Variable{3} is true
% 	wdParameters.Variable{4}.name = 'stick voting threshold';
% 	wdParameters.Variable{4}.min = 0;
% 	wdParameters.Variable{4}.max = 1;
% 	wdParameters.Variable{4}.minStep = 0.01;
% 	wdParameters.Variable{4}.maxStep = 0.2;
% 	wdParameters.Variable{4}.value = 0.1;
% 	wdParameters.Variable{4}.precision = 3;
	
	
% 	%% histogrm equalization of difference image
% 	wdParameters.Variable{5}.name = 'show debug';
% 	wdParameters.Variable{5}.min = 0;
% 	wdParameters.Variable{5}.max = 1;
% 	wdParameters.Variable{5}.value = 0;
% 	wdParameters.Variable{5}.minStep = 1;
% 	wdParameters.Variable{5}.maxStep = 1;
% 	wdParameters.Variable{5}.precision = 0;
	
% 	%% internal threshold for peak detection
% 	wdParameters.Variable{6}.name = 'internal ring T';
% 	wdParameters.Variable{6}.min = 1;
% 	wdParameters.Variable{6}.max = 128;
% 	wdParameters.Variable{6}.value = 50;
% 	wdParameters.Variable{6}.minStep = 1;
% 	wdParameters.Variable{6}.maxStep = 10;
% 	wdParameters.Variable{6}.precision = 0;
	
% 	%% histogrm equalization of difference image
% 	wdParameters.Variable{7}.name = 'external ring T';
% 	wdParameters.Variable{7}.min = 1;
% 	wdParameters.Variable{7}.max = 128;
% 	wdParameters.Variable{7}.value = 30;
% 	wdParameters.Variable{7}.minStep = 1;
% 	wdParameters.Variable{7}.maxStep = 10;
% 	wdParameters.Variable{7}.precision = 0;


% 	wdParameters.Variable{8}.name = 'boundary internal-external';
% 	wdParameters.Variable{8}.min = 1;
% 	wdParameters.Variable{8}.max = 400;
% 	wdParameters.Variable{8}.value = 100;
% 	wdParameters.Variable{8}.minStep = 1;
% 	wdParameters.Variable{8}.maxStep = 10;
% 	wdParameters.Variable{8}.precision = 0;

% 	%% local tokens
% 	wdParameters.Constant{1}.name = 'local tokens';
% 	wdParameters.Constant{1}.min = 4;
% 	wdParameters.Constant{1}.max = 15;
% 	wdParameters.Constant{1}.step = 1;
% 	wdParameters.Constant{1}.value = 6;

% 	%% bw binary threshold
% 	wdParameters.Constant{2}.name = 'tensor voting threshold';
% 	wdParameters.Constant{2}.min = 0;
% 	wdParameters.Constant{2}.max = 1;
% 	wdParameters.Constant{2}.step = 0.001;
% 	wdParameters.Constant{2}.value = 0.01;

% 	%% sigma fied in tensor voting
% 	wdParameters.Constant{4}.name = 'sigma';
% 	wdParameters.Constant{4}.value  = 2;
% 	wdParameters.Constant{4}.min = 1;
% 	wdParameters.Constant{4}.max = 20;
% 	wdParameters.Constant{4}.step = 1;
% 
% 	%% theta range selects the limits of whisker tree selection and speeds up
% 	% whisker selection - it is a 2 element vector
% 	wdParameters.Constant{5}.name = 'thetaRange';
% 	wdParameters.Constant{5}.value = [-2*pi,2*pi];
% 	
% 	%% scalarT for filtering spurious measurements
% 	wdParameters.Constant{6}.name = 'scalarProduct T';
% 	wdParameters.Constant{6}.value = 0.9;
% 	
% 	%% scalarT for filtering spurious measurements
% 	wdParameters.Constant{7}.name = 'rhoT';
% 	wdParameters.Constant{7}.value = 20;
