
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% function [whiskerContour, nextWDChunkData, optionalOutputs]= wdIgor(imageBuffer, wdParameters, previousWDChunkData, snoutTrackingData, objectTrackingData)

function state = wdIgorProcess(job, state)


% wdIgor.m
% syntax:
%   [whiskerContour, nextWDChunkData, optionalOutputs]=
%               wdIgor(imageBuffer, wdParameters, previousWDChunkData, snoutTrackingData, objectTrackingData)
%
% Inputs:
%           - imageBuffer contains the input image/s
%           - wdParam is a data structure containing whisker detection
%           algorithm parameters.
%               Constant parameters:
%                       Constant{1} - number of elements considered for
%                       peak detetction
%                       Constant{2} - tensor voting threshold, touch only
%                       in case of emergency
%                       Constant{3} - not assigned
%                       Constant{4} - sigma, touch only if you are not
%                       satisfied with detetcted whiskers
%                       Constant{5} - theta range, used for filtering
%                       values of left/right whiskers
%                       Constant{6} - filtering scalar product in order to
%                       eliminate measurment that are less than
%                       Constant{7}.value
%                       Constant{7} - selects whiskers close to detected
%                       snout contour
%
%               Variable parameters:
%                       Variable{1} - you can play using differrent
%                       intermediate outputs of processing as input to the
%                       following stages
%                       Variable{2} - sampling step for finding points in
%                       polar coordinates - try first larger values in
%                       order to reduce the ammount of data to be processed
%                       Variable{3} - stick voting activation flag - set to
%                       on only if you have poor data
%                       Variable{4} - stick voting threshold is used for
%                       selecting output of stick voting - NOTE SET TO 4
%                       Variable{1} in this case
%                       Variable{5} - image binarization is used to mask
%                       noise values under selected threshold
%                       Variable{6} - Variable{7} are thresholds used to
%                       select whisker peaks in the internal or external
%                       part of the ring in polar coordinates defined by
%                       Variable{8}
%           - previousChunkData is empty
%           - snoutTrackingData is a data structure containg three fields:
%           snoutTrackingData.center,snoutTrackingData.orientation and
%           snoutTrackingData.contour. Snout center contains [x,y]
%           coordinates and snout.orientation is the theta value in
%           radians, while contour is the [x,y] list of the tracked contour
%           - objectTrackingData contains contours of tracked objects
%           if frameChunk > 1 objectTrackingData is a cell matrix of size(1xframeChunk). For
%           multiple views the size is (numberOfViews X framechunk). Each cell element contains
%           an other cell element with size 1xnumberOfObjects. With one view and frameChunk = 1,
%           objectTrackingData is only the the cell of size 1xnumberOfObjects, where
%           data matrix requires 1st row contains coordinates
%           with y data (row) and the second row contains x data (column
%           coordinates).
%
% Outputs:
%           - if frameChunk > 1 detectedWhiskers is a cell matrix of size(1xframeChunk). For
%           multiple views the size is (numberOfViews X framechunk). Each cell element contains
%           an other cell element with size 1xnumberOfDetectedWhiskers. With one view and frameChunk = 1,
%           detectedWhiskers is only the the cell of size 1xnumberOfDetectedWhiskers, where
%           data matrix requires 1st row contains coordinates
%           with y data (row) and the second row contains x data (column
%           coordinates).
%           - nextChunkData is the updated version of previousChunkData
%           - optionalOutputs is a custom data structure used for passing
%           optional parameters for debugging and other to the general
%           manager

%
%
%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it



%% RETRIEVE STATE

% get frame index
frameIndex = state.frameIndex;

% may as well alert user if we fail on no preproc, since it
% takes a long time to compute this in the error condition
if ~job.isPreprocPresent(frameIndex)
	error('cannot compute wdIgorMeanAngle - no pre-processing is configured (recommend ppBgExtractionAndFilter)');
end

% get the image
imageBuffer = job.getRuntimeFrame(frameIndex);

% this isn't efficient, but it's easy...
imageBuffer = imageBuffer{1};

% get snoutTrackingData
results = job.getResults(frameIndex);
if ~isfield(results{1}, 'snout')
	error('cannot compute wdIgor - no snout tracking available');
end
snoutTrackingData = results{1}.snout;

% uncompress snoutTrackingData
snoutTrackingData.perpdists = double(snoutTrackingData.perpdists);
snoutTrackingData.contour = double(snoutTrackingData.contour);








%% IGOR'S CODE

%% 2. WHISKER DETECTION
fNum = 11;
flagFigure1On = false;

% if ~strcmp(imageBuffer,'double')
if ~isa(imageBuffer, 'double')
    imageBuffer = double(imageBuffer);
    imageBuffer = imageBuffer/max(max(imageBuffer));
end
eqGRAY = imageBuffer;

%% run tensor voting
sigmaField = state.pars.sigma; % sigmaField = 2;
thresh1 = state.pars.stickVotingThreshold; % stick voting thresh1 = 0.1;
thresh = state.pars.tensorVotingThreshold; %
flagStickVotingActive = state.pars.stickVotingActivationFlag;

[q, angleIm, roundness, saliency] = runTensorVoting(eqGRAY, thresh, sigmaField, thresh1,flagStickVotingActive);


%% run binary feature detector

trOuterRing = state.pars.externalRingT / 255;
trInnerRing = state.pars.internalRingT / 255;
trBinary =  max(min(trOuterRing, trInnerRing),1/255);
rhoThresh = state.pars.boundaryInternalExternal;
samplingStep = state.pars.samplingStep;
image2select = state.pars.inputImage;
localTokens = state.pars.localTokens;

switch image2select
    case 1
        imageToPreprocess = eqGRAY;
    case 2
        imageToPreprocess = saliency/max(max(saliency));
    case 3
        imageToPreprocess = roundness/max(max(roundness));
    case 4
        imageToPreprocess = q/max(max(q));
end
%%
X = fliplr(snoutTrackingData.center);
%potentialDensityArray = 1:samplingStep:10;
param = struct('whiskersSmoothingT',trBinary,'localTokensN',localTokens,'thresh1',trInnerRing, 'thresh2',trOuterRing, 'rhoThresh', rhoThresh);


whiskerTree = createWhiskerTree([], samplingStep, imageToPreprocess, X, 'ORIENTEDRADIALDISTANCE', 'maxOnly', param);

% whiskerTree = filterWhiskerTreeByTheta(whiskerTree,-asin(X(4)), thetaRange)

showDebugFlag = state.pars.showDebug;
if showDebugFlag
    stSelectFigure wdIgor_debug
	clf
	p = panel();
	p.pack(3, 2);
	p(1, 1).select();
    imshow(uint8(255*imageToPreprocess));
    colormap(gray)
    axis image
	p(1, 2).select();
    imshow(uint8(255*imageToPreprocess));
    axis image
    hold on
    for i=1:numel(whiskerTree),
        if ~isempty(whiskerTree{i})
            plot(whiskerTree{i}(:,2), whiskerTree{i}(:,1),'rx','MarkerSize', 6, 'LineWidth',2);
        end
    end
end

%% create connectivity matrix and plot it
whiskerTreeLUT = convertInNodesToTable(whiskerTree);

if isempty(whiskerTreeLUT)
	
	error('No points detected - change thresholds');
	
% %     disp('ERROR: No points detected - change thresholds')
% text_Process_Control = get(findobj('Tag','edit_Process_Control'),'String');
%     text_Process_Control = strvcat(text_Process_Control,...
%            'ERROR: No points detected - change thresholds');
%         set(findobj('Tag','edit_Process_Control'), 'Max',2,'Min',0,'String',...
%             text_Process_Control,'FontSize',8,'ForegroundColor','red');
%     detectedW = [];
%     trackingData = [];

end



[connectivityMatrixFromTVpure, whiskerTreeLUTwithOrientations] = extractSpatialConnectivityWithTensorVoting(whiskerTreeLUT, ...
		angleIm, whiskerTree, roundness, saliency, 0,4); %flagFigure1On);
connectivityMatrixFromTVpure = eliminateRedundantEdges(connectivityMatrixFromTVpure, whiskerTree, fNum*flagFigure1On,[0.2 0.7 0.1]);

[connectivityMatrixFromTV, deletedEdgeList] = filterNoisyConnections(connectivityMatrixFromTVpure, whiskerTree, whiskerTreeLUTwithOrientations, 50);
connectivityMatrixFromTV = eliminateRedundantEdges(connectivityMatrixFromTV, whiskerTree, fNum*flagFigure1On,[1 1 1]);

% plot connectivity changes
sNum = fNum + 30;
if flagFigure1On
		figure(sNum)
		imshow(eqGRAY)
		hold on
end
%scalarT = 0.9; rhoT = 20;
scalarT = state.pars.scalarProductT;
rhoT = state.pars.rhoT;

connectivityMatrixFromTV = eliminateRedundantEdges(connectivityMatrixFromTV, whiskerTree, sNum*flagFigure1On,[0.8 0 0]);
connectivityMatrixFromTV = filterConnectivityWithSnoutContour(snoutTrackingData.contour, whiskerTree,  connectivityMatrixFromTV, rhoT, X, scalarT);
connectivityMatrixFromTV = eliminateRedundantEdges(connectivityMatrixFromTV,whiskerTree, sNum*flagFigure1On,[0.6 1 1]);

%% create snout contour LUT
contourLUT =  createContourLUT([], snoutTrackingData);

% LUT fix - 24.02.2010
[c,ia,ib] = intersect(whiskerTreeLUT(:,[3 4]), contourLUT(:,[3 4]),'rows');
% wlut2cut = whiskerTreeLUT(ia,:);
% wlut2paste = contourLUT(ib,:);
% for i=1:numel(ia),
%     % fix connectivity matrix
%     idx = (connectivityMatrixFromTV(:,1) == wlut2cut(i,1)) & ...
%         (connectivityMatrixFromTV(:,2) == wlut2cut(i,2));
%     connectivityMatrixFromTV(idx,1) = wlut2paste(i,1);
%     connectivityMatrixFromTV(idx,2) = wlut2paste(i,2);
%     idx = (connectivityMatrixFromTV(:,3) == wlut2cut(i,1)) & ...
%         (connectivityMatrixFromTV(:,4) == wlut2cut(i,2))
%     connectivityMatrixFromTV(idx,3) = wlut2paste(i,1);
%     connectivityMatrixFromTV(idx,4) = wlut2paste(i,2);
% end
% whiskerTreeLUT(ia,:) = nan;
% whiskerTreeLUT = whiskerTreeLUT(~isnan(whiskerTreeLUT(:,1)),:);
contourLUT(ib,:) = nan;
contourLUT = contourLUT(~isnan(contourLUT(:,1)),:);
%% get strokes and additional connectivity
theta = snoutTrackingData.orientation;
thetaRange = state.pars.thetaRange;
% old position
[nodesListWithHistAndMarker , strokesData, connectivityMap] = ...
		extractStrokesFromConnectivityMatrix(connectivityMatrixFromTV, whiskerTree, theta+thetaRange, contourLUT);

% create smooth connectivity
newConnectivityMatrix = createSmoothConnectionMatrix(whiskerTreeLUTwithOrientations, nodesListWithHistAndMarker, 0, eqGRAY);
connectivityMatrix = eliminateRedundantEdges([connectivityMatrixFromTV; newConnectivityMatrix], whiskerTree, sNum*flagFigure1On,[0.6 1 0]);



% create fur connectivity
radiusSize = 10;
furConnectivityMatrix = createCompleteConnectivity(contourLUT, whiskerTreeLUTwithOrientations, radiusSize);


%% create whiskers
minimalLength = 4; % minimal number of points to trace a stroke

minimalDistance = 0;
flagFigure1On = false;
whiskerTreeLUTwithOrientations = sortrows(whiskerTreeLUTwithOrientations, [7 6]);

detectedW = createWhiskers(whiskerTreeLUTwithOrientations, contourLUT, furConnectivityMatrix, ...
		connectivityMatrix, nodesListWithHistAndMarker, eqGRAY, strokesData, flagFigure1On, minimalLength, minimalDistance);


%% join detection points and fur connectivity

whiskerTreeLUTwithOrientations = [whiskerTreeLUTwithOrientations; contourLUT];

if showDebugFlag
	p(2, 1).select();
    imshow(uint8(255*imageToPreprocess));
    axis image
    hold on
contour = snoutTrackingData.contour;
	plot(contour(2, :), contour(1, :), 'y-');
	
		completeConnectivityMatrix = eliminateRedundantEdges([furConnectivityMatrix; ...
				connectivityMatrix], whiskerTreeLUTwithOrientations, fNum,[1 0.4 0]);
else
		completeConnectivityMatrix = eliminateRedundantEdges([furConnectivityMatrix; ...
				connectivityMatrix], whiskerTreeLUTwithOrientations, 0,[1 0.4 0]);
end



%% 3. store data
trackingData{1,1} = eqGRAY;
trackingData{1,2} = nodesListWithHistAndMarker;
trackingData{1,3} = snoutTrackingData;
trackingData{1,4} = whiskerTreeLUTwithOrientations;
trackingData{1,5} = connectivityMatrix;
trackingData{1,6} = connectivityMap;
trackingData{1,7} = strokesData;
trackingData{1,8} = furConnectivityMatrix;
trackingData{1,9} = deletedEdgeList;
trackingData{1,10} = contourLUT;
trackingData{1,11} = whiskerTree;
trackingData{1,12} = connectivityMatrixFromTV;
trackingData{1,13} = detectedW;
trackingData{1,14} = completeConnectivityMatrix;

nextWDChunkData = trackingData;

% TODO: store this for whisker tracking



whiskerContour = detectedW;


% optionalOutputs = [];







%% STORE RESULTS

% compress results
for w = 1:length(whiskerContour)
	whiskerContour{w} = single(whiskerContour{w});
end

% store results
results = [];
results.contour = whiskerContour;
job.setResults(frameIndex, 'whisker', results);







