
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function  [q1, angleIm, roundness, saliency] = runTensorVoting(Gor, thresh, sigma, thresh1, flagStickVotingOn)

% Author: Perkon Igor    -    perkon@sissa.it
%

% <\inDOC>
% Function runCentralTensorVoting
%
% Inputs:   - Gor is the gray scale image of whiksers
%           - thresh is used to select the binary mask of the whiskers data
%           - sigma is tensor voting field view
%           - thresh1 serves as final threshold for output selection
% Outputs:  
%           - q1 is the output image with tensor values
%           - angleIm is the image with estimated angles uisng tensor
%           voting
%           - roundness image
%           - saliency measure
%
% The function runs tensor voting on the image. At first central tensor
% voting and optionally the stick voting
% <\outDOC>
if nargin < 5
    flagStickVotingOn = true;
else
    flagStickVotingOn = false;
end
    

sigmaStick = 3*sigma;
Gor = double(Gor);
maxG = max(max(Gor));
G = Gor./maxG;
Greal = G;
S = size(G);
G = im2bw(G, thresh);
% added 09.04.2009
G = Greal.*G;
%% G =  bwmorph(G,'thin');
 
T = zeros(S(1), S(2), 2, 2);



ballTF = circularField(sigma);
   
T(:,:,1,1) = (imfilter(G, ballTF(:,:,1,1), 'same' )).*(G>0);
T(:,:,1,2) = (imfilter(G, ballTF(:,:,1,2), 'same' )).*(G>0);
T(:,:,2,1) = (imfilter(G, ballTF(:,:,2,1), 'same' )).*(G>0);
T(:,:,2,2) = (imfilter(G, ballTF(:,:,2,2), 'same' )).*(G>0);



[e1,e2,l1,l2] = convert_tensor_ev(T);

if flagStickVotingOn,
    z = l1-l2;
    l2(z< thresh1) = 0;
    l1(z< thresh1) = 0;


    T = convert_tensor_ev(e1,e2,l1,l2);
    cached_vtf = create_cached_vf(sigmaStick);
    T = calc_vote_stick(T,sigmaStick,cached_vtf);

    [e1,e2,l1,l2] = convert_tensor_ev(T);
end



roundness = l2;
saliency = l1-l2;



angleIm = getLocalAngle(T,sigma,G,0);

S = saliency./max(max(saliency));
q1 = sqrt(S.*G);
%q1 = angleIm;
%q1 = q1.*(bwmorph(q1>0,'thin'));

