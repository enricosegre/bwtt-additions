
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

 function [startPoints, visitedNodes] = exploreConnectivityMatrixAndMarkNodes...
     (inCurve, inMarker, connectivityMatrix, whiskerTree, nodesListWithHistAndMarker,visitedNodes)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:   - inCurve, inMarker are the starting coordinates
%           - onnectivityMatrix [curveNin nodeNumberIdIn curveNout
%           nodeNumberIdOut distance]
%           - whiskerTree is a cell array'ORIENTEDRADIALDISTANCE' then the
%           output is [x,y,c,theta, rho]
%           - nodesListWithHistAndMarker is a nx4 matrix, with [inNode, outNode,
%           hist whiskerID]
%           - visitedNodes is a nx1 array with flags set to 1 when visited
%           
%
% Outputs:  - visitedNodes is a nx1 array with flags set to one
%           - startPoints is an mx3 array with all the elements in the
%           exploration process that have occurence = 2 and other cases(see bellow).
%           [inCurve inMarker hist]
% NOTE: in order to avoid too much iterations, we introduce the recurrence
% level inside iteratedTreeSearch. We quit if it reaches too much ietrations.
%
% NOTE2: the function has been modified to return [0 0] as data for
% maxrecursion reached. 
%
% NOTE3: if the hist is negative, the point is treated also as endNode, in
% order to extract every possible connection even in case of misdetection
% of strokes
% NOTE4: if the starting point is also a point within fur range, only that
% point and the strokeData should be detected! 
%
% NOTE5: eliminated the recursion level filter for start points see NDI
%<\outDOC>




startPoints = zeros(100,3);
startPointsIndex = 0;

idx = ((nodesListWithHistAndMarker(:,1) == inCurve)) & (nodesListWithHistAndMarker(:,2) == inMarker);
if (nodesListWithHistAndMarker(idx,3) < 0)
    % add to the data table
    startPointsIndex = startPointsIndex+1;
    startPoints(1,:) = [inCurve inMarker -1];
end

inElementsTable = [inCurve, inMarker];

[inElementsTable, startPoints, startPointsIndex, visitedNodes, recursionLevel] = iterateTreeSearch...
    (inElementsTable, startPoints, startPointsIndex, connectivityMatrix, visitedNodes, nodesListWithHistAndMarker, whiskerTree, 0);

% don't add the start node since it has always hist>2
% if startPointsIndex
%     startPointsIndex = startPointsIndex+1;
%     startPoints(startPointsIndex,:) = [inCurve, inMarker];
% end

startPoints = startPoints(1:startPointsIndex,:);







%% iterate search function bread first search

function [inElementsTable, startPoints, startPointsIndex, visitedNodes, recursionLevel] = iterateTreeSearch...
    (inElementsTable, startPoints, startPointsIndex, connectivityMatrix, visitedNodes, nodesListWithHistAndMarker, whiskerTree, recursionLevel)


idxOfConnectedEdges = ((connectivityMatrix(:,3) == inElementsTable(1,1)) & (connectivityMatrix(:,4) == inElementsTable(1,2)));
connectedNodes= connectivityMatrix(idxOfConnectedEdges,[1 2]);

for i=1:size(connectedNodes,1),
%<\DEBUG>
%         figure(1)
%         hold on
%         outCurve = inElementsTable(1,1);
%         outMarker = inElementsTable(1,2);
%         inCurve = connectedNodes(i,1);
%         inMarker =connectedNodes(i,2); 
%         x1 = whiskerTree{inCurve}(inMarker,1);
%         y1 = whiskerTree{inCurve}(inMarker,2);
%         x2 = whiskerTree{outCurve}(outMarker,1);
%         y2 = whiskerTree{outCurve}(outMarker,2);
%         line([y1 y2],[x1 x2],'Color','g')
%         plot(y1,x1,'gd');
%         plot(y2,x2,'g+');
%             if isequal(connectedNodes(i,1:2),[3,7])
%         pause(0.1)
%     end
%     if isequal(connectedNodes(i,1:2),[5,6])
%         pause(0.1)
%     end
% [x1 y1 x2 y2 inCurve inMarker outCurve outMarker]             
%<\outDOC>
    idxOFConnectedNode = ((nodesListWithHistAndMarker(:,1) == connectedNodes(i,1)) & (nodesListWithHistAndMarker(:,2) == connectedNodes(i,2)));
    flagIsVisited = visitedNodes(idxOFConnectedNode);

    
    if ~flagIsVisited,
        % if still unvisitedNode, check its hist
        histOfThisNode = nodesListWithHistAndMarker(idxOFConnectedNode, 3);
        markerOfThisNode = nodesListWithHistAndMarker(idxOFConnectedNode, 4);
        if (markerOfThisNode) % (histOfThisNode == 2) && 
            % set it in the startPoints
            startPointsIndex = startPointsIndex+1;
            startPoints(startPointsIndex,:) = [connectedNodes(i,1:2), histOfThisNode];
        elseif (histOfThisNode < 0) && (markerOfThisNode == 0) 
            % NDI 01/09/2008 if (recursionLevel > 0)
            % set it in the startPoints, since is a point close to the
            % snout and does not belong to a stroke
            startPointsIndex = startPointsIndex+1;
            startPoints(startPointsIndex,:) = [connectedNodes(i,1:2), histOfThisNode];
            % end
        else
            % add it to the nodes to visit table
            inElementsTable = [inElementsTable; connectedNodes(i,1:2)];
        end
        visitedNodes(idxOFConnectedNode) = 1;
    end
end
% extract the first element from the que
inElementsTable = inElementsTable(2:end,:);

recursionLevel = recursionLevel + 1;

MAX_RECURSIONS = 150;

if recursionLevel == MAX_RECURSIONS,
    inElementsTable = [];
    % startPointsIndex = 0;
    % startPoints = [];
    startPointsIndex = startPointsIndex + 1;
    startPoints(startPointsIndex,:) = [0 0 0]; % means MAX_RECURSIONS REACHED
end


if ~isempty(inElementsTable)
    [inElementsTable, startPoints, startPointsIndex, visitedNodes, recursionLevel] = iterateTreeSearch...
    (inElementsTable, startPoints, startPointsIndex, connectivityMatrix, visitedNodes, nodesListWithHistAndMarker, whiskerTree, recursionLevel);
end





