
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [outIdx, fieldValue] = getBestTensorVotingEstimates(pointTangent, pointStroke, localStroke, localTheta, localDistances, localTangent, localT, localFilteredWhiskerTreeLUT, filteredPoint)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Input:
%       -) pointTangent is the local tangent in radians
%       -) pointStroke is the local pointStroke number
%       -) localStroke are the stroke numbers of points inside
%       neighbourhood
%       -) localTheta are the direction values of neighbourhood
%       -) localDistance are distances directions
%       -) localTangent is the orientation value in
%       -) localT is the threshold of the field for returning elements
%       -)  localFilteredWhiskerTreeLUT is used for field checking and
%       plotting (see bellow code)
%       -) filteredPoint is the central point
%       (obtained as filteredWhiskerTreeLUT(i,:))
% Output
%       -) outidx are the best (from null to three indexes) of
%       fileteredWhiskerTreeLUT table indexes that are over threshold.
%       The format is 1st element is best, second is first on left and
%       the third is best on right
%       the same indexes are again repeated for the other side
%
%<\outDOC>
%% set field parameters

sigma = 25;
c = 30000;

if nargin < 7,
    localT = 0.6;
end

%% create field
theta = asin(sin(localTheta-pointTangent));
sintheta = sin(theta);
sSquare = ((theta.*localDistances)./sintheta).^2;
sSquare(isnan(sSquare(:))) = localDistances(isnan(sSquare(:))).^2;
kSquare = (2*sintheta./localDistances).^2;
kSquare(isnan(kSquare(:))) = 0;

Sfield =exp(-((sSquare+c*kSquare))/sigma^2);


% weight field with local tangent difference
Stangent = abs(cos(localTangent-pointTangent));
Stangent(isnan(Stangent)) = 1;
S = Stangent' .* Sfield;



% split in left and right continuation
outIdx = nan(6,1);
fieldValue = nan(6,1);

if max(S) > localT,
    costheta = cos(localTheta-pointTangent);
    localSide = sign(costheta);
    [val,pos] = sort(S.*(localSide>0),'descend');
    if localStroke(pos(1)) ~= pointStroke
        if val(1) > localT
            outIdx(1) = pos(1);
            fieldValue(1) = val(1);
            sortedTheta = theta(pos);
            % search for successive elements on left
            for j=2:numel(val),
                if sortedTheta(j) > sortedTheta(1) && val(j) > localT,
                    outIdx(2) = pos(j);
                    fieldValue(2) = val(j);
                    break;
                end
            end
            % search for ssuccessive elements on right
            for j=2:numel(val),
                if sortedTheta(j) <= sortedTheta(1) && val(j) > localT,
                    outIdx(3) = pos(j);
                    fieldValue(3) = val(j);
                    break;
                end
            end
        end
    end


    % repeat for other side
    [val,pos] = sort(S.*(localSide<=0),'descend');
    if localStroke(pos(1)) ~= pointStroke
        if val(1) > localT
            outIdx(4) = pos(1);
            fieldValue(4) = val(1);
            sortedTheta = theta(pos);
            % search for successive elements on left
            for j=2:numel(val),
                if sortedTheta(j) > sortedTheta(1) && val(j) > localT,
                    outIdx(5) = pos(j);
                    fieldValue(5) = val(j);
                    break;
                end
            end
            % search for ssuccessive elements on right
            for j=2:numel(val),
                if sortedTheta(j) <= sortedTheta(1)  && val(j) > localT,
                    outIdx(6) = pos(j);
                    fieldValue(6) = val(j);
                    break;
                end
            end
        end
    end
end

%% plotting enabled by localFilteredWhiskerTreeLUT
if nargin > 8,

    di =1; R = 30;
    [DX, DY] = meshgrid([-R:di:+R],[-R:di:+R]);
    [theta1, len] = cart2pol(DX, DY);
    % % theta = asin(DY./L);
    theta = asin(sin(theta1-pointTangent));
    sintheta = sin(theta);
    %warning('OFF',' Divide by zero.');
    sSquare = ((theta.*len)./sintheta).^2;
    sSquare(isnan(sSquare(:))) = len(isnan(sSquare(:))).^2;
    kSquare = (2*sintheta./len).^2;
    kSquare(isnan(kSquare(:))) = 0;
    figure(6)
    imcontour(exp(-((sSquare+c*kSquare))/sigma^2));
    colorbar
    hold on
    plot([localFilteredWhiskerTreeLUT(:,4) - filteredPoint(4)]+R, [localFilteredWhiskerTreeLUT(:,3) - filteredPoint(3)]+R,'k+');
    for i=1:numel(outIdx),
        if ~isnan(outIdx(i))
            plot([(localFilteredWhiskerTreeLUT(outIdx(i),4) - filteredPoint(4))+R,R], [(localFilteredWhiskerTreeLUT(outIdx(i),3) - filteredPoint(3))+R,R],'b');
        end
    end
    hold off

end


