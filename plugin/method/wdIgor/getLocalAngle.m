
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function angleIm = getLocalAngle(T,sigma,im, flagBar)

% Input:
%       - T tensor field
%       - sigma is field size
%       - im is the original image
%       - flagBar is used for plotting
% Outputs:
%        - angleIm is the angle image
if nargin<2
    sigma = 18.25;
end

wsize = floor( ceil(sqrt(-log(0.01)*sigma^2)*2) / 2 )*2 + 1;
wsize_half = (wsize-1)/2;

% resize the tensor to make calculations easier. This gives us a margin
% around the tensor that's as large as half the window of the voting
% field so when we begin to multiple and add the tensors we dont have
% to worry about trimming the voting field to avoid negative and
% overflow array indices.
Th = size(T,1);
Tw = size(T,2);

Tn = zeros(Th+wsize_half*2,Tw+wsize_half*2,2,2,'double');
Tn((wsize_half+1):(wsize_half+Th), (wsize_half+1):(wsize_half+Tw), :, :) = T(1:end,1:end,:,:);
T = Tn;

% perform eigen-decomposition, assign default tensor from estimate
[e1,e2,l1,l2] = convertTensorEV(T);

% Find everything that's a stick vote.  Prehaps use a threshold here?
%[u,v] = find ( l1-l2 > 0 );
paddedIm = padarray(im,[wsize_half wsize_half]);
%[u , v] = find(paddedIm.*(l1-l2)>0);
[u,v] = find ( l1-l2 > 0 );
angleIm = -inf(size(paddedIm));
% find angle
if flagBar
    d = waitbar(0,'Please wait, finding angle...');
end
% Loop through each stick found in the tensor T.
a = 0;


p = numel(u);
D = zeros(2,p,'double');
D(1,:) = u';
D(2,:) = v';
op = ceil(p*0.01);
t = 0;
for s = D;
    a = a+1;
    t = t + 1;
    if flagBar
        if mod(a,op) == 0
            waitbar(a/p,d);
        end
    end
   

    % the direction is e1 with intensity l1-l2
    v = e1(s(1),s(2),:);

    angle = round(180/pi*atan(v(2)/v(1)));
    if angle < 1
        angle = angle + 180;
    end
    angleIm(D(1,t),D(2,t)) = angle;
end


angleIm = angleIm((wsize_half+1):(wsize_half+Th), (wsize_half+1):(wsize_half+Tw));
if flagBar
close(d);
end
