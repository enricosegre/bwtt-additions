
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function newConnectivityMatrix = filterConnectivityWithSnoutContour( ...
	snoutContour, whiskerTree, connectivityMatrix, rhoT, X, scalarProductT)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function filterConnectivityMapBySnoutAngle
% Inputs:   - snoutContour is the spline contour
%           - whiskerTree structure is the structure of tokens with the
%               following format [x,y,intensity,theta, rho] (average values
%               for each token
%           - connectivityMatrix [curveIn NodeIn curveOut
%           nodeOut distance] are the original links
%           - rhoT defines the distance with is valid the filtering
%           - X are the snout tracking coordinates
% Outputs:  newConnectivityMatrix [curveIn NodeIn curveOut
%           nodeOut distance]
%   
%           TO IMPROVE: 
%           uporabi real contour data, da lahko filtrira se od bolj dalec
%
%<\outDOC>
%%
%X = nextChunkData.X;
%snoutContour = nextChunkData.pp;
%connectivityMatrix = connectivityMatrixFromTV;
%rhoT = 60;
%scalarProductT = 0.85;

%% 1. get local angles
M = size(connectivityMatrix,1);
tangentVectors = nan(M,2);
startPolarPoint = nan(M,2);
for i=1:M,
    inCurve = connectivityMatrix(i,1);
    inMarker = connectivityMatrix(i,2);
    outCurve = connectivityMatrix(i,3);
    outMarker = connectivityMatrix(i,4);
    x1 = whiskerTree{(inCurve)}((inMarker),[1 2]);
    y1 = whiskerTree{(outCurve)}((outMarker),[1 2]);   
    tangentVectors(i,:) = (x1-y1)./norm(x1-y1);
    startPolarPoint(i,:) = whiskerTree{inCurve}((inMarker),[4 5]);
end
% in order to have the same xy of contour
tangentVectors = fliplr(tangentVectors);
%% 2. convert contour to polar cordinates
if ~isstruct(snoutContour)
    pp = cscvn(flipud(snoutContour));
    snoutContour = pp;
end
    
dstep = linspace(snoutContour.breaks(1), snoutContour.breaks(end), 1000);
pts = fnval(snoutContour, dstep);
contourTangents = fnval(fnder(snoutContour), dstep);
contourTangents(1,:) = contourTangents(1,:)./(hypot(contourTangents(1,:),contourTangents(2,:)));
contourTangents(2,:) = contourTangents(2,:)./(hypot(contourTangents(1,:),contourTangents(2,:)));
[thetaC, rhoC] = cart2pol(pts(2,:)-X(2), pts(1,:)-X(1));

%% 3. scan candidate edges and filter by scal product
for i=1:M, 
    % get closest point in theta coordinates
    startTheta = startPolarPoint(i,1);
    startRho = startPolarPoint(i,2);
    [minVal, minPos] = min(abs(thetaC - startTheta));
    % get if point is inside the magic circle
    if abs(startRho - rhoC(minPos)) < rhoT,
        scalarProduct = dot(contourTangents(:,minPos) ,tangentVectors(i,:)');
        if abs(scalarProduct) > scalarProductT,
            % links are parallel
            connectivityMatrix(i,:) = nan(1,5);
        end
    end
end

idx = isnan(connectivityMatrix(:,1));


%% debug connection
if 0
ivals = find(idx);
figure(sNum)
imshow(GRAYshape)
hold on
eliminateRedundantEdges(connectivityMatrixFromTV, whiskerTree, sNum,[0.9 1 1]);
hold on;
fnplt(snoutContour);
connectivityMatrix = connectivityMatrixFromTV;
for i=ivals', 
    % get closest point in theta coordinates
    startTheta = startPolarPoint(i,1);
    startRho = startPolarPoint(i,2);
    [minVal, minPos] = min(abs(thetaC - startTheta));
    
    inCurve = connectivityMatrix(i,1);
    inMarker = connectivityMatrix(i,2);
    outCurve = connectivityMatrix(i,3);
    outMarker = connectivityMatrix(i,4);
    x1 = whiskerTree{(inCurve)}((inMarker),[1 2]);
    y1 = whiskerTree{(outCurve)}((outMarker),[1 2]);   

    % get if point is inside the magic circle
    if (startRho - rhoC(minPos)) < rhoT,
        
        plot([x1(2) y1(2)],[x1(1) y1(1)],'g')
        plot(pts(1,minPos), pts(2,minPos), 'yo');
        scalarProduct = dot(contourTangents(:,minPos) ,tangentVectors(i,:)');
        if abs(scalarProduct) > scalarProductT,
            % links are parallel
            plot([x1(2) y1(2)],[x1(1) y1(1)],'m')
            connectivityMatrix(i,:) = nan(1,5);
        end
    end
end
end
newConnectivityMatrix = connectivityMatrix(~idx,:);
%eliminateRedundantEdges(connectivityMatrixFromTV, whiskerTree, sNum,[0.9 1 1]);
%eliminateRedundantEdges(newConnectivityMatrix, whiskerTree, sNum,[0.9 0 0]);