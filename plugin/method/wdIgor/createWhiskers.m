
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function detectedWhiskers = createWhiskers(...,
    whiskerTreeLUTwithOrientations, contourLUT, furConnectivityMatrix, ...
    connectivityMatrix, nodesListWithHistAndMarker, GRAYshape, strokesData,...
    flagPlot, minimalLength, minimalDistance)


% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function createExtendedStrokes
% Inputs:
%           -whiskerTreeLUTwithOrientations is a matrix, where the data are
%           [inCurve inMarker ::::: xofmarker, yofmarker, c, theta,
%           rho,angle, roundness, saliency]](for ORIENTED RADIAL DISTANCE in createWhiskerTree
%           - contourLUT are the point of the contur data with
%           whiskerTreeLUTwithOrientations data format
%           - furConnectivityMatrix is an [inCurve inMarker outCurve outMarker 1]
%           - connectivityMatrix  is an [inCurve inMarker outCurve
%           outMarker 1]
%           - nodesListWithHistAndMarker is a list with
%               [inCurve, inmarker, histogramOFedges, whiskerId]
%           - GRAYshape is the input image
%           - strokesData is a matrix with the following elements
%                .. [inCurve inMarker outCurve outMarker]
%           - flagPlot is a binary flag for deciding if to plot or not
%           - minimalLength is the minimal lenght of whiskers
%           - minimalDistance checks if the max distance exceeds the
%           threshold
%
% Outputs:
%           - detectedWhiskers is a cell data array containing xy values of
%           single whiskers
%
% 1. For all points we create the edge matrix
% 2. We mark the sink nodes in contourLUT
% 3. For every stroke we calculate its polar values, we apply the fnxtr
% function and we mark the source node
% <\outDOC>

%% 1. create edge matrix
if nargin < 8,
    flagPlot = false;
end
if nargin < 9,
    minimalLenght = 5;
end
if nargin < 10,
    minimalDistance = 0;
end
whiskerTreeLUTwithOrientations = [whiskerTreeLUTwithOrientations; contourLUT];

[val, idxS] = intersect(whiskerTreeLUTwithOrientations, contourLUT,'rows');
idxSinks = zeros(size(whiskerTreeLUTwithOrientations,1),1);
idxSinks(idxS) = 1;

completeConnectivityMatrix = eliminateRedundantEdges([furConnectivityMatrix; ...
    connectivityMatrix], whiskerTreeLUTwithOrientations, flagPlot*0,[1 0.4 0]);

%eliminateRedundantEdges(connectivityMatrixFromTV, whiskerTreeLUTwithOrientations,flagPlot,[0 0.4 1]);

M = size(whiskerTreeLUTwithOrientations,1);
nodeLUT = zeros(M);
edgeMatrix = zeros(M);

for i=1:M,
    nodeLUT(whiskerTreeLUTwithOrientations(i,1)+1, whiskerTreeLUTwithOrientations(i,2)+1) = i;
end
for i=1:size(completeConnectivityMatrix,1),
    edgeMatrix(nodeLUT(completeConnectivityMatrix(i,1)+1, completeConnectivityMatrix(i,2)+1), nodeLUT(completeConnectivityMatrix(i,3)+1, completeConnectivityMatrix(i,4)+1)) = 1;
end


%% 2. sinkNodes

[c,idx,idxb] = intersect(nodesListWithHistAndMarker(:,1:2), whiskerTreeLUTwithOrientations(:,1:2),'rows');
strokeIdx = zeros(size(whiskerTreeLUTwithOrientations,1),1);
strokeIdx(idxb) = nodesListWithHistAndMarker(idx, 4);

if  flagPlot
    figure(2)
    imshow(uint8(255*GRAYshape))
    hold on
    eliminateRedundantEdges(completeConnectivityMatrix, whiskerTreeLUTwithOrientations,2,[0 0.4 1]);
    hold on
end
thetaT = 0.3;
%% 3. find multiple paths

testWhiskers = cell(max(strokeIdx),1);
for strokeId = 1:max(strokeIdx),%55,13
    idx1 = find(strokeIdx == strokeId);
    orderedWLUT = whiskerTreeLUTwithOrientations(idx1,:);
    [orderedWLUT,iib] = sortrows(orderedWLUT,[7 6]);
    rhoa = orderedWLUT(:,7)';
    if  flagPlot
        plot(orderedWLUT(:,4), orderedWLUT(:,3),'color',[0 0 0],'linewidth',2)
        plot(orderedWLUT(:,4), orderedWLUT(:,3),'m+')
    end
    if numel(rhoa) > minimalLength,
        % changed 14.05.2010
        %         idxA = (whiskerTreeLUTwithOrientations(:,1) == strokesData(strokeId,1)) & (whiskerTreeLUTwithOrientations(:,2) == strokesData(strokeId,2));
        %         idxB = (whiskerTreeLUTwithOrientations(:,1) == strokesData(strokeId,3)) & (whiskerTreeLUTwithOrientations(:,2) == strokesData(strokeId,4));
        %         if whiskerTreeLUTwithOrientations(idxA,7) < whiskerTreeLUTwithOrientations(idxB,7),
        %             sourceNodeId = find(idxA);
        %             rhoEnd = whiskerTreeLUTwithOrientations(idxB,7);
        %         else
        %             sourceNodeId = find(idxB);
        %             rhoEnd = whiskerTreeLUTwithOrientations(idxA,7);
        %         end
        %    [val, pos] = max(rhoa);
        %    idx2 = find(idx1);
        %    sourceNodeId = (idx2(pos));
        %
        % changed 14.05.2010 FILTER OUT LAST STROKE POINT
        % FOR END POINT CORRECTION
        thetaa = orderedWLUT(2:end,6)';
        xa = orderedWLUT(2:end,3)';
        ya = orderedWLUT(2:end,4)';
        rhoa = rhoa(2:end);
        idx1(iib(1)) = nan;
        idx1 = idx1(~isnan(idx1));
        sourceNodeId = find((whiskerTreeLUTwithOrientations(:,3) == xa(1)) &...
            (whiskerTreeLUTwithOrientations(:,4) == ya(1)));
        
        
        if numel(rhoa) > 5
            sp3a = fn2fm(spap2(1,  min(numel(rhoa), 3)+1, rhoa, thetaa),'B-');
        else
            sp3a = fn2fm(spap2(1,  3, rhoa, thetaa),'B-');
        end
        % filter nodes out of reach
        interpolatedTheta = fnval(fnxtr(sp3a), whiskerTreeLUTwithOrientations(:,7));
        idxPr = abs(interpolatedTheta -  whiskerTreeLUTwithOrientations(:,6)) > thetaT;
        newEdgeMatrix = edgeMatrix;
        newEdgeMatrix(idxPr,:) = 0;
        newEdgeMatrix(:,idxPr) = 0;
        [i,j] = find(newEdgeMatrix > 0);
        for k=1:numel(i),
            inTheta = whiskerTreeLUTwithOrientations(i(k),6);
            inRho = whiskerTreeLUTwithOrientations(i(k),7);
            inPredictedTheta = interpolatedTheta(i(k));
            outTheta = whiskerTreeLUTwithOrientations(j(k),6);
            outRho = whiskerTreeLUTwithOrientations(j(k),7);
            outPredictedTheta = interpolatedTheta(j(k));
            newEdgeMatrix(i(k),j(k)) = ...
                max(abs((inPredictedTheta-outPredictedTheta) - (inTheta - outTheta)),0.00001);
            
        end
        
        
        % go on newtwork optimization
        G = sparse(newEdgeMatrix);
        sinksInGeneralMatrix = find(double(~idxPr).*idxSinks);
        
        exploredDists = [];
        exploredPaths = cell(1,1000);
        exploredIdx = 0;
        
        [dist, path] = graphshortestpath(G, sourceNodeId);
        % extract paths of endNodes and store them
        for k = sinksInGeneralMatrix',
            extractedPath = path{k};
            if ~isempty(extractedPath)
                exploredIdx = exploredIdx + 1;
                exploredPaths{exploredIdx} = unique([extractedPath, idx1']);
                exploredDists(exploredIdx) = dist(k);
            end
        end
        
        %% calculate area difference between prediction and path
        if exploredIdx>0,
            M = exploredIdx;
            thetaDistances = zeros(1,M);
            exploredDists = thetaDistances;
            L = zeros(1,M);
            j = 0;
            t = 0;
            discreteT = zeros(1,M);
            ss = 0;
            for k=1:M,
                %% calculate geometric and curvature distance
                idx = exploredPaths{k};
                %idx = [idx, idx1']; % add stroke data
                rhoPathPositions = whiskerTreeLUTwithOrientations(idx,7);
                %rhoPathPositions = [rhoa'; rhoPathPositions];
                [rhoPathPositions, ib] = sort(rhoPathPositions);
                exploredPaths{k} = idx(ib); % sort also path
                thetaPathPositions = whiskerTreeLUTwithOrientations(idx,6);
                %thetaPathPositions = [thetaa'; thetaPathPositions];
                xPathPositions = whiskerTreeLUTwithOrientations(idx,4);
                yPathPositions = whiskerTreeLUTwithOrientations(idx,3);
                %xPathPositions = [xa'; xPathPositions];
                %yPathPositions = [ya'; yPathPositions];
                thetaPathPositions = thetaPathPositions(ib);
                xPathPositions = xPathPositions(ib);
                yPathPositions = yPathPositions(ib);
                %spb = spap2(min(3,ceil(numel(rhoPathPositions)/12)),  min(numel(rhoPathPositions), 3)+1, ...
                %    rhoPathPositions, [ xPathPositions'; yPathPositions']);
                %spb = (spap2(1,  min(numel(rhoPathPositions), 3)+1, ...
                %    rhoPathPositions, [ xPathPositions'; yPathPositions']));
                %[kappa, arclenghts] = calculateCurvature(spb, linspace(spb.knots(1),spb.knots(end),50));
                interpolatedPredictionPosition = fnval(fnxtr(sp3a,3), rhoPathPositions);
                j = j + 1;
                thetaDiff = (abs(thetaPathPositions - interpolatedPredictionPosition));
                % trapezoidal rule normlized by total lenght
                L(j) = sum(diff(rhoPathPositions));
                thetaDistances(j) = 1/L(j) * (sum( ((thetaDiff(1:end-1)+thetaDiff(2:end))/2) .* diff(rhoPathPositions)));
                % curvature difference rule
                exploredDists(j) = sum(abs(diff(diff(interpolatedPredictionPosition)) - diff(diff(thetaPathPositions))));
                %plot(rhoPositions, thetaPositions, 'g+'), hold on
                %t(j) = sum(abs(kappa));
                discreteT(j) = 1/L(j) * (sum(abs(diff(thetaDiff)) .* diff(rhoPathPositions)));
            end
            %% calculate shape difference
            alphaW = 0.5; beta = 2;
            
            sinkDistances = alphaW * exploredDists + (1-alphaW) * thetaDistances + beta * discreteT;
            [sortedSinkDistances, pos] = sort(sinkDistances);
            
            
            % sortedSinkDistances = sinkDistances(pos);
            
            %%
            %             figure
            %             plot(rhoa,thetaa,'rs')
            %             hold on
            %             idx = exploredPaths{pos(1)};
            %             plot(whiskerTreeLUTwithOrientations(idx,7),...
            %                 whiskerTreeLUTwithOrientations(idx,6),'cx');
            %             fnplt(sp3a,'m')
            %%
            idx = exploredPaths{pos(1)};
            %idx = [fliplr(idx), idx1']; % add stroke data
            if  flagPlot
                plot(ya, xa, 'mx')
                plot(whiskerTreeLUTwithOrientations(idx,4),...
                    whiskerTreeLUTwithOrientations(idx,3),'g');
            end
            testWhiskers{strokeId} = idx;
        end
    end
end



%% 1. intersect path an root nodes and leave only one root node
for i=1:numel(testWhiskers),
    for j=(i+1):numel(testWhiskers),
        whiskA = testWhiskers{i};
        whiskB = testWhiskers{j};
        [c, ia, ib] = intersect(whiskA, whiskB, 'legacy'); % add 'legacy' to eliminate change of behaviour at or before R2016b
        % if there is less than element difference
        if ~isempty(c)
            diffA = abs(diff([1, sort(ia), numel(whiskA)]));
            diffB =  abs(diff([1, sort(ib), numel(whiskB)]));
            if max(diffA)<=6,
                testWhiskers{i} = [];
            elseif max(diffB)<=6,
                testWhiskers{j} = [];
            end
        end
    end
end

goodWhiskers = false(1,numel(testWhiskers));
for i=1:numel(testWhiskers),
    if ~isempty(testWhiskers{i})
        
        goodWhiskers(i) = 1;
    end
end
goodWhiskers = testWhiskers(goodWhiskers);
detectedWhiskers = cell(size(goodWhiskers));
for i=1:numel(goodWhiskers),
    idx = goodWhiskers{i};
    %% OLD OUTPUT
    %     detectedWhiskers{i} = [whiskerTreeLUTwithOrientations(idx,4),...
    %         whiskerTreeLUTwithOrientations(idx,3)];
    if sum(idx) > 5
        sp3a = spap2(1,  3,whiskerTreeLUTwithOrientations(idx,7),...
            [whiskerTreeLUTwithOrientations(idx,3)'; whiskerTreeLUTwithOrientations(idx,4)']);    
    else
        sp3a = spap2(1,  2,whiskerTreeLUTwithOrientations(idx,7),...
            [whiskerTreeLUTwithOrientations(idx,3)'; whiskerTreeLUTwithOrientations(idx,4)']);
        
    end
    detectedWhiskers{i} = fnval(sp3a, whiskerTreeLUTwithOrientations(idx,7));
    if flagPlot
        plot(whiskerTreeLUTwithOrientations(idx,4),...
            whiskerTreeLUTwithOrientations(idx,3),'g');
        idx;
    end
end

%% final smoothing



% 2. add to path the points
% 3. zakaj potrebujemo 3 kose v splinu?
% uporabi strokes data za zacetni koscek

if 0
    %%
    for exploredIdx=1:numel(sinkDistances),
        figure(3),
        imshow(GRAYshape ),
        hold on ,
        idxA = [fliplr(exploredPaths{exploredIdx})]; % add stroke data
        plot(whiskerTreeLUTwithOrientations(idxA,4),...
            whiskerTreeLUTwithOrientations(idxA,3),'g');
        pause;
    end
end



