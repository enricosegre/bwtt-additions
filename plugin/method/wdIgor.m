

%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% FAIR USE
%
% Citation requirement. This method has been published and
% you should cite the publication in your own work. See the
% URL below for more information:
%
% http://bwtt.sourceforge.net/fairuse



% this is a stub that wraps the directory underneath, where
% the implementation is. you can equally well place the
% implementation in this file.

function output = wdIgor(operation, job, input)

switch operation
	
	case 'info'
		output.author = 'Igor Perkon';
		output.citation = 'citation';
	
	case 'parameters'
		output.pars = wdIgorParameters();
		
	case 'initialize'
		output = wdIgorInitialize(job, input);
		
	case 'process'
		output = wdIgorProcess(job, input);
		
	case 'plot'
		output.h = [];
		results = job.getResults(input.frameIndex);
		if isfield(results{1}, 'whisker')
			whisker = results{1}.whisker;
			col = 'wrgbmyc';
			h = [];
			for w = 1:length(whisker.contour)
				cn = whisker.contour{w};
				h(end+1) = plot(input.h_axis, cn(2, :), cn(1, :), [col(mod(w, length(col)) + 1) '-'], 'linewidth', 1);
			end
			output.h = h;
		end
		
	otherwise
		output = [];
		
end



