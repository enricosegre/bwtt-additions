
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


% this is a BIOTACT Whisker Tracking Tool "tool plugin".

% original author: Igor Perkon
% rebuilt: ben mitch 2012

function data = stCoolization(operation, proj, data)

switch operation
	
	case 'info'
		data = [];
		data.author = 'Igor Perkon';
		
	case 'parameters'
		
		pars = {};
		
		% constants
		pars{end+1} = struct('name', 'minimumObjectPixels', 'value', 6000);
		pars{end+1} = struct('name', 'se_size', 'value', 10);
		pars{end+1} = struct('name', 'histnum', 'value', 32);
		pars{end+1} = struct('name', 'histT', 'value', 100);
		pars{end+1} = struct('name', 'Tdist', 'value', 10); % pixel perpendicular distance
		pars{end+1} = struct('name', 'ROI', 'value', []); % region of interest (initially, not set)
		
		% parameter
		par = [];
		par.name = 'removeBg';
		par.label = 'Remove Background';
		par.help = 'Usually, you will want to remove the background (non-moving objects) from the image before processing. If this isn''t working, for some reason (e.g. because your animal is always in shot), you can disable it here.';
		par.type = 'flag';
		par.value = true;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'thresh';
		par.label = 'Black/White threshold (%)';
		par.help = 'The threshold to use for distinguishing the animal silhouette from the back-lit area, for finding the outline of the animal''s snout.';
		par.type = 'scalar';
		par.range = [0 100];
		par.precision = 0;
		par.step = [1 10];
		par.value = 10;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'maxDistance';
		par.label = 'Template to Snout Error Distance';
		par.type = 'scalar';
		par.range = [10 50];
		par.value = 20;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'minHCGF';
		par.label = 'Minimum goodness of fit (percentage)';
		par.type = 'scalar';
		par.range = [50 100];
		par.value = 80;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'frameStep';
		par.label = 'Frame step (between attempts to Coolize)';
		par.type = 'scalar';
		par.range = [10 100];
		par.value = 50;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'debug';
		par.label = 'Show debug information';
		par.type = 'flag';
		par.value = false;
		pars{end+1} = par;
		
		data.pars = pars;
		
	case 'process'
		
		% get jobs
		jobs = proj.getSelectedJobs();
		if isempty(jobs)
			stMessage('No jobs are selected - Coolization cancelled.');
			return
		end
		
		% validate
		for j = 1:length(jobs)
			job = jobs{j};
			[state, errors, pipelines] = job.getCurrentTaskState();
			if state ~= stJob.TASK_CREATED
				error('All jobs selected for Coolization must be in the TASK_CREATED state');
			end
			checked = false;
			for p = 1:length(pipelines)
				pipeline = pipelines{p};
				if strcmp(pipeline.name, 'snout')
					checked = true;
					m1 = any(ismember(pipeline.methods, 'sdGeneric'));
					m2 = any(ismember(pipeline.methods, 'stShapeSpaceKalman'));
					if ~all([m1 m2])
						error('All jobs selected for Coolization must be set to use sdGeneric and stShapeSpaceKalman');
					end
					if length(pipeline.methods) ~= 2
						error('All jobs selected for Coolization must have no pre-processing methods selected in the "snout" pipeline. This could be relaxed in future, but needs a bit of thought.');
					end
				end
			end
			if ~checked
				error('All jobs selected for Coolization must have a "snout" pipeline');
			end
		end
		
		% report
		proj.log('Coolization beginning...');
		
		% extract
		pars = data.pars;
		
		% 		warning('not getting ROI')
		% get ROI
		frame = jobs{1}.getVideoFrames(1, 'a');
		result = stGetROI(frame, pars.ROI);
		if isscalar(result)
			stMessage('Coolization cancelled by user.');
			return
		else
			pars.ROI = result;
		end
		
		% collect errors
		errors = {};
		
		% create GUI
		gui = stGUI([640 640], gcbf, 'Coolization GUI', 'resizable', 'on', 'style', 'modal');
		% 		warning('non modal dialog');
		
		% failsafe loop
		try
			
			% layout
			h_dialog = gui.getMainWindow();
			panel_root = gui.rootPanel;
			panel_buttons = panel_root.pack([], 'bottom', 24);
			panel_main = panel_root;
			
			% layout and buttons
			h_cancel = createToggleButton('Cancel');
			panel_buttons.pack(h_cancel, 'right', 80);
			% 			h_buttons(2) = createButton('OK', @callback_OK);
			% 			panel_buttons.pack(h_buttons(2), 'right', 80);
			% 			h_buttons(3) = createButton('Change ROI', @callback_Change);
			% 			panel_buttons.pack(h_buttons(3), 'right', 80);
			
			% text
			h_text = uicontrol(h_dialog, ...
				'style', 'text', ...
				'Units', 'pixels', ...
				'String', 'Information', ...
				'Hori', 'left', ...
				'FontName', guipars.fontName, ...
				'FontSize', guipars.fontSize ...
				);
			panel_buttons.pack(h_text, 'right', -1);
			
			% create axes
			h_axis = [];
			for row = 1:2
				panel_row = panel_root.pack([], 'top', -1);
				for col = 1:2
					h_axis(row, col) = axes('parent', h_dialog, 'xtick', [], 'ytick', []);
					panel_row.pack(h_axis(row, col), 'left', -1);
				end
			end
			
			% create handles
			% 			h_overlay = [];
			
			% 			% load one movie frame
			% 			frameRaw = jobs{1}.getVideoFrames(1, 'a');
			%
			% 			% confirm/retrieve ROI
			% 			while 1
			%
			% 				% It is recommended to run ROI on one movie, select
			% 				% the area where the animal enters (so you mask out
			% 				% moving poles and platforms), and save it for all
			% 				% the other movies.
			%
			% 				% display it
			% 				[h_image, h_overlay] = displayImage(h_axis, h_image, h_overlay, frameRaw, pars.ROI);
			%
			% 				% uiwait
			% 				response = 'closed dialog';
			% 				uiwait(h_dialog);
			%
			% 				% check response
			% 				switch response
			% 					case {'closed dialog' 'cancel'}
			% 						error('Coolization was cancelled by user.');
			% 					case {'ok'}
			% 						break
			% 					case {'change'}
			% 						% ok to continue
			% 				end
			%
			% 				% prompt user for new ROI
			% 				set(h_buttons, 'enable', 'off');
			% % 				stMessage(['You should now choose the Region Of Interest (ROI). Coolization will only '...
			% % 					'look for the animal in that region.' 10 10 'Left click to add vertices, right click to close, ' ...
			% % 					'backspace to start again, or double click to accept.']);
			% 				while 1
			% 					pars.ROI = roipoly;
			% 					if ~isempty(pars.ROI)
			% 						break
			% 					end
			% 				end
			% 				set(h_buttons, 'enable', 'on');
			%
			% 			end
			
			% get scale and orientation
			scaleAndOrientation = [];
			for j = 1:length(jobs)
				job = jobs{j};
				sskpars = job.getParameters('snout', 'stShapeSpaceKalman');
				if isfield(sskpars, 'noseTip') && all(~isnan([sskpars.noseTip sskpars.snoutCenter]))
					% derive scale/orientation from SSK pars
					diff = sskpars.snoutCenter - sskpars.noseTip;
					[theta, r] = cart2pol(diff(1), diff(2));
					X3 = r / 80 * (cos(theta + pi/2)) - 1;
					X4 = r / 80 * (sin(theta + pi/2));
					scaleAndOrientation = [X3 X4]';
					break
				end
			end
			
			% check
			if isempty(scaleAndOrientation)
				error(['At least one of the selected jobs must have its stShapeSpaceKalman parameters ' ...
					'(specifically, nose tip and snout center locations) set for Coolization to proceed']);
			end
			
			% not cancelled
			cancel = false;
			
			% not coolized
			coolized = false;
			
			% create filter
			se = strel('disk', pars.se_size);
			
			% debug (speed)
			% 			warning('modified se_size')
			% 			se = strel('disk', 5);
			
			% for each job
			for j = 1:length(jobs)
				
				% get job
				job = jobs{j};
				
				% push job state
				job.stackPush();
				
				% protect job state from changes and collect errors
				% rather than raising them immediately
				try
					
					% clear axes
					for i = 1:4
						cla(h_axis(i));
					end
					
					% report
					basename = stProcessPath('justBasename', job.getVideoFilename());
					set(h_text, 'string', sprintf('Preparing to process %s...', basename));
					drawnow
					
					% extract
					frameRange = job.getFrameRange();
					frameIndices = frameRange.first : pars.frameStep : frameRange.last;
					frameRaw = job.getVideoFrames(frameRange.first, 'a');
					
					% 					[h_image, h_overlay] = displayImage(h_axis, h_image, h_overlay, frameRaw, pars.ROI);
					% 					drawnow
					
					% do background extraction
					if pars.removeBg
						frameBg = stGetBackgroundImage(job);
					end
					
					% start task and get initdata
					job.taskStart();
					initdata = job.getInitializeData('forward');
					
					% for each (skipped) frame
					for fi = 1:length(frameIndices)
						
						% cancel
						if get(h_cancel, 'value')
							break
						end
						
						% get index of frame to process
						frameIndex = frameIndices(fi);
						initdata.frameIndices = frameIndex;
						
						% get frame data
						frameRaw = job.getVideoFrames(frameIndex, 'a');
						
						% display frame
						plotImage(h_axis(1, 1), applyROI(applyNormalise(frameRaw), pars.ROI));
						
						% remove background
						if pars.removeBg
							frameFg = (frameBg - frameRaw);
						else
							frameFg = (max(frameRaw(:)) - frameRaw);
						end
						
						% display frame
						plotImage(h_axis(1, 2), frameFg);
						
						% find objects (animals) in image
						frameBW = imopen(im2bw(frameFg, pars.thresh / 100), se);
						if isempty(pars.ROI)
							frameBW = frameBW;
						else
							frameBW = immultiply(frameBW, pars.ROI);
						end
						
						% display frame
						plotImage(h_axis(2, 1), frameBW);
						
						% plot boundaries of objects
						bounds = bwboundaries(frameBW);
						for b = 1:length(bounds)
							bound = bounds{b};
							plot(h_axis(1, 2), bound(:, 2), bound(:, 1), 'r-', 'linewidth', 2);
						end
						
						% count pixels in found objects
						numberObjectPixels = sum(frameBW(:));
						
						% display frame
						plotImage(h_axis(2, 2), applyROI(applyNormalise(frameRaw), pars.ROI));
						
						% if enough pixels found, we'll have a go
						if numberObjectPixels >= pars.minimumObjectPixels
							
							% ok, enough pixels, good to go
							
							% test fitting
							frameBW = imdilate(uint8(frameBW), se);
							GRAYshape = immultiply(frameRaw, frameBW);
							[counts, x] = imhist(GRAYshape, pars.histnum);
							for i = pars.histnum:-1:1
								if counts(i) > pars.histT
									GRAYshape(frameBW == 0) = x(i);
									break
								end
							end
							
							% prepare plotdata
							plotdata = [];
							plotdata.h_axis = h_axis(2, 2);
							plotdata.frameIndex = frameIndex;
							
							% run sdGeneric
							sdState = job.methodInitialize('snout', 'sdGeneric', initdata);
							job.methodProcess('snout', 'sdGeneric', sdState, frameIndex);
							
							% initialize stShapeSpaceKalman
							stState = job.methodInitialize('snout', 'stShapeSpaceKalman', initdata);
							
							% prepare the parameters for automatic init
							stats = regionprops(frameBW, 'area');
							[val, pos] = max(struct2array(stats));
							stats = regionprops(frameBW, 'centroid');
							stState.pars.scale = scaleAndOrientation(1);
							stState.pars.orientation = scaleAndOrientation(2);
							stState.pars.centroid = stats(pos).Centroid;
							
							% force automatic initialization, even if
							% already set
							stState.pars.automaticInitialize = true; % turn on auto init
							stState.pars.maxDistance = pars.maxDistance; % override this parameter
							stState.pars.noseTip = [NaN NaN]; % force search
							stState.pars.snoutCenter = [NaN NaN]; % force search
							
							% do initialization
							stState = job.methodProcess('snout', 'stShapeSpaceKalman', stState, frameIndex);
							
							% measure HCGF
							HCGF = round(sum(abs(stState.perpdists) < pars.Tdist) / numel(stState.perpdists) * 100);
							
							% recover
							results = job.getResults(frameIndex, 'latest');
							result = results{1};
							recoveredNoseTip = fliplr(round(result.snout.noseTip));
							recoveredSnoutCenter = fliplr(round(result.snout.center));
							
							% plot overlays
							output = job.methodPlot('snout', 'sdGeneric', plotdata);
							% 							if isfield(output, 'h')
							% 								h_overlay = [h_overlay output.h];
							% 							end
							output = job.methodPlot('snout', 'stShapeSpaceKalman', plotdata);
							% 							if isfield(output, 'h')
							% 								h_overlay = [h_overlay output.h];
							% 							end
							h = plot(h_axis(2, 2), recoveredNoseTip(1), recoveredNoseTip(2), 'rx');
							% 							h_overlay = [h_overlay h];
							h = plot(h_axis(2, 2), recoveredSnoutCenter(1), recoveredSnoutCenter(2), 'yo');
							% 							h_overlay = [h_overlay h];
							
							% check for success
							if HCGF >= pars.minHCGF
								
								% ok, enough HCGF, good to go
								proj.log(['Coolization of "' basename '" SUCCESS']);
								
								% success
								coolized = true;
								
								% parametrize job
								sskpars.noseTip = recoveredNoseTip;
								sskpars.snoutCenter = recoveredSnoutCenter;
								job.setParameters('snout', 'stShapeSpaceKalman', sskpars);
								job.stackPop(true);
								job.stackPush();
								
								% and set key frame
								frameRange.initial = frameIndex;
								job.setFrameRange(frameRange);
								
								% and set "parametrized" flag
								job.setUserData('parametrized', true);
								
							else
								
								% no success
								coolized = false;
								
							end
							
						else
							
							% no goodness of fit measure
							HCGF = [];
							
							% no success
							coolized = false;
							
						end
						
						% report
						report = [basename ' F' int2str(frameIndex) ' PixelCount=' int2str(numberObjectPixels)];
						if isempty(HCGF)
							report = [report ' (FAIL)'];
						else
							report = [report ' HCGF=' int2str(HCGF) '%'];
							if coolized
								report = [report ' (SUCCESS)'];
							else
								report = [report ' (FAIL)'];
							end
						end
						if pars.debug
							proj.log(report);
						end
						set(h_text, 'string', report);
						
						% do events
						drawnow
						
						% continue?
						if ~coolized
							continue
						end
						
						% else, we're done
						break
						
					end
					
					if ~coolized
						proj.log(['Coolization of "' basename '" FAILED']);
					end
					
				catch err
					job.stackPop();
					proj.log(['ERROR: ' err.message]);
					errors{end+1} = err;
				end
				
				% pop job
				job.stackPop();
				
				% waitbar
				if get(h_cancel, 'value')
					stMessage('Coolization cancelled by user.');
					break
				end
				
			end
			
		catch err
			try
				gui.close();
			end
			rethrow(err);
		end
		
		% close GUI
		try
			gui.close();
		end
		
		% report
		if length(errors)
			proj.log('Coolization complete (SOME ERRORS OCCURRED - first is displayed).');
			stError(errors{1});
		else
			proj.log('Coolization complete.');
		end
		
		% ok
		data = [];
		data.pars = pars;
		
	case 'plot'
		data = [];
		
	otherwise
		data = [];
		
end



%% IMAGE FUNCTIONS

	function im = applyROI(im, ROI)
		
		% convert uint8 to RGB
		if ndims(im) < 3
			im = repmat(im, [1 1 3]);
		end
		
		if ~isempty(ROI)
			for d = 1
				chan = im(:, :, d);
				chan(~ROI) = chan(~ROI) / 4 + 192;
				im(:, :, d) = chan;
			end
		end
		
	end

	function im = applyNormalise(im)
		
		if stUserData('normaliseFrame')
			im = stNormaliseFrame(im);
		end
		
	end

	function plotImage(h_axis, im)
		
		% convert logical to uint8
		if islogical(im)
			im = uint8(im) * 255;
		end
		
		% convert uint8 to RGB
		if ndims(im) < 3
			im = repmat(im, [1 1 3]);
		end
		
		% show
		cla(h_axis);
		image(im, 'parent', h_axis);
		set(h_axis, 'xtick', [], 'ytick', []);
		hold(h_axis, 'on');
		axis(h_axis, 'image');
		
	end

% 	function [h_image, h_overlay] = displayImage(h_axis, h_image, h_overlay, frameRaw, ROI)
%
% 		% sort h_overlay
% 		delete(h_overlay);
% 		h_overlay = [];
%
% 		% apply ROI
% 		im2 = applyROI(im, pars.ROI);
%
% 		% if image exists, and is concommitant, just reset the cdata
% 		if ~isempty(h_image)
% 			set(h_image, 'cdata', im2);
% 		else
% 			% create image
% 			hold(h_axis, 'off');
% 			h_image = image(im2, 'parent', h_axis);
% 			set(h_axis, 'xtick', [], 'ytick', []);
% 			hold(h_axis, 'on');
% 			axis(h_axis, 'image');
% 		end
%
% 	end



%% COMPONENT

	function h = createButton(text, callback, tooltip)
		
		guipars = stGUIPars();
		
		if nargin < 3
			tooltip = '';
		end
		
		h = uicontrol(h_dialog, ...
			'Style', 'Pushbutton', ...
			'Units', 'pixels', ...
			'String', text, ...
			'Tooltip', tooltip, ...
			'Callback', callback, ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize ...
			);
		
	end

	function h = createToggleButton(text)
		
		guipars = stGUIPars();
		
		h = uicontrol(h_dialog, ...
			'Style', 'Togglebutton', ...
			'Units', 'pixels', ...
			'String', text, ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize ...
			);
		
	end



%% CALLBACK

% 	function callback_Cancel(obj, evt)
%
% 		userPressedCancel = true;
% % 		uiresume(gcbf);
%
% 	end

% 	function callback_OK(obj, evt)
%
% 		response = 'ok';
% 		uiresume(gcbf);
%
% 	end
%
% 	function callback_Change(obj, evt)
%
% 		response = 'change';
% 		uiresume(gcbf);
%
% 	end



end




%% THE REMAINDER IS CODE THAT MAY BE NEEDED IF THE ABOVE
%% CODE IS INCORRECT


%
% % 				% get pp methods
% % 				[state, errors, pipelines] = job.getCurrentTaskState();
% % 				ppMethods = {};
% % 				for p = 1:length(pipelines)
% % 					pipeline = pipelines{p};
% % 					if strcmp(pipeline.name, 'snout')
% % 						for m = 1:length(pipeline.methods)
% % 							method = pipeline.methods{m};
% % 							if strcmp(method(1:2), 'pp')
% % 								ppMethods{end+1} = method;
% % 							end
% % 						end
% % 					end
% % 				end
%
% 			% safe loop
% 			try
%
% % 							% start task and get initdata
% % 							job.taskStart();
% % 							initdata = job.getInitializeData('forward');
% % 							initdata.frameIndices = frameIndex;
%
% % 							% run pp methods
% % 							for m = 1:length(ppMethods)
% % 								method = ppMethods{m};
% % 								ppState = job.methodInitialize('snout', method, initdata);
% % 								job.methodProcess('snout', method, ppState, frameIndex);
% % 							end
%
% % 							% get runtime frame
% % 							imageBuffer = job.getRuntimeFrame(frameIndex, 'snout');
% % 							imageBuffer = imageBuffer{1};
%
% % 						% clean up
% % 						delete(h_overlay);
% % 						h_overlay = [];
%
%
% 					% pop
% 					job.stackPop();
%
%
%
% % 								snoutCenter = stNextChunkData.X(1:2)';
% % 								stParams.Variable{3}.value = round(snoutCenter);
% %
% % 								if pars.debug
% % 									disp(['Cool @ frame ' int2str(frameIndex)]);
% % 									disp(snoutCenter)
% %
% % 									% print figure
% % 									set(gui.h_fig, ...
% % 										'PaperOrientation', 'portrait', ...
% % 										'PaperUnits', 'centimeters', ...
% % 										'PaperPosition', [0 0 20 30]);
% % 									print(gui.h_fig, ...
% % 										strcat(pars.videoFilename, '-cool.png'), '-dpng');
% %
% % 									% save data
% % 					% 						save(strcat(pars.videoFilename, '-cool.mat'), 'imageBuffer', 'snoutTrackingData', 'snoutContour');
% %
% % 								end
% %
% % 								% invert the transform to get back to nose tip
% % 								% IGOR: smarter way of doing this?
% % 								X1 = stNextChunkData.X(1);
% % 								X2 = stNextChunkData.X(2);
% % 								X3 = stNextChunkData.X(3);
% % 								X4 = stNextChunkData.X(4);
% %
% % 								X3 = X3 + 1;
% % 								theta = atan2(X4, X3) - pi/2;
% % 								r = X3 / cos(theta + pi/2) * 80;
% % 								[dx, dy] = pol2cart(theta, r);
% % 								noseTip = round(snoutCenter - [dx dy]);
%
%
% % 							% initialise sdGeneric
% % 							if ~exist('sdParams', 'var')
% %
% % 								[sdParams, sdPrevChunkData] = ...
% % 									sdGenericInitialize(pars.videoFilename, frameIndex);
% %
% % 							end
% %
% % 							% run sdGeneric
% % 							[snoutContour, sdNextChunkData] = sdGeneric(GRAYshape, sdParams, sdPrevChunkData);
% % 							sdPrevChunkData = sdNextChunkData;
% %
% % 							% initialise stShapeSpaceKalman
% % 							if ~exist('stParams', 'var')
% %
% % 								[stParams, stPrevChunkData] = ...
% % 									stShapeSpaceKalmanInitialize(pars.videoFilename, frameIndex);
% %
% % 								% insert maxDistance
% % 								stParams.Variable{1}.value = pars.maxDistance;
% %
% % 							end
% %
% % 							% update SSK pars
% % 							stParams.Constant{6}.value = true; % enable automatic initialization
% % 							stParams.Constant{7}.value = pars.scaleAndOrientation(1); % scale
% % 							stParams.Constant{8}.value = pars.scaleAndOrientation(2); % orientation
% %
% % 							% do some stuff - what?
% % 							stats = regionprops(frameBW, 'area');
% % 							[val, pos] = max(struct2array(stats));
% % 							stats = regionprops(frameBW, 'centroid');
% % 							stParams.Constant{16}.value = stats(pos).Centroid;
% %
% % 							% run SSK
% % 							[snoutTrackingData, stNextChunkData, optionalOutputs] = ...
% % 								stShapeSpaceKalman(snoutContour, stParams, stPrevChunkData);
% %
% % 							% calculate goodness of fit
% % 							HCGF = sum(abs(optionalOutputs) < pars.Tdist) / numel(optionalOutputs);
% %
% % 							% reselect figure
% % 							drawnow
% % 							if ~ishandle(gui.h_fig)
% % 								error('Coolization GUI was closed - Coolization abandoned');
% % 							end
% % 							figure(gui.h_fig)
% %
% % 							% GUI feedback
% % 							if pars.debug
% %
% % 								% DEBUG MODE
% % 								clf
% % 								for s = 1:6
% %
% % 									% subplot
% % 									subplot(3, 2, s)
% %
% % 									% image
% % 									switch s
% % 										case 1
% % 											% background image
% % 											image(repmat(frameBg, [1 1 3]));
% % 											title('frameBg');
% % 										case 2
% % 											% raw video frame
% % 											image(repmat(imageBuffer, [1 1 3]));
% % 											title('imageBuffer');
% % 										case 3
% % 											% frameBg minus video frame
% % 											image(repmat(frameFg, [1 1 3]));
% % 											title('frameFg');
% % 										case 4
% % 											image(repmat(frameBW, [1 1 3]));
% % 											title('frameBW');
% % 										case 5
% % 											image(repmat(frameBW, [1 1 3]));
% % 											title('frameBW');
% % 										case 6
% % 											image(repmat(GRAYshape, [1 1 3]));
% % 											title('GRAYshape');
% % 									end
% %
% % 									% overlays
% % 									hold on
% % 									plot(snoutContour(2,:), snoutContour(1,:),'m')
% % 									plot(snoutTrackingData.contour(2,:), snoutTrackingData.contour(1,:),'g');
% % 									fnplt(stNextChunkData.pp, 'y');
% %
% % 								end
% %
% % 							else
% %
% % 								% PERFORMANCE MODE
% % 								c = get(gca, 'children');
% % 								c = c(c ~= gui.h_image);
% % 								delete(c);
% % 								set(gui.h_image, 'cdata', repmat(imageBuffer, [1 1 3]));
% % 								plot(snoutContour(2,:), snoutContour(1,:), 'm');
% % 								plot(snoutTrackingData.contour(2,:), snoutTrackingData.contour(1,:), 'g');
% % 								fnplt(stNextChunkData.pp, 'y');
% %
% % 							end
% %
% % 							% common plotting code
% % 							title(['Goodness of fit (HCGF) ' int2str(HCGF * 100) '% @ frame ' int2str(frameIndex) ' of ' int2str(movieInfo.NumFrames)])
% % 							drawnow
% %
% % 							% check for success
% % 							if  HCGF < (pars.minHCGF / 100)
% %
% % 								if pars.debug
% % 									disp(['Not cool @ frame ' int2str(frameIndex)]);
% %
% % 									snoutCenter = stNextChunkData.X(1:2);
% % 									disp(snoutCenter')
% % 								end
% %
% % 								stPrevChunkData.X = [];
% %
% % 							else
% %
% % 								% let user know we were successful
% % 								[a, b, c] = fileparts(pars.videoFilename);
% % 								stDisp(['Coolization OK for "' b c '"']);
% %
% % 								% store stParams
% % 								snoutCenter = stNextChunkData.X(1:2)';
% % 								stParams.Variable{3}.value = round(snoutCenter);
% %
% % 								if pars.debug
% % 									disp(['Cool @ frame ' int2str(frameIndex)]);
% % 									disp(snoutCenter)
% %
% % 									% print figure
% % 									set(gui.h_fig, ...
% % 										'PaperOrientation', 'portrait', ...
% % 										'PaperUnits', 'centimeters', ...
% % 										'PaperPosition', [0 0 20 30]);
% % 									print(gui.h_fig, ...
% % 										strcat(pars.videoFilename, '-cool.png'), '-dpng');
% %
% % 									% save data
% % 					% 						save(strcat(pars.videoFilename, '-cool.mat'), 'imageBuffer', 'snoutTrackingData', 'snoutContour');
% %
% % 								end
% %
% % 								% invert the transform to get back to nose tip
% % 								% IGOR: smarter way of doing this?
% % 								X1 = stNextChunkData.X(1);
% % 								X2 = stNextChunkData.X(2);
% % 								X3 = stNextChunkData.X(3);
% % 								X4 = stNextChunkData.X(4);
% %
% % 								X3 = X3 + 1;
% % 								theta = atan2(X4, X3) - pi/2;
% % 								r = X3 / cos(theta + pi/2) * 80;
% % 								[dx, dy] = pol2cart(theta, r);
% % 								noseTip = round(snoutCenter - [dx dy]);
% %
% % 								stParams.Variable{2}.value = noseTip;
% %
% % 								% turn off automatic init
% % 								stParams.Constant{6}.value = false; % disable automatic initialization
%
% 			end
%



