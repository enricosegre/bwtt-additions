
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% original author: lost in the mist; perhaps Goren, Igor.
% rebuilt: ben mitch

function hh = stGetImageHistogram(job, frameIndices)

% hh = stGetImageHistogram(job, frameIndices)
%   get the image histogram. if frameIndices is not specified,
%   the histogram is constructed from the whole video (this
%   can take a little time). "hh" is returned in normalised
%   form - that is, a value of 1 indicates that all the
%   pixels in the image are at that level.

% get frame indices to max over
if nargin < 2

	% use whole video
	[imageBuffer, infoMov] = stLoadMovieFrames(job.getVideoFilename(), 1, 'c');
	frameIndices = unique(round(linspace(1, infoMov.NumFrames, 50)));

	% in dev mode, use a quick version
	if stUserData('devMode')
		frameIndices = unique(round(linspace(1, infoMov.NumFrames, 5)));
	end
	
end

% report only for multi-frame
if length(frameIndices) > 1
	job.log('calculating image histogram...');
end

% prepare hist
hh = zeros(256, 1);

% construct
for i = 1:numel(frameIndices)
	imageBuffer = stLoadMovieFrames(job.getVideoFilename(), frameIndices(i), 'c');
	hh = hh + histc(imageBuffer{1}(:), 0.5:256);
end

% normalise
hh = hh / sum(hh);

end
