
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function contour = stObjectContourFromModel(model, resolution)

% handle type
switch model.type

	case 'circle'
		perim = model.radius * 2 * pi;
		N = max(ceil(perim / resolution), 16);
		theta = linspace(0, 2*pi, N);
		contour = [cos(theta) * model.radius + model.center(1); sin(theta) * model.radius + model.center(2)];

	case 'polygon'
		contour = zeros(2, 0);
		v = [1:size(model.vertices, 2) 1];
		for i = 1:size(model.vertices, 2)
			v1 = model.vertices(:, v(i));
			v2 = model.vertices(:, v(i+1));
			d = v2 - v1;
			l = sqrt(sum(d.^2));
			N = max(ceil(l / resolution), 2);
			frac = linspace(0, 1, N);
			cs = zeros(2, N);
			for i = 1:N
				cs(:, i) = v1 + frac(i) * d;
			end
			contour = [contour cs];
		end

	otherwise
		error('internal error');

end
	
