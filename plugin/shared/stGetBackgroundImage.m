
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% original author: Goren Gordon
% rebuilt: ben mitch

function bgImage = stGetBackgroundImage(job)

% report
job.log('calculating background image...');

% get frame/info
[imageBuffer, infoMov] = stLoadMovieFrames(job.getVideoFilename(), 1, 'c');

% get frame indices to max over
bgIndex = unique(round(linspace(1, infoMov.NumFrames, 60)));

% in dev mode, use a quick version
if stUserData('devMode')
	bgIndex = unique(round(linspace(1, infoMov.NumFrames, 5)));
end

% prepare initial image
bgImage = zeros(size(imageBuffer{1}), 'uint8');

% This code is no longer required, since stLoadMovieFrames
% provides read-ahead optimisation for video files.
%
% % mode
% flagLowMemoryError = false;
% 
% % do max operation
% if flagLowMemoryError
% 	for i=1:numel(bgIndex)
% 		bgImageBuffer = stLoadMovieFrames(job.getVideoFilename(), bgIndex(i), 'c');
% 		bgImage = max(bgImage, bgImageBuffer{1});
% 	end
% else
% 	bgImageBuffer = stLoadMovieFrames(job.getVideoFilename(), bgIndex, 'a');
% 	bgImage = max(bgImageBuffer, [], 3);
% end

% new code, just use the low-memory version
for i=1:numel(bgIndex)
	bgImageBuffer = stLoadMovieFrames(job.getVideoFilename(), bgIndex(i), 'c');
	bgImage = max(bgImage, bgImageBuffer{1});
end

end
