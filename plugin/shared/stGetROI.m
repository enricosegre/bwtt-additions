
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function result = stGetROI(frame, roi)



% create GUI
gui = stGUI([640 480], gcbf, 'Modify Region Of Interest', 'resizable', 'on', 'CloseRequestFcn', @callback_Close);
h_dialog = gui.getMainWindow();
panel_root = gui.rootPanel;

% layout
panel_right = panel_root.pack([], 'right', 168);
panel_buttons2 = panel_right.pack([], 'bottom', 24);
panel_buttons1 = panel_right.pack([], 'bottom', 24);
panel_left = panel_root;

% buttons
panel_buttons2.pack(createButton('Cancel', @callback_Cancel), 'right', 80);
panel_buttons2.pack(createButton('OK', @callback_OK), 'right', 80);
panel_buttons1.pack(createButton('Change', @callback_Change), 'right', 80);
panel_buttons1.pack(createButton('Reset', @callback_Reset), 'right', 80);

% info panel
h_text = createText('');
panel_right.pack(h_text, 'top', -1);

% create axis
h_axis = axes('parent', h_dialog);
panel_left.pack(h_axis, 'top', -1);

% prepare frame for display
if stUserData('normaliseFrame')
	im = repmat(stNormaliseFrame(frame), [1 1 3]);
else
	im = repmat(frame, [1 1 3]);
end

% display frame
displayFrame();

% wait
close = false;
while 1
	uiwait(h_dialog);
	if close
		break
	end
end



%% HELPERS

	function im = applyROI(im, roi)
		
		if ~isempty(roi)
			for d = 1
				chan = im(:, :, d);
				chan(~roi) = chan(~roi) / 4 + 192;
				im(:, :, d) = chan;
			end
		end
		
	end

	function displayFrame()
		
		imWithROI = applyROI(im, roi);
		image(imWithROI, 'parent', h_axis);
		
		% 			% if image exists, and is concommitant, just reset the cdata
		% 			if ~isempty(h_image)
		% 				set(h_image, 'cdata', im2);
		% 			else
		% 				% create image
		% 				hold(h_axis, 'off');
		% 				h_image = image(im2, 'parent', h_axis);
		set(h_axis, 'xtick', [], 'ytick', []);
		% 				hold(h_axis, 'on');
		axis(h_axis, 'image');
		% 			end
		
		% image(h_axis, repmat(frame, [1 1 3])
		
	end



% 			% confirm/retrieve roi
% 			while 1
%
% 				% It is recommended to run roi on one movie, select
% 				% the area where the animal enters (so you mask out
% 				% moving poles and platforms), and save it for all
% 				% the other movies.
%
% 				% display it
% 				[h_image, h_overlay] = displayImage(h_axis, h_image, h_overlay, frameRaw, pars.roi);
%
% 				% uiwait
% 				response = 'closed dialog';
% 				uiwait(h_dialog);
%
% 				% check response
% 				switch response
% 					case {'closed dialog' 'cancel'}
% 						error('Coolization was cancelled by user.');
% 					case {'ok'}
% 						break
% 					case {'change'}
% 						% ok to continue
% 				end
%
% 				% prompt user for new roi
% 				set(h_buttons, 'enable', 'off');
% % 				stMessage(['You should now choose the Region Of Interest (roi). Coolization will only '...
% % 					'look for the animal in that region.' 10 10 'Left click to add vertices, right click to close, ' ...
% % 					'backspace to start again, or double click to accept.']);
% 				while 1
% 					pars.roi = roipoly;
% 					if ~isempty(pars.roi)
% 						break
% 					end
% 				end
% 				set(h_buttons, 'enable', 'on');
%
% 			end


%% CALLBACK

	function callback_Close(obj, evt)
		
		result = false;
		close = true;
		delete(h_dialog);
		
	end

	function callback_Cancel(obj, evt)
		
		result = false;
		close = true;
		delete(h_dialog);
		
	end

	function callback_OK(obj, evt)
		
		result = roi;
		close = true;
		delete(h_dialog);
		
	end

	function callback_Change(obj, evt)

		s = [...
			'Left click:' 10 '  Add vertex' 10 10 ...
			'Right click:' 10 '  Close polygon' 10 10 ...
			'Double click:' 10 '  Accept new ROI' 10 10 ...
			'Backspace:' 10 '  Cancel' 10 10 ...
			];
		set(h_text, 'String', s);
		roi_ = roipoly;
		if ~isempty(roi_)
			roi = roi_;
		end
		set(h_text, 'String', '');
		displayFrame();
		
	end

	function callback_Reset(obj, evt)
		
		roi = [];
		displayFrame();
		
	end


%% COMPONENT

	function h = createButton(text, callback, tooltip)
		
		guipars = stGUIPars();
		
		if nargin < 4
			tooltip = '';
		end
		
		h = uicontrol(h_dialog, ...
			'Style', 'Pushbutton', ...
			'Units', 'pixels', ...
			'String', text, ...
			'Tooltip', tooltip, ...
			'Callback', callback, ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize ...
			);
		
	end

	function h = createText(text, align)
		
		guipars = stGUIPars();
		
		if nargin < 2
			align = 'left';
		end
		
		h = uicontrol(h_dialog, ...
			'Style', 'Edit', ...
			'Units', 'pixels', ...
			'String', text, ...
			'Hori', align, ...
			'Max', 3, ...
			'Min', 1, ...
			'BackgroundColor', [1 1 1], ...
			'Enable', 'off', ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize ...
			);
		
	end



end





