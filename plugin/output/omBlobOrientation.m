
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function state = omBlobOrientation(operation, job, state)

switch operation
	
	case 'info'
		
		state.author = 'Enrico Segre';
	
	case 'parameters'
		
		pars = {};				
		state.pars = pars;
	
	case 'process'

		% empty
		orientation = NaN(length(state.frameIndices), 1);
		
		% extract
		results = job.getResults('all');
		
		% for each frame
		for f = 1:length(state.frameIndices)
			
			% extract
			fi = state.frameIndices(f);
	 		result = results{fi};
			
			% if we have everything we need
			if isfield(result, 'blob') && ~isempty(result.blob)
						
				% extract
				O = result.blob.Orientation;
				
				% for each blob
				for oi = 1:length(O)
					
					% extract
					o = O(1);
					
				end
				
				% store orientation of the first blob
                %  (in future conside all blobs)
				orientation(f, 1) = o(1);
				
			end
		
		end	
		
		% store
    	state.result.orientation = orientation;

		
	case 'plot'

		% create axes
		state.h_axis = gca;
		
		% do plot
		plot(state.h_axis, state.frameIndices, state.result.orientation);
		ylabel('blob orientation (degrees)');
		xlabel('frame index');
		
		% do legend
		n = size(state.result.orientation, 2);
		ll = {};
		for i = 1:n
			ll{i} = ['Blob ' int2str(i)];
		end
		legend(ll);
	
		
	otherwise
		
		state = [];
		
end



