
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


% author: ben mitch


function state = omSnoutData(operation, job, state)

switch operation
	
	case 'info'
		state.author = 'Ben Mitch';

		
		
	case 'parameters'
		
		pars = {};
		
		% parameter
		par = [];
		par.name = 'Tdist';
		par.label = 'Distance threshold for HCGF';
		par.type = 'scalar';
		par.help = 'HCGF is the fraction of points on the detected snout that fall within this distance of the snout template.';
		par.range = [1 50];
		par.value = 10;
		par.step = [1 5];
		par.precision = 0;
		pars{end+1} = par;
		
% 		% parameter
% 		par = [];
% 		par.name = 'fC';
% 		par.label = 'Low-pass cutoff';
% 		par.type = 'scalar';
% 		par.help = 'The filter used is a zero-phase (two pass) 3rd order Butterworth low-pass.';
% 		par.range = [1 50];
% 		par.value = 10;
% 		par.step = [1 5];
% 		par.precision = 1;
% 		pars{end+1} = par;
		
		state.pars = pars;
	

		
	case 'process'

		% extract
		results = job.getResults('all');
% 		fps = job.getMetaData('recordedFrameRate');
% 		mmPerPixel = job.getMetaData('mmPerPixel');
		
		% retrieve snout data
		raw = [];
		raw.center = NaN(length(state.frameIndices), 2);
		raw.noseTip = NaN(length(state.frameIndices), 2);
		raw.orientation = NaN(length(state.frameIndices), 1);
		HCGF = NaN(length(state.frameIndices), 1);
		for f = 1:length(state.frameIndices)
			fi = state.frameIndices(f);
	 		result = results{fi};
			if isfield(result, 'snout')
				raw.center(f, :) = result.snout.center;
				raw.noseTip(f, :) = result.snout.noseTip;
				v = result.snout.noseTip - result.snout.center;
				raw.orientation(f) = atan2(v(1), v(2));
				HCGF(f) = sum(abs(result.snout.perpdists) < state.pars.Tdist) / numel(result.snout.perpdists);  
			end
		end
		
		% compute speed
		raw.speed = sqrt(sum(diff(raw.center, 1, 1).^2, 2));
		if ~isempty(raw.speed)
			raw.speed = [raw.speed(1); raw.speed];
		else
			% if there is only one data point, speed is unknown
			raw.speed = NaN;
		end
		
% 		% convert to mm/sec
% 		if isempty(mmPerPixel) || isempty(fps)
% 			raw.speed = NaN * raw.speed;
% 		else
% 			raw.speed = raw.speed * fps * mmPerPixel;
% 		end
		
		% unwrap
		raw.orientation = unwrap(raw.orientation);
		
% 		% filter
% 		filtered = [];
% 		if ~isempty(fps)
% 			[b, a] = butter(3, state.pars.fC / (fps / 2));
% 			filtered.center = filter_nansafe(b, a, raw.center, true);
% 			filtered.noseTip = filter_nansafe(b, a, raw.noseTip, true);
% 			filtered.orientation = filter_nansafe(b, a, raw.orientation, true);
% 			filtered.speed = filter_nansafe(b, a, raw.speed, true);
% 		else
% 			filtered.center = [];
% 			filtered.noseTip = [];
% 			filtered.orientation = [];
% 			filtered.speed = [];
% 		end
		
		% store
		if any(~isnan(raw.orientation))
			
			% convert to single()
			raw.center = single(raw.center);
			raw.noseTip = single(raw.noseTip);
			raw.orientation = single(raw.orientation);
			raw.speed = single(raw.speed);
			
			% store
			state.result.HCGF = HCGF;
			state.result.raw = raw;
% 			state.result.filtered = filtered;

		else
			state.result = [];
		end
		
		
		
	case 'plot'

		% do plot
		subplot(2, 2, 1, 'parent', state.h_figure);
		plot(state.frameIndices, state.result.raw.orientation * 180 / pi, 'k-');
% 		if isempty(state.result.filtered.orientation)
			legend('raw')
% 		else
% 			hold on
% 			plot(state.frameIndices, state.result.filtered.orientation * 180 / pi, 'r-');
% 			hold off
% 			legend('raw', 'filtered')
% 		end
		
		% label plot
		xlabel('frame index');
		ylabel('head orientation (degrees)');
		
		% do plot
		subplot(2, 2, 2, 'parent', state.h_figure);
		plot(state.frameIndices, state.result.raw.speed, 'k-');
% 		if isempty(state.result.filtered.speed)
			legend('raw')
% 		else
% 			hold on
% 			plot(state.frameIndices, state.result.filtered.speed, 'r-');
% 			hold off
% 			legend('raw', 'filtered')
% 		end
		
		% label plot
		xlabel('frame index');
		ylabel('movement speed (px/sec)');
		
		% do plot
		subplot(2, 2, 3, 'parent', state.h_figure);
		plot(state.frameIndices, state.result.HCGF, 'k-');
		v = axis;
		v(3:4) = [0 1.1];
		axis(v);
		
		% legend
		legend('HCGF', 4);
		
		% label plot
		xlabel('frame index');
		ylabel('HCGF (normalised)');
		
		
		
	otherwise
		
		state = [];
		
end





%% one of my library functions

function s = filter_nansafe(b, a, s, bidirectional)

% interp internal NaNs
s = interpnans(s);

% change end NaNs to be equal to the nearest valid value
for c = 1:size(s, 2)
	i = isnan(s(:, c));
	f = find(i);
	fl = [find(~i, 1, 'first') find(~i, 1, 'last')];
	if isempty(fl)
		continue
	end
	vfl = s(fl, c)';
	dfl = [abs(f - fl(1)) abs(f - fl(2))];
	[temp, j] = min(dfl, [], 2);
	s(f, c) = vfl(j)';
end

% augment signal so that we don't get end effects in the
% filter
N = 250;
h = hanning(N);
h1 = repmat(h(1:N/2), 1, size(s, 2));
h2 = repmat(h(N/2+1:end), 1, size(s, 2));
mn = mean(s, 1);
o1 = s(1, :) - mn;
o2 = s(end, :) - mn;
for c = 1:size(s, 2)
	h1(:, c) = h1(:, c) * o1(c) + mn(c);
	h2(:, c) = h2(:, c) * o2(c) + mn(c);
end
s = [
	h1
	s
	h2
	];

% delegate to filter
s = filter(b, a, s);

% if bidirectional
if nargin == 4
	s = flipud(s);
	s = filter(b, a, s);
	s = flipud(s);
end

% unaugment
s = s(N/2+1:end-N/2, :);





function A = interpnans(A, method)

% function A = interpNaNs(A, method = 'linear')
%
% interpolate through NaNs along the first dimension, so
% that for instance
%
% [NaN NaN 1 NaN NaN 4 NaN]' becomes
% [NaN NaN 1 2 3 4 NaN]'
%
% using the 'linear' method. for other methods, see
% interp1().

if ~exist('method','var')
	method = 'linear';
end

sz = size(A);
N = prod(sz(2:end));

for n = 1:N
	X = A(:,n);
	i_nan = find(isnan(X));
	i_not = find(~isnan(X));
	if length(i_not) > 1
		X(i_nan) = interp1(i_not, X(i_not), i_nan, method);
	end
	A(:,n) = X;
end



