
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function state = omAnnotatedVideo(operation, job, state)

switch operation
	
	case 'info'
		state.author = 'Ben Mitch';
		state.flags = {'noplot'};
		
		
	case 'parameters'
		
		pars = {};
		
		% parameter
		par = [];
		par.name = 'showProgress';
		par.label = 'Show progress';
		par.help = 'If this flag is set, the frames are shown as the videos are created.';
		par.value = false;
		par.type = 'flag';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'outputFilename';
		par.label = 'Output filename';
		par.help = ['Use any of the following tokens to form a filename: %u job UID, %v video file base name. NB: The extension ".avi" is added automatically.' 10 10 'NBB: If you choose just the token %v, multiple jobs operating on the same video file will correspond to the same output file.'];
		par.value = '%v';
		par.type = 'string';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'writer';
		par.label = 'Video writer';
		par.help = 'Choose which of Matlab''s video writers to use. If you are having trouble, or don''t know where to start, see the documentation for this output method.';
		par.options = {'avifile' 'VideoWriter'};
		par.value = 'VideoWriter'; % 'avifile'; % CHANGED for R2014b, because avifile has been removed
		par.type = 'option';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'codecAF';
		par.label = 'Codec (avifile)';
		par.options = {'None' 'Xvid' 'DivX' 'Cinepak'};
		par.value = 'None';
		par.type = 'option';
 		par.show = 'strcmp(pars.writer, ''avifile'')';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'codecVW';
		par.label = 'Codec (VideoWriter)';
		par.options = {'None' 'Motion JPEG' 'Motion JPEG 2000'};
		par.value = 'Motion JPEG';
		par.type = 'option';
 		par.show = 'strcmp(pars.writer, ''VideoWriter'')';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'quality';
		par.type = 'scalar';
		par.label = 'Compression quality (%)';
		par.help = 'Compression quality (percentage) - high numbers are higher quality. This parameter will only affect some writer/codec combinations.';
		par.range = [10 100];
		par.step = [5 10];
		par.value = 75;
		par.show = '(strcmp(pars.writer, ''avifile'') && ~strcmp(pars.codecAF, ''None'')) || (strcmp(pars.writer, ''VideoWriter'') && strcmp(pars.codecVW, ''Motion JPEG''))';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'compressionRatio';
		par.type = 'scalar';
		par.label = 'Compression factor';
		par.help = 'Compression factor (ratio) - low numbers are higher quality. This parameter will only affect some writer/codec combinations.';
		par.range = [1 25];
		par.step = [0.1 1];
		par.precision = 1;
		par.value = 10;
		par.show = '(strcmp(pars.writer, ''VideoWriter'') && strcmp(pars.codecVW, ''Motion JPEG 2000''))';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'fps';
		par.type = 'scalar';
		par.label = 'Playback rate (FPS)';
		par.range = [1 30];
		par.step = [1 5];
		par.value = 25;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'postOp';
		par.label = 'Post-encode operation';
		par.help = 'Command-line to be run after the video file has been written - typically, you would use this if you want to use your own compression (like ffmpeg). The tokens "$src" and "$dst" are expanded to the input and output files.';
		par.value = '';
		par.type = 'string';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'translateTo';
		par.label = 'Translate item to center';
		par.value = 'Do not translate';
		par.options = {'Do not translate' 'Snout center' 'Nose tip'};
		par.type = 'option';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'rotateTo';
		par.label = 'Rotate head to face...';
		par.value = 'Do not rotate';
		par.options = {'Do not rotate' 'Up' 'Right' 'Down' 'Left'};
		par.type = 'option';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'crop';
		par.label = 'Crop factor (%)';
		par.type = 'scalar';
		par.range = [5 100];
		par.step = [5 20];
		par.value = 100;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'scale';
		par.label = 'Scale factor (%)';
		par.type = 'scalar';
		par.range = [5 100];
		par.step = [5 20];
		par.value = 100;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'normalise';
		par.label = 'Normalise image histogram';
		par.value = false;
		par.type = 'flag';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'skipEmpty';
		par.label = 'Skip interposed frames';
		par.help = 'This method writes only the range of the video that contains some results. If some early frames have results, and some late frames, but the frames in the middle do not, this flag decideds whether to include them in the video.';
		par.value = false;
		par.type = 'flag';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'includeFrameIndex';
		par.label = 'Include frame index';
		par.help = 'Overlay the frame index in the top left corner of the video. This does not work yet :).';
		par.value = true;
		par.type = 'flag';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'includeNose';
		par.label = 'Include nose tip';
		par.value = true;
		par.type = 'flag';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'includeSnoutCenter';
		par.label = 'Include snout center';
		par.value = true;
		par.type = 'flag';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'includeSnout';
		par.label = 'Include snout tracking';
		par.value = true;
		par.type = 'flag';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'includeWhiskers';
		par.label = 'Include whisker tracking';
		par.value = true;
		par.type = 'flag';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'colorWhiskers';
		par.label = 'Show whiskers in colour';
		par.value = false;
		par.type = 'flag';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'includeBlobs';
		par.label = 'Include elliptical blob fit';
		par.value = true;
		par.type = 'flag';
		pars{end+1} = par;

        % parameter
		par = [];
		par.name = 'specifyFrameRange';
		par.label = 'Output only specified frame range';
		par.value = false;
		par.type = 'flag';
		pars{end+1} = par;

		% parameter
		par = [];
		par.name = 'frameRangeFirst';
		par.type = 'scalar';
		par.label = 'First frame';
		par.range = [1 10000];
		par.step = [1 10];
		par.value = 1;
		par.constraints = {
			{'<=' 'frameRangeLast'}
			};
 		par.show = 'pars.specifyFrameRange';
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'frameRangeLast';
		par.type = 'scalar';
		par.label = 'Last frame';
		par.range = [1 10000];
		par.step = [1 10];
		par.value = 10000;
		par.constraints = {
			{'>=' 'frameRangeFirst'}
			};
 		par.show = 'pars.specifyFrameRange';
		pars{end+1} = par;
		
		state.pars = pars;
		
		
		
	case 'process'
		
		% extract
		results = job.getResults('all');
		videoInfo = job.getVideoInfo();
		use_vw = strcmp(state.pars.writer, 'VideoWriter');
		
		% construct filename
		filename = state.pars.outputFilename;
		filename = strrep(filename, '%u', job.getUID());
		filename = strrep(filename, '%v', stProcessPath('justBasename', job.getVideoFilename()));
		
		% different extensions for different writers
		if use_vw && strcmp(state.pars.codecVW, 'Motion JPEG 2000')
			filename = [job.getSupplementaryOutputDir() '/' filename '.mj2'];
		else
			filename = [job.getSupplementaryOutputDir() '/' filename '.avi'];
		end
		
		% delete if exists
		if exist(filename, 'file')
			delete(filename);
		end
		
		% switch on writer
		if use_vw

			% create VideoWriter object
			switch state.pars.codecVW
				case 'None'
					vw = VideoWriter(filename, 'Uncompressed AVI');
				case 'Motion JPEG'
					vw = VideoWriter(filename, 'Motion JPEG AVI');
				case 'Motion JPEG 2000'
					vw = VideoWriter(filename, 'Motion JPEG 2000');
			end
			
			% set compression method and frame rate
			vw.FrameRate = state.pars.fps;

			% set quality
			switch state.pars.codecVW
				case 'None'
				case 'Motion JPEG'
					vw.Quality = state.pars.quality;
				case 'Motion JPEG 2000'
					vw.CompressionRatio = state.pars.compressionRatio;
			end
			
			% open it
			open(vw);

		else

			% open AVI file
			w1 = warning('off', 'MATLAB:aviset:compressionUnsupported');
			w2 = warning('off', 'MATLAB:audiovideo:aviset:compressionUnsupported');
			avi = avifile(filename, 'Compression', state.pars.codecAF, 'FPS', state.pars.fps, 'Quality', state.pars.quality);
			warning(w1);
			warning(w2);

		end

		% failsafe
		try
			
			% show
			if state.pars.showProgress
				h_fig = stSelectFigure('omOutputVideo', 'raise');
				clf
				h_axis = axes('parent', h_fig);
				h_image = image(1:videoInfo.Width, 1:videoInfo.Height, zeros(videoInfo.Height, videoInfo.Width, 3, 'uint8'));
				axis(h_axis, 'image');
			end

			% for each frame
			for f = 1:length(state.frameIndices);

				% get results
				fi = state.frameIndices(f);
				res = results{fi};

				% if no results, just skip this frame
				if isempty(fieldnames(res))
					if state.pars.skipEmpty
						continue
					end
				end
				
				% if outside specified frame range, skip
				if state.pars.specifyFrameRange
					if fi < state.pars.frameRangeFirst || fi > state.pars.frameRangeLast
						continue
					end
				end

				% get raw frame
        im = job.getVideoFrames(fi, 'a');
        
        % convert to RGB
				im = repmat(im, [1 1 3]);
				
				% normalise
				if state.pars.normalise
					im = stNormaliseFrame(im);
				end
				
				% handle snout
				t_rc = [];
				rot_angle = [];
				if isfield(res, 'snout')
					
					% get locations (in RC coordinates)
					n_rc = res.snout.noseTip;
					c_rc = res.snout.center;
					s_rc = double(res.snout.contour');

					% get head bearing
					v = n_rc - c_rc;
					bearing = atan2(v(1), v(2));

					% translate
					switch state.pars.translateTo
						case 'Snout center'
							t_rc = c_rc;
						case 'Nose tip'
							t_rc = n_rc;
					end
					
					% rotate
					switch state.pars.rotateTo
						case 'Down'
							rot_angle = bearing - pi / 2;
						case 'Left'
							rot_angle = bearing + pi;
						case 'Right'
							rot_angle = bearing;
						case 'Up'
							rot_angle = bearing + pi / 2;
					end
					
					% augment
					if state.pars.includeNose
						im = implot(im, n_rc(2), n_rc(1), 'r.');
					end

					% augment
					if state.pars.includeSnoutCenter
						im = implot(im, c_rc(2), c_rc(1), 'y.');
					end

					% augment
					if state.pars.includeSnout
						im = implot(im, s_rc(:, 2), s_rc(:, 1), 'o-');
					end
					
				end

				% handle whiskers
				if isfield(res, 'whisker')
				
					% augment
					if state.pars.includeWhiskers
						
						if state.pars.colorWhiskers
							cols = 'wrgbmyc';
						else
							cols = 'w';
						end
						
						cc = res.whisker.contour;
						for c = 1:length(cc)
							w_rc = double(cc{c}');
							col = cols(mod(c, length(cols)) + 1);
							im = implot(im, w_rc(:, 2), w_rc(:, 1), [col '-']);
						end
					end

                end
                
				% handle blobs
				if isfield(res, 'blob')
				
					% augment
                    if state.pars.includeBlobs
                        s=res.blob;
                        for k = 1:length(s)
                            alpha=-s(k).Orientation*pi/180;
                            R=sqrt(s(k).Area);
                            cx=s(k).Centroid(1);
                            cy=s(k).Centroid(2);
                            im = implot(im, cx,cy,'g.');
                            im = implot(im, cx+R*cos(alpha)*[-1,1],...
                                            cy+R*sin(alpha)*[-1,1],'g-');
                            phi=(0:50)*pi/25;
                            ex=s(k).MajorAxisLength*cos(phi)/2;
                            ey=s(k).MinorAxisLength*sin(phi)/2;
                            im = implot(im,...
                                cx+ex*cos(alpha)-ey*sin(alpha),...
                                cy+ex*sin(alpha)+ey*cos(alpha),'g.');
                        end
                    end

				end
				
				% NB: we _could_ do rotation and translation in one
				% operation using imtransform, but it's _much_
				% slower than implementing translate and rotate
				% separately (presumably because the algorithm is
				% more general).
				
				% translate
				if ~isempty(t_rc)
					dst = (videoInfo.Size + 1) / 2;
					src = t_rc;
					err = round(dst - src);
					imsrc = im;
					im = 0 * imsrc;
					msrc = 1:videoInfo.Height;
					nsrc = 1:videoInfo.Width;
					mdst = msrc + err(1);
					ndst = nsrc + err(2);
					minc = mdst >= 1 & mdst <= videoInfo.Height;
					ninc = ndst >= 1 & ndst <= videoInfo.Width;
					im(mdst(minc), ndst(ninc), :) = imsrc(msrc(minc), nsrc(ninc), :);
				end
				
				% rotate
				if ~isempty(rot_angle)
					im = imrotate(im, rot_angle / pi * 180, 'crop');
				end
				
				% crop
				if state.pars.crop ~= 100
					s = state.pars.crop / 100;
					cen = (videoInfo.Size + 1) / 2;
					r = cen(1) + s * ([1 videoInfo.Height] - cen(1));
					c = cen(2) + s * ([1 videoInfo.Width] - cen(2));
					
					% some codecs insist on this
					r = round(r/4) * 4 - [3 0];
					c = round(c/4) * 4 - [3 0];
					
					im = im(r(1):r(2), c(1):c(2), :);
				end
				
				% scale
				if state.pars.scale ~= 100
					sz = size(im);
					sz = sz(1:2) * state.pars.scale / 100;
					sz = ceil(sz/4) * 4; % some codecs insist on this
					im = imresize(im, sz, 'bilinear');
				end

				% handle includeFrameIndex
				if state.pars.includeFrameIndex
					fim = stGetCharImage(int2str(fi), 20);
					fim = repmat(fim, [1 1 3]);
					sz = size(fim);
					im(1:sz(1), 1:sz(2), :) = fim;
				end

				% store frame
				if ~isempty(im)
					if use_vw
						writeVideo(vw, im);
					else
						avi = addframe(avi, im);
					end
				end
				
				% show
				if state.pars.showProgress
					set(h_image, 'cdata', im);
				end

				% progress
				if job.progress(f / length(state.frameIndices))
					break
				end

			end

			% close video file
			if use_vw
				close(vw);
			else
				avi = close(avi);
			end

			% show
			if state.pars.showProgress
				close(h_fig)
			end
			
		catch err
			
			if use_vw
				close(vw);
			else
				avi = close(avi);
			end
			
			% show
			if state.pars.showProgress
				close(h_fig)
			end
			
			rethrow(err);
			
		end

		% post-encoding operation
		if ~isempty(state.pars.postOp)
			cmd = state.pars.postOp;
			tempfilename = [filename '-temp'];
			movefile(filename, tempfilename);
			if ispc
				quote = '"';
			else
				quote = '''';
			end
			cmd = strrep(cmd, '$dst', [quote filename quote]);
			cmd = strrep(cmd, '$src', [quote tempfilename quote]);
			[a, b] = system(cmd);
			if a
				error(b);
			end
			delete(tempfilename);
		end
		
		% return filename
		state.result.filename = filename;
		
	otherwise
		state = [];
		
end








%% COPIED FROM MITCHLIB "implot" function on 07/04/2011

% im = implot(im, x, y, ...)
%
%   provides a similar interface to plot(x, y, ...) but
%   plots into an image.

% Author: Ben Mitch
% Modified: Jan-Mar 2011
% Copyright: Ben Mitch 2011
% License terms: Because the world is pretty and sunny and
%   filled with little rabbits and other beautiful things,
%   shame on you if you make it less beautiful. But do as
%   you will with this code, the little rabbits will not
%   mind.

function im = implot(im, x, y, varargin)

% check im
sz = size(im);
if length(sz) ~= 3 || sz(3) ~= 3 || ~isa(im, 'uint8')
	error('"im" must be an MxNx3 uint8 image');
end
Y = sz(1);
X = sz(2);

% check x and y
sx = size(x);
sy = size(y);
if length(sx) ~= length(sy) || any(sx ~= sy) || length(sx) ~= 2 || ~any(sx == 1)
	error('"x" and "y" must be 1xN or Nx1 vectors');
end
N = max(sx);

% defaults
col = [0 0 0];
style = '-';
linewidth = 2;
markersize = 6;

% interpret other args
for v = 1:length(varargin)
	
	% extract
	arg = varargin{v};
	
	% handle
	if ischar(arg) && any(arg(1) == 'bgrcmykwo') && length(arg == 2) && any(arg(2) == '.ox+-:')
		
		switch arg(1)
			case 'b', col = [0 0 1];
			case 'g', col = [0 1 0];
			case 'r', col = [1 0 0];
			case 'c', col = [0 1 1];
			case 'm', col = [1 0 1];
			case 'y', col = [1 1 0];
			case 'k', col = [0 0 0];
			case 'w', col = [1 1 1];
			case 'o', col = [1 0.5 0];
		end
		
		style = arg(2);
		
		continue
		
	end
	
end

% prepare alpha mask
alpha = ones(Y, X);

% prepare temp vars
markerrng = -markersize:markersize;
maskrng = 1:(2*markersize+1);

% define safe region
safex = [1+markersize X-markersize];
safey = [1+markersize Y-markersize];

% define safe and non-safe points
safe = x >= safex(1) & x <= safex(2) & y >= safey(1) & y <= safey(2);

% do the plotting
switch style
	
	% line
	case '-'
		
		% for each point pair
		for n = 1:N-1

			% get points
			x1 = x(n);
			x2 = x(n+1);
			y1 = y(n);
			y2 = y(n+1);

			% get unit normal on line
			normal_on = unit([x2-x1 y2-y1]');
			
			% get line end location in normal form
			e = [x2-x1 y2-y1] * normal_on;
			
			% get unit normal to line
			rot90 = [0 -1; 1 0];
			normal_to = rot90 * normal_on;
			
			% get line location in normal form
			d = [x1 y1] * normal_to;
			
			% get range to be considered
			xx = floor(min([x1 x2]) - linewidth):ceil(max([x1 x2]) + linewidth);
			yy = floor(min([y1 y2]) - linewidth):ceil(max([y1 y2]) + linewidth);
			
			% cull range to within figure
			xx = xx(xx >= 1 & xx <= X);
			yy = yy(yy >= 1 & yy <= Y);
			
			% get all points
			[XX, YY] = meshgrid(xx, yy);
			
			% get point locations in normal form relative to line
			XY = [XX(:)'; YY(:)'];
			D = normal_to' * XY - d;
			XY = [XX(:)'-x1; YY(:)'-y1];
			E = normal_on' * XY;
			Etemp = E(E>0);
			Etemp = Etemp - e;
			Etemp(Etemp < 0) = 0;
			E(E>0) = Etemp;
			
			% get total range from line
			R = sqrt(D.^2 + E.^2);
			
			% scale by linewidth
			R = 1 - abs(R / (linewidth + 1));
			R(R<0) = 0;
			
			% rounded line
			R = sqrt(R);
			
			% get correctly shaped mask
			mask = reshape(1 - R, length(yy), length(xx));
			
			% do mask
			alpha(yy, xx) = min(alpha(yy, xx), mask);
			
		end
	
	% dot
	case '.'
		
		% get mask
		mask = get_mask(style, markersize);
		
		% lay mask into alpha
		for n = 1:N
			xx = round(x(n) + markerrng);
			yy = round(y(n) + markerrng);
			if safe(n)
				alpha(yy, xx) = min(alpha(yy, xx), mask);
			else
				mx = maskrng;
				my = maskrng;
				i = xx >= 1 & xx <= X;
				xx = xx(i);
				mx = mx(i);
				i = yy >= 1 & yy <= Y;
				yy = yy(i);
				my = my(i);
				alpha(yy, xx) = min(alpha(yy, xx), mask(my, mx));
			end
		end
	
	otherwise
		
		error('style not coded');
		
end



% find affected pixels
f = find(alpha ~= 1);

% get alpha to apply to existing pixel colors
bg = alpha(f);

% prepare color data to insert
col = col * 255;
fg = (1 - bg) * col;

% get image channel size
S = size(im, 1) * size(im, 2);
N = length(f);
I = [f; f+S; f+S+S];

% extract those pixels of image
pix = double(im(I));

% perform operation
pix = pix .* repmat(bg, [3 1]) + fg(:);

% insert pixels back into image
im(I) = uint8(pix);





%% GET MASK

function mask = get_mask(style, L)

N = 2 * L + 1;
cen = L + 1;
mask = zeros(N);

switch style

	case '.'
		for m = 1:N
			for n = 1:N
				rad = ((m - cen) ^ 2 + (n - cen) ^ 2) / ((L + 1)^2);
				mask(m, n) = 1 - rad;
				if mask(m, n) < 0
					mask(m, n) = 0;
				end
			end
		end
	
end

% invert
mask = 1 - mask;





%% UNIT VECTOR

function v = unit(v)

l = sqrt(sum(v.^2));
if l < 1e-3
	v = [1 0]';
else
	v = v / l;
end





