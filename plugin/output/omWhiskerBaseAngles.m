
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function state = omWhiskerBaseAngles(operation, job, state)

switch operation
	
	case 'info'
		
		state.author = 'Ben Mitch';
		state.prerequisites = {'omSnoutData'};
		
		
		
	case 'parameters'
		
		pars = {};
		
		% parameter
		par = [];
		par.name = 'length';
		par.type = 'scalar';
		par.label = 'Whisker base length (pixels)';
		par.help = 'This many pixels of the whisker are used to derive the base angle.';
		par.range = [1 50];
		par.value = 10;
		par.step = [1 5];
		par.precision = 1;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'discardUnderLength';
		par.label = 'Discard short whiskers';
		par.help = 'If this flag is set, angles for whiskers shorter than "Whisker base length" are not computed and are set to NaN. You should not use this flag with wdIgorMeanAngle.';
		par.type = 'flag';
		par.value = false;
		pars{end+1} = par;
		
% 		% parameter
% 		par = [];
% 		par.name = 'fC';
% 		par.type = 'scalar';
% 		par.label = 'Low-pass cutoff';
% 		par.help = 'The filter used is a zero-phase (two pass) 3rd order Butterworth low-pass.';
% 		par.range = [1 50];
% 		par.value = 25;
% 		par.step = [1 5];
% 		par.precision = 1;
% 		pars{end+1} = par;
		
		state.pars = pars;
		
		
		
	case 'process'

		% extract
		results = job.getResults('all');
% 		fps = job.getMetaData('recordedFrameRate');
		
		% retrieve head orientation
		data = job.getOutput('omSnoutData');
		headOrientation = data.raw.orientation;
		
		% retrieve whisker angles
 		raw = cell(length(state.frameIndices), 1);
		anyResults = false;
		for f = 1:length(state.frameIndices)
			fi = state.frameIndices(f);
	 		result = results{fi};
			ori = headOrientation(f);
			angles = [];
			if isfield(result, 'whisker')
				contours = result.whisker.contour;
				anyResults = true;
				for i = 1:length(contours)
					contour = double(contours{i});
					r = contour(1, :);
					c = contour(2, :);
					con = complex(c, r);
					base = con(1);
					lng = 0;
					for shafti = 2:length(con)
						lng = abs(con(shafti) - base);
						if lng >= state.pars.length
							break
						end
					end
					if lng == 0 || (state.pars.discardUnderLength && lng < state.pars.length)
						angles(end+1) = NaN;
					else
						shaft = con(shafti);
						vec = shaft - base;
						ang = smallangle(angle(vec) - ori, true);
						if ang < 0
							ang = -(pi + ang); % values on the LHS are indicated by being -ve
						else
							ang = pi - ang;
						end
						angles(end+1) = ang;
					end
				end
				raw{f} = single(angles);
			end
		end
		
% 		% filter
% 		if isempty(fps)
% 			filtered = NaN * raw;
% 		else
% 			[b, a] = butter(3, state.pars.fC / (fps / 2));
% 			filtered = filter_nansafe(b, a, raw, true);
% 		end
		
		% store
		state = [];
		state.result = [];
		if anyResults
			state.result.raw = raw;
% 			state.result.filtered = filtered;
		end

	case 'plot'

		% create axes
		state.h_axis = gca;
		
		% collate data
		F = length(state.frameIndices);
		ll = [];
		rr = [];
		for f = 1:F
			
			raw = state.result.raw{f};
			l = raw < 0;
			r = ~l;
			
			l = -raw(l)' * 180/pi;
			r = raw(r)' * 180/pi;
			
			ll = [ll; ones(size(l))*state.frameIndices(f) l];
			rr = [rr; ones(size(r))*state.frameIndices(f) r];
			
		end

		% do plot
		plot(ll(:, 1), ll(:, 2), 'bo');
		hold(state.h_axis, 'on');
		plot(rr(:, 1), rr(:, 2), 'ro');
		
		% do axis		
		v = axis(state.h_axis);
		v(1:2) = state.frameIndices([1 end]);
		axis(state.h_axis, v);
		
		% legend
		legend('left', 'right')
		
		% label plot
		xlabel('frame index');
		ylabel('whisker base angle (degrees)');
		
		
		
	otherwise
		
		state = [];
		
end





%% my library functions

function s = filter_nansafe(b, a, s, bidirectional)

% interp internal NaNs
s = interpnans(s);

% change end NaNs to be equal to the nearest valid value
for c = 1:size(s, 2)
	i = isnan(s(:, c));
	f = find(i);
	fl = [find(~i, 1, 'first') find(~i, 1, 'last')];
	if isempty(fl)
		continue
	end
	vfl = s(fl, c)';
	dfl = [abs(f - fl(1)) abs(f - fl(2))];
	[temp, j] = min(dfl, [], 2);
	s(f, c) = vfl(j)';
end

% augment signal so that we don't get end effects in the
% filter
N = 250;
h = hanning(N);
h1 = repmat(h(1:N/2), 1, size(s, 2));
h2 = repmat(h(N/2+1:end), 1, size(s, 2));
mn = mean(s, 1);
o1 = s(1, :) - mn;
o2 = s(end, :) - mn;
for c = 1:size(s, 2)
	h1(:, c) = h1(:, c) * o1(c) + mn(c);
	h2(:, c) = h2(:, c) * o2(c) + mn(c);
end
s = [
	h1
	s
	h2
	];

% delegate to filter
s = filter(b, a, s);

% if bidirectional
if nargin == 4
	s = flipud(s);
	s = filter(b, a, s);
	s = flipud(s);
end

% unaugment
s = s(N/2+1:end-N/2, :);






function theta = smallangle(theta, polarised)

% theta = smallangle(theta[, polarised])
%   constrain all theta to lie in [0, 2PI). if polarised is
%   passed as "true", the range is modified to [-PI, +PI).

theta = mod(theta, 2*pi);
if nargin == 2
	theta(theta>=pi) = theta(theta>=pi) - 2 * pi;
end





function A = interpnans(A, method)

% function A = interpNaNs(A, method = 'linear')
%
% interpolate through NaNs along the first dimension, so
% that for instance
%
% [NaN NaN 1 NaN NaN 4 NaN]' becomes
% [NaN NaN 1 2 3 4 NaN]'
%
% using the 'linear' method. for other methods, see
% interp1().

if ~exist('method','var')
	method = 'linear';
end

sz = size(A);
N = prod(sz(2:end));

for n = 1:N
	X = A(:,n);
	i_nan = find(isnan(X));
	i_not = find(~isnan(X));
	if length(i_not) > 1
		X(i_nan) = interp1(i_not, X(i_not), i_nan, method);
	end
	A(:,n) = X;
end



