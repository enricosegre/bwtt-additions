
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



function [frames, movieInfo] = stLoadMovieFrames(videoFilename, frameIndices)

% custom stLoadMovieFrames() for Ehud Fonio Binary Files
%
% author mitch (based on existing code in stLoadMovieFrames)
% created 06/05/2011



% open file
f = fopen(videoFilename);

% failsafe loop
try
	
	% read header
	s1 = fgets(f, 512);
	n = [];
	for k = 1:(length(s1)/4)
		b = (k-1) * 4 + 1;
		n(end+1) = s1(b) + s1(b+1)*256 + s1(b+2) * 256 * 256 + s1(b+3) * 256 * 256 * 256;
	end
	
	% construct movie info
	movieInfo = [];
	movieInfo.Width = n(2);
	movieInfo.Height = n(1);
	movieInfo.Size = [movieInfo.Height movieInfo.Width];
	if numel(n) < 9
		error(['no frames available in movie "' videoFilename '"']);
	end
	movieInfo.NumFrames = n(9);
	
	% prepare for frame reading
	N = length(frameIndices);
	numPix = prod(movieInfo.Size);
	frames = cell(1, N);
	
	% read frames
	for i = 1:N
		
		% get frame index
		frameIndex = frameIndices(i);
		if frameIndex < 1 || frameIndex > movieInfo.NumFrames
			error('frameIndex out of range');
		end
		
		% read frame
		theByte = 512 * (1 + frameIndex * ceil(numPix/512));
		fseek(f, theByte, 'bof');
		im = fread(f, numPix, '*uint8');
		if numel(im) ~= numPix
			error('incomplete frame read');
		end
		
		% store frame
		frames{i} = reshape(im, movieInfo.Height, movieInfo.Width);
		
		% NB!!! the above line did read as follows in the original
		% code:
		%
		% frames{i} = reshape(im, movieInfo.Height, movieInfo.Width).';
		%
		% that is, it includes the transpose operation. if this
		% transpose operation were included, then Height and
		% Width would be reversed - perhaps this code was
		% developed using square images, so this was not
		% noticed. if the image _does_ need to be transposed, it
		% must also need to be reshaped by Width and Height, so
		% the following line would, I think, be correct:
		%
		% frames{i} = reshape(im, movieInfo.Width, movieInfo.Height).';
		
	end
	
	% close file
	fclose(f);
	
	
	
catch err
	
	% close file
	fclose(f);
	
	% rethrow
	rethrow(err);
	
end





