
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



function [frames, movieInfo] = stLoadMovieFrames(videoFilename, frameIndices)

% custom stLoadMovieFrames() for the A copies used in Sheffield (ATLAS)
%
% author mitch
% created 22/03/2011

if nargout == 2

	w = warning('off');
	avi = aviinfo(videoFilename);
	warning(w);

	movieInfo = [];
	movieInfo.Width = 1024;
	movieInfo.Height = 1024;
	movieInfo.Size = [1024 1024];
	movieInfo.NumFrames = avi.NumFrames;
	
end



% create frames buffer
frames = cell(1, length(frameIndices));

% open file
fid = fopen(videoFilename, 'r');

% for each frame
for f = 1:length(frameIndices)
	
	frameIndex = frameIndices(f);
	
	% get greyscale frame
	offset = 66056 + (frameIndex - 1) * (1115144-66056);
	fseek(fid, offset, 'bof');
	frames{f} = flipud(reshape(fread(fid, 1024*1024, '*uint8'), 1024, 1024)');

end

% close file
fclose(fid);



