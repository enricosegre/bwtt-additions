function [frames, movieInfo] = stLoadMovieFrames(videoFilename, frameIndices, flags)

% [frames, movieInfo] = stLoadMovieFrames(videoFilename, ...
%                               frameIndices, flags)
%
%   Load the specified frames from videoFilename and,
%   optionally, return a movieInfo structure. Each frame in
%   the array is in greyscale UINT8 format. The frames are
%   packaged either as a cell array (if you pass flag "c")
%   or as an HxWxN uint8 array (if you pass flag "a").
%   Default, if neither flag is supplied, is "c".
%
%   The reader used is that specified by
%   stUserData('videoReader'). If the result of that is
%   "automatic", the function chooses from mmread, mmreader
%   and aviread, prioritised in that order, the reader that
%   can successfully read each file.
%
%   If you have movies that are in a format that
%   stLoadMovieFrames is not equipped to read (perhaps not
%   standard video container files), you can provide a
%   custom videoReader in the folder with them. It should
%   have the following profile:
%
% [frames, movieInfo] = stVideoReader(videoFilename, frameIndices)
%
%   and should return a cell array of frames and a movieInfo
%   structure with the fields "Width", "Height" and "Size"
%   == [Height Width] and "NumFrames". Caching is not
%   currently handled for custom readers (you can provide
%   your own if necessary) but may be handled in a future
%   release.
%
% "flags" can include:
%
%   a: Return "frames" as an HxWxN array rather than a cell
%     array of individual frames.
%
%
%
% See also: stRevision, stProject, stJob, stPlugin, stMergePars, stUserData, stLoadMovieFrames.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% Author: Ben Mitch
% Created: 01/03/2011
%
% Because the world is pretty and sunny and
% filled with little rabbits and other beautiful things,
% shame on you if you make it less beautiful. But do as
% you will with this code, the little rabbits will not
% mind.

% CHANGE LOG
%
% 20/11/14 (mitch)
% aviread/mmreader have been removed in 2014b (at least), so
% I've added support for mathworks' latest attempt at
% reading videos which is "VideoReader". fixed a small bug
% and a couple of commenting improves along the way.



persistent cache



%% SPECIAL MOVE (DEBUG)

if nargin == 0 && nargout == 1
	frames = cache;
	return
end



%% DEFAULTS

% videoFilename
if nargin < 1
	error('must supply "videoFilename"');
end

% frameIndices
if nargin < 2
	error('must supply "frameIndices"');
end

% flags
if nargin < 3
	flags = '';
end



%% BASICS

% check file exists
if ~exist(videoFilename, 'file')
	error(['file "' videoFilename '" not found']);
end



%% USE CUSTOM READER IF ONE IS SUPPLIED

% check for custom reader
videoPath = fileparts(videoFilename);
customReaderFilename = [videoPath '/stLoadMovieFrames_local.m'];
if exist(customReaderFilename, 'file')
	d = pwd;
	try
		cd(videoPath);
		if nargout == 2
			[frames, movieInfo] = stLoadMovieFrames_local(videoFilename, frameIndices);
			cd(d);
			if ~isstruct(movieInfo)
				error('custom stLoadMovieFrames must return movieInfo as a struct array');
			end
		else
			frames = stLoadMovieFrames_local(videoFilename, frameIndices);
			cd(d);
		end
		if ~iscell(frames)
			error('custom stLoadMovieFrames_local must return frames as a cell array');
		end
		if length(frames) ~= length(frameIndices)
			error('custom stLoadMovieFrames_local must return cell array of same length as frameIndices');
		end
		if any(flags == 'a')
			sz = size(frames{1});
			sz = [sz length(frameIndices)];
			frames_ = frames;
			frames = zeros(sz, 'uint8');
			for f = 1:length(frameIndices)
				frames(:, :, f) = frames_{f};
			end
		end
		return
	catch err
		cd(d);
		rethrow(err)
	end
end



%% CACHE

if isempty(cache) || ~strcmp(cache.videoFilename, videoFilename)
	cache = [];
	cache.lastRequest = [];
	cache.videoFilename = videoFilename;
	cache.videoPath = fileparts(videoFilename);
	cache.videoReader = [];
	cache.movieInfo = [];
	cache.frames = {};
	cache.frameIndices = [];
	
	% cached data for individual readers
	cache.mm = [];
	cache.vr = [];
end



%% DETECT INCREMENTAL READS FOR READERS THAT USE READ-AHEAD

deltaFrameIndex = 0;
if isscalar(frameIndices)
	if ~isempty(cache.lastRequest)
		deltaFrameIndex = frameIndices - cache.lastRequest;
		if ~any(deltaFrameIndex == [-1 +1])
			deltaFrameIndex = 0;
		end
	end
	cache.lastRequest = frameIndices;
else
	cache.lastRequest = [];
end



%% AUTOMATICALLY DETECT READER

if isempty(cache.videoReader)

	% check for custom video reader for this folder
	if exist([cache.videoPath '/stVideoReader.m'], 'file')
		
		videoReader = 'custom';
		
	else
	
		% get user's preferred video reader
		videoReader = stUserData('videoReader');

		% handle automatic
		if strcmp(videoReader, 'automatic')

			% set to empty
			videoReader = [];
			errors = '';

			% get the matlab version - this saves us some
			% checks which would otherwise be ugly try/catch
			% pairs (these are inefficient)
			v = version('-release');
			v(v == 'a') = '1';
			v(v == 'b') = '2';
			matlabVersion = str2num(v);
			
% 			warning('debug force version');
% 			matlabVersion = 20142;
			
			% if no reader yet
			if isempty(videoReader)
				
				% in later matlab versions, they switched to
				% VideoReader and recommend that
				if matlabVersion >= 20142
					try
						obj = VideoReader(videoFilename);
						f = obj.read(1);

						% ok, it works
						videoReader = 'videoreader';
					catch err
						errors = [errors 'VideoReader: ' err.message 10 10];
					end
				end
				
			end

			% if no reader yet
			if isempty(videoReader)

				% mmreader is a good bet, because it's
				% native and performs well.
				%
				% mmreader does not work well in older
				% matlab versions. we KNOW the following
				% (update this if you get better info):
				%
				% does not work: R2008b
				% does work: R2009b
				% does not work: R2014b
				if matlabVersion >= 20092 && matlabVersion < 20142
					try
						mm = mmreader(videoFilename);
						f = mm.read(1);

						% ok, it works
						videoReader = 'mmreader';
					catch err
						errors = [errors 'mmreader: ' err.message 10 10];
					end
				end
				
			end

			% if no reader yet
			if isempty(videoReader)

				% mmread is a third-party tool http://goo.gl/hsJThZ
				%
				% if mmread is installed, and works, prefer
				% that, because it's fast.
				w = which('mmread');
				if ~isempty(w)
					try
						cache.mm = mmread(videoFilename, 1);

						% ok, it works
						videoReader = 'mmread';
					catch err
						errors = [errors 'mmread: ' err.message 10 10];
					end
				end

			end

			% if no reader yet
			if isempty(videoReader)

				% aviread is known to have been removed at
				% least by 2014b.
				if matlabVersion < 20142
					try
						w = warning('off');
						f = aviread(videoFilename, 1);
						warning(w);

						% ok, it works
						videoReader = 'aviread';
					catch err
						errors = [errors 'aviread: ' err.message 10 10];
					end
				end
				
			end
			
			% if no reader yet
			if isempty(videoReader)
				
				% report failure to find a reader
				error([errors 'unable find a video reader to access the specified file (see above)']);
				
			end

		end

	end
	
	% cache chosen reader
	cache.videoReader = videoReader;

end



%% ESTABLISH WHICH FRAMES NEED READING, AND WHERE TO PUT THEM

% TODO: this could be a setting for people with plenty RAM
cacheLength = 10;

% cache.frames = {'1' '2' '3' '4' '5'}
% cache.frameIndices = [1 2 3 4 5]

% work out what frames will be in temporarily in the cache
% after this read is completed - some of these may be
% dropped after the read, if necessary to comply with the
% cache size
cacheFrameIndices = cache.frameIndices;
for f = 1:length(frameIndices)
	matchesInCache = cacheFrameIndices == frameIndices(f);
	indexInCache = find(matchesInCache);
	if isempty(indexInCache)
		cacheFrameIndices(1, end+1) = frameIndices(f);
	else
		cacheFrameIndices = [cacheFrameIndices(~matchesInCache) frameIndices(f)];
	end
end
cacheFrameIndices = fliplr(cacheFrameIndices);
L = min(max(cacheLength, length(frameIndices)), length(cacheFrameIndices));
cacheFrameIndices = cacheFrameIndices(1:L);
cacheFrameIndices = fliplr(cacheFrameIndices);

% get indices into new cache of output
outputFrameIndices = [];
for f = 1:length(frameIndices)
	outputFrameIndices(f) = find(cacheFrameIndices == frameIndices(f));
end

% create the new cache frames array
cacheFrames = {};
frameIndices = [];
cacheLocations = [];
for f = 1:length(cacheFrameIndices)
	indexInCache = find(cache.frameIndices == cacheFrameIndices(f));
	if isempty(indexInCache) || isempty(cache.frames{indexInCache})
		cacheFrames{f} = [];
		frameIndices(1, end+1) = cacheFrameIndices(f);
		cacheLocations(1, end+1) = f;
	else
		cacheFrames{f} = cache.frames{indexInCache};
	end
end

% update cache
cache.frames = cacheFrames;
cache.frameIndices = cacheFrameIndices;


	

%% DO THE READ OPERATION

if ~isempty(frameIndices)

	switch cache.videoReader

		case 'aviread'

			% update movieInfo
			if isempty(cache.movieInfo)
				w = warning('off');
				info = aviinfo(videoFilename);
				warning(w);
				cache.movieInfo.Width = info.Width;
				cache.movieInfo.Height = info.Height;
				cache.movieInfo.Size = [info.Height info.Width];
				cache.movieInfo.NumFrames = info.NumFrames;
			end

			% do the raw read
			w = warning('off');
			raw = aviread(videoFilename, frameIndices);
			warning(w);
			
			% generate greyscale frames
			for f = 1:length(frameIndices)

				% convert indexed frame to RGB
 				im = frame2im(raw(f));

				% assume greyscale raw, because it's faster (this could be an
				% option, or we could try and infer this from the file) so
				% we can just use the red channel
 				cache.frames{cacheLocations(f)} = im(:, :, 1);

			end

			

		case 'mmread'

			% handle look-ahead (because mmread() reads much
			% faster in consecutive chunks)
			if (deltaFrameIndex == 1) && isscalar(frameIndices)
				frameIndices = frameIndices + [0:cacheLength-1];
				cacheLocations = cacheLocations + [0:cacheLength-1];
				cache.frameIndices = [cache.frameIndices frameIndices(2:end)];
			end
			
			% do the raw read
			raw = mmread(videoFilename, frameIndices);

			% update movieInfo
			if isempty(cache.movieInfo)
				cache.movieInfo.Width = raw.width;
				cache.movieInfo.Height = raw.height;
				cache.movieInfo.Size = [raw.height raw.width];
				% NB: this may not be accurate - see "help mmread"
				cache.movieInfo.NumFrames = abs(raw.nrFramesTotal);
			end

			% generate greyscale frames
			for f = 1:length(frameIndices)

				% convert indexed frame to RGB
				im = frame2im(raw.frames(f));

				% assume greyscale raw, because it's faster (this could be an
				% option, or we could try and infer this from the file) so
				% we can just use the red channel
				cache.frames{cacheLocations(f)} = im(:, :, 1);

			end

			
			
		case 'mmreader'
			
			% create reader
			if isempty(cache.mm)
				cache.mm = mmreader(videoFilename);
				cache.movieInfo.Width = cache.mm.Width;
				cache.movieInfo.Height = cache.mm.Height;
				cache.movieInfo.Size = [cache.mm.Height cache.mm.Width];
				cache.movieInfo.NumFrames = cache.mm.NumberOfFrames;
			end

			% mmreader reads faster in consecutive chunks, but
			% it's less memory efficient, so we just do a
			% frame-by-frame read at the moment. not sure which is
			% better.
			for f = 1:length(frameIndices)

				% do the raw read
				raw = cache.mm.read(frameIndices(f));

				% assume greyscale raw, because it's faster (this could be an
				% option, or we could try and infer this from the file) so
				% we can just use the red channel
				cache.frames{cacheLocations(f)} = raw(:, :, 1);
				
			end

			
			
		case 'videoreader'
			
			% create reader
			if isempty(cache.vr)
				cache.vr = VideoReader(videoFilename);
				cache.movieInfo.Width = cache.vr.Width;
				cache.movieInfo.Height = cache.vr.Height;
				cache.movieInfo.Size = [cache.vr.Height cache.vr.Width];
				cache.movieInfo.NumFrames = cache.vr.NumberOfFrames;
			end

			% frame by frame read - might be worthy of some
			% optimisation at some point
			for f = 1:length(frameIndices)

				% do the raw read
				raw = cache.vr.read(frameIndices(f));

				% assume greyscale raw, because it's faster (this could be an
				% option, or we could try and infer this from the file) so
				% we can just use the red channel
				cache.frames{cacheLocations(f)} = raw(:, :, 1);
				
			end

			
			
		case 'custom'
			
			% switch to folder
			wd = cd;
			try
				cd(cache.videoPath);
			
				% video reader must support this calling convention
				if isempty(cache.movieInfo)
					[cache.frames(cacheLocations), cache.movieInfo] = stVideoReader(videoFilename, frameIndices);
				else
					cache.frames(cacheLocations) = stVideoReader(videoFilename, frameIndices);
				end

				cd(wd);
			catch err
				cd(wd);
				rethrow(err);
			end
			
			
			
		otherwise
			error(['unrecognised video reader "' cache.videoReader '"']);

	end
	
end



%% RETURN ARGUMENTS

frames = cache.frames(outputFrameIndices);

if nargout == 2
	movieInfo = cache.movieInfo;
end

if nargin == 3 && any(flags == 'a')
	frames_ = zeros(cache.movieInfo.Height, cache.movieInfo.Width, length(frames), 'uint8');
	for f = 1:length(frames)
		frames_(:, :, f) = frames{f};
	end
	frames = frames_;
end



%% FINALLY, TRIM THE CACHE

if length(cache.frameIndices) > cacheLength
	cache.frames = cache.frames(end-cacheLength+1:end);
	cache.frameIndices = cache.frameIndices(end-cacheLength+1:end);
end



