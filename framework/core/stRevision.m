function out = stRevision()

% rev = stRevision()
%
%   Return, in "rev", information about the revision of the
%   BWTT that is installed.
%
%
%
% See also: stRevision, stProject, stJob, stPlugin, stMergePars, stUserData, stLoadMovieFrames.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====




% $Rev:: 17                                                  $
%	$Date:: 2015-06-22 19:53:59 +0300 (Mon, 22 Jun 2015)       $




% $Release:: 1.0.1 %



% unknown
matlabString = 'unknown';
matlab = -1;
revision = -1;
release = 'unknown';
date = -1;
string = 'stRevision failed to get revision information';

% try or fail
try
	
	% get matlab version
	matlabString = version;
	v = version('-release');
	v(v == 'a') = '1';
	v(v == 'b') = '2';
	matlab = str2num(v);
	
	% load file
	path = which('stRevision');
	fid = fopen(path, 'r');
	file = fread(fid, Inf, '*char')';
	fclose(fid);

	% find revision
	p = strfind(file, ['Rev' '::']);
	if ~isempty(p)
		line = file(p+5:end);
		q = strfind(line, '$');
		line = line(1:q(1)-1);
		revision = str2num(line);
	end

	% find date
	p = strfind(file, ['Date' '::']);
	if ~isempty(p)
		line = file(p+7:end);
		q = strfind(line, ' ');
		line = line(1:q(1)-1);
		date = datenum(line);
	end

	% find release
	p = strfind(file, ['Release' '::']);
	if ~isempty(p)
		line = file(p+10:end);
		q = strfind(line, ' ');
		line = line(1:q(1)-1);
		release = line;
	end

	% if "dev" is present, we're on the repo. if
	% not, this is a release.
	on_repo = exist([stGetInstallPath() '/../dev'], 'dir');

	% report string
	if on_repo
		string = ['Working Copy (Rev ' int2str(revision) ', last Rel ' release ')'];
	else
		string = ['Release ' release ' (Revision ' int2str(revision) ')'];
	end

end


% report
if nargout
	
	out = [];
	out.matlab = matlab;
	out.matlabString = matlabString;
	out.revision = revision;
	out.release = release;
	out.date = date;
	out.string = string;
	
else
	
	disp(['BIOTACT Whisker Tracking Tool: ' string]);
	
end






 