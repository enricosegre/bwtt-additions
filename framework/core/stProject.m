% Class "stProject"
%
%   This class represents a tracking "project". That is, a
%   collection of tracking "jobs", with some metadata (e.g.
%   order of appearance in the project). A project can be
%   manipulated at the console, or through the BWTT GUI.
%
%
%
% See also: stRevision, stProject, stJob, stPlugin, stMergePars, stUserData, stLoadMovieFrames.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



% Author: Ben Mitch
% Created: 01/03/2011
%
% Because the world is pretty and sunny and
% filled with little rabbits and other beautiful things,
% shame on you if you make it less beautiful. But do as
% you will with this code, the little rabbits will not
% mind.


%
%
% SEARCH FOR "TODO" for jobs that still need completing.
%
%




classdef stProject < handle
	
	
	
	%% PROPERTIES
	
	properties (Access = private)
		
		% folder name of loaded project
		foldername
		
		% callback functions, if loaded in GUI
		callback
		
		% true if project has been modified
		modified
		
		% last time loaded project was saved
		lastSaveTime
		
		% temporary handles to job files
		jobs
		
		% list of indices of selected jobs
		selected
		
		% index of displayed job
		displayed
		
		% what has been invalidated
		invalidated
		
		% persistent data - this is what gets stored in the
		% project file.
		data
		
	end
	
	
	
	%% STATE MANAGEMENT
	
	methods
		
		function proj = stProject(foldername, callback)
			
			% init instance state
			proj.foldername = '';
			proj.callback = [];
			proj.modified = true; % this causes the callback to get fired on new()/load()
			proj.lastSaveTime = now;
			proj.selected = [];
			proj.displayed = [];
			proj.invalidated = {};
			proj.jobs = {};
			
			% store callback
			if nargin >= 2
				proj.callback = callback;
			end
			
			% load (init data)
			if nargin >= 1 && ~isempty(foldername)
				
				% validate foldername
				proj.foldername = stTidyFilename(foldername);
				if ~exist(proj.foldername, 'dir')
					error(['folder "' proj.foldername '" not found']);
				end
				
				% validate foldername
				projectfilename = [proj.foldername '/stProject.mat'];
				if ~exist(projectfilename, 'file')
					error(['file "' projectfilename '" not found']);
				end
				
				% do load
				l = load(projectfilename);
				
				% validate data
				if ~isfield(l, 'data') || ~isfield(l.data, 'version')
					error(['file "' projectfilename '" is not a BWTT project']);
				end
				if l.data.version > stProjectLatestVersion()
					error(['file "' projectfilename '" is too recent for this version of the BWTT to load']);
				end
				
				% lay in data
				proj.data = l.data;
				
				% load all jobs
				d = dir([proj.foldername '/*.stJob']);
				d = [d; dir([proj.foldername '/*.stJob-locked'])];
				failed = '';
				for di = 1:length(d)
					
					% filename should be passed without any -locked
					% suffix
					filename = [proj.foldername '/' strrep(d(di).name, '-locked', '')];
					
					% load job
					try
						job = stJob(filename, proj);
					catch err
						failed = [failed stProcessPath('justFilename', filename) 10 '  ' err.message 10 10];
						continue
					end

% 					% save job immediately if it was upgraded
% 					if job.flagIsSet('upgraded')
% 						job.save('', true);
% 						job.flagClear('upgraded');
% 					end
					
					% if exists already, discard and warn
					if proj.includesUID(job.getUID())
						warning(['job with UID "' job.getUID() '" was not loaded because that UID was already in use']);
					else
						proj.addJob(job);
					end
					
				end
				
				% warn if any failed
				if ~isempty(failed)
					msg = ['WARNING: the following jobs could not be loaded - they may be corrupt, or they may not be job files.' 10 10 failed];
					proj.log(msg);
					stMessage('large', msg);
				end
				
				% update jobs data
				if ~isempty(proj.data.jobs.order)
					order = [];
					for j = 1:proj.getNumberOfJobs()
						header = proj.getJobByIndex(j).getHeader();
						order(j) = stIsIn(proj.data.jobs.order, header.uid);
					end
					order(order == 0) = 1e8;
					[temp, i] = sort(order);
					proj.reorderJobs(i);
				end
				
				% raise events
				proj.raiseProjectEvent('projectOpened');
				proj.setModified(false);
				proj.refresh();
				
			else
				
				% create new project
				data = [];
				data.id = 'BWTT Project';
				data.version = stProjectLatestVersion();
				
				% header
				data.header.uid = stGenerateUID();
				data.header.creator = stGetUserName();
				data.header.created = now;
				data.header.modified = now;
				
				% jobs
				data.jobs.order = {};
				
				% meta
				data.meta = struct();
				
				% tool data
				data.tool = struct();
				
				% outconf
				data.outconf.pars = struct();
				data.outconf.methods = {};
				
				% lay in data
				proj.data = data;
				
				% raise events
				proj.raiseProjectEvent('projectOpened');
				proj.setModified(false);
				proj.refresh();
				
			end
			
		end
		
		function has = includesUID(proj, uid)
			
			for j = 1:proj.getNumberOfJobs()
				if strcmp(proj.getJobByIndex(j).getUID(), uid)
					has = true;
					return
				end
			end
			has = false;
			
		end
		
		function delete(proj)
			
			% if we're deleting the project and we haven't saved,
			% we want to discard changes to jobs (but let them get
			% unlocked if they're locked)
			for p = 1:proj.getNumberOfJobs()
				proj.getJobByIndex(p).discard();
			end
			
			% delete all jobs
			proj.jobs = {};
			
			% raise event
			proj.raiseProjectEvent('projectClosed');
			
		end
		
		function foldername = saveCreate(proj)
			
			% get folder name
			[a, b] = uiputfile('*');
			if isnumeric(a)
				foldername = [];
				return
			end
			
			% fail if folder exists in any form
			foldername = stTidyFilename([b a]);
			if exist(foldername, 'file') || exist(foldername, 'dir')
				error(['cannot save to "' foldername '", file or folder exists']);
			end
			
			% try to create
			backupfoldername = [foldername '/backup'];
			success = mkdir(backupfoldername);
			trashfoldername = [foldername '/trash'];
			success = success && mkdir(trashfoldername);
			outputfoldername = [foldername '/output'];
			success = success && mkdir(outputfoldername);
			if ~success
				error(['cannot save to "' foldername '", failed to create folder structure']);
			end
			
		end
		
		function foldername = save(proj)
			
			% assume not creating
			creating = false;
			
			% get a foldername if we are currently only in memory
			if isempty(proj.foldername)
				creating = true;
				proj.foldername = proj.saveCreate();
				if isempty(proj.foldername)
					foldername = [];
					return
				end
			end
			
			% take copy of data
			data = proj.data;
			
			% update project header
			data.header.modified = now;
			
			% update job order data
			order = {};
			for j = 1:proj.getNumberOfJobs()
				header = proj.getJobByIndex(j).getHeader();
				order{j} = header.uid;
			end
			data.jobs.order = order;
			
			% save all modified jobs
			for j = 1:proj.getNumberOfJobs()
				job = proj.getJobByIndex(j);
				if job.isModified() || creating
					if job.isInMemory() || creating
						header = job.getHeader();
						filename = [proj.foldername '/' header.uid '.stJob'];
						job.save(filename);
					else
						job.save();
					end
				end
			end
			
			% try to save the project
			projectfilename = [proj.foldername '/stProject.mat'];
			save(projectfilename, 'data');
			
			% mark save
			proj.lastSaveTime = now;
			
			% set modified state
			proj.setModified(false);
			
			% return the foldername
			foldername = proj.foldername;
			
			% always invalidate the modified state, to force an
			% update to the title bar display if the save was a
			% saveCreate (in which case, the project name has
			% changed from its UID to its filename)
			proj.invalidate('modified');
			
		end
		
% 		function autosave(proj)
% 			
% 			% raise event
% 			proj.raiseProjectEvent('autosaveBegin');
% 			
% 			% do autosave
% 			proj.log('autosave...')
% 			
% 			% mark
% 			proj.lastSaveTime = now;
% 			
% 			% raise event
% 			proj.raiseProjectEvent('autosaveEnd');
% 			
% 		end
		
		% 			% handle char for history
		% 			if ischar(history)
		% 				history = 0;
		% 			end
		%
		% 			% backup save
		% 			if history
		%
		% 				% warn
		% 				if isempty(tempdir)
		% 					uiwait(msgbox('Save history is on but your temporary directory is not set. Unable to keep history.', 'Warning', 'warn', 'modal'));
		% 					return
		% 				end
		%
		% 				% make sure path exists
		% 				path = [tempdir '/autosave'];
		% 				if ~exist(path, 'dir')
		% 					mkdir(path);
		% 				end
		%
		% 				% delete/roll existing
		% 				backupFilenameBase = [path '/' project.header.uid];
		% 				lastfile = '';
		% 				for n = 25:-1:1
		% 					file = [backupFilenameBase '-' int2str(n) '.mat'];
		% 					if exist(file, 'file')
		% 						if n >= history
		% 							% delete
		% 							delete(file);
		% 						else
		% 							% roll
		% 							movefile(file, lastfile);
		% 						end
		% 					end
		% 					lastfile = file;
		% 				end
		%
		% 				% create new
		% 				if history
		% 					file = [backupFilenameBase '-1.mat'];
		% 					copyfile(filename, file);
		% 				end
		%
		% 			end
		%
		% 		end
		
		function name = getName(proj)
			
			if isempty(proj.foldername)
				name = proj.data.header.uid;
			else
				name = proj.foldername;
			end
			
		end
		
		function foldername = getFolderName(proj)
			
			foldername = proj.foldername;
			
		end
		
	end
	
	
	
	
	
	
	%% MODIFIED
	
	methods
		
		function saveable = getSaveable(proj)
			
			% if modified
			saveable = proj.modified;
			
			% OR, if not yet saved
			if isempty(proj.foldername)
				saveable = true;
			end
			
		end
		
		function modified = getModified(proj)
			
			modified = proj.modified;
			
		end
		
		function setModified(proj, modified)
			
			% default
			if nargin < 2
				modified = true;
			end
			
			% set state
			modifiedStateChanged = proj.modified ~= modified;
			proj.modified = modified;
			
			% invalidate
			if modifiedStateChanged
				proj.invalidate('modified');
			end
			
% 			% autosave
% 			if (now - proj.lastSaveTime) > (stUserData('autoSaveInterval') * 60)
% 				proj.autosave();
% 			end
			
		end
		
	end
	
	
	
	
	%% META DATA
	
	methods
		
		function value = getMetaData(proj, key)
			
			if isfield(proj.data.meta, key)
				value = proj.data.meta.(key);
			else
				value = [];
			end
			
		end
		
		function setMetaData(proj, key, value)
			
			proj.data.meta.(key) = value;
			proj.setModified();
			
		end
		
	end
	
	
	
	
	
	
	%% TOOL DATA
	
	methods
		
		function runTool(proj, toolName)
			
			% retrieve tool data
			data = [];
			if isfield(proj.data.tool, toolName)
				data = proj.data.tool.(toolName);
			end
			
			% run tool
			
			% store data
			proj.data.tool.(toolName) = data;
			proj.setModified();
			
		end
		
% 		function setActiveTool(proj, toolName)
% 			
% 			proj.activeTool = toolName;
% 			
% 		end
% 		
% 		function data = getToolData(proj)
% 			
% 			if isempty(proj.activeTool)
% 				data = [];
% 			else
% 				
% 			end
% 			
% 		end
		
	end
	
	
	
	
	%% OUTPUT
	
	methods
		
		function outconf = getOutputConfig(proj)
			
			outconf = proj.data.outconf;
			
		end
		
		function setOutputConfig(proj, outconf)
			
			changed = ~stIdentical(proj.data.outconf, outconf);
			proj.data.outconf = outconf;
			if changed
				proj.setModified();
			end
			
		end
		
	end
	
	
	
	
	%% JOB LIST MANAGEMENT
	
	methods
		
		function invalidate(proj, item)
			
			proj.invalidated = union(proj.invalidated, item);
%   			proj.log(['INVALIDATE: ' item]);
			
		end
			
		function refresh(proj)
			
			% Refresh project-dependent display elements
			%
			% proj.refresh()
			
%   			proj.log('REFRESH');

			% if the joblist is invalidated, there is no need to
			% process invalidated jobs, because they will all get
			% updated anyway when the joblist is updated.
			nojobs = ismember('joblist', proj.invalidated);

			% update invalidated jobs
			for j = 1:proj.getNumberOfJobs()
				
				% even if "nojobs", we still call isInvalidated()
				% because it clears the invalidated flag...
				if proj.getJobByIndex(j).isInvalidated()

					% but we only send the event if not "nojobs"
					if ~nojobs
						proj.raiseProjectEvent('jobInvalidated', struct('job', proj.getJobByIndex(j), 'indexInProject', j));
					end
					
				end
			end
			
			% handle hierarchical invalidated items, each of which
			% subsumes those that follow it
			if ismember('joblist', proj.invalidated)
				proj.raiseProjectEvent('jobListChanged');
			elseif ismember('selected', proj.invalidated)
				proj.raiseProjectEvent('jobSelectedChanged');
			elseif ismember('displayed', proj.invalidated)
				proj.raiseProjectEvent('jobDisplayedChanged');
			end

			% handle modified item
			if ismember('modified', proj.invalidated)
				proj.raiseProjectEvent('modifiedStateChanged', struct('modified', proj.modified));
			end
			
			% clear all invalidated items
			proj.invalidated = {};
			
% 			proj.log('REFRESH OUT');
			
		end
		
		function numberOfJobs = getNumberOfJobs(proj)

			numberOfJobs = length(proj.jobs);

		end
		
		function job = getJobByIndex(proj, index)
			
			job = proj.jobs{index};
			
		end
		
		function job = getDisplayedJob(proj)
			
			if isempty(proj.displayed)
				job = [];
			else
				job = proj.jobs{proj.displayed};
			end
			
		end
		
		function index = getDisplayedJobIndex(proj)
			
			index = proj.displayed;
			
		end
		
		function jobs = getSelectedJobs(proj)
			
			jobs = proj.jobs(proj.selected);
			
		end
		
		function is = isDisplayed(proj, job)
			
			if isempty(proj.displayed)
				is = false;
			else
				is = job == proj.jobs{proj.displayed};
			end
			
		end
		
		function addJob(proj, job)
			
			proj.jobs{end+1} = job;
			proj.setModified();
			proj.invalidate('joblist');
			
		end
		
		function deleteJobs(proj, indices)
			
			if ~isempty(indices)
				keep = setdiff(1:proj.getNumberOfJobs(), indices);
				proj.jobs = proj.jobs(keep);
				proj.setModified();
				proj.invalidate('joblist');
			end
			
		end

		function reorderJobs(proj, order)
			
			if length(order) ~= length(proj.jobs)
				error('cannot reorder jobs - order is wrong length');
			end
			proj.jobs = proj.jobs(order);
			proj.setModified();
			proj.invalidate('joblist');
			
		end

		function setSelected(proj, selected)
			
			if ~stIdentical(selected, proj.selected)
				
				% get list of jobs that are being deselected, so we
				% can advise them to free their JIT data
				deselected = setdiff(proj.selected, selected);
				if ~isempty(deselected)
					for d = deselected
						proj.jobs{d}.deselect();
					end
				end				
				
				proj.selected = selected;
				if isempty(selected)
					proj.setDisplayed([]);
				else
					% keep existing if possible, else choose first of selected
					if isempty(proj.displayed) || ~any(proj.displayed == proj.selected)
						proj.setDisplayed(selected(1));
					end
				end
				proj.invalidate('selected');
				
			end
			
		end
		
		function setDisplayed(proj, displayed)
			
			if ~stIdentical(displayed, proj.displayed)
				if ~isempty(proj.displayed)
					proj.jobs{proj.displayed}.invalidate();
				end
				proj.displayed = displayed;
				if ~isempty(proj.displayed)
					proj.jobs{proj.displayed}.invalidate();
				end
				proj.invalidate('displayed');
			end
			
		end
		
	end
	
	
	
	
	%% JOB OPERATIONS
	
	methods
		
		function runSelectedJobs(proj)
			
			% pare
			if ~pareSelection(proj, [], [stJob.TASK_CREATED])
				return
			end
			selected = proj.selected;
			
			% make sure they are all saved
			permission = false;
			for j = 1:proj.getNumberOfJobs()
				if proj.getJobByIndex(j).isModified()
					if ~permission
						if ~stConfirm('Before processing any jobs, all jobs in the project need to be saved. Do you want to continue and save all jobs?');
							return
						end
						permission = true;
					end
					proj.getJobByIndex(j).save();
				end
				proj.getJobByIndex(j).release();
			end
			proj.refresh();
			
			% check if they are all parametrized
			for j = selected
				pard = ~isempty(proj.getJobByIndex(j).getUserData('parametrized'));
				if ~pard
					if ~stConfirm('unparametrized', 'Some of the selected jobs have not been marked as parametrized. Are you sure that they all have the correct parameters?');
						return
					end
					break;
				end
			end
			
			% get filenames
			filenames = {};
			for j = selected
				filenames{end+1} = stProcessPath('justFilename', proj.getJobByIndex(j).getVideoFilename());
			end
			[mode, flagUpdateOutputs] = stProcessGUI(filenames);
			
			% collect errors, show them at the end
			errors = {};
			
			% get output configuration data
			outconf = proj.getOutputConfig();
 			            
			% handle mode
			switch mode
				
				case 0
					return
					
				case 1
					
					% lock them all in advance
					for j = selected
						proj.getJobByIndex(j).lock();
					end
					
					% run them
					cancel = false;
					for j = selected
						try

							% housekeeping
							proj.setDisplayed(j);
							proj.refresh();
							job = proj.getJobByIndex(j);
							
							% run task
							job.taskRun();
							
							% update outputs
							if flagUpdateOutputs
								for m = 1:length(outconf.methods)
									job.updateOutput(outconf.methods{m}, outconf.pars);
								end
							end

							% save job
							job.save();
							
						catch err
							cancel = strcmp(err.identifier, 'stJob:UserCancel');
							errors{end+1} = err;
							try
								proj.getJobByIndex(j).save();
							end
						end
						if cancel
							break
						end
					end

				case 2
					
					% warning
					if stConfirm('Parallel execution is experimental - do you want to continue? Please let us know how it goes if you do.')
					
						% load them all separately from the project so
						% that they're not linked to any of the GUI
						filenames = {};
						for j = selected
							filenames{end+1} = proj.getJobByIndex(j).getFilename();
						end

						% run them
						% matlabpool open  %obsolesced; instead:
                        poolobj=parpool;
						parfor j = 1:length(filenames)
							try
								% need to set so it's not empty, but not
								%   like this
                                % proj.callback='@bwtt/projectEvent';

								progress = 0;
								job = stJob(filenames{j});
								progress = 1;
								job.lock();
								progress = 2;
								job.taskRun();
								progress = 3;
								
								% update outputs
								if flagUpdateOutputs
									for m = 1:length(outconf.methods)
										job.updateOutput(outconf.methods{m}, outconf.pars);
									end
								end
								progress = 4;
								
								job.save();
								progress = 5;
								job.release();
								progress = 6;
								
							catch err
								disp(err);
								switch progress
									case {0 1 6 5}
										disp(['********' 10 'WARNING Job "' filenames{j} '" failed during cycling - compute this job in the GUI for more information' 10 '********']);
									case {2 3}
										% error during taskRun() or updateOutput()
										job.save();
										job.release();
									case {4}
										% error during save()
										job.release();
										disp(['********' 10 'WARNING Job "' filenames{j} '" failed to save, though it processed correctly - compute this job in the GUI for more information' 10 '********']);
								end
							end
							job = [];
						end
						% matlabpool close % obsolesced
                        delete(poolobj)

						% reload them in the GUI
						for j = selected
							proj.getJobByIndex(j).lock();
							proj.getJobByIndex(j).release();
						end
						
					end

				case 3
					stMessage('I''m still thinking about how best to implement this!');
					
			end
			
			% errors
			if ~isempty(errors)
				stMessage('I am sorry. Some jobs did not complete correctly - these will be marked with a red cross in the jobs list. You can view the error messages from each individual job by selecting the job and pressing CTRL+E. I don''t know what that means, perhaps you do!');
			end
			
		end
		
		function undeleteJobs(proj)
			
			% get list of jobs in trash
			uids = stUndeleteGUI(proj.foldername);
			
			% undelete them
			if ~isempty(uids)
				
				% select none
				proj.setSelected([]);
				
				% do the undelete
				fail = false;
				selected = [];
				for u = 1:length(uids)
					src = [proj.foldername '/trash/' uids{u} '.stJob'];
					dst = [proj.foldername '/' uids{u} '.stJob'];
					if movefile(src, dst)
						proj.addJob(stJob(dst, proj));
						selected(end+1) = proj.getNumberOfJobs();
					else
						fail = true;
					end
				end
				if fail
					stMessage('I wish that I could have undeleted all of the jobs you selected, but I couldn''t. I am sorry, I am just a pony. You can probably do it manually; just move the files from the trash folder inside the project folder up into the main project folder, and reload the project. Good luck, neigh!');
				end
				
				% select undeleted
				proj.setSelected(selected);
				
			end
			
		end
		
		function selected = selectJobs(proj, selected)
			
			% handle char
			if ischar(selected)
				
				switch selected
					
					case 'all'
						% select all jobs that are not LE
						selected = [];
						for j = 1:proj.getNumberOfJobs()
							[state, elsewhere] = proj.getJobByIndex(j).getState();
							if ~elsewhere
								selected(end+1) = j;
							end
						end
						
					case 'ready'
						% select all jobs that are ready to run (TASK_CREATED)
						selected = [];
						for j = 1:proj.getNumberOfJobs()
							[state, elsewhere] = proj.getJobByIndex(j).getState();
							taskState = proj.getJobByIndex(j).getCurrentTaskState();
							if taskState == stJob.TASK_CREATED && ~elsewhere
								selected(end+1) = j;
							end
						end
						
					case 'with'
						% select all jobs that have the same task
						% content as the displayed job
						selected = [];
						disp = proj.getDisplayedJob();
						if ~isempty(disp)
							hash = disp.getCurrentTaskHash();
							for j = 1:proj.getNumberOfJobs()
								if strcmp(proj.getJobByIndex(j).getCurrentTaskHash(), hash)
 									selected(end+1) = j;
 								end
							end
						end						
				end
				
			end
			
			% handle int32 (delta)
			if isa(selected, 'int32')
				delta = double(selected);
				selected = proj.selected;
				if isempty(selected)
					return
				end
				selected = selected(1) + delta;
				if selected < 1 || selected > proj.getNumberOfJobs()
					return
				end
			end
			
			% store
			proj.setSelected(selected);
			
		end
		
		function sortJobs(proj, type)
			
			nJobs = proj.getNumberOfJobs();
			
			switch type
				
				case 'taskStatus'
					sortKeys = {};
					for j = 1:nJobs
						[state, errors, pipelines] = proj.getJobByIndex(j).getCurrentTaskState();
						key = [num2str(state) '_' num2str(isempty(errors)) '_' proj.getJobByIndex(j).getCurrentTaskHash()];
						sortKeys{end+1} = key;
					end
					[temp, order] = sort(sortKeys);
					
				case 'taskHash'
					sortKeys = {};
					for j = 1:nJobs
						key = proj.getJobByIndex(j).getCurrentTaskHash();
						sortKeys{end+1} = key;
					end
					[temp, order] = sort(sortKeys);
					
				case 'alphaPartial'
					sortKeys = {};
					for j = 1:nJobs
						sortKeys{end+1} = stProcessPath('justFilename', proj.getJobByIndex(j).getVideoFilename());
					end
					[temp, order] = sort(sortKeys);
					
				case 'alphaFull'
					sortKeys = {};
					for j = 1:nJobs
						sortKeys{end+1} = proj.getJobByIndex(j).getVideoFilename();
					end
					[temp, order] = sort(sortKeys);
					
				case 'modification'
					sortKeys = [];
					for j = 1:nJobs
						header = proj.getJobByIndex(j).getHeader();
						sortKeys(end+1) = -header.modified;
					end
					[temp, order] = sort(sortKeys);
					
				case 'moveUp'
					if isempty(proj.selected) || min(proj.selected) == 1
						return
					end
					notselected = find(~ismember(1:nJobs, proj.selected));
					order = zeros(1, nJobs);
					order(proj.selected - 1) = proj.selected;
					order(order == 0) = notselected;
					
				case 'moveToTop'
					if isempty(proj.selected)
						return
					end
					notselected = find(~ismember(1:nJobs, proj.selected));
					order = [proj.selected notselected];
					
				case 'moveDown'
					if isempty(proj.selected) || max(proj.selected) == nJobs
						return
					end
					notselected = find(~ismember(1:nJobs, proj.selected));
					order = zeros(1, nJobs);
					order(proj.selected + 1) = proj.selected;
					order(order == 0) = notselected;
					
				case 'moveToBottom'
					if isempty(proj.selected)
						return
					end
					notselected = find(~ismember(1:nJobs, proj.selected));
					order = [notselected proj.selected];
					
				case 'selectedToTop'
					if isempty(proj.selected)
						return
					end
					notselected = find(~ismember(1:nJobs, proj.selected));
					order = [proj.selected notselected];
					
				otherwise
					error('2: Variable not found');
					
			end

			% select none, but store selected
			selected = logical(zeros(1, nJobs));
			selected(proj.selected) = 1;
			proj.setSelected([]);
			
			% do the sort, with events
			proj.reorderJobs(order);
			selected = find(selected(order));
			
			% select originally selected
			proj.setSelected(selected);
			
		end
		
		function addJobs(proj, videoFilenames)

			n = proj.getNumberOfJobs();
			if ~isempty(videoFilenames)
				for v = 1:length(videoFilenames)
					proj.addJob(stJob(videoFilenames{v}, proj));
				end
				proj.setModified();
				proj.setMetaData('mruJobDirectory', fileparts(videoFilenames{1}));
				proj.setSelected(n+1:proj.getNumberOfJobs());
			end
			
		end
		
		function result = pareSelection(proj, jobStates, taskStates, allowDisplayed)

			% arguments
			if nargin < 4
				allowDisplayed = false;
			end

			% original selection
			oldselected = proj.selected;
			
			% deselect LE jobs
			selected = [];
			for j = proj.selected
				
				% if allowDisplayed, allow displayed
				if ~allowDisplayed || (j ~= proj.displayed)
				
					% pare on elsewhere
					[jobState, elsewhere] = proj.getJobByIndex(j).getState();
					if elsewhere
						continue
					end
					
				end
				
				% ok
				selected(end+1) = j;
				
			end
			
			% reselect
			pared = length(selected) < length(proj.selected);
			proj.setSelected(selected);
			proj.refresh();
			
			% handle elsewhere message
			if pared
				stMessage('Some of the selected jobs were locked elsewhere and cannot be operated upon. It''s fun being a pony!');
			end
			
			% deselect pared jobs
			selected = [];
			for j = proj.selected
				
				% if allowDisplayed, allow displayed
				if ~allowDisplayed || (j ~= proj.displayed)
				
					% pare on job state
					if ~isempty(jobStates)
						[jobState, elsewhere] = proj.getJobByIndex(j).getState();
						if ~any(jobState == jobStates)
							continue;
						end
					end

					% pare on task state
					if ~isempty(taskStates)
						taskState = proj.getJobByIndex(j).getCurrentTaskState();
						if ~any(taskState == taskStates)
							continue;
						end
					end

				end
				
				% ok
				selected(end+1) = j;
				
			end

			% reselect
			pared = length(selected) < length(proj.selected);
			proj.setSelected(selected);
			proj.refresh();
			
			% warnings
			exclusion = '';
			result = true;
			if isempty(selected)
				stMessage(['None of the selected jobs are suitable for this operation, so the operation will not be performed.' 10 10 exclusion]);
				proj.setSelected(oldselected);
				proj.refresh();
				result = false;
			elseif pared
				if ~stConfirm(['Some of the selected jobs are not suitable for this operation - do you want to perform the operation on the remaining jobs?' 10 10 exclusion]);
					result = false;
					proj.setSelected(oldselected);
					proj.refresh();
				end
			end
			
		end
		
		function updateOutput(proj, jobIndices, methods)

			outconf = proj.getOutputConfig();
			
			if nargin < 2
				jobIndices = 1:proj.getNumberOfJobs();
			end
			
			if nargin < 3
				methods = outconf.methods;
			end
			
			% update outputs
			proj.log('updating output for all specified jobs...');
			for j = jobIndices
				job = proj.getJobByIndex(j);
				for m = 1:length(methods)
					job.updateOutput(methods{m}, outconf.pars);
				end
			end
			
		end
		
		function export = export(proj, jobIndices)
			
			if nargin < 2
				jobIndices = 1:proj.getNumberOfJobs();
			end
			
			if ischar(jobIndices)
				switch jobIndices
					case 'selected'
						jobIndices = proj.selected;
					otherwise
						error(jobIndices);
				end
			end
			
			% update outputs
			proj.updateOutput(jobIndices);
			
			export = [];
			export.meta.created = datestr(now);
			export.meta.bwtt = stRevision;
			export.meta.project = proj.data.header;
			export.meta.project.version = proj.data.version;
			export.index = struct();
			export.jobs = {};
			
			% list all used methods for citation information
			methods = {};
			
			for j = jobIndices
				job = proj.getJobByIndex(j);
				mm = job.getAllUsedMethods;
				for i = 1:length(mm)
					m = mm{i};
					m = m{2};
					methods = union(methods, m);
				end
				jj = [];
				jj.uid = job.getUID();
				jj.videoFilename = job.getVideoFilename();
				jj.videoBasename = stProcessPath('justBasename', job.getVideoFilename());
				jj.jobFilename = job.getFilename();
				jj.frameIndices = job.getFrameRange('export');
% 				jj.meta = job.getMetaData();
				jj.results = job.getResults(jj.frameIndices);
				jj.outputs = job.getOutputs();
				export.jobs{end+1} = jj;
				export.index.(job.getUID()) = length(export.jobs);
			end
			
			% add citation information
			citation = false;
			coauth = false;
			methodlist = '';
			for m = 1:length(methods)
				cit = stGetCitation(methods{m});
				if ~isempty(cit)
					citation = true;
					if strcmp(cit, 'co-authorship')
						coauth = true;
					end
					methodlist = [methodlist methods{m}, ', '];
				end
			end
			if citation
				export.citation = 'Citations are required.';
				msg = ['See http://bwtt.sourceforge.net/fairuse for details.'];
				msg = ['You have used the following methods: ' methodlist(1:end-2) '.' 10 msg];
				msg = ['Citations are required.' 10 msg];
				if coauth
					msg = ['Co-authorship may be requested.' 10 msg];
					export.co_authorship = 'Co-authorship may be requested.';
				end
				export.methods_used = methodlist(1:end-2);
				export.moreinfo = 'See http://bwtt.sourceforge.net/fairuse for details.';
			else
				msg = ['None of the methods you have used require citations.'];
				export.citation = 'No citations are required.';
			end
			citinfo = '';
			citinfo = [citinfo '--- BWTT MESSAGE: CITATION INFORMATION FOR THIS EXPORT ---' 10];
			citinfo = [citinfo msg];
			disp(citinfo);
			if coauth && stUserData('showCARequirement')
				doc = [ ...
					'Some of your exported data relies on a method which has not yet been published ' ...
					'and you may be requested to offer co-authorship of the first publication ' ...
					'you make using this method. Please visit http://bwtt.sourceforge.net/fairuse for details. ' ... 
					'The list of methods you have used is included below.' 10 10 ...
					'Click on Accept if you understand this message, otherwise please click Cancel to ' ...
					'abandon the export operation. ' 10 10 ...
					'NB: You can hide this message in Preferences. If you do, please make sure to ensure ' ...
					'fair use yourself.' 10 10 citinfo];
				response = stMessageBoxGUI(doc, 'fairuse', 'Co-authorship Requirement');
				if ~response
					error('User did not accept FAIRUSE - unable to continue');
				end
			end
		
		end
		
		function actOnSelectedJobs(proj, action, data)
		
			% check some are selected
			if isempty(proj.selected)
				stMessage('No jobs are selected, silly!');
				return
			end

			% delegate
			switch action

				case 'clearFlags'
					if proj.pareSelection([], [])
						for j = proj.selected
							proj.getJobByIndex(j).setUserData('parametrized', []);
						end
						proj.log(['cleared flags from ' int2str(length(proj.selected)) ' jobs']);
					end

				case 'findMissing'
					if proj.pareSelection([], [])
            areMissing = [];
            for j = proj.selected
              job = proj.getJobByIndex(j);
              vf = job.getVideoFilename();
              if ~exist(vf, 'file')
                proj.log(['MISSING: ' vf]);
                areMissing(1, end+1) = j;
              end
            end
            
            % none?
            if isempty(areMissing)
              stMessage('None of the selected jobs have missing video files.');
              return
            end
            
            % confirm?
            proj.setSelected(areMissing);
            proj.refresh();
						if ~stConfirm([int2str(length(areMissing)) ' jobs (as shown) have a missing video file. Are you sure you want to search for these video files in a specified directory (any changes made are not reversible)?'])
							return
            end
            
            % ask for search directory
            warnMissingInfo = true;
            mru = proj.getMetaData('mruJobDirectory');
           	foldername = uigetdir(mru, 'Choose the top level directory in which to search. Sub-directories will also be searched.');
            if ~isnumeric(foldername)
              for j = areMissing
                job = proj.getJobByIndex(j);
                vf = job.getVideoFilename();
                [a, b, c] = fileparts(vf);
                vf = [b c];
                vff = stFindFileInTree(vf, foldername);
                if ~isempty(vff)
                  proj.log(['FOUND: ' vf]);
                  videoInfo = job.getVideoInfo();
                  if isempty(videoInfo)
                    if warnMissingInfo
                      if ~stConfirm('medium', ['At least one job (see log window) had no video information stored in it, so I cannot confirm that the video I found is a match by resolution and frame count. Do you want to use it anyway, based only on its matching filename?' 10 10 'NB: If you answer yes, you will not be asked again if this condition arises for other videos.'])
                        return
                      end
                      warnMissingInfo = false;
                    end
                  else
                    % video info must match as well as
                    % filename
                    [firstFrame, info] = stLoadMovieFrames(vff, 1);
                    if stIdentical(info, videoInfo)
                      job.setVideoFilename(vff);
                    else
                      proj.log(['FAIL MATCH VIDEO INFO: ' vf]);
                    end
                  end
                else
                  proj.log(['NOT FOUND: ' vf]);
                end
              end
              
              % since the "displayed" job in the GUI caches
              % information, it may have the old filename
              % stored in it. we do this to force it to be
              % refreshed.
              proj.setSelected([]);
              proj.refresh();
              
            end
					end

				case 'clearOutputs'
					if proj.pareSelection([], [])
						for j = proj.selected
							proj.getJobByIndex(j).clearOutputs();
						end
						proj.log(['cleared output from ' int2str(length(proj.selected)) ' jobs']);
					end
					
				case 'parametrize'
					if proj.pareSelection([], [stJob.TASK_CREATED])
						if ~isempty(proj.selected)
							stParametersGUI(proj.getSelectedJobs());
						end
					end

				case 'deparametrize'
					if proj.pareSelection([], [])
						if ~stConfirm('Are you sure you want to remove all parameters from the selected jobs? The parameters for those jobs will return to the defaults.')
							return
						end
						for j = proj.selected
							proj.getJobByIndex(j).clearParameters();
							proj.getJobByIndex(j).setUserData('parametrized', []);
						end
					end

				case 'propagateParameters'
					if proj.pareSelection([], [stJob.TASK_CREATED], true)
						selected = setdiff(proj.selected, proj.displayed);
						if ~isempty(selected)
							
							% show GUI
 							srcjob = proj.getJobByIndex(proj.displayed);
							prop = stPropagateGUI(srcjob);
							if ~isempty(prop)
							
								if ~stConfirm('medium', ['Are you sure you want to propagate the selected parameters from the displayed job to all the selected jobs?' 10 10 'NB: This will OVERWRITE these parameters of the selected jobs!' 10 10 'NB: Parameters that are marked as not to be propagated (such as locations in the frame) will not be affected by this operation.'])
									return
								end
								allsrcpars = srcjob.getAllParameters();
								
								for m = 1:length(prop)
									
									cachekey = prop{m};
									pos = find(cachekey == '_', 1, 'first');
									pipeline = cachekey(1:pos-1);
									method = cachekey(pos+1:end);
									defpars = stPlugin(method, 'parameters');

									methodsrcpars = allsrcpars.(cachekey);
									
									for j = selected
										
										dstjob = proj.getJobByIndex(j);
										methoddstpars = dstjob.getParameters(pipeline, method);
										
										resultingpars = [];
										
										for p = 1:length(defpars)
											defpar = defpars{p};
											if strcmp(defpar.type, 'constant')
												% if constant, no need to have it in
												% resultingpars
												continue
											end
											if ~defpar.propagate
												if isfield(methoddstpars, defpar.name)
													% if present in dst, keep that
													% value
													resultingpars.(defpar.name) = methoddstpars.(defpar.name);
												end
												% but in any case, do not propagate
												continue
											end
											if isfield(methodsrcpars, defpar.name)
												% if present in src, use that
												resultingpars.(defpar.name) = methodsrcpars.(defpar.name);
											else
												% otherwise, leave absent, so
												% default is used - this means that
												% parameters that are missing in the
												% src will be set to default in the
												% dst, which is correct
											end
										end
										
										dstjob.setParameters(pipeline, method, resultingpars);
									
										% can't set this flag - may not have set
										% all pars...
% 										proj.getJobByIndex(j).setUserData('parametrized', true);
									end
								end
								
							end
						end
					end

				case 'close'
					if proj.pareSelection([], [stJob.TASK_FINISHED])
						for j = proj.selected
							proj.getJobByIndex(j).taskClose();
						end
					end

				case 'release'
					if proj.pareSelection(stJob.STATE_LOCKED, [])
						for j = proj.selected
							proj.getJobByIndex(j).release();
						end
					end

				case 'stampOnLock'
					
					% confirm
					if stConfirm('large', ...
						['Jobs are usually marked LE (locked elsewhere) because they are in use elsewhere.' ...
						' In this case, manually unlocking them will lead to DATA CORRUPTION.' 10 10 ...
						'However, if Matlab crashes whilst jobs are locked, they may be left in the locked state.' ...
						' In this case, the only solution is to manually unlock them.' 10 10 ...
						'If you click Yes, the selected jobs that are LE will be manually unlocked.' ...
						' It is up to you to be ABSOLUTELY SURE that the jobs are not open elsewhere.' ...
						' If you are SURE that these jobs are marked LE because of a Matlab crash, go ahead and click Yes to recover the jobs.'] ...
					)
						
						jobs = proj.getSelectedJobs();
						actonjobs = {};
						for j = 1:length(jobs)
							[jobState, elsewhere] = jobs{j}.getState();
							if elsewhere
								actonjobs{end+1} = jobs{j};
							end
						end

						% act
						if ~isempty(actonjobs)

							for j = 1:length(actonjobs)
								job = actonjobs{j};
								job.stampOnLock();
% 								job.lock();
							end

						end
						
					end

				case 'restart'
					if proj.pareSelection([], [stJob.TASK_STARTED stJob.TASK_FINISHED])
						if ~stConfirm('Are you sure you want to restart the current task of the selected jobs? Any previously computed results will be gone too thoroughly for a little pony to recover them.')
							return
						end
						for j = proj.selected
							proj.getJobByIndex(j).taskRestart();
						end
					end

				case 'delete'
					if ~proj.pareSelection([stJob.STATE_READONLY], [])
						stMessage('Jobs cannot be deleted unless they are in the RO (read only) state. Save them, unlock them, then delete them.');
						return
					end

					% confirm
					if ~stConfirm('Are you sure you want to sent the selected jobs to the trash? They will probably be recoverable but... I''m only a pony.')
						return
					end
					
					% unselect
					selected = proj.selected;
					proj.setSelected([]);

					% get UIDs before we close them
					uids = {};
					for j = selected
						header = proj.getJobByIndex(j).getHeader();
						uids{1, end+1} = header.uid;
					end

					% close the deleted jobs
					proj.deleteJobs(selected);

					% and send the job files to the trash
					success = true;
					for uid = uids
						filename = [proj.foldername '/' uid{1} '.stJob'];
						trashfilename = [proj.foldername '/trash/' uid{1} '.stJob'];
						if ~movefile(filename, trashfilename)
							success = false;
						end
					end

					% error
					if ~success
						stError('Failed to delete all selected jobs - those that failed will be present next time you load the project.');
					end

				case 'createTask'
					if ~proj.pareSelection([], [stJob.TASK_ABSENT stJob.TASK_CREATED])
						return
					end

					% warn on overwrite existing tasks
					for j = proj.selected
						taskState = proj.getJobByIndex(j).getCurrentTaskState();
						if taskState == stJob.TASK_CREATED
							if ~stConfirm('medium', [ ...
									'Some of the selected jobs already have a current task. ' ...
									'If you continue, these will be overwritten.' 10 10 ...
									'This will not overwrite any results, since these tasks have not been processed yet, but I thought I''d come to see you and give you this friendly warning anyway. ' ...
									'I love helping my friends!' 10 10 ...
									'Do you want to overwrite these tasks? If you click No I''ll cancel this operation. Neigh-ei-eigh!!!'])
								return
							end
							break
						end
					end

					% create
					pipelines = data;
					for j = proj.selected
						state = proj.getJobByIndex(j).getCurrentTaskState();
						switch state
							case stJob.TASK_CREATED
								proj.getJobByIndex(j).taskDelete();
						end
						proj.getJobByIndex(j).taskCreate(pipelines);
						proj.getJobByIndex(j).setUserData('parametrized', []);
					end

			end
				
		end
		
	end
	
	
	
	
	
	
	%% EVENT PROCESSING
	
	methods
		
		function out = log(proj, msg)
			
			if ~isempty(proj.callback)
				out = proj.raiseProjectEvent('log', struct('msg', msg));
			else
				disp(msg);
			end
			
		end
		
		function cancel = progress(proj, fraction)
			
			if ~isempty(proj.callback)
				cancel = proj.raiseProjectEvent('progress', struct('fraction', fraction, 'offerPause', true));
			else
				disp(['progress: ' int2str(fraction*100) '%']);
				cancel = false;
			end
			
		end
		
		function indexInProject = getJobIndex(proj, job)
			
			for j = 1:proj.getNumberOfJobs()
				if proj.getJobByIndex(j) == job
					indexInProject = j;
					return
				end
			end
			
			indexInProject = [];
			
		end
		
		function result = jobEvent(proj, eventType, data)
			
			switch eventType
				case {'progress' 'log'}
				otherwise
					proj.log(['$[JOB] ' eventType]);
			end
			
			switch eventType
				
				case {'log' 'progress'}
					
					% propagate
					result = proj.raiseProjectEvent(eventType, data);
					
				case {'jobInvalidated' 'jobFrameRangeChanged'}
					
					% propagate
					data.indexInProject = proj.getJobIndex(data.job);
					result = proj.raiseProjectEvent(eventType, data);
					
				case {'jobProcessFrame'}
					
					% propagate
% 					data.indexInProject = proj.getJobIndex(data.job);
					result = proj.raiseProjectEvent(eventType, data);
					
				otherwise
					proj.log(['(UNRECOGNISED JOB EVENT: ' eventType ')']);
					
			end
			
		end
		
		function result = raiseProjectEvent(proj, eventType, data)
			
			% empty data
			if nargin < 3
				data = struct();
			end
			
			% default data
			switch eventType
				case {'jobListChanged' 'jobSelectedChanged' 'jobDisplayedChanged'}
					% job list update event (3 severities)
					data = [];
					data.jobs = proj.jobs;
					data.selected = proj.selected;
					data.displayed = proj.displayed;
			end
			
			% side effects
			switch eventType
				case {'jobInvalidated' 'jobFrameRangeChanged'}
					if data.job.isModified()
						% mark project as modified
						proj.setModified(true);
					end
			end
			
			% raise event
			if ~isempty(proj.callback)
				data.project = proj;
				result = proj.callback(eventType, data);
			else
				result = [];
			end
			
		end
		
	end
	
	
	
	
	
	
	
	
	
	
	
	%% INFORMATION
	
	methods
		
		function info = jobInfo(proj)
			
			info = '';
			for j = proj.selected
				info = [info proj.getJobByIndex(j).getInfo() 10];
			end
			
		end
		
	end
	
	
	
	% 		function showInfo(proj, h_parent)
	%
	% 			info = [];
	% 			info = [info 'Filename:     ' proj.filename 10];
	% 			info = [info 'UID:          ' proj.project.header.uid 10];
	% 			info = [info 'Version:      ' int2str(proj.project.version) 10];
	% 			info = [info 'Owner:        ' proj.project.header.owner 10];
	% 			info = [info 'Created:      ' datestr(proj.project.header.created) 10];
	% 			info = [info 'Modified:     ' datestr(proj.project.header.modified) 10];
	% 			info = [info '-' 10];
	% 			info = [info 'Item Count:   ' int2str(length(proj.project.jobs)) 10];
	% 			project = proj.project;
	% 			w = whos('project');
	% 			info = [info 'Project Size: ' int2str(w.bytes / 1024 / 1024) ' MB' 10];
	% 			info = [info 10];
	%
	% 			% display it
	% 			stReportGUI(h_parent, info, 'Project Information');
	%
	% 		end
	
	% 		function t = getSecsSinceLastSave(proj)
	%
	% 			t = round((now - proj.lastSaveTime) * 24 * 3600);
	%
	% 		end
	
	
	
	% 		function uid = getUID(proj)
	%
	% 			uid = proj.project.header.uid;
	%
	% 		end
	
	% 		function saveCopy(proj, filename)
	%
	% 			% extract
	% 			project = proj.project;
	%
	% 			% drop cache (we don't store this to disk)
	% 			project.cache = struct();
	%
	% 			% do save
	% 			save(filename, 'project');
	%
	% 		end
	
	
	
	
	%% HIDE SUPERCLASS METHODS
	
	methods (Hidden = true)
		
		function out = vertcat(p, q)
			error('stJob:MethodNotImplemented', 'concatenation is not supported');
		end
		
		function out = horzcat(p, q)
			error('stJob:MethodNotImplemented', 'concatenation is not supported');
		end
		
		function out = cat(dim, p, q)
			error('stJob:MethodNotImplemented', 'concatenation is not supported');
		end
		
		function out = ge(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = le(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = lt(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = gt(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = eq(p, q)
			out = eq@handle(p, q);
		end
		
		function out = ne(p, q)
			out = ne@handle(p, q);
		end
		
		function out = addlistener(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
		function out = findobj(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
		function out = findprop(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
		function out = notify(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
	end
	
	
	
	
end





%% LATEST VERSION

function version = stProjectLatestVersion()

% latest version supported by this class
version = 10;

end





