
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function [mode, updateOutputs] = stProcessGUI(filenames)

% pars
h_parent = gcbf;
wh = [480 480];

% create
gui = stGUI(wh, h_parent, 'Process jobs');
rect = gui.rootPanel;
h_dialog = gui.getMainWindow();

% layout
buts = rect.pack([], 'bottom', 32);
rect = rect.pack([], 'bottom', -1);

% message
h_text = uicontrol(h_dialog, ...
	'style', 'text', ...
	'hori', 'left', ...
	'fontname', 'Arial', ...
	'fontsize', 14, ...
	'string', 'Processing Mode', ...
	'units', 'pixels' ...
	);
rect.pack(h_text, 'top', 24);

% processing mode selector
h_mode = uibuttongroup(h_dialog, ...
	'visible', 'on' ...
	);
uh = 24;
temp = rect.pack([], 'top', uh*3);
temp.pack([], 'left', 16);
mode = temp.pack([], 'left', -1);
h_radio = uicontrol(h_mode, ...
	'Style', 'Radio', ...
	'fontname', 'Helvetica', ...
	'fontsize', 10, ...
	'UserData', 1, ...
	'String', 'Immediate Sequential Local Processing' ...
	);
mode.pack(h_radio, 'top*', uh);
h_radio = uicontrol(h_mode, ...
	'Style', 'Radio', ...
	'fontname', 'Helvetica', ...
	'fontsize', 10, ...
	'UserData', 2, ...
	'String', 'Immediate Parallel Local Processing (matlabpool, parfor)' ...
	);
mode.pack(h_radio, 'top*', uh);
h_radio = uicontrol(h_mode, ...
	'Style', 'Radio', ...
	'fontname', 'Helvetica', ...
	'fontsize', 10, ...
	'UserData', 3, ...
	'String', 'Parallel Batch (prepare script for external processing)' ...
	);
mode.pack(h_radio, 'top*', uh);

% message
h_text = uicontrol(h_dialog, ...
	'style', 'text', ...
	'hori', 'left', ...
	'fontname', 'Arial', ...
	'fontsize', 14, ...
	'string', 'Additional Operations', ...
	'units', 'pixels' ...
	);
rect.pack(h_text, 'top', 24);

% selector
h_updateOutputs = uicontrol(h_dialog, ...
	'style', 'checkbox', ...
	'hori', 'left', ...
	'fontname', 'Helvetica', ...
	'fontsize', 10, ...
	'string', 'Update Outputs at the same time', ...
	'Value', true, ...
	'units', 'pixels' ...
	);
rect.pack(h_updateOutputs, 'top', 24);

% message
h_text = uicontrol(h_dialog, ...
	'style', 'text', ...
	'hori', 'left', ...
	'fontname', 'Arial', ...
	'fontsize', 14, ...
	'string', 'Job List', ...
	'units', 'pixels' ...
	);
rect.pack(h_text, 'top', 24);

% selector
h_list = uicontrol(h_dialog, ...
	'style', 'listbox', ...
	'hori', 'left', ...
	'fontname', stUserData('listFontName'), ...
	'fontsize', stUserData('listFontSize'), ...
	'BackgroundColor', [1 1 1], ...
	'string', filenames, ...
	'Min', 1, 'Max', 3, ...
	'Value', [], ...
	'units', 'pixels' ...
	);
rect.pack(h_list, 'top', -1);

% buttons
buttons = {'OK' 'Cancel'};
bw = 64;
for b = 1:length(buttons)
	h_buttons(1, b) = uicontrol(h_dialog, ...
		'style', 'pushbutton', ...
		'fontname', 'Helvetica', ...
		'fontsize', 10, ...
		'userdata', b, ...
		'callback', @button_callback, ...
		'string', buttons{b}, ...
		'units', 'pixels' ...
		);
end

% layout buttons
for b = 1:length(h_buttons)
	buts.pack(h_buttons(b), 'right', bw);
end

% layout
gui.resize();

% wait
mode = 0;
updateOutputs = false;
gui.waitForClose();






	function button_callback(h_button, evt)
		
		response = buttons{get(h_button, 'userdata')};
 		if strcmp(response, 'OK')
			mode = get(get(h_mode, 'SelectedObject'), 'UserData');
			updateOutputs = get(h_updateOutputs, 'value');
 		end
		gui.close();
		
	end



end




