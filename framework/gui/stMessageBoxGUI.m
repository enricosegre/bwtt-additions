
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function response = stMessageBoxGUI(msg, IconString, title)

% get callback figure
h_parent = gcbf;



%% PREAMBLE

% defaults
OKstring = 'OK';
CANCELstring = '';

% default icon string
if nargin < 2
	IconString = 'help';
end

% default title
if nargin < 3
	title = 'BIOTACT Whisker Tracking Tool';
end



%% HANDLE IconString

switch IconString
	
	case 'eula'
		IconString = 'warning';
		OKstring = 'Accept';
		CANCELstring = 'Cancel';
		title = [title ' - EULA'];
		
	case 'fairuse'
		IconString = 'info';
		OKstring = 'Accept';
		CANCELstring = 'Cancel';
		title = [title ' - FAIRUSE'];
		
	case 'info'
		title = [title ' - Information'];
		
	case 'help'
		title = [title ' - Help'];
		
	case {'warn' 'warning'}
		title = [title ' - Warning'];
		
	case 'error'
		title = [title ' - Error'];
		
		% translate error object
		if isa(msg, 'MException') || (isstruct(msg) && isfield(msg, 'stack'))
			err = msg;
			if ~isempty(err.identifier)
				msg = [err.identifier 10 10 '    ' err.message 10];
			else
				msg = [err.message 10];
			end
			msg = [msg '___________________________________________' 10 10];
			for s = 1:length(err.stack)
				s = err.stack(s);
				if s.line
					msg = [msg '    ' s.name];
					msg = [msg ' (line ' int2str(s.line) ')'];
					msg = [msg 10];
				end
			end
		end
	
	otherwise
		error('IconString must be "info", "warning" or "error"'); 
		
end


% add revision
rev = stRevision();
msg = [msg 10 '___________________________________________' 10 10];
msg = [msg 'BIOTACT Whisker Tracking Tool ' rev.string 10];
msg = [msg 'Matlab ' rev.matlabString];
if ispc
	msg = [msg ' (PC)'];
elseif ismac
	msg = [msg ' (MAC)'];
elseif isunix
	msg = [msg ' (NIX)'];
else
	msg = [msg ' (UNKNOWN OS)'];
end

% split msg
if ~isempty(msg)
	msg = stStringExplode(char(10), msg);
end
				



%% HANDLE METRICS

% size and position
wh = [600 300];
pos = stPositionDialog(h_parent, wh);

% create it
h_dialog = dialog( ...
	'Name', title, ...
	'Units', 'pixels', ...
	'Position', pos, ...
	'WindowStyle', 'modal');
	



%% DO LAYOUT

% pars
pars = [];
pars.margin = 8;
pars.bh = 32;

% rectangle
b = 10;
rect = [b+[1 1] wh-2*b];

% split for buttons/text
[right, left] = pack(rect, 'right', 51);

% create edit control
h_edit = create_control(h_dialog, right, msg, wh);

% create button
[left, item] = pack(left, 'top', pars.bh);
h_control = uicontrol(h_dialog, ...
	'Position', item, ...
	'String', OKstring, ...
	'Callback', @close_OK, ...
	'Style', 'Pushbutton');

% create button
if ~isempty(CANCELstring)
	[left, item] = pack(left, 'top', pars.bh);
	h_control = uicontrol(h_dialog, ...
		'Position', item, ...
		'String', 'Cancel', ...
		'Callback', @close_CANCEL, ...
		'Style', 'Pushbutton');
end

% create button
[left, item] = pack(left, 'top', pars.bh);
h_control = uicontrol(h_dialog, ...
	'Position', item, ...
	'String', 'Copy', ...
	'Callback', @copy_to_clipboard, ...
	'Style', 'Pushbutton');



%% DO ICON

IconHeight = 51;
[left, item] = pack(left, 'top', IconHeight);

% fail silently if fail
try
	
	% create an axes for the icon
	IconAxes=axes(                                   ...
		'Parent'          ,h_dialog               , ...
		'Units'           ,'pixels'                , ...
		'Position'        ,item                 , ...
		'Tag'             ,'IconAxes'                ...
		);
	
	if ~strcmp(IconString,'custom')
		a = load('dialogicons.mat');
		switch(IconString)
			case {'warn' 'warning'}
				IconData=a.warnIconData;
				a.warnIconMap(256,:)=get(h_dialog,'color');
				IconCMap=a.warnIconMap;
				
			case {'help' 'info'}
				IconData=a.helpIconData;
				a.helpIconMap(256,:)=get(h_dialog,'color');
				IconCMap=a.helpIconMap;
				
			case 'error'
				IconData=a.errorIconData;
				a.errorIconMap(146,:)=get(h_dialog,'color');
				IconCMap=a.errorIconMap;
		end
	end
	
	% place the icon
	Img = image('CData',IconData,'Parent',IconAxes);
	set(h_dialog, 'Colormap', IconCMap);
	if ~isempty(get(Img,'XData')) && ~isempty(get(Img,'YData'))
		set(IconAxes          , ...
			'XLim'            ,get(Img,'XData')+[-0.5 0.5], ...
			'YLim'            ,get(Img,'YData')+[-0.5 0.5]  ...
			);
	end
	
	set(IconAxes          , ...
		'Visible'         ,'off'       , ...
		'YDir'            ,'reverse'     ...
		);
	
end



%% OK, WAIT FOR USER

% wait for closure
response = 0;
uiwait(h_dialog);






%% CALLBACK OK

	function close_OK(obj, evt)
		
		response = 1;
		close(h_dialog);
		
	end



%% CALLBACK CANCEL

	function close_CANCEL(obj, evt)
		
		close(h_dialog);
		
	end



%% CALLBACK COPY

	function copy_to_clipboard(obj, evt)
		
		try
			
			s = get(h_edit, 'String');
			ss = '';
			for l = 1:size(s, 1);
				line = s(l, :);
				f = find(line ~= 32, 1, 'last');
				if isempty(f) line = ''; else line = line(1:f); end;
				ss = [ss line 10];
			end
			clipboard('copy', ss);
			
		catch err
			
			errordlg('Copy failed - try manually using the mouse.', 'Copy Failed');
			rethrow(err)
			
		end
		
	end



%% CREATE CONTROL FUNCTION

	function [h_control] = create_control(h_parent, rect, value, wh)
		
		h = wh(2) - 68;
		bgcol = [1 1 1];
		pos = rect; %[rect.x rect.y-h rect.w h];
		
		h_control = uicontrol(h_parent, ...
			'Position', pos, ...
			'String', value, ...
			'FontName', 'Tahoma', ...
			'FontSize', 10, ...
			'Max', 2, ...
			'Min', 0, ...
			'HorizontalAlignment', 'left', ...
			'BackgroundColor', bgcol, ...
			'Style', 'edit');
		
	end



%% RECT PACK FUNCTION

	function [a, b] = pack(a, side, size)
		
		b = a;
		if any(side == '*')
			margin = 0;
			side = side(side ~= '*');
		else
			margin = pars.margin;
		end
		
		switch side
			case 'top'
				b(4) = size;
				b(2) = a(2) + a(4) - size;
				a(4) = a(4) - size - margin;
			case 'bottom'
				b(4) = size;
				a(4) = a(4) - size - margin;
				a(2) = a(2) + size + margin;
			case 'right'
				b(3) = size;
				b(1) = a(1) + a(3) - size;
				a(3) = a(3) - size - margin;
			case 'left'
				b(3) = size;
				a(3) = a(3) - size - margin;
				a(1) = a(1) + size + margin;
			case 'shrink'
				b(1:2) = b(1:2) + size;
				b(3:4) = b(3:4) - 2 * size;
		end
		
	end



end

