
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



classdef stGUI < handle
	
	properties
		
		h_dialog
		rootPanel
		resizeFcn
		closeFcn
		errorFcn
		margin
		
	end
	
	methods
		
		function gui = stGUI(wh, h_parent, title, varargin)
			
			if mod(length(varargin), 2)
				error('expects key/value pairs only after main parameters');
			end
			
			resizable = 'off';
			windowStyle = 'modal';
			gui.closeFcn = '';
			keyPressFcn = '';
			gui.margin = 8;
			for a = 1:2:length(varargin)
				switch varargin{a}
					case 'resizable'
						resizable = varargin{a+1};
					case 'style'
						windowStyle = varargin{a+1};
					case 'CloseRequestFcn'
						gui.closeFcn = varargin{a+1};
					case 'ErrorFcn'
						gui.errorFcn = varargin{a+1};
					case 'ResizeFcn'
						gui.resizeFcn = varargin{a+1};
					case 'KeyPressFcn'
						keyPressFcn = varargin{a+1};
					case 'margin'
						gui.margin = varargin{a+1};
					otherwise
						error(['unrecognised "' varargin{a} '"']);
				end
			end
			
			% determine position
			if nargin < 1
				wh = [480 320];
			end
			if nargin < 2
				h_parent = [];
			end
			pos = stPositionDialog(h_parent, wh);
			
			% default title
			if nargin < 3
				title = 'Preferences';
			end
			
			% create dialog
			gui.h_dialog = dialog( ...
				'Name', title, ...
				'Units', 'pixels', ...
				'Position', pos, ...
				'UserData', gui, ...
				'ResizeFcn', @resize, ...
				'KeyPressFcn', keyPressFcn, ...
				'CloseRequestFcn', @close, ...
				'Resize', resizable, ...
				'WindowStyle', windowStyle);
			
			% root panel
			gui.rootPanel = stPanel([], []);
			
		end
		
		function noInterrupts(gui)
			
			% prevent events interrupting other events
			hh = findall(gui.h_dialog);
			for h_ = hh'
				set(h_, 'Interruptible', 'off', 'BusyAction', 'cancel');
			end
			
		end
		
		function setResizeFcn(gui, func)
			
			gui.resizeFcn = func;
			
		end
		
		function waitForClose(gui)
			
			try
				if ishandle(gui.h_dialog)
					uiwait(gui.h_dialog);
				end
			end
			
		end
		
		function h = getMainWindow(gui)
			
			h = gui.h_dialog;
			
		end
		
		function close(gui)
			
			try
				
				if ~isempty(gui.closeFcn)
					gui.closeFcn();
				else
					gui.delete();
				end
				
			catch err
				
				if ~isempty(gui.errorFcn)
					gui.errorFcn(err);
				else
					rethrow(err);
				end
				
			end
			
		end
		
		function delete(gui)
			
			if ishandle(gui.h_dialog)
				delete(gui.h_dialog);
			end
			
		end
		
		function resize(gui)
			
			try
				
				% since Matlab R2014b, this function gets called before the
				% dialog window has been created. we can detect this
				% because pp comes back empty. as a result, once the caller
				% has populated the GUI, they will now have to explicitly
				% call resize() to force the layout to get done, since the
				% resize() call that would previously have arrived on yield
				% will now not arrive.
				pp = get(gui.h_dialog, 'position');
				
				if ~isempty(pp)
					gui.rootPanel.resize([gui.margin+[1 1] pp(3:4)-[1 1]*2*gui.margin]);
					
					if ~isempty(gui.resizeFcn)
						gui.resizeFcn(gui);
					end
				end
				
			catch err
				
				if ~isempty(gui.errorFcn)
					gui.errorFcn(err);
				else
					rethrow(err);
				end
				
			end
			
		end
		
	end
	
end



function resize(obj, evt)

gui = get(obj, 'UserData');
gui.resize();

end

function close(obj, evt)

gui = get(obj, 'UserData');
gui.close();

end




