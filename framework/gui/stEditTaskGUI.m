
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function pipelines = stEditTaskGUI(pipelines)



%% GET PLUGINS

persistent plugins
if isempty(plugins)
	plugins = getPlugins();
end



%% CREATE GUI

% create GUI
gui = stGUI([720 320], gcbf, 'Edit Task', ...
	'CloseRequestFcn', @callback_Close, ...
	'ErrorFcn', @callback_Error, ...
	'style', 'modal', ...
	'resizable', 'off');
panelRoot = gui.rootPanel;
h_dialog = gui.getMainWindow();
theCallbackError = [];
guipars = stGUIPars;



%% CREATE LAYOUT

% buttons
panelButtons = panelRoot.pack([], 'bottom', 24);
BW = 80;
SH = 24;
panelButtons.pack(createButton('Cancel', @callback_Close), 'right', BW);
panelButtons.pack(createButton('OK', @callback_OK), 'right', BW);

% for each pipeline
for p = 1:length(plugins)
	
	plugin = plugins{p};
	h_frame = createPanel(upper(plugin.pipelineName));
	panelPipeline = panelRoot.pack(h_frame, 'left', -1);
	panelPipeline.setChildMargin([8 8 8 20]);
	
	% find pipeline
	methods = {};
	for i = 1:length(pipelines)
		if strcmp(pipelines{i}.name, plugin.pipelineName)
			methods = pipelines{i}.methods;
			break
		end
	end
	
	% create selector
	h_select = createListbox(plugin.p, @callback_SelectMethod, true);
	panelPipeline.pack(h_select, 'top', -1);
	inc = [];
	dec = [];
	for m = 1:length(plugin.p)
		f = find(ismember(methods, plugin.p{m}));
		if ~isempty(f)
			inc(f) = m;
		else
			dec(end+1) = m;
		end
	end
	set(h_select, 'string', plugin.p([inc dec]), 'value', 1:length(inc));
	plugins{p}.hp = h_select;
	
	% create selector
	h_select = createSelector(plugin.t, @callback_SelectMethod);
	panelPipeline.pack(h_select, 'bottom', SH);
	for m = 1:length(methods)
		f = find(ismember(plugin.t, methods{m}));
		if ~isempty(f)
 			set(h_select, 'value', f);
		end
	end
	plugins{p}.ht = h_select;
	
	% create selector
	h_select = createSelector(plugin.d, @callback_SelectMethod);
	panelPipeline.pack(h_select, 'bottom', SH);
	for m = 1:length(methods)
		f = find(ismember(plugin.d, methods{m}));
		if ~isempty(f)
 			set(h_select, 'value', f);
		end
	end
	plugins{p}.hd = h_select;
	
	% update pipelines
	updatePipeline(p);
	
end



%% FINALIZE

drawnow
for p = 1:3
	dress(plugins{p}.hp, p);
	dress(plugins{p}.hd, p);
	dress(plugins{p}.ht, p);
end



%% INITIALISE AND WAIT

% no interrupts
gui.noInterrupts();

% layout
gui.resize();

% wait for GUI
pipelines = {};
gui.waitForClose();

% check for error
if ~isempty(theCallbackError)
	rethrow(theCallbackError);
end



%% CALLBACKS

	function s = getStrings(obj)
		
			% get value
			value = get(obj, 'value');
			string = get(obj, 'string');
			s = string(value)';
			
	end

	function callback_SelectMethod(obj, evt)

		try

			% get context
			p = get(obj, 'userdata');
			dress(obj, p);
			
			% re-order
			switch get(obj, 'style')
				case 'listbox'
					v = get(obj, 'value');
					s = get(obj, 'string');
					i = ismember(1:length(s), v);
 					s = cat(1, s(i), s(~i));
					set(obj, 'string', s, 'value', 1:sum(i));
			end
			
			% update pipeline
			updatePipeline(p);

		catch err
			callback_ErrorFcn(err);
		end
		
	end

	function updatePipeline(p)

		plugin = plugins{p};

		% construct methods
		methods = {};

		% get each
		methods = cat(2, methods, getStrings(plugin.hp));
		methods = cat(2, methods, getStrings(plugin.hd));
		methods = cat(2, methods, getStrings(plugin.ht));

		% remove (do nothing)
		i = ismember(methods, '(do nothing)');
		methods = methods(~i);

		% lay in
		plugins{p}.methods = methods;
		
	end

	function callback_ErrorFcn(err)

		% store error
		theCallbackError = err;

		% force close gui
		gui.delete();
		
	end

	function callback_OK(varargin)
		
		% construct pipelines
		for p = 1:length(plugins)
			plugin = plugins{p};
			if ~isempty(plugin.methods)
				pp = [];
				pp.name = plugin.pipelineName;
				pp.methods = plugin.methods;
				pipelines{end+1} = pp;
			end
		end
		
		% force close gui
		gui.delete();
		
	end

	function callback_Close(varargin)

		% force close gui
		gui.delete();
		
	end



%% HELPERS

	function dress(h, p)
		
		cols = {[1 0 0] [0 0.5 0] [0 0 1]};
		col = cols{p};
		
		set(h, 'UserData', p);
		set(h, 'ForegroundColor', col);
		
		switch get(h, 'style')
			
			case 'listbox'
				try
					jScrollPane = stFindJObj(h, 'persist');
					jListbox = jScrollPane.getViewport.getComponent(0);
					col = java.awt.Color(col(1), col(2), col(3));
					set(jListbox, 'SelectionBackground', col);
					set(jListbox, 'RequestFocusEnabled', false);
				end

			case 'popupmenu'
				v = get(h, 'value');
				if v == 1
					set(h, 'ForegroundColor', col);
					set(h, 'BackgroundColor', [1 1 1]);
				else
					set(h, 'ForegroundColor', [1 1 1]);
					set(h, 'BackgroundColor', col);
				end
				
		end
		
	end

	function h = createSelector(string, callback)
		
		h = uicontrol(h_dialog, ...
			'Style', 'popupmenu', ...
			'String', string, ...
			'BackgroundColor', [1 1 1], ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize, ...
			'Max', 1, ...
			'Min', 0, ...
			'Callback', callback ...
			);
		
	end

	function h = createListbox(string, callback, multiselect)

		h = uicontrol(h_dialog, ...
			'Style', 'Listbox', ...
			'String', string, ...
			'Min', 100, 'Max', multiselect+101, ...
			'Units', 'pixels', ...
			'BackgroundColor', [1 1 1], ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize, ...
			'Value', [], ...
			'Callback', callback ...
			);

	end

	function h = createPanel(text)
		
		h = uipanel(h_dialog, ...
			'Title', text ...
			);
		
	end

	function h = createButton(text, callback)
		h = uicontrol(h_dialog, ...
			'String', text, ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize, ...
			'Callback', callback, ...
			'Style', 'Pushbutton');
	end

	function plugins = getPlugins()

		% get directory
		direc = stPlugin();
		
		% create
		plugins = {};
		pipelineNames = {'object' 'snout' 'whisker'};
		
		% for each pipeline
		for p = 1:3

			% pipeline
			pp = [];
			pp.pipelineName = pipelineNames{p};
			
			% for each domain
			for d = 'pdt'
				
				if d == 'p'
					role = 'pp';
					pp.(d) = {};
				else
					role = [pp.pipelineName(1) d];
					pp.(d) = {'(do nothing)'};
				end
				
				list = direc.(role).list;
				for l = 1:length(list)
					pp.(d){end+1} = list{l};
				end

			end
			
			% lay in
			plugins{p} = pp;
			
		end

	end

end


