
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function data = stAnyParsGUI(data, titl)



%% CREATE GUI

% create GUI
gui = stGUI([640 640], gcbf, titl, ...
	'CloseRequestFcn', @callback_Cancel, ...
	'ErrorFcn', @callback_ErrorFcn, ...
	'style', 'modal', ...
	'resizable', 'off');
panelRoot = gui.rootPanel;
h_dialog = gui.getMainWindow();
theCallbackError = [];
guipars = stGUIPars();



%% CREATE STATE

state = [];
state.currentMethod = [];
state.pars = [];



%% CREATE LAYOUT

% create method selector
panelMethodSelector = panelRoot.pack(createSelector(data.methods, @callback_SelectMethod), 'top', 24);

% create parameter area
panelButtons = panelRoot.pack([], 'bottom', 24);
panelPars = panelRoot.pack([], 'top', -1);

% create buttons
BW = 80;
panelButtons.pack(createButton('Cancel', @callback_Cancel), 'right', BW);
panelButtons.pack(createButton('OK', @callback_OK), 'right', BW);



%% INITIALISE AND WAIT

% no interrupts
gui.noInterrupts();

% layout
gui.resize();

% initialise
callback_SelectMethod();

% wait for GUI
gui.waitForClose();

% check for error
if ~isempty(theCallbackError)
	rethrow(theCallbackError);
end



%% CALLBACKS

	function callback_SelectMethod(obj, evt)

		try
			
			% retrieve current pars
			retrieve();
		
			% change to new
			state.currentMethod = get(panelMethodSelector.getH(), 'Value');
			methodName = data.methods{state.currentMethod};

			% get new pars
			if isfield(data.pars, methodName)
				pars = data.pars.(methodName);
			else
				pars = [];
			end
			
			% initialise panelPars
			panelPars.clear();
			state.pars = stPars([], [], pars, methodName, panelPars, 2, h_dialog, @callback_ErrorFcn, [], stClickLink([]));
			panelPars.resize();

		catch err
			callback_ErrorFcn(err);
		end
		
	end

	function callback_ErrorFcn(err)

		% store error
		theCallbackError = err;

		% force close gui
		gui.delete();
		
	end

	function callback_OK(varargin)
		
		% retrieve current pars
		retrieve();

		% force close gui
		gui.delete();
		
	end

	function callback_Cancel(varargin)
		
		% return empty
		data = [];
		
		% force close gui
		gui.delete();
		
	end



%% HELPERS

	function retrieve()
		
			% retrieve existing
			if ~isempty(state.pars)
				methodName = data.methods{state.currentMethod};
				data.pars.(methodName) = state.pars.getPars();
			end
		
	end

	function h = createSelector(options, callback)
		
		h = uicontrol(h_dialog, ...
			'Style', 'popupmenu', ...
			'String', options, ...
			'BackgroundColor', [1 1 1], ...
			'Max', 1, ...
			'Min', 0, ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize, ...
			'Callback', callback ...
			);
		
	end

	function h = createButton(text, callback, tooltip)

		if nargin < 3
			tooltip = '';
		end

		h = uicontrol(h_dialog, ...
			'Style', 'Pushbutton', ...
			'Units', 'pixels', ...
			'String', text, ...
			'Tooltip', tooltip, ...
			'Callback', callback, ...
			'FontName', guipars.fontName, ...
			'FontSize', guipars.fontSize ...
			);

	end

end


