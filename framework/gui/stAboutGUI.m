
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% author ben mitch
% created 05 Jul 2012

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function stAboutGUI

% pars
h_parent = gcbf;
wh = [480 480];

% create
gui = stGUI(wh, h_parent, 'About');

% basic information
revision = stRevision();

% layout
rect = gui.rootPanel;
h_dialog = gui.getMainWindow();

% header
h_text = uicontrol(h_dialog, ...
	'style', 'text', ...
	'hori', 'left', ...
	'fontname', 'Arial', ...
	'fontweight', 'bold', ...
	'fontsize', 12, ...
	'string', 'The BIOTACT Whisker Tracking Tool (BWTT)', ...
	'units', 'pixels' ...
	);
rect.pack(h_text, 'top', 24);

% text
text = [revision.string 10 'Published ' datestr(revision.date) 10 'Copyright 2015'];
h_text = uicontrol(h_dialog, ...
	'style', 'text', ...
	'hori', 'left', ...
	'fontname', 'Arial', ...
	'fontsize', 10, ...
	'string', text, ...
	'units', 'pixels' ...
	);
rect.pack(h_text, 'top', 64);

% buttons
subrect = rect.pack([], 'top', 32);
buttons = {'EULA' 'NOTES' 'FAIRUSE' 'CREDITS' 'GPL3'};
for b = 1:length(buttons)
	
	% button
	h_button = uicontrol(h_dialog, ...
		'Style', 'Pushbutton', ...
		'fontname', 'Helvetica', ...
		'fontsize', 10, ...
		'callback', @button_callback, ...
		'String', buttons{b} ...
		);
	subrect.pack(h_button, 'left', -1);
	
end

% text
h_text = uicontrol(h_dialog, ...
	'Style', 'Edit', ...
	'fontname', 'Helvetica', ...
	'fontsize', 10, ...
	'min', 1, ...
	'max', 3, ...
	'backgroundcolor', 'white', ...
	'enable', 'inactive', ...
	'String', '' ...
	);
rect.pack(h_text, 'bottom', -1);

% layout
gui.resize();

% wait
display_file('EULA');
uiwait(h_dialog);



	function button_callback(h_button, evt)
		
		s = get(h_button, 'string');
		if isequal(s, 'NOTES')
			s = 'RELEASE NOTES';
		end
		display_file(s);
		
	end

	function display_file(s)
		
		filename = [stGetInstallPath() '/' s '.txt'];
		fid = fopen(filename, 'r');
		if fid ~= -1
			s = fread(fid, Inf, '*char')';
			fclose(fid);
			s(s == 13) = '';
			s = stStringExplode(char(10), s);
			set(h_text, 'string', s);
		end
		
	end

end




