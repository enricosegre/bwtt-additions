
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function currentFrame = stReviewGUI(job, currentFrame)

% clc
% job = stJob('T:\STProjects\test2\E77E_5AA8_0DA4_4AD2.stJob');
% currentFrame = 151;
% warning('debug mode');

% pars
callbackErr = [];
h_parent = gcbf;
wh = [800 600];
pars = stGUIPars;
videoInfo = job.getVideoInfo();
showOverlay = true;
h_overlay = [];
methods = job.getAllUsedMethods();
numberOfTasks = job.getTaskCount();

% create
gui = stGUI(wh, h_parent, 'Tracking Review', 'resizable', 'on', ...
	'ResizeFcn', @gui_resize, 'KeyPressFcn', @callback_keypress);
rect = gui.rootPanel;
h_dialog = gui.getMainWindow();

% add axis
h_axis = axes( ...
	'Units', 'pixels', ...
	'Box', 'on', ...
	'Parent', h_dialog ...
	);

% add image
h_image = image( ...
	[1 videoInfo.Width], [1 videoInfo.Height], NaN, ...
	'Parent', h_axis);

% finalise axis
hold(h_axis, 'on');
set(h_axis, 'xtick', [], 'ytick', []);

% add current frame indicator
h_frameIndicator = text( ...
	4, 4, int2str(currentFrame), ...
	'fontweight', 'bold', 'color', 'black', 'BackgroundColor', 0.8*[1 1 1], ...
	'hori', 'left', 'vert', 'top', ...
	'parent', h_axis);

% % add axis
% h_axis_2 = axes( ...
% 	'Units', 'pixels', ...
% 	'Box', 'on', ...
% 	'Parent', h_dialog ...
% 	);
%
% % finalise axis
% hold(h_axis_2, 'on');
% set(h_axis_2, 'xtick', [], 'ytick', []);

% % create "selected task" slider
% h_axis_task = axes( ...
% 	'Units', 'pixels', ...
% 	'Box', 'on', ...
% 	'Parent', h_dialog ...
% 	);
% cvs = stVideoSlidersGUI(h_axis_task);
% % 	vs.updateFrameRange(info.frames);
% % 	vs.updateCurrentFrame(currentFrame);
% set(cvs.getHandles(), ...
% 	'ButtonDownFcn', @callback_mousedown, 'UserData', []);

% layout
rhs = rect.pack([], 'right', 200);
lhs = rect.pack([], 'right', -1);
% panel_slider = lhs.pack(h_axis_task, 'bottom', 24);
buts = rhs.pack([], 'bottom', 32);
items = rhs.pack([], 'bottom', -1);
% tracking = lhs.pack(h_axis_2, 'bottom', 32);
ax = lhs.pack(h_axis, 'bottom', -1);

% global flag
h_show_overlay_label = uicontrol(h_dialog, ...
	'String', 'Press T to hide tracking', ...
	'HorizontalAlignment', 'left', ...
	'FontName', pars.fontName, ...
	'FontSize', pars.fontSize, ...
	'Style', 'text');
items.pack(h_show_overlay_label, 'top', 16);

% for each task
vss = {};
for t = 1:numberOfTasks
	
	% layout
	head = items.pack([], 'top', 16);
	
	% extract
	info = job.getTaskInfo(t);
	
	% if task not yet finished, do not display it
	if isempty(info.time.finished)
		numberOfTasks = t - 1;
		break
	end
	
	str = sprintf('%d : %d', info.frames.first, info.frames.last);
 	str = [datestr(info.time.finished, 1) ' (' str ')'];
	enabled = 'on';
	if isempty(str)
		str = '(Task not finished)';
		enabled = 'off';
	end
	
	% check
	h_check = uicontrol(h_dialog, ...
		'String', '', ...
		'HorizontalAlignment', 'right', ...
		'UserData', t, ...
		'Enable', enabled, ...
		'FontName', pars.fontName, ...
		'FontSize', pars.fontSize, ...
		'Value', info.active.flag, ...
		'KeyPressFcn', @callback_keypress, ...
		'Callback', @callback_check, ...
		'Style', 'checkbox');
	head.pack(h_check, 'right', 16);
	
	% label
	h_label = uicontrol(h_dialog, ...
		'String', str, ...
		'HorizontalAlignment', 'left', ...
		'FontName', pars.fontName, ...
		'FontSize', pars.fontSize, ...
		'Style', 'text');
	head.pack(h_label, 'right', -1);
	
	% add axis
	h_axis_task = axes( ...
		'Units', 'pixels', ...
		'Box', 'on', ...
		'Parent', h_dialog ...
		);
	items.pack(h_axis_task, 'top', 24);
	
	% add images
	vs = stVideoSlidersGUI(h_axis_task);
	vs.updateFrameRange(info.frames);
	vs.updateCurrentFrame(currentFrame);
	set(vs.getHandles(), ...
		'ButtonDownFcn', @callback_mousedown, 'UserData', t);
	vss{t} = vs;
	
end

% no interrupts
gui.noInterrupts();

% layout
gui.resize();

% update
update_display();

% wait
gui.waitForClose();
if ~isempty(callbackErr)
	rethrow(callbackErr)
end


	function callback_keypress(obj, evt)
		
		try
			
			frameStep = 1;
			if ~isempty(evt.Modifier)
				switch evt.Modifier{1}
					case 'shift'
						frameStep = 10;
					case 'control'
						frameStep = 50;
				end
				key = [evt.Modifier{1} '+' evt.Key];
			else
				key = [evt.Key];
			end
			
			switch evt.Key
				
				case 'rightarrow'
					change_frame(int32(frameStep));

				case 'leftarrow'
					change_frame(int32(-frameStep));

				case 'escape'
					gui.close();
					
				case 't'
					showOverlay = ~showOverlay;
					if showOverlay
						set(h_show_overlay_label, 'string', 'Press T to hide tracking');
					else
						set(h_show_overlay_label, 'string', 'Press T to show tracking');
					end
					update_display();
					
				case {'home' 'leftbracket'}
					helper_setFrame('first', 'current');
					
				case {'end' 'rightbracket'}
					helper_setFrame('last', 'current');
					
				case {'k'}
					helper_setFrame('initial', 'current');
					
				case {'1' '2' '3' '4' '5' '6' '7' '8' '9'}
					helper_setFrame('step', key(end) - 48);
					
				case {'0'}
					helper_setFrame('step', 10);
					
			end
			
		catch err
			
			callbackError(err);
			
		end
		
	end

	function change_frame(frame)
		
		if isa(frame, 'int32')
			currentFrame = min(max(currentFrame + frame, 1), videoInfo.NumFrames);
		else
			currentFrame = frame;
		end
		currentFrame = double(currentFrame);
% 		changeDisplayedFrame(currentFrame);
		update_display();
		
	end

	function update_display()
		
		frame = job.getVideoFrames(currentFrame);
		frame = frame{1};
		if stUserData('normaliseFrame')
			frame = stNormaliseFrame(frame);
		end
		set(h_image, 'cdata', repmat(frame, [1 1 3]));
		set(h_frameIndicator, 'string', int2str(currentFrame));
		
		delete(h_overlay);
		h_overlay = [];
		
		if showOverlay

			plotdata = [];
			plotdata.h_axis = h_axis;
			plotdata.frameIndex = currentFrame;

			for m = 1:length(methods)
				method = methods{m};
				result = job.methodPlot(method{1}, method{2}, plotdata);
				if ~isempty(result) && isfield(result, 'h')
					h_overlay = [h_overlay result.h];
				end
			end
			
		end
		
		axis(h_axis, [0.5+[0 videoInfo.Width] 0.5+[0 videoInfo.Height]])
		axis(h_axis, 'image')
        set(h_axis,'xlim',[1 videoInfo.Width], 'ylim',[1 videoInfo.Height]);

		
		for t = 1:numberOfTasks
			vss{t}.updateCurrentFrame(currentFrame);
		end
		
		drawnow
		
	end

	function gui_resize(gui)
		
		
	end

	function callback_button(h_button, evt)
		
		try
			
			% 		response = buttons{get(h_button, 'userdata')};
			%  		if strcmp(response, 'OK')
			% 			mode = get(get(h_mode, 'SelectedObject'), 'UserData');
			%  		end
			gui.close();
			
		catch err
			callbackError(err);
		end
		
	end

	function callback_check(obj, evt)
		
		try
			
			t = get(obj, 'UserData');
			v = logical(get(obj, 'Value'));
			job.setActiveState(t, v);
			update_display();
			
		catch err
			callbackError(err);
		end
		
	end

	function callback_mousedown(obj, evt)
		
		f = gcbf;
		switch get(f, 'selectiontype')
			case 'normal'
				t = get(obj, 'UserData');
				if isempty(t)
					vs = cvs;
				else
					vs = vss{t};
				end
				cp = get(vs.getAxis(), 'CurrentPoint');
				x = round(cp(1));
				change_frame(x);
		end
		
	end

	function callbackError(err)
		
		callbackErr = err;
		gui.close();
		
	end

	function helper_setFrame(varargin)
		
		frames = job.getFrameRange();
		available = job.getFrameRange('available');
		
		constrain.initial = true;
		constrain.last = true;
		constrain.first = true;
		cf = fieldnames(constrain);
		
		for n = 1:2:length(varargin)
			key = varargin{n};
			val = varargin{n+1};
			if ischar(val)
				switch val
					case 'current'
						val = currentFrame;
					otherwise
						error('badness');
				end
			end
			val = min(max(val, 1), available);
			frames.(key) = val;
			constrain.(key) = false;
		end
		
		% constrain other, then all
		for loop = 1:2
			for c = 1:length(cf)
				if loop == 1 && ~constrain.(cf{c})
					continue
				end
				switch cf{c}
					case 'initial'
						frames.initial = min(frames.initial, frames.last);
						frames.initial = max(frames.initial, frames.first);
					case 'last'
						frames.last = max(frames.last, frames.initial);
						frames.last = max(frames.last, frames.first);
					case 'first'
						frames.first = min(frames.first, frames.initial);
						frames.first = min(frames.first, frames.last);
				end
			end
		end
		
		job.setFrameRange(frames);
		
	end

end

