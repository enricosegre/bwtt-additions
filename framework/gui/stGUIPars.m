
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function pars = stGUIPars

pars = [];
pars.fontName = 'Tahoma'; % prefer Helvetica, but it doesn't have a displayable char(1) character
pars.fontSize = 10;
pars.smallFontSize = 8;
pars.headingFontName = 'Arial';
pars.headingFontSize = 18;
pars.listFontName = stUserData('listFontName');
pars.listFontSize = stUserData('listFontSize');
pars.logFontName = 'Helvetica';
pars.logFontSize = 8;
pars.margin = 8;
			
