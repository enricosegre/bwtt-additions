
function bwtt(varargin)

% bwtt
%
%		Start the BIOTACT Whisker Tracking Tool GUI.
%
% bwtt <project>
%
%		Start the BIOTACT Whisker Tracking Tool GUI and open the
%		specified project.
%
% bwtt .
%
%		Start the BIOTACT Whisker Tracking Tool GUI and open the
%		most recently opened project.

%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



%% SECURITY AND POLITICS

% % SOFTWARE EXPIRY
% try
% 	t_expire = stJob.checkExpiry();
% catch e
% 	throwAsCaller(e);
% end

% USER MUST ACCEPT EULA
rev = stRevision();
rev = rev.revision;
rev_last = stUserData('lastLoadedRevision');

if rev ~= rev_last
	
	path = stGetInstallPath();
	
	% SHOW EULA
	docpath = [path '/EULA.txt'];
	if ~exist(docpath, 'file')
		error('EULA.txt not found');
	end
	fid = fopen(docpath, 'r');
	doc = fread(fid, Inf, '*char')';
	doc = strrep(doc, char([13 10]), char([10]));
	doc = strrep(doc, char([13]), char([10]));
	fclose(fid);
	response = stMessageBoxGUI(doc, 'eula');
	if ~response
		error('User did not accept EULA - unable to continue');
	end
	
	% SHOW FAIRUSE
	docpath = [path '/FAIRUSE.txt'];
	if ~exist(docpath, 'file')
		error('FAIRUSE.txt not found');
	end
	fid = fopen(docpath, 'r');
	doc = fread(fid, Inf, '*char')';
	doc = strrep(doc, char([13 10]), char([10]));
	doc = strrep(doc, char([13]), char([10]));
	fclose(fid);
	stMessage(['Neigh!!! Welcome to the BWTT :)' 10 10 'PLEASE read the next message carefully, it is NOT the usual legalese, just a request for fair use. The little pony thanks you!']);
	response = stMessageBoxGUI(doc, 'fairuse');
	if ~response
		error('User did not accept FAIRUSE - unable to continue');
	end
	
	% OK
	stUserData('lastLoadedRevision', rev);
	
end



%% PREAMBLE

% raise existing GUI
cc = allchild(0);
for c = cc'
	ud = get(c, 'UserData');
	if isstruct(ud) && isfield(ud, 'id') && strcmp(ud.id, 'BWTTMainGUI')
		figure(c)
		return
	end
end

% prevent parfor warning (it's ok!)
warning('off', 'MATLAB:mir_warning_maybe_uninitialized_temporary')



%% ST_STATE INITIALISATION

if stUserData('devMode')
	warning('RISK OF DATA LOSS: devMode is on - this mode should only be used during development');
	global st_state
end

% create
st_state = struct();
st_project = [];

try
	
	% we initialise the structure of st_state right here, so that
	% it is always available in other functions, avoiding boring
	% calls to isfield(st_state, ...) all over the place
	
	% create field "state"
	st_state.state.loaded = false;
	st_state.state.debugMode = stUserData('devMode');
	st_state.state.bigScreen = stUserData('bigScreen');
	st_state.state.normaliseFrame = stUserData('normaliseFrame');
	
	% create field "logs"
	st_state.log.strings = {};
	st_state.log.lastmsgid = 0;
	st_state.log.logfilename = [stUserData('temporaryDirectory') '/bwtt.log'];
	st_state.log.logfile = fopen(st_state.log.logfilename, 'w');
	
	% create field "revision"
	st_state.revision = stRevision();
	
	% create field "constants"
	st_state.constants.previewWindowSize = [288 512];
	st_state.constants.sliderAxisHeight = 4;
	
	% create field "callbacks"
	st_state.callbacks.log = @stLog;
	st_state.callbacks.progress = @stProgress;
	st_state.callbacks.showFrame = @stShowFrame;
	
	% create field "displayed"
	st_state.displayed = [];
	
	% create GUI
	userEvent = [];
	userEvent.menuCommand = @userEvent_MenuCommand;
	userEvent.keyPress = @userEvent_KeyPress;
	userEvent.pushButton = @userEvent_PushButton;
	userEvent.modifyState = @userEvent_ModifyState;
	userEvent.buttonDown = @userEvent_ButtonDown;
	userEvent.closeRequest = @userEvent_CloseRequest;
	st_state.gui = stMainGUI(userEvent);
	st_state.h = st_state.gui.state.h;
	
	% create empty java handles object
	st_state.j.log = [];
	
	% lay in keypress callback to all objects that will accept it
	h_keypress = findall(st_state.h.mainGUI);
	for h = h_keypress'
		try
			desc = get(h, 'type');
			if strcmp(desc, 'uicontrol')
				desc = [desc ' ' get(h, 'style')];
			end
			
			switch desc
				case {'uimenu' 'uipanel' 'uicontrol text' 'uicontrol edit'}
					% no action needed, cannot get focus
				case {'uicontrol pushbutton' 'uicontrol checkbox' 'uicontrol togglebutton' ...
						'uicontrol listbox' 'uicontrol popupmenu' 'axes' 'figure' 'rectangle' 'line' 'image'}
					% lay in keypress
		 			set(h, 'keypressfcn', @userEvent_KeyPress);
				otherwise
					warning(desc)
			end
		end
	end
	
	% get plugins
	plugins = stPlugin();
	st_state.outputPlugins = plugins.om.list;
	st_state.toolPlugins = plugins.tool.list;
	
	% set default pipeline state
	helper_setDefaultPipelineState();
	
	% retrieve window position
	remember = stUserData('rememberWindowPosition');
	if ~strcmp(remember, 'Not managed')
		try
			set(st_state.h.mainGUI, 'Position', stUserData('storedWindowPosition'));
		end
	end
	
	% retrieve normaliseFrame state
	set(st_state.h.preview.normalise, 'Value', st_state.state.normaliseFrame);
	
	% finalize GUI
	if stUserData('devMode')
% 		set(st_state.h.mainGUI, 'Color', [1 0 0]);
	else
		set(st_state.h.menu.Debug, 'visible', 'off');
	end
	if st_state.state.debugMode
		set(st_state.h.menu.Debug_Mode, 'Checked', 'on');
	end
	st_state.gui.finalize();
	
	% open an existing project
	if nargin
		if ischar(varargin{1})
			if strcmp('mostRecent', varargin{1}) || strcmp('.', varargin{1})
				recent = stUserData('recentProjects!');
				if ~isempty(recent)
					projectOperation('openProject', recent{1});
				else
					warning(['Ignoring "mostRecent" - no recent projects are listed']);
				end
			else
				projectOperation('openProject', varargin{1});
			end
		end
	end
	
	% if not, start a new project
	if isempty(st_project)
		projectOperation('newProject');
	end
	
	% turn off figure handle visibility so it can't be accessed
	% from the command line. NB: we can't do this before
	% finishing this call, because we are NOT currently in a
	% callback (in all subsequent processing, we are).
	set(st_state.h.mainGUI, 'HandleVisibility', 'callback');
	
% 	% SOFTWARE EXPIRY
% 	n_days = ceil(t_expire - now);
% 	stLog(['LICENSE EXPIRES ON: ' datestr(t_expire) ' (in ' int2str(n_days) ' days)']);

	% GPL WARNING
	stLog('This program is Copyright 2015. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See EULA.txt for full details.');

	% mark as loaded
	st_state.state.loaded = true;

	
	
catch err
	
	try
		delete(st_state.h.mainGUI);
	end
	rethrow(err);
	
end










%% USER EVENTS

% selection of any static menu command
	function userEvent_MenuCommand(hObject, evt)
		userEvent_commonEntryPoint(hObject, ['menucommand ' get(hObject, 'Label')]);
	end

% selection of recent file item from menu
	function userEvent_RecentProject(hObject, evt)
		userEvent_commonEntryPoint(hObject, 'menucommand Open Recent Project', str2num(get(hObject, 'Tag')));
	end

% key press from any object
	function userEvent_KeyPress(hObject, evt)
		
		% this one seems to be buggy
		if strcmp(evt.Character, '#')
			evt.Key = '#';
		end
		
		key = [evt.Key];
		mod = evt.Modifier;
		while ~isempty(mod)
			key = [mod{1} '+' key];
			mod = mod(2:end);
		end
		userEvent_commonEntryPoint(hObject, ['keypress ' key]);
	end

% click of a push button
	function userEvent_PushButton(hObject, evt)
		userEvent_commonEntryPoint(hObject, ['pushbutton ' get(hObject, 'String')])
	end

% modification of various objects
	function userEvent_ModifyState(hObject, evt)
		userEvent_commonEntryPoint(hObject, ['modifystate ' get(hObject, 'Tag')])
	end

% mouse click on object
	function userEvent_ButtonDown(hObject, evt)
		userEvent_commonEntryPoint(hObject, ['buttondown ' get(hObject, 'Tag')])
	end

% GUI close request
	function userEvent_CloseRequest(hObject, evt)
		% simulate menu selection
		userEvent_commonEntryPoint(hObject, ['menucommand Exit'])
	end

% common entry point
	function userEvent_commonEntryPoint(hObject, key, data)

		% PERF
		t0 = clock();
% 		profile on
		
		% do not respond to events during load
		if ~st_state.state.loaded
			return
		end
		
		% for debug convenience, we immediately discard some
		% vacuous events
		switch key
			case {
					'keypress control+control'
					'keypress shift+shift'
					'keypress alt+alt'
					'menucommand Recent Projects'
					'menucommand Project'
					'menucommand Selected'
					'menucommand Tools'
					'menucommand Debug'
					'menucommand Help'
					}
				return
		end
		
		% debug
		stLog(['$[USER] ' key]);
		
		% disable listbox so we don't miss callbacks if user
		% changes its state
		set(st_state.h.project.listbox, 'enable', 'off');
		
		% catch for reenable
		try
			
			switch key
				
				
				
				
				% PROJECT PANEL
				
				case 'modifystate Project Listbox'
					index = get(st_state.h.project.listbox, 'Value');
					st_project.selectJobs(index);
					
				case {'keypress uparrow'},					st_project.selectJobs(int32(-1));
				case {'keypress downarrow'},				st_project.selectJobs(int32(1));
					
				case {'keypress alt+uparrow'},			st_project.sortJobs('moveUp');
				case {'keypress alt+downarrow'},		st_project.sortJobs('moveDown');
					
				case {'keypress alt+control+uparrow'},			st_project.sortJobs('moveToTop');
				case {'keypress alt+control+downarrow'},		st_project.sortJobs('moveToBottom');
					
					
					
					
					% VIDEO PREVIEW PANEL
					
				case 'pushbutton +'
					job = st_project.getDisplayedJob();
					if ~isempty(job)
						currentFrame = stReviewGUI(job, st_state.displayed.currentFrame);
						changeDisplayedFrame(currentFrame);
					end
					
				case {'pushbutton Normalise'}
					st_state.state.normaliseFrame = get(hObject, 'Value') ~= 0;
					stUserData('normaliseFrame', st_state.state.normaliseFrame);
					changeDisplayedFrame(st_state.state.normaliseFrame);
					
				case 'keypress leftarrow',						changeDisplayedFrame(int32(-1));
				case 'keypress shift+leftarrow',			changeDisplayedFrame(int32(-10));
				case 'keypress control+leftarrow',		changeDisplayedFrame(int32(-50));
				case 'keypress rightarrow',						changeDisplayedFrame(int32(1));
				case 'keypress shift+rightarrow',			changeDisplayedFrame(int32(10));
				case 'keypress control+rightarrow',		changeDisplayedFrame(int32(50));
					
					
					
					
					
					% FRAMES PANEL
					
				case 'buttondown Video Slider'
					
					cp = get(st_state.h.preview.slider.getAxis(), 'CurrentPoint');
					x = round(cp(1));
					st = get(st_state.h.mainGUI, 'SelectionType');
					
					switch st
						case 'normal'
							changeDisplayedFrame(x);
					end
					
				case 'modifystate FSL'
					str = get(hObject, 'String');
					pstr = '';
					f = find(str == ':');
					switch length(f)
						case 2
							first = round(str2num(str(1:f(1)-1)));
							step = round(str2num(str(f(1)+1:f(2)-1)));
							last = round(str2num(str(f(2)+1:end)));
							pstr = sprintf('%d:%d:%d', first, step, last);
						case 1
							first = round(str2num(str(1:f(1)-1)));
							step = 1;
							last = round(str2num(str(f(1)+1:end)));
							pstr = sprintf('%d:%d', first, last);
					end
					if strcmp(str, pstr)
						helper_setFrame('first', first, 'last', last, 'step', step);
					else
						error('the data entered for frame range was not understood');
					end
					
				case 'modifystate I'
					f = str2num(get(hObject, 'String'));
					if ~isempty(f) && f == round(f) && strcmp(num2str(f), get(hObject, 'String'))
						helper_setFrame('initial', f);
					else
						error('the data entered for initial frame was not understood');
					end
					
				case 'pushbutton S',			helper_setFrame('first', 1, 'last', 10, 'initial', 1, 'step', 1);
				case 'pushbutton A',			helper_setFrame('first', 1, 'last', 1e6, 'initial', -1, 'step', 1);
				case {'keypress k'},			helper_setFrame('initial', 'current');
				case {'keypress 0'},			helper_setFrame('step', 10);

				case {'keypress leftbracket' 'keypress home'},		helper_setFrame('first', 'current');
				case {'keypress rightbracket' 'keypress end'},		helper_setFrame('last', 'current');
					
				case {'keypress 1' 'keypress 2' 'keypress 3' 'keypress 4' 'keypress 5' 'keypress 6' 'keypress 7' 'keypress 8' 'keypress 9'}
					helper_setFrame('step', key(end) - 48);
					
					
					
					
					
					
					% PROCESSING PANEL
					
				case 'pushbutton Edit Task'
					job = st_project.getDisplayedJob();
					if ~isempty(job)
						[state, errors, pipelines] = job.getCurrentTaskState();
						pipelines = stEditTaskGUI(pipelines);
						if ~isempty(pipelines)
		 					st_project.actOnSelectedJobs('createTask', pipelines);
							updateProcessingListbox(st_project.getDisplayedJob());
						end
					end
					
				case 'pushbutton Task Pars'
					st_project.actOnSelectedJobs('parametrize');
					
				case 'modifystate Quick Task'
					job = st_project.getDisplayedJob();
					if ~isempty(job)
						s = get(hObject, 'string');
						qt = s{get(hObject, 'value')};
 						pipelines = stQuickTask(qt);
						if ~isempty(pipelines)
		 					st_project.actOnSelectedJobs('createTask', pipelines);
							updateProcessingListbox(st_project.getDisplayedJob());
						end
					end
					
					
					
					
					
					
					
					% OUTPUT PANEL

				case 'modifystate OutputListbox'
					
 					st = get(st_state.h.mainGUI, 'SelectionType');
 					index = st_project.getDisplayedJobIndex();
 					job = st_project.getDisplayedJob();
					
 					if ~isempty(index) && strcmp(st, 'open')
						
 						s = get(hObject, 'string');
 						s = s(get(hObject, 'Value'));
						if length(s) == 1
							s = s{1};
							f1 = strfind(s, '<data>');
							f2 = strfind(s, '</data>');
							methodName = s(f1+6:f2-1);
	 						
% 							outconf = st_project.getOutputConfig();
% 							if isfield(outconf.pars, methodName)
% 								pars = outconf.pars.(methodName);
% 							else
% 								pars = [];
% 							end
				
% 							if stUserData('devMode')
% 								job.clearOutputs();
% 							end

							st_project.updateOutput(index, {methodName});
							
							info = stPlugin(methodName, 'info');
							if any(ismember(info.flags, 'noplot'))

								stLog(['plugin "' methodName '" does not generate a plot']);
								
							else
								
								state = [];
								state.frameIndices = job.getFrameRange('export');
								state.result = job.getOutput(methodName);

								if ~isempty(state.result)
									label = [methodName ' ' stProcessPath('justFilename', job.getVideoFilename()) ' (' job.getUID() ')'];
									state.h_figure = stSelectFigure(label, 'raise');
									clf(state.h_figure);
									stPlugin(methodName, 'plot', job, state);
								end
								
							end
						
						end
						
					end
						
				case 'pushbutton Set Outputs'
					h = st_state.h.output;
					outconf = st_project.getOutputConfig();
					s = get(h.listbox, 'string');
					v = get(h.listbox, 'value');
					outconf.methods = s(v)';
					for m = 1:length(outconf.methods)
						s = outconf.methods{m};
						f1 = strfind(s, '<data>');
						f2 = strfind(s, '</data>');
						s = s(f1+6:f2-1);
						outconf.methods{m} = s;
					end
 					st_project.setOutputConfig(outconf);
					helper_refreshOutputsList();
					
				case 'pushbutton Output Pars'
					outconf = st_project.getOutputConfig();
					h = st_state.h.output;
					s = get(h.listbox, 'string');
					v = get(h.listbox, 'value');
					methods = s(v)';
					for m = 1:length(methods)
						s = methods{m};
						f1 = strfind(s, '<data>');
						f2 = strfind(s, '</data>');
						s = s(f1+6:f2-1);
						outconf.methods = union(outconf.methods, s);
					end
 					outconf = stAnyParsGUI(outconf, 'Output Method Parameters');
 					if ~isempty(outconf)
 						st_project.setOutputConfig(outconf);
 					end
					
				case 'pushbutton Export'
% 					if stConfirm(['Do you want to export just the selected jobs? If you click No, all suitable jobs will be exported.' 10 10 'NB: You can also obtain the exported results by loading the project at the command-line and calling export() on it.'])
	 					export = st_project.export('selected');
% 					else
% 	 					export = st_project.export();
% 					end
					[filename, pathname] = uiputfile('*.mat', 'Export Jobs', ...
						st_project.getMetaData('mruExportFilename'));
					if ischar(filename)
						filename = [pathname filename];
						save(filename, 'export');
						st_project.setMetaData('mruExportFilename', filename);
					end
					
					
					
					% EXECUTION PANEL
					
				case 'pushbutton >'
					st_project.runSelectedJobs();
					
				case 'pushbutton ||'
					% no action
					
				case ['pushbutton ' char(1)]
					% no action
					
					
					
					
					% FILE MENU
					
				case 'menucommand File'
					% rebuild recent projects menu
					delete(get(st_state.h.menu.Recent_Projects, 'children'));
					recent = stUserData('recentProjects!');
					for r = 1:length(recent)
						uimenu(st_state.h.menu.Recent_Projects, ...
							'Label', recent{r}, ...
							'Callback', @userEvent_RecentProject, ...
							'Tag', int2str(r), ...
							'Enable', 'on');
					end
					
					% grey Save if appropriate
					if isempty(st_project)
						saveable = false;
					else
						saveable = st_project.getSaveable();
					end
					set(st_state.h.menu.Save, 'ForegroundColor', (0.5-0.5*saveable)*[1 1 1])
					
				case 'menucommand Open Recent Project'
					recent = stUserData('recentProjects!');
					projectOperation('openProject', recent{data});
					
				case {'menucommand New Project'},									projectOperation('newProject');
				case {'menucommand Open Project'},								projectOperation('openProject');
				case {'menucommand Save' 'keypress control+s'},		projectOperation('saveProject');
					
				case 'menucommand Preferences'
					
					% get information required from stUserData
					[values, prefs] = stUserData();
					
					% display prefs GUI
					[prefs, prefsChanged] = ...
						stPreferencesGUI(prefs, values, st_state.h.mainGUI, 'Preferences');
					
					% update preferences that have changed
					if ~isempty(prefs)
						
						% for each pref that has changed
						fn = fieldnames(prefsChanged);
						for n = 1:length(fn)
							if prefsChanged.(fn{n})
								
								% store it
								key = fn{n};
								value = prefs.(key);
								stUserData(key, value);
								
								% handle specials
								switch key
									case 'rememberWindowPosition'
										logid = stLog('Storing current window position...');
										stUserData('storedWindowPosition', get(st_state.h.mainGUI, 'Position'));
										stLog(logid);
								end
								
							end
						end
						
					end
					
					% update fonts
					set([st_state.h.project.listbox st_state.h.processing.listbox st_state.h.output.listbox], ...
						'fontname', stUserData('listFontName'));
					set(st_state.h.project.listbox, ...
						'fontsize', stUserData('listFontSize'));
					
				case {'menucommand Larger Font' 'keypress control+add'}
					helper_changeFontSize(+1);
					
				case {'menucommand Smaller Font' 'keypress control+subtract'}
					helper_changeFontSize(-1);
					
				case {'keypress control+numpad1' 'keypress control+numpad2' 'keypress control+numpad3' ...
						  'keypress control+numpad4' 'keypress control+numpad5' 'keypress control+numpad6' ...
						  'keypress control+numpad7' 'keypress control+numpad8' 'keypress control+numpad9'}
					helper_changeFontName(str2num(key(end)));
					
				case 'menucommand Open Temporary Directory'
					tempdir = stUserData('temporaryDirectory');
					if ~isempty(tempdir)
						if isunix
							system(['nautilus ''' tempdir ''' &']);
						elseif ispc
							tempdir(tempdir == '/') = '\'; % explorer.exe must have backslashes for this to work correctly
							system(['explorer "' tempdir '" &']);
						end
					else
						stMessage('Temporary Directory is not set.');
					end
					
				case 'menucommand Open Project Directory'
					if ~isempty(st_project)
						foldername = st_project.getFolderName();
						if isempty(foldername)
							stMessage('Project is not saved yet.');
						else
							if isunix
								system(['nautilus ''' foldername ''' &']);
							elseif ispc
								foldername(foldername == '/') = '\'; % explorer.exe must have backslashes for this to work correctly
								system(['explorer "' foldername '" &']);
							end
						end
					else
						stMessage('No project is loaded.');
					end
					
				case 'menucommand Show Log File'
					edit(st_state.log.logfilename);
					
				case 'menucommand Exit'
					
					try
						
						if confirmNotModified()
							quit cancel;
						else
							
							% delete project
							st_project = [];
							
							% finalize preview
							stLoadPreviewFrame();
							
							% store window position
							remember = stUserData('rememberWindowPosition');
							if strcmp(remember, 'Position when last closed')
								stUserData('storedWindowPosition', get(st_state.h.mainGUI, 'Position'));
							end
							
							% exit
							stLog('Shutting down...');
							drawnow
							
							% delete the window
							delete(ancestor(hObject,'figure'))
							
							% skip post-event processing
							return
							
						end
						
					catch err
						
						stError(err);
						stMessage('An error occurred during shutdown - I will gather my pony strength and attempt once more to force a shutdown when you click OK.');
						delete(ancestor(hObject,'figure'))
						
						% skip post-event processing
						return
						
					end
					
					
					
					% PROJECT MENU
					
				case 'menucommand Add Job'
					projectOperation('addJob');
					
				case 'menucommand Add Directory'
					projectOperation('addDirectory');
					
				case {'menucommand All' 'keypress control+a'},								st_project.selectJobs('all');
				case {'menucommand Ready To Run' 'keypress control+r'},				st_project.selectJobs('ready');
				case {'menucommand All With Same Task' 'keypress control+w'},	st_project.selectJobs('with');
					
				case 'menucommand Project Information'
					projectOperation('showInfo');
					
				case 'menucommand By Task Content',				st_project.sortJobs('taskHash');
				case 'menucommand By Task Status',				st_project.sortJobs('taskStatus');
				case 'menucommand By Video Path',					st_project.sortJobs('alphaFull');
				case 'menucommand By Video Filename',			st_project.sortJobs('alphaPartial');
				case 'menucommand By Modification Date',	st_project.sortJobs('modification');
					
				case 'menucommand Undelete'
					st_project.undeleteJobs();
					
					
					
					% SELECTED MENU
					
				case {
						'menucommand Delete'
						'keypress delete'
						}
					st_project.actOnSelectedJobs('delete');
					
				case 'menucommand Release Locked Elsewhere',	st_project.actOnSelectedJobs('stampOnLock');
				case 'menucommand Release Write Lock',				st_project.actOnSelectedJobs('release');
				case 'menucommand Clear Flags',								st_project.actOnSelectedJobs('clearFlags');
				case 'menucommand Clear Outputs',							st_project.actOnSelectedJobs('clearOutputs');
				case 'menucommand Restart Current Task',			st_project.actOnSelectedJobs('restart');
				case 'menucommand Close Current Task',				st_project.actOnSelectedJobs('close');
				case 'menucommand Find Missing Videos',				st_project.actOnSelectedJobs('findMissing');
					
				case {'menucommand Job Information' 'keypress control+i'}
					info = st_project.jobInfo();
					if ~isempty(info)
						stInfo(info);
					end
					
				case 'menucommand Remove Parameters'
					st_project.actOnSelectedJobs('deparametrize');
					
					
					
					% DISPLAYED MENU

				case 'menucommand Propagate Parameters'
					st_project.actOnSelectedJobs('propagateParameters');
					
				case {'menucommand Set Tag' 'keypress control+t'}
					tag = stInput('Enter the new tag for the job.');
					if ~isempty(tag)
						st_project.getDisplayedJob().setUserData('tag', tag{1});
					end
					
				case 'menucommand Preload Video'
					if strcmp(stUserData('previewPolicy'), 'On disk')
						logid = stLog(['Preloading (press Stop to cancel)...']);
						job = st_project.getDisplayedJob();
						if ~isempty(job)
							[previewFrame, videoInfo] = stLoadPreviewFrame(1, job, st_state.constants.previewWindowSize);
							ff = find(~videoInfo.PreviewFramesAvailable);
							for f = 1:length(ff)
								changeDisplayedFrame(ff(f));
								if stProgress(f/length(ff));
									break
								end
							end
						end
						stLog(logid);
					end
					
				case {'menucommand Show Error Message' 'keypress control+e'}
					job = st_project.getDisplayedJob();
					if ~isempty(job)
						errors = job.collectErrors();
						if ~isempty(errors)
							stError(errors{1});
						else
							stMessage('Good news! There was no error!');
						end
					end
					
					
					
					
					
					% DEBUG MENU
					
				case 'menucommand Raise Error'
					error('Some of the little rabbits are not where they are supposed to be.');
					
				case 'menucommand Debug Mode'
					st_state.state.debugMode = ~st_state.state.debugMode;
					if st_state.state.debugMode
						set(st_state.h.menu.Debug_Mode, 'Checked', 'on');
						stLog('Debug mode is now on');
					else
						set(st_state.h.menu.Debug_Mode, 'Checked', 'off');
						stLog('Debug mode is now off');
					end
					
				case 'menucommand Run Test Operation'
					for i = 1:100
						pause(0.1);
						cancel = stProgress(i/100, true);
						if cancel
							break
						end
					end
					
					
					
					% HELP MENU
					
				case {
						'menucommand Documentation'
						}
					web(['http://bwtt.sourceforge.net']);
					
				case 'menucommand About'
					stAboutGUI();
					
					
					
					% TOOLS MENU
					
				otherwise
					
					for t = 1:length(st_state.toolPlugins)
						menucommand = ['menucommand ' st_state.toolPlugins{t}];
						if strcmp(key, menucommand)
							
							% run tool
							projectOperation('runTool', st_state.toolPlugins{t});
							break
							
						end
					end
					
			end
			
		catch err
			
			% display
			stError(err);

		end

		% clear cancel/pause states
		st_state.gui.clearCancelAndPause();
		
		% refresh
		try
			st_project.refresh();
		catch err
			stError(err);
		end
		
		% enable listbox
		set(st_state.h.project.listbox, 'enable', 'on');
		
		% PERF
		if st_state.state.debugMode % stUserData('devMode')
			t1 = clock();
			T = etime(t1, t0);
			disp(['PERF: ' num2str(T) ' seconds']);
	% 		profile report
	% 		profile off
		end
		
	end







%% PROJECT OPERATIONS

	function projectOperation(operationType, data)
		
		stLog(['$[OP] ' operationType]);
		
		switch operationType
			
			case 'runTool'
				
				% get tool name
				toolName = data;
				
				% it's quick'n'dirty to store the tool parameters in
				% meta data - no problem, if we decide to change
				% this in future it'll be easy to move them. it
				% might be a perfectly sensible place, anyway.
				
				% handle parameters
				pars = st_project.getMetaData(['toolParameters_' toolName]);
				defpars = stPlugin(toolName, 'parameters');
				
% 				warning('not getting pars');
				if ~isempty(defpars)
					
					% parametrize
					conf = [];
					conf.pars.(toolName) = pars;
					conf.methods = {toolName};
					conf = stAnyParsGUI(conf, [toolName ' Parameters']);
					if isempty(conf)
						return
					end
 					pars = conf.pars.(toolName);
					st_project.setMetaData(['toolParameters_' toolName], pars);
					
				end
				
				% run the tool
				data = [];
				data.pars = stMergePars(pars, defpars);
				data = stPlugin(toolName, 'process', st_project, data);
				
				% if a field "pars" was returned, the tool has
				% updated its own pars
				if isfield(data, 'pars')
					st_project.setMetaData(['toolParameters_' toolName], data.pars);
				end
			
			case 'newProject'
				
				% confirm permission to continue
				if confirmNotModified()
					return
				end
				
				% report
				logid = stLog('Creating new project...');
				
				% do it
				st_project = [];
				st_project = stProject('', @projectEvent);
				
				% reset GUI state
				helper_setDefaultPipelineState();
 				helper_refreshOutputsList();
				
				% report
				stLog(logid);
				
			case 'openProject'
				
				% confirm permission to continue
				if confirmNotModified()
					return
				end
				
				% get foldername if not supplied
				if ~exist('data', 'var')
					foldername = uigetdir( ...
						stUserData('lastUsedProjectDirectory'), 'Open Project');
					if isnumeric(foldername)
						return
					end
					data = stTidyFilename(foldername);
				end
				
				% report
				logid = stLog('Loading existing project...');
				
				% close the old project
				st_project = [];
				
				% update GUI state
				helper_setDefaultPipelineState();
 				helper_refreshOutputsList();
				
				% open the project (includes validation)
				st_project = stProject(data, @projectEvent);
				
				% meta data
				stUserData('lastUsedProjectDirectory', fileparts(data));
				helper_addRecentProject(data);

				% update GUI state
 				helper_refreshOutputsList();
				
				% report
				stLog(logid);
				
			case 'saveProject'
				
				% do operation
				if st_project.getSaveable()
					logid = stLog('Saving project...');
					foldername = st_project.save();
					if isempty(foldername)
						logid.result = 'CANCELLED';
					else
						stUserData('lastUsedProjectDirectory', fileparts(foldername));
						helper_addRecentProject(foldername);
					end
					stLog(logid);
				end
				
			case 'showInfo'
				stMessage('Project information not implemented. Neigh.');
				% 					st_project.showInfo(st_state.h.mainGUI);
				
			case 'addJob'
				
				% get filename if not supplied
				if ~exist('data', 'var')
					
					% get filename
					[filenameName, filenamePath] = uigetfile('*.*', 'Add Job', ...
						st_project.getMetaData('mruJobDirectory'));
					if isnumeric(filenameName)
						return
					end
					
					% tidy
					data = [filenamePath filenameName];
					
				end
				
				% do operation
				logid = stLog(['Adding "' stProcessPath('justFilename', data) '"...']);
				st_project.addJobs({data});
				stLog(logid);
				
			case 'addDirectory'
				path = uigetdir(st_project.getMetaData('mruJobDirectory'), 'Add Directory');
				if isnumeric(path)
					return
				end
				dd = dir(path);
				filenames = {};
				for n = 1:length(dd)
					filename = [path '/' dd(n).name];
					[dr, f, e] = fileparts(filename);
					if strcmp(lower(e), '.avi')
						logid = stLog(['Adding "' stProcessPath('justFilename', filename) '"...']);
						filenames{end+1} = filename;
						stLog(logid);
					end
				end
				if isempty(filenames)
					stMessage('No AVI files were found in that directory');
				else
					st_project.addJobs(filenames);
				end
				
			otherwise
				stLog(['(UNRECOGNISED PROJECT OPERATION: ' operationType ')']);
				
		end
		
	end



%% PROJECT EVENTS

	function result = projectEvent(eventType, data)
		
		switch eventType
			case {'progress' 'log'}
			otherwise
				stLog(['$[PROJ] ' eventType]);
		end
		
		result = [];
		
		switch eventType
			
			case 'projectOpened'
				
			case 'projectClosed'
				
				% NB: st_project is no longer valid in this event
				
				% display no job
				changeDisplayedJob([]);
				
				% empty job list
				set(st_state.h.project.listbox, 'String', {}, 'Value', []);
				
			case 'jobListChanged'
				
				% JOB LIST UPDATE (1 of 3)
				% the contents of the job list have changed
				
				% update job list
				strings = {};
				for j = 1:length(data.jobs)
					string = helper_formatJobsListEntry(data.jobs{j});
					strings{end+1} = string;
				end
				set(st_state.h.project.listbox, ...
					'String', strings, 'Value', data.selected);
				
				% delegate remaining processing
				projectEvent('jobSelectedChanged', data);
				
			case 'jobSelectedChanged'

				% JOB LIST UPDATE (2 of 3)
				% the selected items of the job list have changed
				
				% update displayed selected items in job list
				set(st_state.h.project.listbox, 'Value', data.selected);
				
				% handle menu enabling
				if isempty(data.selected)
					enable = 'off';
				else
					enable = 'on';
				end
				set([st_state.h.menu.Selected st_state.h.menu.Displayed st_state.h.processing.buttons st_state.h.processing.quick], ...
					'enable', enable);

				% delegate remaining processing
				projectEvent('jobDisplayedChanged', data);
				
			case 'jobDisplayedChanged'

				% JOB LIST UPDATE (2 of 3)
				% the displayed item of the job list has changed
				
				% usually, the displayed job is always the first of
				% the selected jobs but, during processing of
				% tracking, the project uses this event to change to
				% the job being processed. everything should work as
				% usual, since there's no assumption anywhere that
				% the first selected job is the one that is
				% displayed.
				changeDisplayedJob(data);
				
			case 'modifiedStateChanged'
				
				% update window title
				projectName = data.project.getName();
				if data.modified
					projectName = [projectName '*'];
				end
				set(st_state.h.mainGUI, 'Name', ['BIOTACT Whisker Tracking Tool - [' projectName ']'])
				
			case 'jobFrameRangeChanged'
				changeFrameRanges(data.job, data.indexInProject);
				
			case 'jobInvalidated'
				changeJobsListEntry(data.job, data.indexInProject);
				
			case 'log'
				if isstruct(data.msg)
					stLog(data.msg);
				else
					result = stLog(data.msg);
				end
				
			case 'progress'
				result = stProgress(data.fraction, data.offerPause);
				
			case {'jobProcessFrame'}
				changeDisplayedFrame(data.frameIndex);
				result = st_state.h.preview.axis;
				
			otherwise
				stLog(['(UNRECOGNISED PROJECT EVENT: ' eventType ')']);
				
		end
		
	end








%% GRAPHICAL: ANY JOB

	function changeFrameRanges(job, indexInProject)
		
		header = job.getHeader();
		if ~isempty(st_state.displayed) && strcmp(st_state.displayed.uid, header.uid)
			changeDisplayedFrameRanges(job);
		end
		
		% in any case, update list entry
		changeJobsListEntry(job, indexInProject);
		
	end

	function changeJobsListEntry(job, indexInProject)
		
		strings = get(st_state.h.project.listbox, 'String');
		strings{indexInProject} = helper_formatJobsListEntry(job);
		set(st_state.h.project.listbox, 'String', strings);
		
	end



%% GRAPHICAL: "DISPLAYED" JOB

	function changeDisplayedMetaData(job)
		
% 		% get value
% 		value = job.getMetaData('viewIndex');
% 		if isempty(value)
% 			s = ['VIEW: not set'];
% 		else
% 			s = ['VIEW: ' int2str(value)];
% 		end
% 		
% 		% get value
% 		value = job.getMetaData('recordedFrameRate');
% 		if isempty(value)
% 			s = [s ', RFR: not set'];
% 		else
% 			s = [s ', RFR: ' num2str(value)];
% 		end
% 		
% 		% get value
% 		value = job.getMetaData('mmPerPixel');
% 		if isempty(value)
% 			s = [s ', MPP: not set'];
% 		else
% 			s = [s ', MPP: ' num2str(value)];
% 		end
% 
% 		% display
% 		set(st_state.h.preview.metaData, 'String', s);
		
	end

	function changeDisplayedFrameRanges(job)
		
		% update FSL and I
		frames = job.getFrameRange('complete');
		
		% update video slider pointers
		st_state.h.preview.slider.updateFrameRange(frames);
		
		% update text boxes
		set(st_state.h.preview.FSL, 'String', sprintf('%i:%i:%i', [frames.first frames.step frames.last]));
		set(st_state.h.preview.I, 'String', sprintf('%i', frames.initial));
		
	end

	function changeDisplayedFrame(frameIndex)
		
		% change in normalise state is indicated as a logical
		if isa(frameIndex, 'logical')
			if isempty(st_state.displayed)
				return
			else
				frameIndex = st_state.displayed.currentFrame;
			end
		end
		
		% delta frame is indicated as an int32
		if isa(frameIndex, 'int32')
			if isempty(st_state.displayed)
				return
			else
				frameIndex = st_state.displayed.currentFrame + double(frameIndex);
			end
		end
		
		% if a job is loaded (if not, ignore any such calls)
		if ~isempty(st_state.displayed)
			
			% constrain
			frameIndex = min(max(frameIndex, 1), st_state.displayed.availableFrames);
			
			% store new frame index
			st_state.displayed.currentFrame = frameIndex;
			
			% get the preview frame
			[previewFrame, videoInfo] = stLoadPreviewFrame(frameIndex);
			
			% previewFrame will be empty if something's gone wrong
			if isempty(previewFrame)
				set(st_state.h.preview.image, 'cdata', 0);
				return
			end
			
			% normalise
			if st_state.state.normaliseFrame
				previewFrame = stNormaliseFrame(previewFrame);
			end
			
			% update image
			set(st_state.h.preview.image, 'cdata', repmat(previewFrame, [1 1 3]));
			
			% update current frame indicator
			set(st_state.h.preview.current_frame_indicator, 'string', ...
				int2str(frameIndex));
			
			% update video slider
			st_state.h.preview.slider.updateCurrentFrame(frameIndex);
			st_state.h.preview.slider.updateFramesAvailable(videoInfo.PreviewFramesAvailable);
			
			% no, not yet, because we might be calling during
			% processing, and in that case we want to hold off on
			% drawnow until we've done all the overlays. the job
			% itself will call drawnow when it's done all the
			% method overlays.
			% 			drawnow
			
		else
			
			% ignore
			
		end
		
	end

	function changeDisplayedJob(data)
		
		% can be passed no data
		if isempty(data)
			data = [];
			data.jobs = [];
			data.displayed = [];
		end
		
		% clear if no job or no match
		if isempty(data.displayed)
			clearAll = true;
			job = [];
		else
			job = data.jobs{data.displayed};
			header = job.getHeader();
			clearAll = false;
			if ~isempty(st_state.displayed) && ~strcmp(st_state.displayed.uid, header.uid)
				clearAll = true;
			end
		end
		
		% clear
		if clearAll && ~isempty(st_state.displayed)
			
			% delete or hide all graphics objects
			delete(st_state.h.preview.image);
			delete(st_state.h.preview.current_frame_indicator);
			st_state.h.preview.image = [];
			st_state.h.preview.current_frame_indicator = [];
			
			% hide components of slider by changing axis
			axis(st_state.h.preview.slider.getAxis(), [0.5 1.5 1000 1001]);
			
			% empty strings in FSL and I
			set(st_state.h.preview.FSL, 'String', '');
			set(st_state.h.preview.I, 'String', '');
			
% 			% clear meta data
% 			set(st_state.h.preview.metaData, 'String', ''); %(no job is displayed)');
			
% 			% update jobs list entry
% 			st_state.displayed.indexInProject
% 			changeJobsListEntry
			
			% clear data
			st_state.displayed = [];
			
		end
		
		% only need to continue if we have a job
		if ~isempty(data.displayed)
			
			% recreate if necessary
			if isempty(st_state.displayed)
				
				% create empty graphics handles so that code that
				% runs later works ok even if the call to
				% stLoadPreviewFrame() fails
				st_state.h.preview.image = [];
				st_state.h.preview.current_frame_indicator = [];
				
				% create data
        displ = [];
				displ.indexInProject = data.displayed;
				displ.currentFrame = job.getFrameRange('initial');
				displ.availableFrames = job.getFrameRange('available');
				displ.uid = header.uid;
				[previewFrame, displ.previewInfo] = ...
					stLoadPreviewFrame(displ.currentFrame, ...
					job, st_state.constants.previewWindowSize);
				NumFrames = displ.previewInfo.NumFrames;
				st_state.displayed = displ;
        
				% normalise
				if st_state.state.normaliseFrame
					previewFrame = stNormaliseFrame(previewFrame);
				end
				
				% create appropriately sized image object in preview axis
				osz = st_state.displayed.previewInfo.Size;
				psz = st_state.displayed.previewInfo.PreviewSize;
				hold(st_state.h.preview.axis, 'off');
				st_state.h.preview.image = image( ...
					1:osz(2), 1:osz(1), repmat(previewFrame, [1 1 3]), ...
					'Parent', st_state.h.preview.axis);
				set(st_state.h.preview.axis, 'xtick', []);
				set(st_state.h.preview.axis, 'ytick', []);
				
				% hold on the axis - if any code overlays additional
				% data, it is that code's responsibility to clean it
				% up after it's finished. though, hold off is
				% called, above, just before a new image is created,
				% so that will clean up anything that's been left
				% behind.
				hold(st_state.h.preview.axis, 'on');
				
				% set axis limits such that aspect ratio is correct, but such that entire
				% image is displayed within the limits
				wsz = st_state.constants.previewWindowSize;
				aspectRatio = wsz(2) / wsz(1);
				
				% constrain axis by preview movie height
				axis_min_width = osz(2);
				axis_min_height = osz(1);
				axis_height = axis_min_height;
				axis_width = axis_height * aspectRatio;
				
				% if that overflows axis width, use width as the constraint instead
				if axis_width < axis_min_width
					axis_width = axis_min_width;
					axis_height = axis_width / aspectRatio;
				end
				
				% now, choose the axis offsets so that the movie frame is displayed "in the
				% middle"
				d = axis_width - osz(2);
				if d > 0
					ow = d / 2;
				else
					ow = 0;
				end
				d = axis_height - osz(1);
				if d > 0
					oh = d / 2;
				else
					oh = 0;
				end
				
				% set axis
				ax = [0.5+[[0 axis_width]-ow 0.5+[0 axis_height]-oh]];
				axis(st_state.h.preview.axis, ax);
				
				% add current frame indicator
				st_state.h.preview.current_frame_indicator = text( ...
					8, 12, int2str(st_state.displayed.currentFrame), ...
					'fontweight', 'bold', 'color', 'black', 'BackgroundColor', 0.8*[1 1 1], ...
					'hori', 'left', 'vert', 'top', ...
					'parent', st_state.h.preview.axis);
				
				% and delegate remaining updates
				changeDisplayedFrame(int32(0));
				changeFrameRanges(job, data.displayed);
				changeDisplayedMetaData(job);
				
			end
			
		end
		
		updateProcessingListbox(job);

	end
			
	function updateProcessingListbox(job)
			
		% update processing listbox
		ss = {};
		if ~isempty(job)
			[state, errors, pipelines] = job.getCurrentTaskState();
			col = [];
			col.object = 'FF0000';
			col.snout = '008000';
			col.whisker = '0000FF';
			for p = 1:length(pipelines)
				if p > 1
					ss{end+1} = '';
				end
				pp = pipelines{p};
				ss{end+1} = ['<html><span style="font-weight:bold">' upper(pp.name) '</span></html>'];
				s = ['<html>'];
				for m = 1:length(pp.methods)
					ss{end+1} = [s '&nbsp;&nbsp;<span style="color:' col.(pp.name) '">' pp.methods{m} '</span></html>'];
				end
			end
		end
		set(st_state.h.processing.listbox, 'string', ss, 'value', []);
			
	end

	function modified = confirmNotModified()
		
		% if no project yet exists, then not modified
		if isempty(st_project)
			modified = false;
			return
		end
		
		% confirm not modified, or obtain permission to continue
		modified = st_project.getModified();
		if modified
			
			if stUserData('devMode')
				disp(['WARNING: discarding modified project (because "devMode" is on)']);
				modified = false;
			else
				modified = ~stConfirm(['The project has been modified and has not been saved. Do you want to continue?' ...
					10 10 'Clicking "Yes" will PERMANENTLY discard your changes NOT JUST to the project but to ALL JOBS.'], ...
					{'Yes' 'No'});
			end
			
		end
		
	end








%% USER FEEDBACK FUNCTIONS

	function loglink = stLog(msg)
		
		% skip empty
		if isempty(msg)
			loglink = [];
			return
		end
		
		% skip debug messages
		if ischar(msg) && ~isempty(msg) && msg(1) == '$'
			if ~st_state.state.debugMode
				loglink = [];
				return
			end
			
			% else, trim "$" from beginning
			msg = msg(2:end);
		end
		
% 		% get current messages
% 		msgs = get(st_state.h.execute.log, 'String');
		
		% handle returned loglink or text message
		if isstruct(msg)
			
			% this is a returned loglink structure, from an
			% earlier "open section" message, and this is the
			% closing of that section
			loglink = msg;
			
			% if it matches the most recent message...
			if loglink.msgid == st_state.log.lastmsgid;
				st_state.log.strings{end} = [st_state.log.strings{end} ' (' loglink.result ')'];
				if st_state.log.logfile ~= -1
					fwrite(st_state.log.logfile, [st_state.log.strings{end} 10]);
				end
			else
				st_state.log.strings{end+1} = [datestr(now, 13) '  ' '... (' loglink.result ')'];
				if st_state.log.logfile ~= -1
					fwrite(st_state.log.logfile, [st_state.log.strings{end} 10]);
				end
				st_state.log.lastmsgid = st_state.log.lastmsgid + 1;
			end
			
			% if output argument requested, that's a misuse
			if nargout
				error('misuse of stLog() - do not request an output argument when closing a section');
			end
			loglink = [];
			
		else
			
			% this is a plain text message
			msg = [datestr(now, 13) '  ' msg];
			st_state.log.strings{end+1} = msg;
			st_state.log.lastmsgid = st_state.log.lastmsgid + 1;
			if st_state.log.logfile ~= -1
				fwrite(st_state.log.logfile, [st_state.log.strings{end} 10]);
			end
			
			% if output argument requested, return loglink object
			if nargout
				loglink = [];
				loglink.msgid = st_state.log.lastmsgid;
				loglink.result = 'OK';
			end
			
		end
		
		% crop to recent messages
		max_entries = 25;
		if length(st_state.log.strings) > max_entries
			st_state.log.strings = st_state.log.strings(end-max_entries+1:end);
		end
% 		disp(length(st_state.log.strings))
		
		% set current messages
		set(st_state.h.execute.log, 'String', st_state.log.strings);
		
		% ok
		drawnow
		
		% make bottom most line visible
		% http://undocumentedmatlab.com/blog/setting-line-position-in-edit-box-uicontrol
		if isempty(st_state.j.log)
			
			% this may fail - if it does, just leave it for next time
			try
				
				% obtain java handle
				j = stFindJObj(st_state.h.execute.log, 'persist');
				st_state.j.log = j.getComponent(0).getComponent(0);
				
				% 		% turn off text wrapping so filenames don't get mangled
				% 		st_state.j.log.setWrapping(0);
				% 		set(st_state.j.log, 'HorizontalScrollBarPolicy', 30);
				
			end
			
		end
		
		% do job
		if ~isempty(st_state.j.log)
			try
				st_state.j.log.setCaretPosition(st_state.j.log.getDocument.getLength);
			end
		end
		
	end

	function cancel = stProgress(frac, offerPause)

		% default args
		if nargin < 2
			offerPause = false;
		end
		
		% constrain
		w = min(max(1e-6, frac), 1);
		
		% update progress bar
		set(st_state.h.execute.waitbar_image, 'position', [0 0 w 1]);
		drawnow
		
		% get states
		[cancel, pause] = st_state.gui.isPauseOrCancel();
		
		% offer pause?
		if offerPause && pause
			
			% display
			stMessage('Operation paused - click OK to continue.');
			
		end
		
		% clear cancel/pause states
		st_state.gui.clearCancelAndPause();
		
	end

% 	function h_axis = stShowFrame(video, frameIndex, frameData)
%
% 		% display frame
% 		if nargin < 3
% 			frameData = [];
% 		end
% 		displayThisVideoFrame(video.indexInProject, video.filename, frameIndex, frameData);
%
% 		% return handle to axis for augmentation
% 		h_axis = st_state.h.preview.axis;
%
% 	end







%% HELPER METHODS

	function helper_setDefaultPipelineState()
		
		set(st_state.h.processing.listbox, 'string', {}, 'value', []);
		
		% 		% set selections
% 		pipelines = 'oswp';
% 		operations = 'pdto';
% 		for p = pipelines
% 			for o = operations
% 				role = [p o];
% 				if isfield(st_state.h.pipeline, role)
% 					if any(o == 'dt')
% 						set(st_state.h.pipeline.(role), 'Value', 1);
% 					else
% 						set(st_state.h.pipeline.(role), 'Value', []);
% 					end
% 				end
% 			end
% 		end
		
	end

	function helper_setFrame(varargin)
		
		jobs = st_project.getSelectedJobs();
		
		if length(jobs) > 1
			if ~stConfirm('Are you sure you want to change the frame range of all selected jobs?')
				return
			end
		end
		
		for j = 1:length(jobs)
			
			job = jobs{j};
		
			frames = job.getFrameRange();
			available = job.getFrameRange('available');

			constrain.initial = true;
			constrain.last = true;
			constrain.first = true;
			cf = fieldnames(constrain);

			for n = 1:2:length(varargin)
				key = varargin{n};
				val = varargin{n+1};
				if ischar(val)
					switch val
						case 'current'
							val = st_state.displayed.currentFrame;
						otherwise
							error('badness');
					end
				end
				val = min(max(val, 1), available);
				frames.(key) = val;
				constrain.(key) = false;
			end

			% constrain other, then all
			for loop = 1:2
				for c = 1:length(cf)
					if loop == 1 && ~constrain.(cf{c})
						continue
					end
					switch cf{c}
						case 'initial'
							frames.initial = min(frames.initial, frames.last);
							frames.initial = max(frames.initial, frames.first);
						case 'last'
							frames.last = max(frames.last, frames.initial);
							frames.last = max(frames.last, frames.first);
						case 'first'
							frames.first = min(frames.first, frames.initial);
							frames.first = min(frames.first, frames.last);
					end
				end
			end

			job.setFrameRange(frames);
			
		end
		
	end

	function label = helper_formatJobsListEntry(job)
		
		% frame ranges
		frames = job.getFrameRange();
		
% 		nframes = frames.last - frames.first + 1;
% 		if frames.step > 1
% 			label = sprintf('%i/%i', nframes, frames.step);
% 		else
% 			label = sprintf('%i', nframes);
% 		end
% 		label = rpad(label, 7);

		% frame range
		label = lpad(sprintf('%i', frames.first), 4);
		if frames.step == 1
			label = [label sprintf(' : ', frames.step)];
		else
			label = [label sprintf(':%i:', frames.step)];
		end
		label = [label sprintf('%i', frames.last)];
		label = rpad(label, 12);
		
		% key frame
		if st_state.state.bigScreen
	 		label = [label rpad(sprintf('(%i)', frames.initial), 7)];
		end
		
		% filename (job name, effectively)
		label = [label stProcessPath('justFilename', job.getVideoFilename())];
		
		% pipelines
		layout = 'osw';
% 		cols = {[0 0.5 1] [0 0 1] [1 0 0] [1 0 1]};
		mark = '   ';
		[state, errors, pipelines, JITdataLoaded] = job.getCurrentTaskState();
		for p = 1:length(pipelines)
			pp = pipelines{p};
			l = find(layout == pp.name(1));
			mark(l) = upper(pp.name(1));
		end
		mark = strrep(mark, 'O', '<span style="color:#FF0000">O</span>');
		mark = strrep(mark, 'S', '<span style="color:#008000">S</span>');
		mark = strrep(mark, 'W', '<span style="color:#0000FF">W</span>');
% 		mark = strrep(mark, 'P', '<span style="color:#FF00FF">P</span>');
% 		mark = ['<i>' mark '</i>'];
		label = [mark ' ' label];
		
		% job state
		[state, elsewhere] = job.getState();
		switch state
			case stJob.STATE_READONLY
				if elsewhere
					status = 'LE';
					fg = 'white';
					bg = 'red';
					st = '';
				else
					status = 'RO';
					fg = '#008000';
					bg = '';
					st = '';
				end
			case stJob.STATE_LOCKED
				status = 'LW';
				fg = 'blue';
				bg = '';
				st = '';
			case stJob.STATE_MODIFIED
				status = 'MO';
				fg = 'red';
				bg = '';
				st = '';
		end
		status = ['<span style="background-color:' bg '; color:' fg '; font-style:' st '">' status '</span>'];
		
		% task state
		[state, errors] = job.getCurrentTaskState();
		sp = '&nbsp;';
		dot = '&#9679;';
		cross = '&times;';
		raq = '&raquo;';
		loaded = '&diams;';
		switch state
			case stJob.TASK_ABSENT
				div = ['<span style="color: #666666">' dot '</span>' sp sp sp];
			case stJob.TASK_CREATED
				if ~isempty(job.getUserData('parametrized'))
					div = [sp '<span style="color: #00CCFF">' dot '</span>' '<span style="color: blue">' dot '</span>' sp];
				else
					div = [sp '<span style="color: #00CCFF">' dot '</span>' sp sp];
				end
			case stJob.TASK_STARTED
				div = [sp '<span style="color: #00CCFF">' dot raq '</span>' sp];
			case stJob.TASK_FINISHED
				if length(errors)
					div = [sp sp sp '<span style="color: red">' cross '</span>'];
				else
					div = [sp sp sp '<span style="color: #00CC00">' dot '</span>'];
				end
		end
		
		% displayed state
% 		if JITdataLoaded
% 			displ = [loaded sp];
% 		else
			displ = [sp sp];
% 		end
		dispbg = '<span>';
		if ~isempty(st_project)
			if st_project.isDisplayed(job)
% 				if JITdataLoaded
% 					displ = [loaded '>']
% 				else
					displ = ['>>'];
% 				end
				displ = ['<span style="background-color:#999999">' displ '</span>'];
% 				dispbg = '<span style="background-color: #FFB333">';
			end
		end
		
		% debug
		if JITdataLoaded
			displ = [loaded displ];
		else
			displ = [sp displ];
		end
		
		% job tag
		tag = job.getUserData('tag');
		if ~isempty(tag)
			label = [label ' (' tag ')'];
		end
		
		% 		% job flags
		% 		flags = '  ';
		% 		if job.flagIsSet('parametrized')
		% 			flags(1) = 'P';
		% 		end
		% 		if job.flagIsSet('processed')
		% 			flags(2) = 'C';
		% 		end
		% 		label = [flags '  ' label];
		
		% finalise
		label = strrep(label, ' ', '&nbsp;');
		label = ['<html>' displ sp dispbg status sp div sp label '</span></html>'];
		
	end

	function helper_addRecentProject(filename)
		
		% why does this happen? does it happen?
		if isempty(filename)
			warning('empty filename in helper_addRecentProject()');
			return
		end
		
		% get current recent file list
		recent = stUserData('recentProjects!');
		if isempty(recent)
			recent = {};
		end
		
		% remove if already present (so it goes to the top)
		for r = 1:length(recent)
			if stCompareFilenames(recent{r}, filename)
				recent = recent([1:r-1 r+1:end], 1);
				break
			end
		end
		
		% add this at the top of the list
		recent = flipud(recent);
		recent{end+1, 1} = filename;
		recent = flipud(recent);
		
		% trim list
		if length(recent) > 10
			recent = recent(1:10);
		end
		
		% set new recent file list
		stUserData('recentProjects', recent);
		
	end

	function helper_changeFontSize(delta)
		
		[state, prefs] = stUserData;
		listFontSize = stUserData('listFontSize');
		f = find([prefs.listFontSize.options{:}] == listFontSize);
		if ~isempty(f)
			f = f + delta;
			if f >= 1 && f <= length(prefs.listFontSize.options)
				stUserData('listFontSize', prefs.listFontSize.options{f});
				set(st_state.h.project.listbox, ...
					'fontsize', stUserData('listFontSize'));
				stLog(['selected font size ' int2str(stUserData('listFontSize')) 'pt']);
			end
		end

	end

	function helper_changeFontName(index)
		
		[state, prefs] = stUserData;
		if index <= length(prefs.listFontName.options) && ~strcmp(prefs.listFontName.options{index}, stUserData('listFontName'))
			stUserData('listFontName', prefs.listFontName.options{index});
			set([st_state.h.project.listbox st_state.h.processing.listbox st_state.h.output.listbox], ...
				'fontname', stUserData('listFontName'));
			stLog(['selected font "' stUserData('listFontName') '"']);
		end

	end

	function helper_refreshOutputsList()
		
		% get selected po methods
		if isempty(st_project)
			outconf = [];
			outconf.methods = {};
		else
			outconf = st_project.getOutputConfig();
		end
		
		% get list of all po methods
		methods = st_state.outputPlugins;
		
		% construct strings
		s1 = {};
		s2 = {};
		for m = 1:length(methods)
			method = methods{m};
			smethod = ['<data>' method '</data>'];
			s = ['<html>'];
			selected = any(ismember(outconf.methods, method));
			if selected
				s = [s '[&nbsp;' smethod '&nbsp;]'];
			else
				s = [s '&nbsp;&nbsp;' smethod '&nbsp;&nbsp;'];
			end
			s = [s '</html>'];
			if selected
				s1{end+1, 1} = s;
			else
				s2{end+1, 1} = s;
			end
		end
		
		% lay in
		set(st_state.h.output.listbox, 'string', cat(1, s1, s2), 'value', 1:length(s1));
		
	end

	function s = lpad(s, L)
		
		if length(s) < L
			s = [repmat(' ', 1, L - length(s)) s];
		end
		
	end

	function s = rpad(s, L)
		
		if length(s) < L
			s = [s repmat(' ', 1, L - length(s))];
		end
		
	end








% END TOP-LEVEL FUNCTION
end





