
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function stParametersGUI(jobs)



%% CREATE GUI

% remove jobs with no open task
keep = logical([]);
for j = 1:length(jobs)
	[state, errors, pipelines] = jobs{j}.getCurrentTaskState();
	keep(j) = state == stJob.TASK_CREATED && ~isempty(pipelines);
end
jobs = jobs(keep);
if isempty(jobs)
	stMessage('None of the selected jobs have a current task that is not already completed, so the parameters GUI will not be started.');
	return
end

% create GUI
gui = stGUI([820 640], gcbf, 'Parameters GUI', ...
	'CloseRequestFcn', @callback_Close, ...
	'ErrorFcn', @callback_ErrorFcn, ...
	'style', 'modal', ...
	'resizable', 'on');
% warning('not modal');
gui.setResizeFcn(@resizeGUI);
panelRoot = gui.rootPanel;
h_dialog = gui.getMainWindow();
theCallbackError = [];



%% CREATE LAYOUT

% pars
BH = 24;
BW = 64;
reduc = 0.3;

% create LHS/RHS
rhs = panelRoot.pack([], 'right', 240);
lhs = panelRoot.pack([], 'right', -1);

% LHS: create axis
h_axis = axes('parent', h_dialog, 'units', 'pixels');
lhs.pack(h_axis, 'left', -1);

% RHS: create job label
rhs.pack(createTitle('Select Job', [1 0 0]), 'top', 16);

% RHS: create job selector
jobNames = {};
for j = 1:length(jobs)
	jobNames{j} = stProcessPath('justFilename', jobs{j}.getVideoFilename());
end
panelJobSelector = rhs.pack(createSelector(jobNames, @callback_SelectJob), 'top', 24, reduc);

% RHS: create job label
rhs.pack(createTitle('Select Pipeline', [0 0.5 1]), 'top', 16);

% RHS: create job selector
panelPipelineSelector = rhs.pack(createSelector({' '}, @callback_SelectPipeline), 'top', 24, reduc);

% RHS: create job buttons
buttons = rhs.pack([], 'top', BH, reduc);
buttons.pack(createButton('Next', @callback_Next), 'right', BW);
buttons.pack(createButton('Test', @callback_Test), 'right', BW);
buttons.pack(createButton('Test PP', @callback_TestPP), 'right', BW);

% RHS: create parameter area
packing = rhs.pack([], 'top', 8);
panelPars = rhs.pack([], 'top', -1);



%% SHARED STATE

% set shared state
st.job = [];
st.selectedJob = [];
st.pipelines = {};
st.clickLink = stClickLink(h_axis);

% h shared
% h_dim = [];
h_image = [];
h_status = [];



%% INITIALISE AND WAIT

% no interrupts
gui.noInterrupts();

% layout
gui.resize();

% initialise
callback_SelectJob();

% wait for GUI
gui.waitForClose();

% check for error
if ~isempty(theCallbackError)
	rethrow(theCallbackError);
end







%% CALLBACKS

	function callback_SelectJob(obj, evt)
		
		% find
		obj = panelJobSelector.getH();
		newJob = jobs{get(obj, 'Value')};
		
		% check
		if ~isempty(st.job) && newJob == st.job
			return
		end
		
		% confirm discard current job
		if ~deselectCurrentJob(false)
			set(obj, 'value', st.selectedJob);
			return
		end
		
		% clear old display data
		for p = 1:length(st.pipelines)
			delete(st.pipelineOverlays{p});
		end
		delete(h_image);
		h_image = [];
		delete(h_status);
		h_status = [];
% 		delete(h_dim);
% 		h_dim = [];

		% lay in new job
		st.job = newJob;
		st.selectedJob = get(obj, 'Value');
		
		% get a raw frame
		rawFrame = stLoadMovieFrames(st.job.getVideoFilename(), st.job.getFrameRange('initial'), 'a');
		if stUserData('normaliseFrame')
			rawFrame = stNormaliseFrame(rawFrame);
		end
		
		% set shared state
		st.pipelines = {};
		st.pipelineImages = {};
		st.pipelineOverlays = {};
		st.selectedPipeline = [];
		st.pipelineProcessed = [];
		st.frameSize = size(rawFrame);
		
		% create image
		h_image = image(repmat(rawFrame, [1 1 3]), 'parent', h_axis);
		st.clickLink.setCallbackOnObject(h_image);
		set(h_axis, 'xtick', [], 'ytick', []);
		hold(h_axis, 'on');
		axis(h_axis, 'equal')
		set(h_axis, 'Color', 0.5*[1 1 1]);

% 		% create dim patch
% 		dimx = st.frameSize(2);
% 		dimy = st.frameSize(1);
% 		h_dim = patch([100 dimx dimx 100], [1 1 dimy dimy], 'w', 'parent', h_axis, 'FaceAlpha', 0.5, 'visible', 'off');
		
		% add status text
		h_status = text(0, 0, '', ...%st.frameSize(2)/2, st.frameSize(1)/2, '', ...
			'hori', 'left', ...
			'vert', 'top', ...
			'fontsize', 12, 'fontname', 'courier new', 'fontweight', 'bold', ...
			'color', [1 0 0], ...
			'backgroundcolor', [0 0 0], ...
			'parent', h_axis ...
			);
		
		% pipelines
		[temp1, temp2, st.pipelines] = st.job.getCurrentTaskState();
		pipelineNames = {};
		for p = 1:length(st.pipelines)
			pp = st.pipelines{p};
			pipelineNames{p} = pp.name;
			st.pipelineProcessed(p) = false;
			st.pipelineImages{p} = rawFrame;
			st.pipelineOverlays{p} = [];
		end
		
		% set state of pipeline selector
		set(panelPipelineSelector.getH(), 'string', pipelineNames, 'value', 1);

		% select a pipeline
		callback_SelectPipeline();
		
		% stackPush() job state
		st.job.stackPush();

	end



%% STATE CONTROL

	function okToContinue = deselectCurrentJob(save)
		
		okToContinue = true;
		
		% if no job currently selected
		if isempty(st.job)
			return
		end
		
		% if save
		if save
			
			% keep changes to pars
			st.job.stackPop(true);

			% mark job as parametrized
			st.job.setUserData('parametrized', true);
			
		else
			
% 			% if any changes have been made
% 			if ~stIdentical(st.job.stack.pars, st.job.data.pars)
% 			
% 				% ok to discard?
% 				if ~stConfirm('Are you sure you want to discard your changes to the parameters of this job?');
% 					okToContinue = false;
% 					return
% 				end
% 				
% 			end
			
% 			% discard changes to pars
% 			st.job.stackPop();

			% new code: keep pars, regardless
			st.job.stackPop(true);
			
		end
		
		% deselect job
		st.job = [];
		
	end

	function callback_ErrorFcn(err)

		% store error
		theCallbackError = err;

		% discard changes to pars and reset job state - this might
		% fail, in particular if stackPush() has not yet been called,
		% and if we get stuck here the GUI ain't closing, so...
		try
			st.job.stackPop();
		end
		
		% force close gui
		gui.delete();
		
	end

	function callback_Next(varargin)

		try
			
			% get context
			lastInList = st.job == jobs{length(jobs)};
			p = get(panelPipelineSelector.getH(), 'value');
			lastPipeline = p == length(get(panelPipelineSelector.getH(), 'string'));
			
			% if not last pipeline, move to next
			if ~lastPipeline
			
				% process current
				if ~st.pipelineProcessed(st.selectedPipeline)
					testPipeline(true);
				end
				
				% move on
				set(panelPipelineSelector.getH(), 'value', p+1);
				callback_SelectPipeline();
				return
				
			end
			
			% if last job in list
			if lastInList
				if stConfirm('That was the last job - do you want to close the parameters GUI?')
					deselectCurrentJob(true);
					gui.delete();
				end
			else
				deselectCurrentJob(true);
				set(panelJobSelector.getH(), 'value', get(panelJobSelector.getH(), 'value') + 1);
				callback_SelectJob();
			end

		catch err
			
			callback_ErrorFcn(err);
			
		end
		
	end

	function callback_Close(varargin)
		
		try

			if deselectCurrentJob(false)
				gui.delete();
			end
			
		catch err
			
			callback_ErrorFcn(err);
			
		end
		
	end







%% TEST CALLBACK

	function callback_TestPP(obj, evt)
		try
			testPipeline(false);
		catch err
			% display the error and try and continue
			stError(err);
		end
	end

	function callback_Test(obj, evt)
		try
			testPipeline(true);
		catch err
			% display the error and try and continue
			stError(err);
		end
	end
		
	function testPipeline(doAllMethods)

		try
			
			% freeze
			freeze(['processing...']);
			
			% extract
			pipeline = st.pipelines{st.selectedPipeline};
			
			% get methods, and pare if necessary
			methods = pipeline.methods;
			if ~doAllMethods
				methods_ = methods;
				methods = {};
				for m = 1:length(methods_)
					mm = methods_{m};
					if ~strcmp(mm(1:2), 'pp')
						break
					end
					methods{end+1} = mm;
				end
			end
			
			% prepare plotdata
			plotdata = [];
			plotdata.h_axis = h_axis;
			plotdata.frameIndex = st.job.getFrameRange('initial');

			% start task (with restart, if necessary)
			state = st.job.getCurrentTaskState();
			if state ~= stJob.TASK_CREATED
				st.job.taskRestart(true);
			end
			st.job.taskStart();
			
			% init each method
			states = {};
			for m = 1:length(methods)
				method = methods{m};
				freeze(['initializing ' method '...']);
				initdata = st.job.getInitializeData('forward');
				states{m} = st.job.methodInitialize(pipeline.name, method, initdata);
			end
			
			% run one frame of each method
			for m = 1:length(methods)
				method = methods{m};
				freeze(['running ' method '...']);
				st.job.methodProcess(pipeline.name, method, states{m}, initdata.frameIndices(1));
			end
			
			% get frame
			frame = st.job.getRuntimeFrame(plotdata.frameIndex, pipeline.name);
			st.pipelineImages{st.selectedPipeline} = frame{1};
			set(h_image, 'cdata', repmat(st.pipelineImages{st.selectedPipeline}, [1 1 3]));
			
			% clear overlays
			delete(st.pipelineOverlays{st.selectedPipeline});
			st.pipelineOverlays{st.selectedPipeline} = [];
			
			% plot each method
			h_overlay = [];
			for m = 1:length(methods)
				method = methods{m};
				output = st.job.methodPlot(pipeline.name, method, plotdata);
				if isfield(output, 'h')
					h_overlay = [h_overlay output.h];
				end
			end
			
			% raise h_status
			stRaise(h_axis, h_status);
			
			% store
			st.pipelineOverlays{st.selectedPipeline} = h_overlay;
			st.pipelineProcessed(st.selectedPipeline) = true;
			
			% lay in callbacks
			st.clickLink.setCallbackOnObject(h_overlay);
			
			% unfreeze
			freeze([]);

		catch err

			% unfreeze
			freeze([]);
			
			% rethrow
			rethrow(err);
				
		end
		
	end








%% CALLBACKS

	function callback_ParsChanged(pars)
		
 		% set pars in job
		pipeline = st.pipelines{st.selectedPipeline};
 		st.job.setParameters(pipeline.name, pars.user.methodName, pars.getPars());
		
	end

	function callback_SelectPipeline(varargin)
		
		try
			
			% find
			obj = panelPipelineSelector.getH();
			st.selectedPipeline = get(obj, 'Value');
			pipeline = st.pipelines{st.selectedPipeline};
			reducedMargin = 0.25;
			
			% set frame
			im = st.pipelineImages{st.selectedPipeline};
			set(h_image, 'cdata', repmat(im, [1 1 3]));
			
			% hide overlays from other pipelines, show our own
			for p = 1:length(st.pipelines)
				if p == st.selectedPipeline
					set(st.pipelineOverlays{p}, 'visible', 'on');
				else
					set(st.pipelineOverlays{p}, 'visible', 'off');
				end
			end
			
			% choose wording for aux click
			if ismac
				rc = 'Ctrl click';
			else
				rc = 'Right click';
			end
			
			% prepare click types
			clicktypes = {
				'normal' 'Left click'
				'alt' rc
				'extend' 'Shift click'
				};
			next_clicktype = 1;
			
			% prepare click links (empty for now)
			st.clickLinks = struct();
			
			% clear
			st.clickLink.clear();
			panelPars.clear();
			
			% for each method
			for m = 1:length(pipeline.methods)
				
				% extract
				method = pipeline.methods{m};
				defpars = stPlugin(method, 'parameters');
				pars = st.job.getParameters(pipeline.name, method, true);
				
				% heading
				head = panelPars.pack([], 'top', 16);
				head.pack(createTitle(method, [0 0 1]), 'right', -1);

				% collect panels
				obj = stPars(st.job, h_axis, pars, method, panelPars, 1, h_dialog, @callback_ErrorFcn, @callback_ParsChanged, st.clickLink);
				obj.user.methodName = method;

				% collapse button
				h_button = createButton('+', @callback_collapse, obj);
				head.pack(h_button, 'right', 16, 0.5);
				
				% packing
				packing = panelPars.pack([], 'top', 8);
				
			end
			
			% resize
			panelPars.resize();
			
		catch err
			
			callback_ErrorFcn(err);
			
		end
		
	end

	function callback_collapse(obj, evt)
		
 		pars = get(obj, 'UserData');
		pars.toggleShow();
		
	end







%% HELPERS

	function resizeGUI(gui)
		axis(h_axis, 'equal');
	end

	function freeze(str)
		
		if ~isempty(str)
			
% 			if isempty(h_dim)
% 				dimy = st.frameSize(1);
% 				dimx = st.frameSize(2);
% 				h_dim = patch([1 dimx dimx 1], [1 1 dimy dimy], 'w', 'parent', h_axis, 'FaceAlpha', 0.5);
% 			end
% set(h_dim, 'visible', 'on');
			k = getKids(h_dialog);
			for i = 1:length(k)
				try
					set(k(i), 'enable', 'off');
				end
			end
			set(h_status, 'string', str);
			
		else
			
% 			delete(h_dim);
% 			h_dim = [];
% set(h_dim, 'visible', 'off');
			k = getKids(h_dialog);
			for i = 1:length(k)
				try
					set(k(i), 'enable', 'on');
				end
			end
			set(h_status, 'string', '');
			
		end
		
		drawnow
		
	end

	function h = getKids(h)
		
		c = get(h, 'children')';
		h = c;
		
		for i = 1:length(c)
			h = [h getKids(c(i))];
		end
		
	end

	function h = createTitle(text, color)

		h = uicontrol(h_dialog, ...
			'String', text, ...
			'fontsize', 10, 'fontname', 'Tahoma', 'fontweight', 'bold', ...
			'Hori', 'Left', ...
			'ForegroundColor', color, ...
			'Style', 'Text');

	end

	function h = createButton(text, callback, userdata)
		
		if nargin < 3
			userdata = '';
		end
		
		h = uicontrol(h_dialog, ...
			'Style', 'pushbutton', ...
			'String', text,...
			'UserData', userdata, ...
			'Callback', callback ...
			);
		
	end

	function h = createCheckbox(text, callback, userdata)
		
		if nargin < 3
			userdata = '';
		end
		
		h = uicontrol(h_dialog, ...
			'Style', 'checkbox', ...
			'String', text,...
			'UserData', userdata, ...
			'Callback', callback ...
			);
		
	end

	function h = createSelector(options, callback)
		
		h = uicontrol(h_dialog, ...
			'Style', 'popupmenu', ...
			'String', options, ...
			'BackgroundColor', [1 1 1], ...
			'Max', 1, ...
			'Min', 0, ...
			'Callback', callback ...
			);
		
	end


end


