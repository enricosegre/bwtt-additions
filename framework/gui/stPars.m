
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


classdef stPars < handle
	
	properties
		
		job
		h_axis
		methodName
		pars
		defpars
		panelCaller
		panelColumns
		panelItems
		h_dialog
		errorFcn
		changedFcn
		clickLink
		user
		
	end
	
	methods
		
		function obj = stPars(job, h_axis, pars, methodName, panelCaller, ncols, h_dialog, errorFcn, changedFcn, clickLink)
			
			% NB: job is not always supplied. if it is supplied,
			% it is passed on to 'editParameters' if any of the
			% parameters are of type 'custom'.
			
			obj.job = job;
			obj.h_axis = h_axis;
			obj.methodName = methodName;
			obj.pars = pars;
			obj.defpars = stPlugin(methodName, 'parameters');
			obj.panelCaller = panelCaller;
			obj.panelItems = {};
			obj.h_dialog = h_dialog;
			obj.errorFcn = errorFcn;
			obj.changedFcn = changedFcn;
			obj.clickLink = clickLink;
			
			if ~isempty(panelCaller)

				% if using columns, NB that toggleShow() will not
				% work correctly
				if ncols > 1
				
					% create column container
					panelColumnContainer = panelCaller.pack([], 'top', -1);
				
					% create columns
					for col = 1:ncols
						obj.panelColumns{col} = panelColumnContainer.pack([], 'left', -1);
					end
					
				else

					obj.panelColumns = {panelCaller};
					
				end
				
				% create pars
				for p = 1:length(obj.defpars)
					obj.addParPanel(p);
				end
				
				% update show
				obj.updateShow();
				
				% defaults button
				h_def = uicontrol(obj.h_dialog, ...
					'Style', 'pushbutton', ...
					'String', 'Defaults',...
					'Callback', @callback_SetDefaults, ...
					'UserData', {obj} ...
					);
				sub = obj.panelCaller.pack([], 'top', 24);
				sub.pack(h_def, 'right', 64);
				
				% store
				obj.panelItems = cat(1, obj.panelItems, {sub});
				
			end
			
		end
		
		function toggleShow(obj)
			
			for p = 1:length(obj.panelItems)
				obj.panelItems{p}.show(-1);
			end
			obj.panelCaller.resize();
			
		end
		
		function value = getPar(obj, key)
			
			pars = obj.getAllPars();
			value = pars.(key);
			
		end
		
		function pars = getPars(obj)
			
			pars = obj.pars;
			
		end
		
		function pars = getAllPars(obj)
			
			pars = stMergePars(obj.pars, obj.defpars);
			
		end
		
		function addParPanel(obj, p)
			
			% get defpar
			defpar = obj.defpars{p};
			value = obj.getPar(defpar.name);
			
			% ignore constant
			if strcmp(defpar.type, 'constant')
				return
			end
			
			% collect
			panelItems = {};
			
			% get number of non-constant pars
			parcount = 0;
			for i = 1:length(obj.defpars)
				if ~strcmp(obj.defpars{i}.type, 'constant')
					parcount = parcount + 1;
				end
				if i == p
					parindex = parcount;
				end
			end
			
			% get parent
			colindex = floor((parindex-1) / parcount * length(obj.panelColumns)) + 1;
			panelColumn = obj.panelColumns{colindex};
			
			% add label
			labelHeight = 16;
			if ismac
				labelHeight = 24;
			end
			switch defpar.type
				case 'flag'
				otherwise
					h_label = uicontrol(obj.h_dialog, ...
						'Style', 'text', ...
						'String', defpar.label, ...
						'HorizontalAlignment', 'left' ...
						);
					if isempty(defpar.help)
						lab = panelColumn.pack(h_label, 'top', labelHeight, 0.25);
					else
						lab = panelColumn.pack([], 'top', labelHeight, 0.25);
						h_help = uicontrol(obj.h_dialog, ...
							'Style', 'pushbutton', ...
							'String', '?', ...
							'ForegroundColor', [1 1 1], ...
							'BackgroundColor', [0 0 1], ...
							'Userdata', defpar.help, ...
							'Callback', @callback_help ...
							);
						panelItems{end+1} = lab.pack(h_help, 'left', 16);
						panelItems{end+1} = lab.pack(h_label, 'left', -1);
					end
					panelItems{end+1} = lab;
			end
			
			% switch on type
			switch defpar.type
				
				case 'option'
					
					% create editable value
					h_check = uicontrol(obj.h_dialog, ...
						'Style', 'popupmenu', ...
						'Value', find(ismember(defpar.options, value)), ...
						'BackgroundColor', [1 1 1], ...
						'String', defpar.options, ...
						'Callback', @callback_ChangeValue, ...
						'UserData', {obj p} ...
						);
					panelItems{end+1} = panelColumn.pack(h_check, 'top', 16);
					
				case 'flag'
					
					% create editable value
					h_check = uicontrol(obj.h_dialog, ...
						'Style', 'checkbox', ...
						'Value', value, ...
						'String', defpar.label, ...
						'Callback', @callback_ChangeValue, ...
						'UserData', {obj p} ...
						);
					
					if isempty(defpar.help)
						lab = panelColumn.pack(h_check, 'top', 16, 0.25);
					else
						lab = panelColumn.pack([], 'top', 16, 0.25);
						h_help = uicontrol(obj.h_dialog, ...
							'Style', 'pushbutton', ...
							'String', '?', ...
							'ForegroundColor', [1 1 1], ...
							'BackgroundColor', [0 0 1], ...
							'Userdata', defpar.help, ...
							'Callback', @callback_help ...
							);
						panelItems{end+1} = lab.pack(h_help, 'left', 16);
						panelItems{end+1} = lab.pack(h_check, 'left', -1);
					end
					
					panelItems{end+1} = lab;
					
				case 'custom'
					
					% create editable value
					h_edit = uicontrol(obj.h_dialog, ...
						'Style', 'pushbutton', ...
 						'Callback', @callback_ChangeValue, ...
 						'UserData', {obj p}, ...
						'String', 'Edit' ...
						);
					panelItems{end+1} = lab.pack(h_edit, 'right', 64);
					
% 					if isempty(defpar.help)
% 						lab = panelColumn.pack(h_check, 'top', 16, 0.25);
% 					else
% 						lab = panelColumn.pack([], 'top', 16, 0.25);
% 						h_help = uicontrol(obj.h_dialog, ...
% 							'Style', 'pushbutton', ...
% 							'String', '?', ...
% 							'ForegroundColor', [1 1 1], ...
% 							'BackgroundColor', [0 0 1], ...
% 							'Userdata', defpar.help, ...
% 							'Callback', @callback_help ...
% 							);
% 						panelItems{end+1} = lab.pack(h_help, 'left', 16);
% 						panelItems{end+1} = lab.pack(h_check, 'left', -1);
% 					end
					
					panelItems{end+1} = lab;
					
				case 'string'
					
					% create editable value
					h_edit = uicontrol(obj.h_dialog, ...
						'Style', 'edit', ...
						'String', value, ...
						'BackgroundColor', [1 1 1], ...
						'Callback', @callback_ChangeValue, ...
						'Hori', 'Left', ...
						'UserData', {obj p} ...
						);
					panelItems{end+1} = panelColumn.pack(h_edit, 'top', 20);
					
				case 'scalar'
					
					% create editable value
					h_edit = uicontrol(obj.h_dialog, ...
						'Style', 'edit', ...
						'String', formatPar(value, defpar), ...
						'BackgroundColor', [1 1 1], ...
						'Callback', @callback_ChangeValue, ...
						'UserData', {obj p} ...
						);
					panelItems{end+1} = lab.pack(h_edit, 'right', 64);
					
					% create slider
					rng = diff(defpar.range);
					h_slider = uicontrol(obj.h_dialog, ...
						'Style', 'slider',...
						'Max', defpar.range(2), ...
						'Min', defpar.range(1), ...
						'Value', value, ...
						'SliderStep', [defpar.step(1)/rng defpar.step(2)/rng],...
						'BackgroundColor', [1 1 1], ...
						'Callback', @callback_ChangeValue, ...
						'UserData', {obj p} ...
						);
					panelItems{end+1} = panelColumn.pack(h_slider, 'top', 16);
					
				case 'xy'

					% get click link
					link = obj.clickLink.makeLink(obj, p);

					% create symbol
					h_sym = plot(link.h_axis, value(1), value(2), defpar.symbol, ...
						'ButtonDownFcn', link.callback, ...
						'UserData', obj.clickLink ...
						);
					panelColumn.addSup(h_sym);

					% create un-editable value
					h_edit = uicontrol(obj.h_dialog, ...
						'Style', 'text', ...
						'String', sprintf(['%d, %d'], value), ...
						'Tag', 'update', ...
						'UserData', h_sym, ...
						'BackgroundColor', [1 1 1] ...
						);
					panelItems{end+1} = lab.pack(h_edit, 'right', 64);

					% create info
					infomsg = [link.text ' image to change'];
					h_info = uicontrol(obj.h_dialog, ...
						'Style', 'text',...
						'String', infomsg, ...
						'BackgroundColor', get(obj.h_dialog, 'Color') ...
						);
					panelItems{end+1} = panelColumn.pack(h_info, 'top', 16);

				otherwise
					error(defpar.type)
					
			end
			
			% margin before next par
			panelItems{end+1} = panelColumn.pack([], 'top', 0);
			
			% store
			obj.defpars{p}.panelItems = panelItems;
			
			% store
			obj.panelItems = cat(1, obj.panelItems, panelItems');
			
		end
		
		function customChangeValue(obj, index)

			% get defpar
			defpar = obj.defpars{index};
			
			% get existing value
			value = obj.getPar(defpar.name);
			
			% call method to do edit
			data = [];
			data.h_axis = obj.h_axis;
			data.par = defpar;
			data.value = value;
			result = stPlugin(obj.methodName, 'editParameter', obj.job, data);
			
			% respond
			if ~isempty(result)
				
				% lay in
				obj.pars.(defpar.name) = result.value;

				% service show
				obj.updateShow();

				% callback
				if ~isempty(obj.changedFcn)
					obj.changedFcn(obj);
				end

			end
			
		end
		
		function changeValue(obj, index, newValue)
			
			% extract
			defpar = obj.defpars{index};
			
			% if not supplied, just update
			if nargin < 3
				
				% use existing
				newValue = obj.getPar(defpar.name);
				
			else
				
				% range
				switch defpar.type
					case {'scalar'}
						if ischar(newValue)
							newValue = str2num(newValue);
						end
						newValue = max(min(newValue, defpar.range(2)), defpar.range(1));
				end
				
				% precision
				switch defpar.type
					case 'scalar'
						scale = 10 ^ defpar.precision;
						newValue = round(newValue * scale) / scale;
						mindiff = 1 / scale;
					case 'xy'
						newValue = round(newValue);
				end
				
				% get other pars
				pars = obj.getAllPars();
				
				% constraints
				switch defpar.type
					case {'scalar'}
						for c = 1:length(defpar.constraints)
							cn = defpar.constraints{c};
							conValue = pars.(cn{2});
							switch cn{1}
								case '<'
									newValue = min(newValue, conValue-mindiff);
								case '>'
									newValue = max(newValue, conValue+mindiff);
								case '<='
									newValue = min(newValue, conValue);
								case '>='
									newValue = max(newValue, conValue);
							end
						end
				end
				
				% lay in
				obj.pars.(defpar.name) = newValue;
				
			end
			
			% update display
			panelItems = defpar.panelItems;
			for s = 1:length(panelItems)
				
				% get h
				h = panelItems{s}.getH();
				
				% handle update
				if ~isempty(h)
					
					switch get(h, 'style')
						
						case {'checkbox' 'slider'}
							set(h, 'value', newValue);
							
						case 'edit'
							switch defpar.type
								case 'scalar'
									set(h, 'string', formatPar(newValue, defpar));
								case 'string'
									set(h, 'string', newValue);
							end
							
						case 'text'
							switch defpar.type
								case 'xy'
									if strcmp(get(h, 'Tag'), 'update')
										set(h, 'string', sprintf('%d, %d', newValue));
										h_sym = get(h, 'UserData');
										set(h_sym, 'xdata', newValue(1), 'ydata', newValue(2));
									end
							end
							
						case 'popupmenu'
							indexValue = find(ismember(defpar.options, newValue));
							set(h, 'value', indexValue);
							
						case 'text'
							% the label - ignore this
							
						case 'pushbutton'
							% the help button - ignore this
							
						otherwise
							error(get(h, 'style'))
							
					end
					
				end
				
			end
			
			% service show
			obj.updateShow();
			
			% callback
			if ~isempty(obj.changedFcn)
				obj.changedFcn(obj);
			end
			
		end
		
		function updateShow(obj)
			
			% get pars now
			pars = obj.getAllPars();
			
			% for each par
			for p = 1:length(obj.defpars)
				
				% extract
				defpar = obj.defpars{p};
				if isempty(defpar.show)
					continue
				end
				
				% handle
				s = eval(defpar.show);
				
				% update
				changed = false;
				for q = 1:length(defpar.panelItems)
					if defpar.panelItems{q}.show(s)
						changed = true;
					end
				end
				
				% resize
				if changed
					obj.panelCaller.resize();
				end
				
			end
			
		end
		
		function setDefaults(obj)
			
			obj.pars = [];
			for p = 1:length(obj.defpars)
				if ~strcmp(obj.defpars{p}.type, 'constant')
					obj.changeValue(p);
				end
			end
			
		end
		
	end
	
end




function s = formatPar(value, defpar)

s = sprintf(['%.' int2str(defpar.precision) 'f'], value);

end

function callback_ChangeValue(hObject, evt)

% extract
st = get(hObject, 'style');
ud = get(hObject, 'UserData');
obj = ud{1};
p = ud{2};

% catch errors
try
	
	% get value
	switch st
		case 'popupmenu'
			s = get(hObject, 'string');
			v = get(hObject, 'value');
			obj.changeValue(p, s{v});
		case {'slider' 'checkbox'}
			obj.changeValue(p, get(hObject, 'value'));
		case 'edit'
			obj.changeValue(p, get(hObject, 'String'));
		case 'pushbutton'
			switch get(hObject, 'string')
				case 'Edit'
					obj.customChangeValue(p);
			end
		otherwise
			error(st)
	end
	
catch err
	obj.errorFcn(err);
end

end

function callback_SetDefaults(hObject, evt)

% catch errors
try
	
	% extract
	ud = get(hObject, 'UserData');
	obj = ud{1};
	
	% call
	obj.setDefaults();
	
catch err
	obj.errorFcn(err);
end

end

function callback_help(hObject, evt)

help = get(hObject, 'userdata');
stMessage(help);

end


