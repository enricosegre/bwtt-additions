
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


classdef stPanel < handle
	
	properties (Access = private)
	
		% handle of managed object
		h
		
		% children
		children
		
		% parent only data
		rootmargin
		childmargin
		
		% self packing data
		dim
		dimend
		size
		margin
		
		% cached rect
		rect

		% supplemental handles, deleted on clear()
		h_sup
		
		% hidden if true
		hide
		
	end
	
	methods
		
		function panel = stPanel(h, size, margin)

			if nargin < 2
				size = [];
			end
			
			if nargin < 3
				margin = [];
			end
			
			panel.h = h;
			panel.children = [];
			panel.rootmargin = 8;
			panel.childmargin = [0 0 0 0];
			panel.dim = [];
			panel.dimend = [];
			panel.size = size;
			panel.margin = margin;
			panel.rect = [];
			panel.h_sup = [];
			panel.hide = false;

		end
		
		function delete(panel)
			
			h = [panel.h panel.h_sup];
			delete(h(ishandle(h)));
			
		end
		
		function h = getH(panel)
			
			h = panel.h;
			
		end
		
		function vis(panel, h_parent)
			
			panel.h = uicontrol('style', 'pushbutton', ...
				'parent', h_parent, 'backgroundcolor', [1 0 0]);
			
		end
		
		function changed = show(panel, value)
			
			switch value
				
				case -1
					% toggle
					changed = true;
					panel.hide = ~panel.hide;
					
				case 0
					% hide
					changed = panel.hide == false;
					panel.hide = true;
					
				case 1
					% show
					changed = panel.hide == true;
					panel.hide = false;
					
			end
			
		end
		
		function setChildMargin(panel, childmargin)
			
			panel.childmargin = childmargin;
			
		end
		
		function rect = resize(panel, rect)

			% store/retrieve rect
			if nargin == 2
				panel.rect = rect;
			else
				rect = panel.rect;
			end
			
			% if no rect yet, can't do it yet
			if isempty(rect)
				return
			end

			% position self
			set(panel.h, 'units', 'pixels', 'position', rect);
			
			% apply childmargin
			rect = rect + [panel.childmargin(1:2) -panel.childmargin(1:2)-panel.childmargin(3:4)];

			% extract
			pos = rect(1:2);
			size = rect(3:4);
			stretchySizes = [];
			
			% pack children
			for c = 1:length(panel.children)
				
				% get child
				child = panel.children(c);
				
				% handle stretchy
				if child.size == -1
					
					if isempty(stretchySizes)
					
						% get remaining space
						stretchyCount = 0;
						availSpace = size(child.dim);
						stretchyIndices = [];
						for c2 = c:length(panel.children)
							child2 = panel.children(c2);
							if child2.dim ~= child.dim
								error('cannot change packing dim when packing stretchy');
							end
							if child2.hide
								if child2.size == -1
									stretchyIndices = [stretchyIndices c2];
								end
								continue
							end
							if child2.size == -1
								stretchyCount = stretchyCount + 1;
								stretchyIndices = [stretchyIndices c2];
							else
								availSpace = availSpace - child2.size;
							end
							if c2 ~= c
								availSpace = availSpace - child2.margin * child2.rootmargin;
							end
						end
						stretchySizes(stretchyIndices) = availSpace / stretchyCount;
						
					end
					
					% get size
					childsize = stretchySizes(c);
					
				else
					
					% get size
					childsize = child.size;
					
				end
				
				% switch on packing size
				if childsize > 0 || (childsize == 0 && isempty(child.h))
					
					if child.hide
						child.resize([25000 0 10 10]);
					else

% 						% if no more space, cannot actually pack
% 						if size(child.dim) <= 0
% 							continue
% 						end
						
						% absolute packing size (pixels)
						csize = size;
						csize(child.dim) = childsize;
						cpos = pos;
						if child.dimend == 2
							cpos(child.dim) = cpos(child.dim) + size(child.dim) - childsize;
						end
						child.resize([cpos csize]);

						% reduce remaining space
						sz = childsize + child.margin * child.rootmargin;
						size(child.dim) = size(child.dim) - sz;
						if child.dimend == 1
							pos(child.dim) = pos(child.dim) + sz;
						end
						
					end
					
				else
					
					% invalid packing size
					warning('bad packing size - resizing halted');
					
				end
				
			end
			
		end
		
		function child = pack(panel, h, side, size, margin)

			% default arg
			if nargin < 5
				margin = 1;
			end
			
			% handle '*' suffix (collapse margin)
			if any(side == '*')
				margin = 0;
				side = side(side ~= '*');
			end
			
			% create panel
			child = stPanel(h, size, margin);
			
			% pass on rootmargin
			child.rootmargin = panel.rootmargin;

			% handle side
			switch side
				case 'left'
					child.dim = 1;
					child.dimend = 1;
				case 'right'
					child.dim = 1;
					child.dimend = 2;
				case 'top'
					child.dim = 2;
					child.dimend = 2;
				case 'bottom'
					child.dim = 2;
					child.dimend = 1;
				otherwise
					error('bad value for "side"');
			end

			% add child
			if isempty(panel.children)
				panel.children = child;
			else
				panel.children(end+1) = child;
			end
			
		end
		
		function clear(panel)
			
			% must explicitly call delete(), in case handle
			% graphics objects held by panel have recursive
			% references to panel object which happens, for
			% instance, when stPars is used.
			for c = 1:length(panel.children)
				cc = panel.children(c);
				cc.clear();
				delete(cc);
			end
			
			% clear children
			panel.children = [];
			
		end
		
		function addSup(panel, h)
			
			panel.h_sup(end+1) = h;
			
		end
		
	end
	
end

