
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function prop = stPropagateGUI(job)

% get list of methods
pars = job.getAllParameters();
fn = fieldnames(pars);
methods = {};
for n = 1:length(fn)
	key = fn{n};
	methods{end+1} = key;
% 	pos = find(key == '_', 1, 'first');
% 	pipeline = key(1:pos-1);
% 	method = key(pos+1:end);
% 	
end

% pars
h_parent = gcbf;
wh = [480 480];
guipars = stGUIPars;

% create
gui = stGUI(wh, h_parent, 'Propagate parameters');
rect = gui.rootPanel;
h_dialog = gui.getMainWindow();

% layout
buts = rect.pack([], 'bottom', 32);

% rect = rect.pack([], 'bottom', -1);

% message
h_text = uicontrol(h_dialog, ...
	'style', 'text', ...
	'hori', 'left', ...
	'fontname', guipars.fontName, ...
	'fontsize', guipars.fontSize, ...
	'string', 'Select all the methods from which you want to propagate parameters.', ...
	'units', 'pixels' ...
	);
rect.pack(h_text, 'top', 24);

% % processing mode selector
% h_mode = uibuttongroup(h_dialog, ...
% 	'visible', 'on' ...
% 	);
% uh = 24;
% temp = rect.pack([], 'top', uh*3);
% temp.pack([], 'left', 16);
% mode = temp.pack([], 'left', -1);
% h_radio = uicontrol(h_mode, ...
% 	'Style', 'Radio', ...
% 	'fontname', 'Helvetica', ...
% 	'fontsize', 10, ...
% 	'UserData', 1, ...
% 	'String', 'Immediate Sequential Local Processing' ...
% 	);
% mode.pack(h_radio, 'top*', uh);
% h_radio = uicontrol(h_mode, ...
% 	'Style', 'Radio', ...
% 	'fontname', 'Helvetica', ...
% 	'fontsize', 10, ...
% 	'UserData', 2, ...
% 	'String', 'Immediate Parallel Local Processing (matlabpool, parfor)' ...
% 	);
% mode.pack(h_radio, 'top*', uh);
% h_radio = uicontrol(h_mode, ...
% 	'Style', 'Radio', ...
% 	'fontname', 'Helvetica', ...
% 	'fontsize', 10, ...
% 	'UserData', 3, ...
% 	'String', 'Parallel Batch (prepare script for external processing)' ...
% 	);
% mode.pack(h_radio, 'top*', uh);
% 
% % message
% h_text = uicontrol(h_dialog, ...
% 	'style', 'text', ...
% 	'hori', 'left', ...
% 	'fontname', 'Arial', ...
% 	'fontsize', 14, ...
% 	'string', 'Job List', ...
% 	'units', 'pixels' ...
% 	);
% rect.pack(h_text, 'top', 24);

% listbox
h_list = uicontrol(h_dialog, ...
	'style', 'listbox', ...
	'hori', 'left', ...
	'fontname', stUserData('listFontName'), ...
	'fontsize', stUserData('listFontSize'), ...
	'BackgroundColor', [1 1 1], ...
	'fontname', guipars.listFontName, ...
	'fontsize', guipars.listFontSize, ...
	'string', methods, ...
	'value', 1:length(methods), ...
	'Min', 1, 'Max', 3, ...
	'units', 'pixels' ...
	);
rect.pack(h_list, 'top', -1);

% buttons
buttons = {'OK' 'Cancel'};
bw = 64;
for b = 1:length(buttons)
	h_buttons(1, b) = uicontrol(h_dialog, ...
		'style', 'pushbutton', ...
		'fontname', guipars.fontName, ...
		'fontsize', guipars.fontSize, ...
		'userdata', b, ...
		'callback', @button_callback, ...
		'string', buttons{b}, ...
		'units', 'pixels' ...
		);
end

% layout buttons
for b = 1:length(h_buttons)
	buts.pack(h_buttons(b), 'right', bw);
end

% layout
gui.resize();

% wait
prop = [];
gui.waitForClose();






	function button_callback(h_button, evt)
		
		response = buttons{get(h_button, 'userdata')};
 		if strcmp(response, 'OK')
			s = get(h_list, 'string');
			v = get(h_list, 'value');
			prop = s(v);
 		end
		gui.close();
		
	end



end




