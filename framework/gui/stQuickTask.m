
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function result = stQuickTask(arg)

if nargin < 1
	arg = 'get all';
end

switch arg
	
	case 'get all'
		
		result = {'Snout Tracking' 'Whisker Tracking (wdIgorMeanAngle)' 'Whisker Tracking (wdIgor)'};
		
	case 'Snout Tracking'
		
		result = {};
		
		pipeline = [];
		pipeline.name = 'snout';
		pipeline.methods = {'sdGeneric' 'stShapeSpaceKalman'};
		result{end+1} = pipeline;
		
	case 'Whisker Tracking (wdIgorMeanAngle)'
		
		result = {};
		
		pipeline = [];
		pipeline.name = 'snout';
		pipeline.methods = {'sdGeneric' 'stShapeSpaceKalman'};
		result{end+1} = pipeline;
		
		pipeline = [];
		pipeline.name = 'whisker';
		pipeline.methods = {'ppBgExtractionAndFilter' 'wdIgorMeanAngle'};
		result{end+1} = pipeline;
		
	case 'Whisker Tracking (wdIgor)'
		
		result = {};
		
		pipeline = [];
		pipeline.name = 'snout';
		pipeline.methods = {'sdGeneric' 'stShapeSpaceKalman'};
		result{end+1} = pipeline;
		
		pipeline = [];
		pipeline.name = 'whisker';
		pipeline.methods = {'ppBgExtractionAndFilter' 'wdIgor'};
		result{end+1} = pipeline;
		
	otherwise
		
		error(arg);
	
end

end
