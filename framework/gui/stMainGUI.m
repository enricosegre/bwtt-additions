
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


classdef stMainGUI < handle
	
	properties
		
		pars
		state
		h_dialog
		
		% when a panel is created, it is set as the new...
		h_parent
		
	end
	
	methods
		
		function gui = stMainGUI(userEvent)
			
			gui.h_parent = [];
			
			pars = stGUIPars;
			pars.dialogSize = [1000 680];
			% 			pars.dialogSize = [1600 1000];
			pars.previewSize = [512 288];
			pars.panelBorder = [6 7 -18 -32];
			pars.labelHeight = 16;
			pars.runWidth = 44;
			pars.checkHeight = 20;
			gui.pars = pars;
			
			
			
			%% CREATE DIALOG
			
			% get screen size
			ss = get(0, 'ScreenSize');
			ss = ss(3:4);
			
			% define dialog size and position
			wh = pars.dialogSize;
			pos = [(ss-wh)/2 wh];
			
			% create dialog
			state = [];
			gui.h_dialog = figure( ...
				'WindowStyle', 'normal', ... % not modal so that user can still use the command window
				'Visible', 'off', ...
				'Color', get(0, 'DefaultUicontrolBackgroundColor'), ...
				'Name', 'BIOTACT Whisker Tracking Tool', ...
				'Units', 'pixels', ...
				'IntegerHandle', 'off', ...
				'HandleVisibility', 'on', ...
				'PaperPositionMode', 'auto', ...
				'MenuBar', 'none', ...
				'DockControls', 'off', ...
				'NumberTitle', 'off', ...
				'CloseRequestFcn', userEvent.closeRequest, ...
				'KeyPressFcn', userEvent.keyPress, ...
				'UserData', struct('id', 'BWTTMainGUI', 'gui', gui), ...
				'Position', pos ...
				);
			state.h.mainGUI = gui.h_dialog;
			
			
			% dialog() settings...
			%
			%   'BackingStore'      - 'off'
			%   'ButtonDownFcn'     - 'if isempty(allchild(gcbf)), close(gcbf), end'
			%   'InvertHardcopy'    - 'off'
			% 				'Resize', 'off', ...
			%   'PaperPositionMode' - 'auto'
			
			
			
			%% PROJECT CONTROL
			
			% add panel
			state.h.project.panel = gui.createPanel('Job List');
			
			% add listbox
			state.h.project.listbox = uicontrol(state.h.project.panel, ...
				'Style', 'Listbox', ...
				'Units', 'pixels', ...
				'Callback', userEvent.modifyState, ...
				'Tag', 'Project Listbox', ...
				'BackgroundColor', [1 1 1], ...
				'Min', 1, 'Max', 3, ...
				'FontName', pars.listFontName, ...
				'FontSize', pars.listFontSize ...
				);
			
			% 			% add text (autosave indicator)
			% 			state.h.project.autosave = uicontrol( ...
			% 				'Style', 'Text', ...
			% 				'Units', 'pixels', ...
			% 				'String', 'Autosaving...', ...
			% 				'BackgroundColor', [0.5 0.5 1], ...
			% 				'ForegroundColor', [1 1 1], ...
			% 				'Visible', 'off', ...
			% 				'FontWeight', 'Bold', ...
			% 				'FontName', pars.fontName, ...
			% 				'FontSize', 18 ...
			% 				);
			
			
			
			%% VIDEO PREVIEW
			
			% add panel
			state.h.preview.panel = gui.createPanel('Video Preview & Frame Range');
			
			% add axis
			state.h.preview.axis = axes( ...
				'xtick', [], ...
				'ytick', [], ...
				'Units', 'pixels', ...
				'Box', 'on', ...
				'Parent', state.h.preview.panel, ...
				'FontName', pars.fontName, ...
				'FontSize', pars.fontSize ...
				);
			
			% add button
			state.h.preview.review = gui.createButton( ...
				'+', userEvent.pushButton, 'Show the review window');
			
			% add checkbox
			state.h.preview.normalise = gui.createCheckbox( ...
				'Normalise', userEvent.pushButton);
			
% 			% add checkbox
% 			state.h.preview.showMetaData = gui.createCheckbox( ...
% 				'Show Meta Data', userEvent.pushButton);
% 			
% 			% add meta data display
% 			state.h.preview.metaData = gui.createText( ...
% 				'Meta Data', 'right');
% 			set(state.h.preview.metaData, 'backgroundcolor', [0 0 0], 'foregroundcolor', [1 1 0]);
			
% 			% add meta data display
% 			state.h.preview.metaData = gui.createText( ...
% 				'', 'right');
			
			% add axis
			h_slider_axis = axes( ...
				'xtick', [], ...
				'ytick', [], ...
				'Units', 'pixels', ...
				'Box', 'on', ...
				'ButtonDownFcn', userEvent.buttonDown, ...
				'Parent', state.h.preview.panel, ...
				'Tag', 'Video Slider', ...
				'FontName', pars.fontName, ...
				'FontSize', pars.fontSize ...
				);
			
			% add button
			state.h.preview.A = gui.createButton('A', ...
				userEvent.pushButton, 'Select (A)ll frames');
			
			% add button
			state.h.preview.S = gui.createButton('S', ...
				userEvent.pushButton, 'Select a (S)mall number of frames');
			
			% add textbox
			state.h.preview.FSL = gui.createEdit('FSL', userEvent.modifyState);
			
			% add textbox
			state.h.preview.I = gui.createEdit('I', userEvent.modifyState);
			
			% add label
			state.h.preview.labelRange = gui.createText('Frame Range:', 'right');
			state.h.preview.labelKey = gui.createText('Key Frame:', 'right');
			
			% add video slider object
			state.h.preview.slider = stVideoSlidersGUI(h_slider_axis);
			
			% add button down functions to objects in slider axis so
			% that they bubble up events to the axis (slight hack)
			set(state.h.preview.slider.getHandles(), ...
				'ButtonDownFcn', userEvent.buttonDown, ...
				'Tag', 'Video Slider');
			
			
			
			
			
			
			%% PROCESSING
			
			% add panel
			state.h.processing.panel = gui.createPanel('Processing Methods');
			
			% add textbox
			state.h.processing.listbox = gui.createListbox( ...
				'ProcessingListbox', userEvent.modifyState, true, []);
			set(state.h.processing.listbox, ...
				'FontName', pars.listFontName, ...
				'FontSize', pars.fontSize, ...
				'Enable', 'off');
			
			% add selector
			state.h.processing.quickLabel = gui.createText('Quick Edit:', 'left');
			state.h.processing.quick = gui.createSelector( ...
				stQuickTask(), userEvent.modifyState, 'Quick Task');
			
			% add buttons
			state.h.processing.buttons = [];
			state.h.processing.buttons(end+1) = gui.createButton( ...
				'Edit Task', userEvent.pushButton, 'Create or change open task of selected jobs');
			state.h.processing.buttons(end+1) = gui.createButton( ...
				'Task Pars', userEvent.pushButton, 'Parametrize the open task of the selected jobs');
			
			
			
			%% OUTPUT
			
			% add panel
			state.h.output.panel = gui.createPanel('Output Methods');
			
			% add listbox
			state.h.output.listbox = gui.createListbox( ...
				'OutputListbox', userEvent.modifyState, true, 'po');
			set(state.h.output.listbox, ...
				'FontName', pars.listFontName, ...
				'FontSize', pars.fontSize, ...
				'ForegroundColor', [1 0 1]);
			
			% add buttons
			state.h.output.buttons1(1) = gui.createButton( ...
				'Set Outputs', userEvent.pushButton, 'Set output methods for project to those currently selected');
			state.h.output.buttons1(2) = gui.createButton( ...
				'Output Pars', userEvent.pushButton, 'Parametrize output methods');
			
			% add buttons
			state.h.output.buttons2(1) = gui.createButton( ...
				'XXXX', userEvent.pushButton, '');
			state.h.output.buttons2(2) = gui.createButton( ...
				'Export', userEvent.pushButton, 'Export results and outputs for all or selected jobs');
			
			set(state.h.output.buttons2(1), 'visible', 'off');
			
			
			
			%% EXECUTION
			
			% add panel
			state.h.execute.panel = gui.createPanel('Execution');
			
			% add log
			state.h.execute.log = uicontrol(state.h.execute.panel , ...
				'Style', 'Edit', ...
				'Units', 'pixels', ...
				'Max', 3, ...
				'Min', 1, ...
				'HorizontalAlignment', 'Left', ...
				'BackgroundColor', [1 1 1], ...
				'FontName', pars.logFontName, ...
				'FontSize', pars.logFontSize ...
				);
			
			% add axis
			state.h.execute.waitbar = axes( ...
				'xtick', [], ...
				'ytick', [], ...
				'Units', 'pixels', ...
				'Box', 'on', ...
				'Parent', state.h.execute.panel, ...
				'FontName', pars.fontName, ...
				'FontSize', pars.fontSize ...
				);
			
			% add image
			axis(state.h.execute.waitbar, [0 1 0 1]);
			state.h.execute.waitbar_image = rectangle('position', [0 0 1e-6 1]);
			set(state.h.execute.waitbar_image, 'facecolor', [1 0 0]);
			
			% add buttons
			state.h.execute.play = gui.createButton( ...
				'>', userEvent.pushButton, 'Run all jobs that are ready');
			state.h.execute.pause = gui.createToggleButton( ...
				'||', userEvent.pushButton, 'Pause the operation');
			state.h.execute.cancel = gui.createToggleButton( ...
				char(1), userEvent.pushButton, 'Cancel the operation');
			
			
			
			
			%% ADD MENU
			
			% get tools
			plugins = stPlugin();
			tools = plugins.tool.list;
			
			% create menu
			menu = {
				{'File' {
				{'New Project'}
				{'Open Project'}
				{'Recent Projects' {}}
				{'Save' 'S'}
				{'-'}
				{'Preferences'}
				{'Smaller Font'}
				{'Larger Font'}
				{'Open Temporary Directory'};
				{'Open Project Directory'};
				{'Show Log File'};
				{'-'}
				{'Exit'}
				}}
				{'Project' {
				{'Add Job'}
				{'Add Directory'}
				{'Undelete'}
				{'-'}
				{'Select' {
				{'All' 'A'}
				{'Ready To Run' 'R'}
				{'All With Same Task' 'W'}
				}}
				{'Sort' {
				{'By Task Content'}
				{'By Task Status'}
				{'By Video Path'}
				{'By Video Filename'}
				{'By Modification Date'}
				}}
				{'-'}
				{'Project Information'}
				}}
				{'Selected' {
				{'Release Locked Elsewhere'}
				{'Release Write Lock'}
				{'-'}
				{'Clear Flags'}
				{'Remove Parameters'}
				{'Clear Outputs'}
				{'Delete'}
				{'-'}
				{'Restart Current Task'}
				{'Close Current Task'}
				{'-'}
        {'Find Missing Videos'}
				{'-'}
				{'Job Information' 'I'}
				}}
				{'Displayed' {
				{'Propagate Parameters'}
				{'Set Tag' 'T'}
				{'Preload Video'}
				{'Show Error Message' 'E'}
				}}
				{'Tools' {
				tools
				}}
				{'Debug' {
				{'Debug Mode'}
				{'Run Test Operation'}
				{'Raise Error'}
				}}
				{'Help' {
				{'Documentation'}
				{'-'}
				{'About'}
				}}
				};
			state.h.menu = createMenu(state.h.mainGUI, [], menu, userEvent.menuCommand);
			
			
			% store state
			gui.state = state;
			
			
		end
		
		function finalize(gui)
			
			% raise resize
			try
				gui.resize();
			end
			drawnow
			
			% prevent events interrupting other events
			hh = findall(gui.h_dialog);
			for h_ = hh'
				set(h_, 'Interruptible', 'off', 'BusyAction', 'cancel');
			end
			
			% make GUI visible
			set(gui.state.h.mainGUI, 'Visible', 'on');
			drawnow
			
			% this must be laid in late, so resizes following plot()
			% commands don't generate errors
			set(gui.state.h.mainGUI, 'ResizeFcn', @stgui_resize);
			
			% fancy Java shit for appearances' sake
			gui.prepareJava();
			
		end
		
		function prepareJava(gui)
			
			% 			gui.state.java.pipeline = struct();
			try
				revision = stRevision();
				if revision.matlab >= 20072
					% 				roles = {'op' 'od' 'ot' 'sp' 'sd' 'st' 'wp' 'wd' 'wt' 'po'};
					% 				try
					% 					for r = 1:length(roles)
					% 						role = roles{r};
					%
					% 						% extract
					% 						where = 'pipeline';
					% 						jScrollPane = stFindJObj(gui.state.h.(where).(role), 'persist');
					% 						jListbox = jScrollPane.getViewport.getComponent(0);
					% 						gui.state.java.(where).(role).j = jListbox;
					%
					% 						% prevent focus, so it doesn't get keyboard events
					% 						% and respond to up and down arrows
					% 						set(jListbox, 'RequestFocusEnabled', false);
					% 						% 								jListbox.setActionMap([]);
					%
					% 						% set background color
					% 						switch role(1)
					% 							case 'o'
					% 								col = java.awt.Color(0, 0.5, 0);
					% 							case 's'
					% 								col = java.awt.Color.blue;
					% 							case 'w'
					% 								col = java.awt.Color.red;
					% 							case 'p'
					% 								col = java.awt.Color.magenta;
					% 						end
					% 						gui.state.java.(where).(role).col = col;
					%
					% 						% delegate
					% 						gui.updateJava(role);
					%
					% 					end
					
					% project listbox
					jScrollPane = stFindJObj(gui.state.h.project.listbox, 'persist');
					jListbox = jScrollPane.getViewport.getComponent(0);
					jListbox.setActionMap([]); % disable normal navigation keyboard responses
					% 					% 					jcol = java.awt.Color(0.67, 0.67, 0.68);
					% 					% 					jcol = java.awt.Color(0.8, 0.8, 0.8);
					jcol = java.awt.Color(1, 0.7, 0.2);
					set(jListbox, 'SelectionBackground', jcol);
					% 					set(jListbox, 'RequestFocusEnabled', false);
					
					% output listbox
					jScrollPane = stFindJObj(gui.state.h.output.listbox, 'persist');
					jListbox = jScrollPane.getViewport.getComponent(0);
					jListbox.setActionMap([]); % disable normal navigation keyboard responses
					jcol = java.awt.Color(1, 0, 1);
					set(jListbox, 'SelectionBackground', jcol);
					% 					set(jListbox, 'RequestFocusEnabled', false);
					
					
				end
			catch
				global bwtt_warned_java_fail
				if isempty(bwtt_warned_java_fail)
					warning('Java stuff did not work - some GUI misbehaviour is possible (please report this if it happens every time)');
					bwtt_warned_java_fail = 1;
				end
			end
			
		end
		
		% 		function updateJava(gui, role)
		%
		% % 			try
		% %
		% % 				% get object
		% % 				where = 'pipeline';
		% % 				h = gui.state.h.(where).(role);
		% % 				j = gui.state.java.(where).(role).j;
		% % 				col = gui.state.java.(where).(role).col;
		% %
		% % 				% is multiselect?
		% % 				multiselect = (get(h, 'Max') - get(h, 'Min')) > 1;
		% %
		% % 				% if not multiselect, and value 1 is selected, grey
		% % 				if multiselect || get(h, 'Value') > 1
		% % 					set(j, 'SelectionBackground', col);
		% % 				else
		% % 					set(j, 'SelectionBackground', java.awt.Color(0.9, 0.9, 0.9));
		% % 				end
		% %
		% % 			end
		%
		% 		end
		
		function [cancel, pause] = isPauseOrCancel(gui)
			
			cancel = get(gui.state.h.execute.cancel, 'value');
			pause = get(gui.state.h.execute.pause, 'value');
			
		end
		
		function isPause(gui)
			
		end
		
		function clearCancelAndPause(gui)
			
			% make sure pause and stop are off
			set(gui.state.h.execute.cancel, 'value', 0);
			set(gui.state.h.execute.pause, 'value', 0);
			
		end
		
		function resize(gui, size)
			
			state = gui.state;
			pars = gui.pars;
			
			if nargin == 2
				p = get(state.h.mainGUI, 'position');
				p(3:4) = size;
				set(state.h.mainGUI, 'position', p);
			end
			
			% get size of window
			pos = get(state.h.mainGUI, 'Position');
			winsize = pos(3:4);
			
			% 			% minimum size
			% 			minsize = [840 512];
			% 			if any(winsize < minsize)
			% 				pos(3:4) = max(pos(3:4), minsize);
			% 				set(state.h.mainGUI, 'Position', pos);
			% 				winsize = pos(3:4);
			% 			end
			
			% prepare rectangle for whole window
			main_whole = [pars.margin+[1 1] winsize-2*pars.margin];
			
			% break into panels - if the window is tall enough, we
			% do this in a compact way to give maximum space to
			% the project listbox. if it's not, we move the
			% execution panel under the project listbox, to make
			% enough room for the processing panels.
			layoutmode = winsize(2) >= 850;
			[main_left, main_right] = gui.pack(main_whole, 'right', pars.previewSize(1) + 18);
			[main_right, main_preview] = gui.pack(main_right, 'top', pars.previewSize(2) + 88); % + 24); % for metaData
			if layoutmode
				main_project = main_left;
				if stUserData('devMode')
					[main_middle, main_execute] = gui.pack(main_right, 'bottom', main_right(4)/2);
				else
					[main_middle, main_execute] = gui.pack(main_right, 'bottom', 160);
				end
				[main_output, main_processing] = gui.pack(main_middle, 'left', main_right(3)/2);
			else
				[main_project, main_execute] = gui.pack(main_left, 'bottom', 160);
				[main_output, main_processing] = gui.pack(main_right, 'left', main_right(3)/2);
			end
			
			
			
			%% panel "preview"
			
			% panel
			h = state.h.preview;
			rect = main_preview;
			rect = gui.move(h.panel, rect);
			
			% axis
			[rect, ax] = gui.pack(rect, 'top', pars.previewSize(2));
			ax(3:4) = pars.previewSize;
			gui.move(h.axis, ax);
			[temp, button] = gui.pack(ax, 'right', 24);
			[temp, button] = gui.pack(button, 'top', 24);
			button = button + [4 4 0 0];
			gui.move(h.review, button);
			button = button + [4 -16 -8 -8];
			gui.move(h.normalise, button);
			
% 			[temp, button] = gui.pack(ax, 'right', 24);
% 			[temp, button] = gui.pack(button, 'bottom', 24);
% 			button = button + [4 4 -8 -8];
% 			gui.move(h.showMetaData, button);
% 			button = button + [-400 0 +400-24 0];
% 			gui.move(h.metaData, button);
			
% 			% meta data display
% 			[rect, item] = gui.pack(rect, 'top', 16);
% 			gui.move(h.metaData, item);

			% controls
			[rect, item] = gui.pack(rect, 'top', 20);
			gui.move(h.slider.getAxis(), item);
			[rect, item] = gui.pack(rect, 'left', 20);
			gui.move(h.A, item);
			[rect, item] = gui.pack(rect, 'left', 20);
			gui.move(h.S, item);
			[rect, item] = gui.pack(rect, 'right', 60);
			gui.move(h.I, item);
			[rect, item] = gui.pack(rect, 'right', 80);
			item(2) = item(2) - 2;
			gui.move(h.labelKey, item);
			[rect, item] = gui.pack(rect, 'right', 120);
			gui.move(h.FSL, item);
			rect(2) = rect(2) - 2;
			gui.move(h.labelRange, rect);
			
			
			
			%% panel "project"
			
			% panel
			rect = main_project;
			h = state.h.project;
			rect = gui.move(h.panel, rect);
			
			% listbox
			gui.move(h.listbox, rect);
			
			
			
			%% panel "processing"
			
			% panel
			h = state.h.processing;
			rect = gui.move(h.panel, main_processing);
			[rect, buttons] = gui.pack(rect, 'top', 24);
			[rect, quick] = gui.pack(rect, 'top', 24);
			
			% listbox
			gui.move(h.listbox, rect);
			
			% selector
			[selector, label] = gui.pack(quick, 'left', 80);
			gui.move(h.quick, selector);
			gui.move(h.quickLabel, label + [0 -4 0 0]);
			
			% buttons
			nb = length(h.buttons);
			bw = (buttons(3) - (nb - 1) * gui.pars.margin) / nb;
			for b = 1:nb
				[buttons, button] = gui.pack(buttons, 'left', bw);
				gui.move(h.buttons(b), button);
			end
			
			
			
			%% panel "output"
			
			% panel
			h = state.h.output;
			rect = gui.move(h.panel, main_output);
			[rect, buttons1] = gui.pack(rect, 'top', 24);
			[rect, buttons2] = gui.pack(rect, 'top', 24);
			
			% listbox
			gui.move(h.listbox, rect);
			
			% buttons
			nb = length(h.buttons1);
			bw = (buttons1(3) - (nb - 1) * gui.pars.margin) / nb;
			for b = 1:nb
				[buttons1, button] = gui.pack(buttons1, 'left', bw);
				gui.move(h.buttons1(b), button);
			end
			
			% buttons
			nb = length(h.buttons2);
			bw = (buttons2(3) - (nb - 1) * gui.pars.margin) / nb;
			for b = 1:nb
				[buttons2, button] = gui.pack(buttons2, 'left', bw);
				gui.move(h.buttons2(b), button);
			end
			
			
			
			%% panel "execute"
			
			% panel
			bw = 40;
			rect = main_execute;
			h = state.h.execute;
			rect = gui.move(h.panel, rect);
			[rect, sub] = gui.pack(rect, 'bottom', bw*0.75);
			
			% log
			gui.move(h.log, rect);
			
			% buttons
			[sub, button] = gui.pack(sub, 'right', bw);
			gui.move(h.cancel, button);
			[sub, button] = gui.pack(sub, 'right', bw);
			gui.move(h.pause, button);
			[sub, button] = gui.pack(sub, 'right', bw);
			gui.move(h.play, button);
			
			% waitbar
			[sub, temp] = gui.pack(sub, 'top', 0);
			[sub, temp] = gui.pack(sub, 'bottom', 0);
			gui.move(h.waitbar, sub);
			
		end
		
		function [a, b] = pack(gui, a, side, size)
			
			pars = gui.pars;
			
			b = a;
			if any(side == '*')
				margin = 0;
				side = side(side ~= '*');
			else
				margin = pars.margin;
			end
			
			switch side
				case 'top'
					b(4) = size;
					b(2) = a(2) + a(4) - size;
					a(4) = a(4) - size - margin;
				case 'bottom'
					b(4) = size;
					a(4) = a(4) - size - margin;
					a(2) = a(2) + size + margin;
				case 'right'
					b(3) = size;
					b(1) = a(1) + a(3) - size;
					a(3) = a(3) - size - margin;
				case 'left'
					b(3) = size;
					a(3) = a(3) - size - margin;
					a(1) = a(1) + size + margin;
				case 'shrink'
					b(1:2) = b(1:2) + size;
					b(3:4) = b(3:4) - 2 * size;
			end
			
		end
		
		function rect = move(gui, h, rect)
			
			set(h, 'position', rect);
			
			% return a rectangle that represents the client area
			% of this panel
			rect = [1 1 rect(3:4)];
			rect = rect + gui.pars.panelBorder;
			
		end
		
		function h = createPanel(gui, text, visible)
			
			if nargin < 3
				visible = 'on';
			end
			
			% add panel
			h = uipanel(gui.h_dialog, ...
				'Title', text, ...
				'Units', 'pixels', ...
				'Visible', visible, ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.fontSize ...
				);
			
			% new parent
			gui.h_parent = h;
			
		end
		
		function h = createSelector(gui, string, callback, tag)
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'popupmenu', ...
				'String', string, ...
				'Tag', tag, ...
				'BackgroundColor', [1 1 1], ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.fontSize, ...
				'Max', 1, ...
				'Min', 0, ...
				'Callback', callback ...
				);
			
		end
		
		function h = createButton(gui, text, callback, tooltip)
			
			if nargin < 3
				tooltip = '';
			end
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Pushbutton', ...
				'Units', 'pixels', ...
				'String', text, ...
				'Tooltip', tooltip, ...
				'Callback', callback, ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.fontSize ...
				);
			
			% 				'FontName', 'Tahoma', ...
			
		end
		
		function h = createToggleButton(gui, text, callback, tooltip)
			
			if nargin < 3
				tooltip = '';
			end
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Togglebutton', ...
				'Units', 'pixels', ...
				'String', text, ...
				'Min', 0, ...
				'Max', 1, ...
				'Tooltip', tooltip, ...
				'Callback', callback, ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.fontSize ...
				);
			
			% 				'FontName', 'Tahoma', ...
			
		end
		
		function h = createEdit(gui, tag, callback)
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Edit', ...
				'Units', 'pixels', ...
				'BackgroundColor', [1 1 1], ...
				'Callback', callback, ...
				'FontName', gui.pars.fontName, ...
				'Tag', tag, ...
				'FontSize', gui.pars.fontSize ...
				);
			
		end
		
		function h = createLabel(gui, text)
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Text', ...
				'Units', 'pixels', ...
				'String', text, ...
				'Hori', 'left', ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.smallFontSize ...
				);
			
		end
		
		function h = createText(gui, text, align)
			
			if nargin < 3
				align = 'left';
			end
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Text', ...
				'Units', 'pixels', ...
				'String', text, ...
				'Hori', align, ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.fontSize ...
				);
			
		end
		
		function h = createHeading(gui, text)
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Text', ...
				'Units', 'pixels', ...
				'String', text, ...
				'Hori', 'right', ...
				'FontName', gui.pars.headingFontName, ...
				'FontSize', gui.pars.headingFontSize ...
				);
			
		end
		
		function h = createListbox(gui, tag, callback, multiselect, userdata)
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Listbox', ...
				'Min', 100, 'Max', multiselect+101, ...
				'Units', 'pixels', ...
				'Tag', tag, ...
				'BackgroundColor', [1 1 1], ...
				'Value', [], ...
				'UserData', userdata, ...
				'Callback', callback, ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.fontSize ...
				);
			
		end
		
		function h = createCheckbox(gui, text, callback, tag)
			
			if nargin < 3
				callback = [];
			end
			
			if nargin < 4
				tag = '';
			end
			
			h = uicontrol(gui.h_parent, ...
				'Style', 'Checkbox', ...
				'Units', 'pixels', ...
				'String', text, ...
				'Callback', callback, ...
				'Tag', tag, ...
				'FontName', gui.pars.fontName, ...
				'FontSize', gui.pars.smallFontSize ...
				);
			
		end
		
	end
	
end








%% HELPERS

function h = createMenu(h_parent, h, menu, menuCommandCallback)

separator = false;

for i = 1:length(menu)
	
	m = menu{i};
	label = m{1};
	if length(m) == 2
		data = m{2};
	else
		data = '';
	end
	
	if strcmp(label, '-')
		separator = true;
		continue
	end
	
	if ischar(data)
		accelerator = data;
	else
		accelerator = '';
	end
	
	key = label;
	key(key == ' ') = '_';
	
	h.(key) = uimenu(h_parent, ...
		'Callback', menuCommandCallback, ...
		'Accelerator', accelerator, ...
		'Label', label ...
		);
	
	if separator
		set(h.(key), 'Separator', 'on');
	end
	
	if iscell(data)
		h = createMenu(h.(key), h, data, menuCommandCallback);
	end
	
	separator = false;
	
end


end



%% CALLBACKS

function stgui_resize(obj, evt)

ud = get(obj, 'userdata');
gui = ud.gui;
try
	gui.resize();
end

end



