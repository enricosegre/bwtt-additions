
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


classdef stClickLink < handle
	
	properties
		
		h_axis
		links
		
	end
	
	methods
		
		function obj = stClickLink(h_axis)
			
			obj.h_axis = h_axis;
			obj.links = {
				{'Left click' [] 'normal'}
				{'Right click' [] 'alt'}
				{'Shift click' [] 'extend'}
				};
			
		end
		
		function link = makeLink(obj, callee, index)
			
			for l = 1:length(obj.links)
				if isempty(obj.links{l}{2})
					link = [];
					link.text = obj.links{l}{1};
					link.callee = callee;
					link.callback = @callback;
					link.index = index;
					link.st = obj.links{l}{3};
					link.h_axis = obj.h_axis;
					obj.links{l}{2} = link;
					return
				end
			end
			
			error('ran out of click links');
			
		end
		
		function clear(obj)
			
			for l = 1:length(obj.links)
				obj.links{l}{2} = [];
			end			
			
		end
		
		function click(obj, st)
			
			cp = get(obj.h_axis, 'CurrentPoint');
		 	xy = round(cp(1, 1:2));
			
			for l = 1:length(obj.links)
				if ~isempty(obj.links{l}{2})
					if strcmp(st, obj.links{l}{3})
						link = obj.links{l}{2};
						link.callee.changeValue(link.index, xy);
						return
					end
				end
			end
			
		end
		
		function setCallbackOnObject(obj, h)

			set(h, 'ButtonDownFcn', @callback);
			set(h, 'UserData', obj);

		end
	
	end
	
end

function callback(obj, evt)

st = get(gcbf, 'selectiontype');
ud = get(obj, 'UserData');
ud.click(st);

end



