
% result = stProcessPath(component, path)
%   stProcessPath returns some component of the passed path.
%   component can be any of the following.
%
% justExtension
% removeExtension
% justPath
% justFilename


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function result = stProcessPath(component, path)

[a, b, c] = fileparts(path);

switch component
	
	case 'justExtension'
		result = [c(2:end)];
		
	case 'removeExtension'
		result = [a filesep b];
		
	case 'justPath'
		result = a;
		
	case 'justFilename'
		result = [b c];
		
	case 'justBasename'
		result = [b];
		
end

