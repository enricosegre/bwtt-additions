

%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function im = stGetCharImage(str, pixel_height)

persistent cache

if isempty(cache)
	
	imsrc = imread([stGetInstallPath() '/resource/stGetCharImage.png']);
	cache.imsrc = imsrc(:, :, 1);
	cache.charwidth = 40;
	cache.charheight = 90;
	cache.usecharheight = 80;
	
end

im = [];

for c = str
	
	chari = c - 31;
	if chari < 1 || chari > 96
		chari = 32; % ?
	end
	
	% convert to m, n
	[n, m] = ind2sub([32 3], chari);
	
	% get image
	m = m - 1;
	n = n - 1;
	yy = (1:cache.charheight) + m * cache.charheight;
	xx = (1:cache.charwidth) + n * cache.charwidth;
	imch = cache.imsrc(yy, xx);
	im = [im imch(1:cache.usecharheight, :)];
	
end

if nargin == 2 && pixel_height ~= cache.usecharheight
	im = imresize(im, pixel_height / cache.usecharheight);
end

end
