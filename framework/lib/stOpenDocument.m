
% stOpenDocument(filename)
%   attempt to open the specified document.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function stOpenDocument(filename)

% path = [stGetInstallPath() '/docs/' filename];
path = filename;

% try
	
	ext = stProcessPath('justExtension', filename);
	
	switch ext
		
		case 'pdf'
			if ispc
				winopen(strrep(path, '/', '\'));
			else
				% don't know how, in general - evince might be
				% installed, though...
				system(['evince ''' path ''' &']);
			end
			
		otherwise
			% try matlab's facility
			open(path);
			
	end
	
% catch err
% 	
% 	warning(['failed open document "' path '"']);
% 	
% end
% 
% 
% 
