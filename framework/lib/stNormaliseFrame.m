
% frame = stNormaliseFrame(frame)
%   transform the frame so that the 2nd and 98th luminance
%   percentile lie at 2% and 98% luminance.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function frame = stNormaliseFrame(frame)

lim = 2;
lev = [lim 100-lim] / 100;

hh = histc(frame(:), 0.5:256);
hh = hh / sum(hh);
chh = cumsum(hh);
l1 = find(chh > lev(1), 1, 'first') / 255;
l2 = find(chh < lev(2), 1, 'last') / 255;
frame = imadjust(frame, [l1; l2], lev', 1);

end


