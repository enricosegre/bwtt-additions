

%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b



function [response, input] = stMessage(varargin)

% defaults
icon = 'pony';
h_parent = gcbf;
wh = [480 240];
message = 'No Message';
title = 'Little pony says...';
buttons = {'OK'};

% parse arguments
for a = 1:length(varargin)
	arg = varargin{a};
	if ishandle(arg)
		h_parent = arg;
	elseif ischar(arg)
		switch arg
			case 'small'
				wh = [480 240];
			case 'medium'
				wh = [480 320];
			case 'large'
				wh = [560 320];
			case 'unparametrized'
				icon = arg;
			otherwise
				if arg(1) == '!'
					title = arg(2:end);
				else
					message = arg;
				end
		end
	elseif iscell(arg)
		buttons = arg;
	end
end

% get resource
icon = stGetResource(icon);

% create
gui = stGUI(wh, h_parent, title, 'margin', 16);
rect = gui.rootPanel;
h_dialog = gui.getMainWindow();

% image
h_image_axis = axes( ...
	'Units', 'pixels', ...
	'Parent', h_dialog ...
	);
col = get(h_dialog, 'color');
for c = 1:3
	im(:, :, c) = icon(:, :, c) * col(c);
end
h_image = image(im, 'parent', h_image_axis);
set(h_image_axis, 'visible', 'off');

% split message for display
message = stStringExplode(char(10), message);

% message
h_text = uicontrol(h_dialog, ...
	'style', 'text', ...
	'hori', 'left', ...
	'fontname', 'Helvetica', ...
	'fontsize', 10, ...
	'string', message, ...
	'units', 'pixels' ...
	);

% input
if nargout == 2
	h_input = uicontrol(h_dialog, ...
		'style', 'edit', ...
		'fontname', 'Helvetica', ...
		'hori', 'left', ...
		'fontsize', 10, ...
		'backgroundcolor', [1 1 1], ...
		'units', 'pixels' ...
	);
else
	h_input = [];
end

% buttons
% if length(buttons) > 1
% 	bw = 48;
% else
	bw = 64;
% end
for b = 1:length(buttons)
	h_buttons(1, b) = uicontrol(h_dialog, ...
		'style', 'pushbutton', ...
		'fontname', 'Helvetica', ...
		'fontsize', 10, ...
		'userdata', b, ...
		'callback', @button_callback, ...
		'string', strrep(buttons{b}, '*', ''), ...
		'units', 'pixels' ...
		);
end

% keydown
set([h_dialog h_text h_buttons h_input], ...
	'keypressfcn', @keydown_callback);

% layout
lhs = rect.pack([], 'left', 150);
pad = rect.pack([], 'left', 12);
rhs = rect.pack([], 'left', -1);
im = lhs.pack(h_image_axis, 'top', 150);
but = rhs.pack([], 'bottom', 32);
if ~isempty(h_input)
	rhs.pack(h_input, 'bottom', 32);
end
rhs.pack(h_text, 'bottom', -1);
for b = 1:length(h_buttons)
	but.pack(h_buttons(b), 'left', bw);
end

% defaultResponse
defaultButtonIndex = 1;
defaultResponse = buttons{1};
for b = 1:length(buttons)
	if any(buttons{b} == '*')
		defaultResponse = strrep(buttons{b}, '*', '');
		defaultButtonIndex = b;
		break;
	end
end

% layout
gui.resize();

% focus
if ~isempty(h_input)
	uicontrol(h_input);
else
	uicontrol(h_buttons(defaultButtonIndex));
end


% wait
response = '';
input = {};
gui.waitForClose();






	function button_callback(h_button, evt)
		
		response = strrep(buttons{get(h_button, 'userdata')}, '*', '');
		if strcmp(response, 'OK') && ~isempty(h_input)
			input = {get(h_input, 'string')};
		end
		gui.close();
		
	end

	function keydown_callback(h_object, evt)
		
		switch evt.Key
			case 'return'
				if h_object == h_dialog
					response = defaultResponse;
					if strcmp(response, 'OK') && ~isempty(h_input)
						input = {get(h_input, 'string')};
					end
					gui.close();
				else
					for b = 1:length(h_buttons)
						if h_object == h_buttons(b)
							response = strrep(buttons{b}, '*', '');
							if strcmp(response, 'OK') && ~isempty(h_input)
								input = {get(h_input, 'string')};
							end
							gui.close();
						end
					end
				end
			case 'escape'
				gui.close();
		end
		
	end





end




