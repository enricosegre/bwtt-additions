
% position = stPositionDialog(h_parent, widthHeight)
%		return the on-screen position of a dialog of specified
%		size that will be centred over the window h_parent. if
%		h_parent is empty, the dialog is centred on the first
%		monitor.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function position = stPositionDialog(h_parent, widthHeight)

if isempty(h_parent)
	position = get(0, 'ScreenSize');
else
	set(h_parent, 'Units', 'pixels');
	position = get(h_parent, 'Position');
end

dw = (position(3) - widthHeight(1)) / 2;
dh = (position(4) - widthHeight(2)) / 2;
position = [position(1)+dw position(2)+dh widthHeight];



