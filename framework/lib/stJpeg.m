
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function out = stJpeg(in, quality)

% author ben mitch
%
% this file seconds the imwrite/imread functions to
% implement a jpeg codec.

tempdir = stUserData('temporaryDirectory');
if isempty(tempdir)
	tempdir= '.';
end
tempfile = [tempdir '/temp.jpg'];

if nargin == 2
	imwrite(in, tempfile, 'Quality', quality);
	fid = fopen(tempfile, 'rb');
	out = fread(fid, '*uchar');
	fclose(fid);
else
	fid = fopen(tempfile, 'wb');
	fwrite(fid, in);
	fclose(fid);
	out = imread(tempfile);
end

